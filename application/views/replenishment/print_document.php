<!DOCTYPE html>
<html>

<head>
    <title>Material Release. <?= $get['replenishment_no'] ?></title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inter&display=swap" rel="stylesheet">
    <style type="text/css">
        @page {
            margin: 0cm 0cm;
			padding: 3cm 4cm;
            font-family: 'Inter', sans-serif;
        }

        body {
            margin: 0px;
            font-size: 9px;
            padding: 14px;
        }

        .text-center {
            text-align: center;
        }

        .text-right {
            text-align: right;
        }

        .text-left {
            text-align: left;
        }

        .my-0 {
			margin-bottom: 0px; 
			margin-top: 0px;
        }

        h2 {
            margin-top: 5px;
            margin-bottom: 2px;
        }

        .table {
            margin: 0;
            border-collapse: collapse;
            border: 0;
        }

        .table>tbody>tr>td {
            border-top: 0;
        }

        .table>tbody>tr>.br {
            border-right: 0;
        }

        .table>tbody>tr>.bl {
            border-left: 0;
        }

        .td-valign-top>tbody>tr>td {
            vertical-align: top;
        }

        .bl {
            border-left: 1px solid #000 !important;
        }

        .bt {
            border-top: 1px solid #000 !important;
        }

        .nowrap {
            white-space: nowrap !important;
        }

        .page_break {
            page-break-after: auto;
        }

		.body{
			margin: 45px 80px;
		}
    </style>
</head>

<body>
	
	<?php 
		$pembuat = $this->db->where([
			'user_id' => $get['user_id']
		])->get('tbl_user')->row();

		$approved = $this->db->where([
			'user_id' => $get['approved_by']
		])->get('tbl_user')->row();
	?>
	<table width="100%" border="1px" style="margin-bottom: 0px; border-collapse: collapse">
		<tr>
			<td width="35%" class="text-center" style="border-bottom: 1px solid #fff !important;">
				<img width="125" src="http://localhost/skinsol-wms/assets/src/img/logo-skinsol.png">
			</td>
			<td width="60%" style="border-bottom: 1px solid #fff !important;">
				<h1 class="text-center" style="margin-top: 40px;">CATATAN PENGOLAHAN BATCH<br/> PROSES PRODUKSI</h1><br/>
				<h2 class="text-center" style="margin-top: 10px;"><?= substr($get['wo_product_name'], 0, strpos($get['wo_product_name'], '.')); ?></h2>
			</td>
			<td width="35%" style="border-bottom: 1px solid #fff !important;">
				<div style="margin-left:5px;" class="my-0">
					<h2 class="text-left my-0">Standar : CPKB,HALAL,ISO 9001,IS0 45001,IS0 14001,GMP</h2>
				</div>
			</td>
		</tr>
	</table>

	<table width="100%" border="1px" style="margin-bottom: 0px; border-collapse: collapse">
		<tr>
			<td width="35%" style="border-bottom: 1px solid #fff !important;">
				<h3 class="text-center my-0">No. Dokumen. <?= $get['replenishment_no'] ?></h3>
				<h3 class="text-center my-0">No Revisi : 01</h3>
				<h3 class="text-center my-0">Berlaku : <?= $get['tgl_berlaku'] ?></h3>
			</td>					
			<td width="30%" style="border-bottom: 1px solid #fff !important;">
				<!-- <h3 class="text-center my-0">Besar Bets : <br> <?= $get['qty'] .'Kg'?> / <?= $get['qty'] .'pcs'  ?></h3> -->
				<h3 class="text-center my-0">Besar Bets : <?= $get['wo_pack'] .' pcs'  ?></h3>
			</td>
			<td width="30%" style="border-bottom: 1px solid #fff !important;">
				<h3 class="text-center my-0">Kode Produk : <?= substr($get['wo_product_code'], 0, strpos($get['wo_product_code'], '.')); ?> </h3>
				<!-- <h3 class="text-center my-0">No.Batch : <?= $get['work_order_batch'].'.'.$detail[0]->fg_id ?></h3> -->
				<h3 class="text-center my-0">No.Batch : <?= substr($get['revision_num'], 0, strpos($get['wo_formula'], '.')).'.'.$get['wo_product_code'];?></h3>
			</td>
			<td width="35%" style="border-bottom: 1px solid #fff !important;">
				<div style="margin-left:5px" class="my-0">
					<h3 class="text-center my-0"> Halaman </h3>
					<h3 class="text-center my-0"> 1 dari 1</h3>
				</div>
			</td>
		</tr>
	</table>


	<table width="100%" border="1px" style="margin-bottom: 20px; border-collapse: collapse">
		<tr>
			<td width="35%">
				<!-- <h3 class="text-center my-0">Bentuk Sediaan : <br> <?= $get['bentuk_sediaan'] ?></h3> -->
				<h3 class="text-center my-0">Bentuk Sediaan : <br> <?= substr( $get['packaging_name'] , strpos($get['packaging_name'], ',') + 1)?> </h3>
				<h3 class="text-center my-0">Kemasan Primer : <br> <?= substr( $get['packaging_name'] , strpos($get['packaging_name'], '-') + 1)?> </h3>
			</td>					
			<td width="30%">
				<h3 class="text-center my-0">Disusun Oleh : <br> <?= $pembuat->name?></h3>
			</td>
			<td width="30%">
				<h3 class="text-center my-0">Disetujui Oleh : <br> <?= $approved->name ?></h3>
			</td>
			<td width="35%">
				<div style="margin-left:5px" class="my-0">
					<h3 class="text-center my-0"> Tanggal Pengolahan </h3>
					<h3 class="text-center my-0"> Tanggal Mulai : <?= $get['tgl_mulai'] ?></h3>
					<h3 class="text-center my-0"> Tanggal Selesai : <?= $get['tgl_selesai'] ?></h3>
				</div>
			</td>
		</tr>
	</table>

	<h1><b>PENGEMBANGAN BAHAN</b></h1>

	<table width="100%" border="1px" style="margin-bottom: 40px; border-collapse: collapse">
		<tr>
			<th width="20%">Nama Bahan</th>
			<th width="20%">Batch Num</th>
			<th width="30%">Jumlah Yang Dibutuhkan</th>
			<th width="30%">Jumlah Yang Ditimbang</th>
		</tr>

		<?php 

			foreach($fg_materials as $mtr):
		?>
			<tr>
				<td style="padding-left: 10px"><?= $mtr->material_name ?></td>
				<td class="text-center"><?= $mtr->material_box_id .' ('. $mtr->batch_id . ')' ?></td>
				<td class="text-center"><?= $mtr->qty ?> gram </td>
				<td class="text-center"><?= $mtr->qty ?> gram </td>
			</tr>
		<?php endforeach; ?>
	</table>
</body>

</html>
