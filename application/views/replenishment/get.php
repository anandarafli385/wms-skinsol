<form action="<?= base_url() ?>admin/replenishment/update_status" method="POST">
	<div class="modal-body m-3">
		<div class="mb-3">
			<div class="form-group">
				<input type="hidden" name="replenishment_id" value="<?= $get['replenishment_id'] ?>">
			</div>
		</div>

		<div class="mb-3">
		  <label for="approval">Status Approval Material Release</label>
		  <select class="form-control select2-edit" name="approval">
		  	<option value="<?= $get['approval'] ?>" disabled><?= $get['approval'] ?></option>
			<option value="approved">Approved</option>
			<option value="not approved">Not Approved</option>
		  </select>
		</div>

	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
		<button type="submit" class="btn btn-primary">Save changes</button>
	</div>
</form>

<script>
	$(function(){
		$(".select2-edit").select2({
				theme: 'bootstrap-5',
				dropdownParent: $("#statusModal"),
				width: 'auto'
			});
	})

	// $(".box-edit").change(function(e) {
		// var description = $(this).find(":selected").data('description');
		// var unit = $(this).find(":selected").data('unit');

		// $(this).closest('tr').children('td.material_description').text(description);
		// $(this).closest('tr').children('td.material_unit').text(unit);

		// console.log(description)
		// console.log(unit)
		// console.log('test')
	// });

</script>
