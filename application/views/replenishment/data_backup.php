<main class="content">
	<div class="container-fluid p-0">

		<h1 class="h3 mb-3">Replenishment</h1>

		<div class="row">
			<div class="col-12 col-xl-12">
				<div class="card">
					<div class="card-header">
						<h5 class="card-title">Replenishment
							<div class="float-end">
								<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#addModal"><i class="fa fa-plus"></i> Add Data</button>
							</div>
						</h5>
						<h6 class="card-subtitle text-muted">List data replenishment
						</h6>
					</div>
					<div class="card-body">
						<?= $this->session->flashdata('msg') ?>
						<table class="table table-striped dataTable">
							<thead>
								<tr>
									<th>#</th>
									<th>No Order</th>
									<th>Date Time</th>
									<th>Material</th>
									<th>Qty</th>
									<th>Unit</th>
									<th>Replenishment By</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$no = 1;
								foreach ($replenishment as $row) : ?>
									<tr>
										<td><?= $no++ ?></td>
										<td><?= $row['replenishment_id'] ?></td>
										<td><?= date('d M Y H:i', strtotime($row['created_at'])) ?></td>
										<td><?= $row['material'] ?></td>
										<td><?= $row['qty'] ?></td>
										<td><?= $row['unit'] ?></td>
										<td><?= $row['name'] ?></td>
										<td class="table-action">
											<a href="#!" data-bs-toggle="modal" data-bs-target="#editModal" data-id="<?= $row['replenishment_id'] ?>"><i data-feather="edit"></i></a>
											<a href="#!" onclick="delete_data('<?= $row['replenishment_id'] ?>')"><i data-feather="trash-2"></i></a>
										</td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>

		</div>

	</div>
</main>

<div class="modal fade" id="addModal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Add Data Replenishment</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body m-3">
				<div class="mb-3">
					<div class="form-group">
						<label for="">Order Number</label>
						<select required class="form-control select2 work_order_id" id="wo_id" name="work_order_id">
							<option value="0">Choose Work Order</option>
							<?php foreach ($work_order as $item) : ?>
								<option value="<?= $item['work_order_id'] ?>"><?= $item['no_work_order'] ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>

				<div class="mb-3" id="wo_info" style="display: none">
					<div class="row mb-2">
						<div class="col-6">
							<label class="form-label">Total FG</label>
							<input type="text" class="form-control form-control-lg" id="wo_total_fg" value="" readonly>
						</div>

						<div class="col-6">
							<label class="form-label">Total Material</label>
							<input type="text" class="form-control form-control-lg" id="wo_total_material"  readonly>
						</div>
						
					</div>

					<div class="row mb-2">
						<div class="col-6">
							<label class="form-label">Created by</label>
							<input type="text" class="form-control form-control-lg" id="wo_created_by" readonly>
						</div>

						<div class="col-6">
							<label class="form-label">Created at</label>
							<input type="text" class="form-control form-control-lg" id="wo_created_at" readonly>
						</div>
					</div>

					<label for="" class="mb-2">Material List</label>

					<table class="table table-bordered table-inverse table-responsive">
						<thead class="thead-default">
							<tr>
								<th>#</th>
								<th>Material</th>
								<th>Needed</th>
								<!-- <th></th> -->
							</tr>
							</thead>
							<tbody id="table_material">
							</tbody>
					</table>
				</div>

				<div class="mb-3">
					<label class="form-label">Replenishment By</label>
					<input type="text" class="form-control form-control-lg" name="user_name" value="<?= $this->session->name; ?>" readonly>
					<input type="hidden" class="form-control" name="user_id" value="<?= $this->session->user_id; ?>" required>
				</div>
				<div class="mb-3">
					<label class="form-label">Location</label>
					<input type="text" class="form-control form-control-lg" name="location" placeholder="Scan Location" id="location" required>
				</div>

				<div class="mb-3">
					<label class="form-label">Material List</label>
					<div class="table-responsive">
						<table class="table table-sm table-striped">
							<thead>
								<tr>
									<th width="30%">Material</th>
									<th>Description</th>
									<th>Qty</th>
									<th>Unit</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										<select name="material_box[]" class="form-control material_id  select2" required disabled>
											<option value="">- Select One -</option>

										</select>
									</td>
									<td id="material_description">-</td>
									<td>
										<input type="number" class="form-control form-control-lg" name="qty[]" placeholder="Qty" min="0" required>
									</td>
									<td>PCS</td>
									<td><button type="button" class="btn btn-success" onclick="addrow(this)">+</button>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
				<button type="button" onclick="add_replenishment()" class="btn btn-primary">Save changes</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="editModal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Edit Data Replenishment</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div id="edit_data">

			</div>
		</div>
	</div>
</div>

<script>
	$("[name='material_box[]']").change(function(e) {
		var description = $(this).find(":selected").data('description');
		var unit = $(this).find(":selected").data('unit');

		$(this).closest('tr').children('td#material_description').text(description);
		$(this).closest('tr').children('td#material_unit').text(unit);
	});

	$(".box-edit").change(function(e) {
		console.log('berubah')
	});

	$("#editModal").on('shown.bs.modal', function(e) {
		var id = $(e.relatedTarget).data('id');
		$.ajax({
			type: "POST",
			url: "<?= base_url() ?>admin/replenishment/get",
			data: {
				id: id
			}
		}).done(function(response) {
			$("#edit_data").html(response);
		});
	});

	$("#wo_id").on('select2:select', function(e) {
		let value = e.params.data.id
		let url = "<?= base_url() ?>"

		if (value != 0) {
			// console.log(value)
			$.ajax({
				url: url + 'admin/work_order/get_work_order/' + value,
				method: 'GET',
				success: (res) => {
					console.log(res)

					// Fill input
					$("#wo_total_fg").val(res.data.data_wo_detail.length)
					$("#wo_total_material").val(res.data.data_material.length)
					$("#wo_created_by").val(res.data.data_user.name)
					$("#wo_created_at").val(res.data.data_wo.created_at)


					// Show hidden element
					$("#wo_info").css({
						"display" : "block"
					})

					let no = 1
					res.data.data_material.forEach(element => {
						let html = `
							<tr>
								<td>`+ no++ +`</td>
								<td>`+ element.material +`</td>
								<td>`+ element.material_needed +`</td>
							</tr>
						` 

						$("#table_material").append(html)

					});
				}
			})
		} else {
			$("#wo_info").css({
				"display" : "none"
			})
		}
	})

	function addrow(btn) {
		var row_copy = `
			<td>
				<select name="material_box[]" class="form-control select2" required>
					<option value="">- Select One -</option>
				</select>
			</td>
			<td id="material_description">-</td>
			<td>
				<input type="number" class="form-control" name="qty[]" placeholder="Qty" min="0"
					required>
			</td>
			<td>PCS</td>
			<td></td>
		`;
		$(btn).closest("tbody").append("<tr>" + row_copy + "</tr>");
		var btn_delete = '<button type="button" class="btn btn-danger" onclick="deleterow(this)">-</button>';
		$(btn).closest("tbody").find("tr:last").find("td:last").html(btn_delete);

		var location = $("[name=location]").val();
		$.ajax({
			type: "POST",
			url: "<?= base_url() ?>admin/replenishment/get_location",
			dataType: 'html',
			data: {
				location: location
			}
		}).done(function(response) {
			if (response !== 'notfound') {
				// $("[name=location]").attr('readonly', 'readonly');
				$("[name='material_box[]']").removeAttr('disabled');
				$("[name='material_box[]']").html(response);
			} else {
				$("[name='material_box[]']").attr('disabled');
			}
		})

		$(".select2").select2({
			theme: 'bootstrap-5',
			dropdownParent: $("#addModal")
		});
	}

	function deleterow(btn) {
		$(btn).closest("tr").remove();
	}


	function delete_data(id) {
		Swal.fire({
			title: 'Konfirmasi ?',
			text: "Apakah kamu yakin ?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#28a745',
			cancelButtonColor: '#dc3545',
			confirmButtonText: 'Yes!',
			cancelButtonText: 'No!',
			showLoaderOnConfirm: true,
			preConfirm: function() {
				return new Promise(function(resolve, reject) {
					$.ajax({
						method: 'POST',
						url: "<?= base_url() ?>admin/replenishment/delete",
						data: {
							id: id
						}
					}).done(function(msg) {
						if (msg == "ok") {
							swal.fire("OK!", "Data berhasil dihapus!", "success").then(
								function() {
									location.reload();
								})
						} else {
							swal.fire("Gagal!", msg, "error").then(function() {
								location.reload();
							})
						}
					})
				})
			},
			allowOutsideClick: () => !Swal.isLoading()
		})
	}

	function addrow_edit(btn) {
		var row_copy = `
			<td>
				<select name="material_box_edit[]" class="form-control select2-edit box-edit" required>
					<option value="">- Select One -</option>
				</select>
			</td>
			<td class="material_description">-</td>
			<td>
				<input type="number" class="form-control" name="qty_edit[]" placeholder="Qty" min="0"
					required>
			</td>
			<td class="material_unit">PCS</td>
			<td></td>
		`;
		$(btn).closest("tbody").append("<tr>" + row_copy + "</tr>");
		var btn_delete = '<button type="button" class="btn btn-danger" onclick="deleterow(this)">-</button>';
		$(btn).closest("tbody").find("tr:last").find("td:last").html(btn_delete);

		var location = $("#get_location").val();
		$.ajax({
			type: "POST",
			url: "<?= base_url() ?>admin/replenishment/get_location",
			dataType: 'html',
			data: {
				location: location
			},
		}).done(function(response) {
			if (response !== 'notfound') {
				$("[name='material_box_edit[]']").html(response);
			} else {
				$("[name='material_box_edit[]']").attr('disabled');
			}
		})

		$(".select2-edit").select2({
			theme: 'bootstrap-5',
			dropdownParent: $("#editModal")
		});
	}


	$("[name=location]").keyup(function(e) {
		if (e.which == 13) {
			var location = $("[name=location]").val();
			$.ajax({
				type: "POST",
				url: "<?= base_url() ?>admin/replenishment/get_location",
				dataType: 'html',
				data: {
					location: location
				}
			}).done(function(response) {
				if (response !== 'notfound') {
					swal.fire("OK!", "Location found!", "success").then(function() {
						// $("[name=location]").attr('readonly', 'readonly');
						$("[name='material_box[]']").removeAttr('disabled');
						$("[name='material_box[]']").html(response);
					});
				} else {
					swal.fire("Error!", "Location not found!", "error").then(function() {
						$("[name=location]").val('');
					});
				}
			})
		}
	});

	function add_replenishment() {
		// Form
		var work_order_id = $("[name=work_order_id]").val();
		var user_id = $("[name=user_id]").val();
		var location = $("[name=location]").val();

		// Table
		var material_box = $("[name='material_box[]']").map(function() {
			return $(this).val();
		}).get();
		var qty = $("[name='qty[]']").map(function() {
			return $(this).val();
		}).get();;

		if (work_order_id == '') {
			swal.fire("Error!", "Must input Work Order first!", 'error');
		} else if (location == '') {
			swal.fire("Error!", "Must input location first!", 'error');
		} else {
			$.ajax({
				method: "POST",
				url: "<?= base_url() ?>admin/replenishment/add",
				data: {
					location: location,
					work_order_id: work_order_id,
					user_id: user_id,

					material_box: material_box,
					qty: qty
				}
			}).done(function(response) {
				console.log(response)
				if (response == 'OK') {
					swal.fire("OK!", "Successful add replenishment", "success").then(function() {
						window.location.reload();
					});
				} else {
					swal.fire("Error!", "Failed add replenishment", "error").then(function() {
						window.location.reload();
					});
				}
			});
		}
	}
</script>