<main class="content">
	<div class="container-fluid p-0">

		<h1 class="h3 mb-3">Material Release</h1>

		<div class="row">
			<div class="col-12 col-xl-12">
				<div class="card">
					<div class="card-header">
						<h5 class="card-title">Material Release
							<?php if($this->session->level == 0 || $this->session->level == 3): ?>
							<div class="float-end">
								<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#addModal"><i class="fa fa-plus"></i> Add Data</button>
							</div>
							<?php endif; ?>
						</h5>
						<h6 class="card-subtitle text-muted">List data Material Release
						</h6>
					</div>
					<div class="card-body">
						<?= $this->session->flashdata('msg') ?>
						<table class="table table-striped dataTable">
							<thead>
								<tr>
									<th>#</th>
									<th>Release No.</th>
									<th>No Work Order</th>
									<th>Batch Num</th>
									<th>PO Num</th>
									<th>Qty</th>
									<th>Status</th>
									<th>Status Approval</th>
									<th>Released By</th>
									<th>Created At</th>
									<th>Approved By</th>
									<th>Approved At</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$no = 1;
								foreach ($replenishment as $row) :
										$pembuat = $this->db->where([
											'user_id' => $row['user_id']
										])->get('tbl_user')->row();

										$approve = $this->db->where([
											'user_id' => $row['approved_by']
										])->get('tbl_user')->row();
								?>
									<tr>
										<td><?= $no++ ?></td>
										<td><?= $row['replenishment_no'] ?></td>
										<td><?= $row['work_order_id'] ?></td>
										<td><?= $row['wo_formula'] ?></td>
										<td><?= $row['wo_po_num'] ?></td>
										<td><?= $row['wo_pack'] ?></td>
										<td><?= $row['wo_status'] ?></td>
										<td>
											<?php if($row['approval'] == 'not approved'): ?>
												<span class="badge bg-danger"><?= $row['approval'] ?></span>
											<?php elseif($row['approval'] == 'approved'): ?>
												<span class="badge bg-success"><?= $row['approval'] ?></span>
											<?php elseif($row['approval'] == 'waiting'): ?>
												<span class="badge bg-warning"><?= $row['approval'] ?></span>
											<?php endif; ?>		
										</td>
										<td><?= $pembuat->name ?></td>
										<td><?= date('d M Y H:i', strtotime($row['created_at'])) ?></td>
										<td><?= is_null($row['approved_by']) ? "-" : $approve->name  ?></td>
										<td><?= is_null($row['approved_at']) ? "-" : date('d M Y H:i', strtotime($row['approved_at'])) ?></td>
										<td class="table-action">
											<a href="<?= base_url() ?>admin/replenishment/release_document/<?= $row['replenishment_id'] ?>" title="Release Document" target="_blank"><i data-feather="eye"></i></a>
											<?php if($this->session->level == 0 ||  $this->session->level == 4 || $this->session->level == 5): ?>
												<?php if($row['approval'] == 'waiting'): ?>
													<a href="#!" data-bs-toggle="modal" data-bs-target="#statusModal" data-id="<?= $row['replenishment_id'] ?>"><i data-feather="edit" title="Edit Status"></i></a>
												<?php endif; ?>
											<?php endif; ?>
											<?php if($this->session->level == 0 || $this->session->level == 3): ?>
												<?php if($row['approval'] != 'approved'): ?>
													<a href="#!" onclick="delete_data('<?= $row['replenishment_id'] ?>')"><i data-feather="trash-2"></i></a>
												<?php endif; ?>
											<?php endif; ?>
										</td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>

		</div>

	</div>
</main>

<div class="modal fade" id="addModal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Add Data Material Release</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body m-3">
				<div class="mb-3">
					<label class="form-label">Released No.</label>
					<input type="text" class="form-control form-control-lg" name="replenishment_no" value="<?= "RN " . date('Ymd') . rand(0,999) ?>" readonly>
				</div>

				<div class="mb-3">
					<div class="form-group" id="select-add">
						<label for="">Work Order Number</label>
						<select required class="form-control select2" id="wo_id" name="work_order_id">
							<option value="0">Choose Work Order</option>
							<?php foreach ($api_wo as $item) : ?>
								<option value="<?= $item->id ?>"><?= $item->po_num ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>

				<div class="mb-3" id="wo_batch" style="display: none;">
					<div class="form-group">
						<label for="">Batch Number</label>
						<select required class="form-control select2" name="wo_batch_item" id="wo_batch_item">
						</select>
					</div>

					<div class="mb-3">
				        <label class="form-label">Tanggal Berlaku</label><br>
						<input type="date" class="form-control" name="tgl_berlaku" placeholder="Expired date" required>
				    </div>

					<div class="mb-3">
				        <label class="form-label">Tanggal Mulai</label><br>
						<input type="date" class="form-control" name="tgl_mulai" placeholder="Expired date" required>
				    </div>
				</div>

				<div class="mb-3" id="wo_info" style="display: none">
					<div class="row mb-2">
						<div class="col-6">
							<label class="form-label">Formula Num</label>
							<input type="text" class="form-control form-control-lg" name="wo_formula_num" id="wo_formula_num" value="" readonly>
						</div>

						<div class="col-6">
							<label class="form-label">PO Num</label>
							<input type="text" class="form-control form-control-lg" name="wo_po_num" id="wo_po_num" readonly>
						</div>

					</div>

					<div class="row mb-2">
						<div class="col-6">
							<label class="form-label">Pack</label>
							<input type="text" class="form-control form-control-lg" name="wo_pack" id="wo_pack" readonly>
						</div>

						<div class="col-6">
							<label class="form-label">Status</label>
							<input type="text" class="form-control form-control-lg" name="wo_status" id="wo_status" readonly>
						</div>
					</div>

					<div id="batch_info" style="display: none;">
						<div class="row mb-2">
							<div class="col-6">
								<label class="form-label">Product Code</label>
								<input type="text" class="form-control form-control-lg" name="wo_product_code" id="wo_product_code" value="" readonly>
							</div>

							<div class="col-6">
								<label class="form-label">Product Name</label>
								<input type="text" class="form-control form-control-lg" name="wo_product_name" id="wo_product_name" readonly>
							</div>						
						</div>
					</div>

					<div id="pkg_info" style="display: none;">
						<div class="row mb-2">
							<div class="col-6">
								<label class="form-label">Package Code</label>
								<input type="text" class="form-control form-control-lg" name="wo_pkg_code" id="wo_pkg_code" value="" readonly>
							</div>

							<div class="col-6">
								<label class="form-label">Package Name</label>
								<input type="text" class="form-control form-control-lg" name="wo_pkg_name" id="wo_pkg_name" readonly>
							</div>						
						</div>
					</div>

					<label for="" class="mb-2">Work Order Material List</label>

					<table class="table table-bordered table-inverse table-responsive">
						<thead class="thead-default">
							<tr>
								<th width="5%">#</th>
								<th width="30%">Material</th>
								<!-- <th width="15%">Needed</th> -->
								<th width="15%">Qty</th>
								<th width="50%">Location (Expired Date)</th>
							</tr>
						</thead>
						<tbody id="table_material">
						</tbody>
					</table>
					<div class="align-items-center text-center mx-auto" id="loading">
						<h6>Getting Material From Formula,Please Wait...</h6>
						<div class="spinner-border text-info"></div>
					</div>

					<!-- <label for="" class="mb-2">Work Order Package List</label>

					<table class="table table-bordered table-inverse table-responsive">
						<thead class="thead-default">
							<tr>
								<th width="5%">#</th>
								<th width="30%">Material</th>
								<th width="15%">Qty</th>
								<th width="50%">Location (Expired Date)</th>
							</tr>
						</thead>
						<tbody id="table_material">
						</tbody>
					</table> -->
				</div>

				<div class="mb-3">
					<label class="form-label">Released By</label>
					<input type="text" class="form-control form-control-lg" name="user_name" value="<?= $this->session->name; ?>" readonly>
					<input type="hidden" class="form-control" name="user_id" value="<?= $this->session->user_id; ?>" required>
				</div>

<!-- 				<div class="mb-3">
					<label class="form-label">Batch Num</label>
					<div class="row">
						<div class="col-12">
							<select class="form-control" id="camera"></select>
						</div>
						<div class="col-12 text-center">
							<canvas height="200px"></canvas>
						</div>
						<div class="col-12">
							<input type="text" class="form-control" id="batch_num" name="batch_num" placeholder="Scan Id Receiving">
						</div>
					</div>
				</div>
				<div class="mb-3">
					<label class="form-label">Box List</label>
					<div id="box_list" style="display: none">
						<div class="table-responsive">
							<table class="table table-sm table-striped">
								<thead>
									<tr>
										<th>Batch No.</th>
										<th>PO No.</th>
										<th>Material</th>
										<th>Qty</th>
										<th>Location</th>
										<th>Aksi</th>
									</tr>
								</thead>
								<tbody id="box_tbody">
								</tbody>
							</table>
						</div>
					</div>
				</div>
 -->

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
				<button type="button" onclick="add_replenishment()" class="btn btn-primary">Save changes</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="statusModal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Status Data Material Release</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div id="status_data"></div>
		</div>
	</div>
</div>

<div class="modal fade" id="editModal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Edit Data Material Release</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div id="edit_data">

			</div>
		</div>
	</div>
</div>

<script>
	let arr_material_box = [];
	let arr_batch_name = [];
	let arr_batch_num = [];
	let arr_batch_id = [];	
	let arr_material_qty = [];
	let arr_material_loc = [];

	$("#statusModal").on('shown.bs.modal', function (e) {
		var id = $(e.relatedTarget).data('id');
		$.ajax({
			type: "POST",
			url: "<?= base_url() ?>admin/replenishment/get",
			data: {
				id: id
			}
		}).done(function (response) {
			$("#status_data").html(response);
		});
	});


	$("[name='material_box[]']").change(function(e) {
		var description = $(this).find(":selected").data('description');
		var unit = $(this).find(":selected").data('unit');

		$(this).closest('tr').children('td#material_description').text(description);
		$(this).closest('tr').children('td#material_unit').text(unit);
	});

	$(".box-edit").change(function(e) {
		console.log('berubah')
	});

	$("#editModal").on('shown.bs.modal', function(e) {
		var id = $(e.relatedTarget).data('id');
		$.ajax({
			type: "POST",
			url: "<?= base_url() ?>admin/replenishment/get",
			data: {
				id: id
			}
		}).done(function(response) {
			$("#edit_data").html(response);
		});
	});

	$("#wo_id").on('select2:select', function(e) {
		let value = e.params.data.id
		let url = "<?= base_url() ?>"
		// console.log(value)
		if (value != 0) {
			$.ajax({
				url: url + 'admin/replenishment/get_api_detail_wo/' + value,
				method: 'GET',
				success: (res) => {
					var id_cust = res.data.customer_id
					let options = "";
					$('#wo_batch_item').empty();
					$('#wo_batch_item').append('<option value="0">Choose Batch Number</option>')
					$("#wo_batch").css({
						"display": "none"
					})
			        $('#batch_info').css({
						"display": "none"
					})
			        $('#wo_info').css({
						"display": "none"
					})
			        $('#pkg_info').css({
						"display": "none"
					})
					$.ajax({
						url : url + 'admin/replenishment/get_api_product',
						method : 'GET',
						success : (res) => {
							$('#wo_batch').css({
								"display": "block"
							})
					        $.each(res.data, function(index, value){
					        	if(id_cust == value.customer_id){
					        		options = `
					        		<option value="${value.formula_id}">${value.product_code} (${value.product_name})</option>
					        		`
					        		$('#wo_batch_item').append(options)
					        	}
					        })
						} 
					})
				}
			})
		} else {
			$("#wo_info").css({
				"display": "none"
			})

			$("#wo_batch").css({
				"display": "none"
			})

			$("#batch_info").css({
				"display": "none"
			})
		}
	})

	let mtr = []
	let loc = []
	let arr_material_need = [];
	let arr_material_name = [];
	let packaging_name = "";
	let packaging_code = "";
	let revision_num = "";
	$("#wo_batch").on('select2:select', function(e) {
		let value = e.params.data.id
		let url = "<?= base_url() ?>"

		if (value != 0) {
			// $.ajax({
			// 	url : url + 'admin/replenishment/get_api_formula/'+e.params.data.id,
			// 	method : "GET",
			// 	success : (resp) => {
			// 		let id = resp.data.id
			// 		// console.log(resp.data)
			// 		$.ajax({
			// 			url : url + 'admin/replenishment/get_api_product',
			// 			method : 'GET',
			// 			success : (res) => {
			// 		        $.each(res.data, function(index, value){
			// 		        	if(id == value.formula_id){
			// 		        		let id_product = value.id;

			// 		        		let data = {
			// 		        			labelling : value.id_labelling,
			// 		        			packaging : value.id_packaging,
			// 		        			product_name : value.product_name,
			// 		        			product_code : value.product_code,
			// 		        		}

			// 						$.ajax({
			// 							url : url + 'admin/replenishment/get_product_activity',
			// 							method : 'GET',
			// 							success : (res) => {
			// 						        $.each(res.data, function(index, value){
			// 						        	// if(id_product == value.po_product_id){
			// 						        		console.log(value)
			// 						        	// }
			// 						        })
			// 							} 
			// 						})
			// 		        		// console.log(data)
			// 		        	}
			// 		        })
			// 			} 
			// 		})
			// 	}
			// })
	        $('#batch_info').css({
				"display": "none"
			})
			$.ajax({
				url : url + 'admin/replenishment/get_api_product',
				method : "GET",
				success : (res) => {
			        $.each(res.data, function(index, value){
			        	if(e.params.data.id == value.formula_id){
							$("#wo_product_code").val(value.product_code);
							$("#wo_product_name").val(value.product_name);
							$.ajax({
								url : url + 'admin/replenishment/get_api_packaging/'+value.id_packaging,
								method : "GET",
								success : (res) => {
									packaging_name = res.data.packaging_name
									packaging_code = res.data.packaging_code
									$("#wo_pkg_code").val(res.data.packaging_code);
									$("#wo_pkg_name").val(res.data.packaging_name);
							        $('#pkg_info').css({
										"display": "block"
									})	
								}
							})
							$.ajax({
								url : url + 'admin/replenishment/get_trial_revision/'+value.trial_revision_data_id,
								method : "GET",
								success : (res) => {
									revision_num = res.data.revision_num		
								}
							})
			        	}
			        })
			        $('#batch_info').css({
						"display": "block"
					})				
				}
			})
			$.ajax({
				url: url + 'admin/replenishment/get_api_formula/' + value,
				method: 'GET',
				success: (res) => {
					$("#wo_formula_num").val(res.data.formula_num);
					$("#wo_po_num").val(res.data.trial_revision.trial.po_customer.po_num)
					$("#wo_pack").val(res.data.trial_revision.trial.po_customer_detail.pack)
					$("#wo_status").val(res.data.trial_revision.trial.po_customer.status)

					$("#wo_info").css({
						"display": "block"
					})

					$("#table_material").empty()
	        		let formula_id = res.data.id
	        		$.ajax({
	        			url: url + 'admin/replenishment/get_api_detail_material',
	        			method : "GET",
	        			success: (res) => {
	        				// console.log(res.putaway)
	        				$.each(res.data,function(index,value){
	        					let data = {
	        						batch_id : value.id,
	        						batch_num : value.batch_num, 
	        						material_id : value.material_id,
	        						material_name : value.material != null ? value.material.material_name : '-',
	        						exp_date : value.expired_date,
	        					};
	        					mtr.push(data);
	        				})
	        				$.each(res.putaway,function(index,value){
	        					let data = {
	        						id : value.batch_id,
	        						material_id : value.material_id, 
	        						location : value.location,
	        					};
	        					loc.push(data);
	        				})
	        				// console.log(res.putaway);
	        				let ops = "";
			        		$.ajax({
			        			url: url + 'admin/replenishment/get_api_detail_formula',
			        			method : "GET",
			        			success: (res) => {
									let no = 1
		        					let z = "";
			        				let data_option = [];
			        				let data_material = [];
			        				let option = "";
		        					$.each(loc,function(i,v){
	        							$.each(mtr,function(idx,val){
		        							if(val.batch_id == v.id){
		        								if(val.material_id == v.material_id){		
			        								datas = {
			        									material_id : val.material_id,
			        									html : `<option value="${v.location}">${v.location}(${val.exp_date == 'null' ? '-' : val.exp_date})</option>`
		        									}
			        								data_option.push(datas)
			        								// console.log(datass)
		        								}
		        							}
		        						})
	        						})
			        				let txt_html = ""
			        				$.each(res.data,function(index,value){
			        					if(formula_id == value.formula_id){
			        						var datas = {
			        							material_name : value.material.material_name,
			        							quantity : value.quantity,
			        							material_id : value.material_id,
			        						}
			        						data_material.push(datas);
											arr_batch_id.push(value.id)
											arr_batch_name.push(value.material.material_name)
											arr_batch_num.push(value.material.material_code)
											// arr_material_need.push(value.quantity);
											// arr_material_name.push(value.material.material_name);
					        				}
			        				})
									const newMap = data_material.map((data) => {
									  let id = data.material_id;
									  let newData = data_option.filter((d) => d.material_id === id);

									  let options = [];
									  newData.forEach((nd) => {
									    options.push(nd.html);
									  });

									  return { ...data, options };
									});
									console.log(newMap);

			        				$.each(newMap,function(index,value){
										let html = `
											<tr>
												<td>${no++}</td>
												<td>${value.material_name}</td>
												<td>
									                <input type="number" class="form-control" name="qty[]" placeholder="Qty" value="${value.quantity}" required>
												</td>
												<td>
													${`<select name="location[]" id="location" class="form-control select2-loc" required>${value.options}</select>` || '<select name="location[]" id="location" class="form-control select2-loc" required><option readonly value="">No Location Found</option></select>'}
												</td>

											</tr>	
										`	
										$("#table_material").append(html)	
										console.log('append table',value)							        			

									})
									$("#loading").css({
										"display": "none"
									})
			        			}
			        		})
	        			}
	        		})
				}
			})
		} else {
			$("#wo_info").css({
				"display": "none"
			})

			$("#wo_batch").css({
				"display": "none"
			})

			$("#batch_info").css({
				"display": "none"
			})
		}
	})



	// $("[name=batch_num]").keyup(function(e) {
	// 	if (e.which == 13) {
	// 		var batch_num = $("[name=batch_num]").val();
	// 		if(arr_batch_num.includes(batch_num)){
	// 			console.log('batch_num already scanned')
	// 			swal.fire("Error!", "Duplicated batch num!", "error").then(function () {
	// 				$("[name=batch_num]").val('');
	// 			});
	// 		}else{
	// 			$.ajax({
	// 				type: "POST",
	// 				url: "<?= base_url() ?>admin/replenishment/get_box",
	// 				data: {
	// 					batch_num: batch_num
	// 				}
	// 			}).done(function (response) {
	// 				// response = JSON.parse(response)
	// 				// console.log(response)
	// 				if (response.message == 'OK') {
	// 					swal.fire("OK!", "Batch Num found!", "success").then(function () {
	// 						$("[name=batch_num]").val('').focus();
							
	// 						// Show alert
	// 						// $(".alert").alert();
	// 						$(".alert").css({
	// 							'display' : 'block'
	// 						})

	// 						let lokasi = response.lokasi

	// 						$("#alert-text").html('This material batch will available to put at : <br/>');
	// 						$("#alert-text").append('<ul style="margin-left: 15px">');
	// 						lokasi.forEach(element => {
	// 							$("#alert-text").append('<li>'+element+'</li>')
	// 						});
	// 						$("#alert-text").append('</ul>');

	// 						// Table
	// 						let html = `
	// 							<tr>
	// 								<td>${response.data.batch_num}</td>
	// 								<td>${response.data.purchase.purchase_num}</td>
	// 								<td>${response.data.material.material_name}</td>
	// 								<td>
	// 					                <input type="number" class="form-control form-control" name="qty[]" placeholder="Qty" value="1" required>
	// 								</td>
	// 								<td>
	// 									<select name="location[]" id="location" class="form-control">
	// 										<?php foreach ($location as $loc) : ?>
	// 											<option value="<?= $loc->location_name ?>"><?= $loc->location_name ?></option>
	// 										<?php endforeach; ?>
	// 									</select>
	// 								</td>
	// 								<td>
	// 									<button type="button" class="btn btn-danger" onclick="delete_box(this, '${response.data.batch_num}', '${response.data.id}')">-</button>
	// 								</td>
	// 							</tr>
	// 						`;

	// 						// push array of batch num & material box id
	// 						arr_batch_num.push(response.data.batch_num)
	// 						arr_batch_id.push(response.data.id)

	// 						$("#box_list").css({
	// 							'display' : 'block'
	// 						});


	// 						$('#box_tbody').append(html)
	// 					});
	// 				} else if (response.message == 'duplicate') {
	// 					swal.fire("Error!", "Duplicated batch num!", "error").then(function () {
	// 						$("[name=batch_num]").val('');
	// 					});
	// 				} else {
	// 					swal.fire("Error!", "Box code not found!", "error").then(function () {
	// 						$("[name=batch_num]").val('');
	// 					});
	// 				}
	// 			})
	// 		}
	// 	}
	// });

	function addrow(btn) {
		var row_copy = `
			<td>
				<select name="material_box[]" class="form-control select2" required>
					<option value="">- Select One -</option>
				</select>
			</td>
			<td id="material_description">-</td>
			<td>
				<input type="number" class="form-control" name="qty[]" placeholder="Qty" min="0"
					required>
			</td>
			<td>PCS</td>
			<td></td>
		`;
		$(btn).closest("tbody").append("<tr>" + row_copy + "</tr>");
		var btn_delete = '<button type="button" class="btn btn-danger" onclick="deleterow(this)">-</button>';
		$(btn).closest("tbody").find("tr:last").find("td:last").html(btn_delete);

		var location = $("[name=location]").val();
		$.ajax({
			type: "POST",
			url: "<?= base_url() ?>admin/replenishment/get_location",
			dataType: 'html',
			data: {
				location: location
			}
		}).done(function(response) {
			if (response !== 'notfound') {
				// $("[name=location]").attr('readonly', 'readonly');
				$("[name='material_box[]']").removeAttr('disabled');
				$("[name='material_box[]']").html(response);
			} else {
				$("[name='material_box[]']").attr('disabled');
			}
		})

		$(".select2").select2({
			theme: 'bootstrap-5',
			dropdownParent: $("#addModal")
		});
	}

	function delete_box(btn, batch_num, batch_id) {
		let index = arr_batch_num.indexOf(batch_num)
    	arr_batch_num.splice(index, 1)

		let index_id = arr_batch_id.indexOf(batch_id)
    	arr_batch_id.splice(index_id, 1)

		$(btn).closest("tr").remove();
		// $.ajax({
		// 	type: "POST",
		// 	url: "<?= base_url() ?>admin/putaway/delete_box",
		// 	data: {
		// 		key: key
		// 	}
		// }).done(function (response) {
		// 	if (response.includes('OK')) {
		// 		// load_box();
		// 	} else {
		// 		swal.fire("Error!", "Delete box failed!", "error");
		// 	}
		// });
	}


	function delete_data(id) {
		Swal.fire({
			title: 'Konfirmasi ?',
			text: "Apakah kamu yakin ?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#28a745',
			cancelButtonColor: '#dc3545',
			confirmButtonText: 'Yes!',
			cancelButtonText: 'No!',
			showLoaderOnConfirm: true,
			preConfirm: function() {
				return new Promise(function(resolve, reject) {
					$.ajax({
						method: 'POST',
						url: "<?= base_url() ?>admin/replenishment/delete",
						data: {
							id: id
						}
					}).done(function(msg) {
						if (msg.includes("ok")) {
							swal.fire("OK!", "Data berhasil dihapus!", "success").then(
								function() {
									location.reload();
								})
						} else {
							swal.fire("Gagal!", msg, "error").then(function() {
								location.reload();
							})
						}
					})
				})
			},
			allowOutsideClick: () => !Swal.isLoading()
		})
	}

	function addrow_edit(btn) {
		var row_copy = `
			<td>
				<select name="material_box_edit[]" class="form-control select2-edit box-edit" required>
					<option value="">- Select One -</option>
				</select>
			</td>
			<td class="material_description">-</td>
			<td>
				<input type="number" class="form-control" name="qty_edit[]" placeholder="Qty" min="0"
					required>
			</td>
			<td class="material_unit">PCS</td>
			<td></td>
		`;
		$(btn).closest("tbody").append("<tr>" + row_copy + "</tr>");
		var btn_delete = '<button type="button" class="btn btn-danger" onclick="deleterow(this)">-</button>';
		$(btn).closest("tbody").find("tr:last").find("td:last").html(btn_delete);

		var location = $("#get_location").val();
		$.ajax({
			type: "POST",
			url: "<?= base_url() ?>admin/replenishment/get_location",
			dataType: 'html',
			data: {
				location: location
			},
		}).done(function(response) {
			if (response !== 'notfound') {
				$("[name='material_box_edit[]']").html(response);
			} else {
				$("[name='material_box_edit[]']").attr('disabled');
			}
		})

		$(".select2-edit").select2({
			theme: 'bootstrap-5',
			dropdownParent: $("#editModal")
		});
	}

	function add_replenishment() {
		// Form
		var replenishment_no = $("[name=replenishment_no]").val();
		var work_order_id = $("[name=work_order_id]").val();
		var work_order_batch = $("[name=wo_batch_item]").val();
		var wo_formula = $("[name=wo_formula_num]").val();
		var wo_po_num = $("[name=wo_po_num]").val();
		var wo_pack = $("[name=wo_pack]").val();
		var wo_status = $("[name=wo_status]").val();
		var wo_product_code = $("[name=wo_product_code]").val();
		var wo_product_name = $("[name=wo_product_name]").val();
		var tgl_berlaku = $("[name=tgl_berlaku]").val();
		var tgl_mulai = $("[name=tgl_mulai]").val();
		var user_id = $("[name=user_id]").val();

		// Table
		// var material_box = $("[name='material_box[]']").map(function() {
		// 	return $(this).val();
		// }).get();
		var qty = $("[name='qty[]']").map(function() {
			return $(this).val();
		}).get();;
		var location = $("[name='location[]']").map(function() {
			return $(this).val();
		}).get();;

		if (work_order_id == '') {
			swal.fire("Error!", "Must input Work Order first!", 'error');
		} else if (location.includes('')) {
			swal.fire("Error!", "Must input location first!", 'error');
		} else {
			// console.log(arr_material_box)
			$.ajax({
				method: "POST",
				url: "<?= base_url() ?>admin/replenishment/add",
				data: {
					replenishment_no: replenishment_no,
					work_order_id: work_order_id,
					work_order_batch: work_order_batch,
					wo_formula: wo_formula,
					wo_po_num: wo_po_num,
					wo_pack: wo_pack,
					wo_status: wo_status,
					wo_product_name: wo_product_name,
					wo_product_code: wo_product_code,
					tgl_berlaku : tgl_berlaku,
					tgl_mulai : tgl_mulai,
					packaging_name: packaging_name,
					packaging_code: packaging_code,
					revision_num: revision_num,
					user_id: user_id,
					batch_name : arr_batch_name,
					batch_num : arr_batch_num,
					batch_id : arr_batch_id,
					batch_location : location,
					batch_qty : qty,
				}
			}).done(function(response) {
				console.log(response)
				if (response.includes('OK')) {
					swal.fire("OK!", "Successful add replenishment", "success").then(function() {
						window.location.reload();
					});
				} else {
					swal.fire("Error!", "Failed add replenishment", "error").then(function() {
						window.location.reload();
					});
				}
			});

			// console.log(arr_material_box)
			console.log(arr_batch_num)
			console.log(arr_batch_id)
			console.log(qty)
			console.log(location)

		}
	}
	var arg = {
		resultFunction: function(result) {
			var e = $.Event( "keyup", { keyCode: 13 } );
			$("#batch_num").val(result.code).trigger(e);
			// $('body').append($('<li>' + result.format + ': ' + result.code + '</li>'));
		}
	};
	var decoder = $("canvas").WebCodeCamJQuery(arg).data().plugin_WebCodeCamJQuery;
	decoder.buildSelectMenu("#camera");
	decoder.play();
	/*  Without visible select menu
	    decoder.buildSelectMenu(document.createElement('select'), 'environment|back').init(arg).play();
	*/
	$('#camera').on('change', function() {
		decoder.stop().play();
	});
</script>