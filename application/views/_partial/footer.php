<footer class="footer">
	<div class="container-fluid">
		<div class="row text-muted">
			<div class="col-6 text-start">
				<p class="mb-0">
					<a class="text-muted" href="https://digitalrise.id/" target="_blank"><strong>Digital RISe
							Indonesia</strong></a> &copy;
				</p>
			</div>
			<div class="col-6 text-end">
				<ul class="list-inline">
					<li class="list-inline-item">
						<a class="text-muted" href="https://digitalrise.id/" target="_blank">Support</a>
					</li>
					<li class="list-inline-item">
						<a class="text-muted" href="https://digitalrise.id/" target="_blank">Help Center</a>
					</li>
					<li class="list-inline-item">
						<a class="text-muted" href="https://digitalrise.id/" target="_blank">Privacy</a>
					</li>
					<li class="list-inline-item">
						<a class="text-muted" href="https://digitalrise.id/" target="_blank">Terms</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</footer>
</div>
</div>
<!-- <script src="<?= base_url() ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script> -->
<script src="<?= base_url() ?>assets/plugins/sweetalert2/dist/sweetalert2.all.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/select2/dist/js/select2.min.js"></script>
<script src="https://unpkg.com/feather-icons@4.28.0/dist/feather.min.js"></script>
<!-- <script src="https://kodinger.blob.core.windows.net/script/faster.js"></script> -->
<!-- DataTables  & Plugins -->
<script src="<?= base_url('assets') ?>/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url('assets') ?>/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?= base_url('assets') ?>/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?= base_url('assets') ?>/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>



<script>
	feather.replace()

</script>

<!-- Datatable -->
<script>
	$(function () {
		$(".dataTable").DataTable({
			responsive: true
		})

		$(".select2").select2({
			theme: 'bootstrap-5',
			dropdownParent: $("#addModal"),
			width: 'auto'
		});

		$(".select2").select2({
			theme: 'bootstrap-5',
			dropdownParent: $("#select-add"),
			width: 'auto'
		});

		$(".selects2").select2({
			theme: 'bootstrap-5',
			dropdownParent: $("#select-log"),
			width: 'auto'
		});
		

		$(".select-dashboard").select2({
			theme: 'bootstrap-5',
			width: 'auto'
		});

		$(".select2-edit").select2({
			theme: 'bootstrap-5',
			dropdownParent: $("#editModal"),
			width: 'auto'
		});

		$(".select2-location").select2({
			theme: 'bootstrap-5',
			dropdownParent: $("#select-add"),
		});

	})



</script>
<script src="<?= base_url() ?>assets/static/js/app.js"></script>
</body>

</html>
