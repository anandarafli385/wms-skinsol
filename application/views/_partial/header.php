<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="PT Skinsol Kosmetik Industri">
	<meta name="author" content="Digital RISe Indonesia">
	<meta name="keywords" content="PT Skinsol Kosmetik Industri">

	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link rel="shortcut icon" href="<?= base_url() ?>assets/src/img/logo2.png" />

	<title><?= $site_title ?> - PT Skinsol Kosmetik Industri</title>

	<link href="<?= base_url() ?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?= base_url() ?>assets/plugins/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
	<link href="<?= base_url() ?>assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?= base_url() ?>assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet">
	<link href="<?= base_url() ?>assets/plugins/select2-bootstrap5-theme/select2-bootstrap-5-theme.min.css" rel="stylesheet">

	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;600&display=swap" rel="stylesheet">
	<!-- DataTables -->
	<link rel="stylesheet" href="<?= base_url('assets') ?>/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" href="<?= base_url('assets') ?>/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
	<link href="<?= base_url() ?>assets/static/css/app.css" rel="stylesheet">

	<script src="<?= base_url() ?>assets/plugins/jquery/dist/jquery.min.js"></script>
	<script src="<?= base_url() ?>assets/plugins/webcodecamjs/js/qrcodelib.js"></script>
	<script src="<?= base_url() ?>assets/plugins/webcodecamjs/js/webcodecamjquery.js"></script>
</head>

<body>
	<div class="wrapper">
		<nav id="" class="sidebar js-sidebar">
			<div class="sidebar-content js-simplebar">
				<a class="sidebar-brand py-0" href="<?= base_url() ?>">
					<!-- <span class="align-middle">PT Skinsol Kosmetik Industri</span> -->
					<img src="<?= base_url() ?>assets/src/img/logo2.png" alt="logo" class="align-middle my-4" style="width: 100%">
				</a>

				<ul class="sidebar-nav">
<!-- 
					<li class="sidebar-item <?= active_menu('dashboard') ?>">
						<a class="sidebar-link" href="<?= base_url('admin/dashboard') ?>">
							<i class="align-middle" data-feather="sliders"></i> <span class="align-middle">Dashboard</span>
						</a>
					</li>

 -->
					<!-- Jika Level tidak sama dengan production -->
					<?php if($this->session->level == 0 || $this->session->level == 1 || $this->session->level == 2 || $this->session->level == 5): ?>
					<li class="sidebar-header">
						Warehouse Incoming
					</li>

					<li class="sidebar-item">
						<div class="accordion accordion-flush active" id="accordionFlushExample">
						  <div class="accordion-item sidebar-item <?= active_menu('purchasing_order') ?> <?= active_menu('purchasing_order_kemas') ?>">
								<a class="sidebar-link accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne" style="color: rgba(233,236,239,.5); background-color: #222e3c; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important;">
									<i class="align-middle" data-feather="file-plus"></i> <span class="align-middle" style="font-size: 14px;">Purchasing Order</span>
								</a>
						    <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample" style="color: rgba(233,236,239,.5); background-color: #222e3c; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important;">
								<div class="ms-4 sidebar-item <?= active_menu('purchasing_order') ?>">
									<a class="sidebar-link" href="<?= base_url('admin/purchasing_order') ?>">
										</i> <span class="align-middle">Bahan Baku</span>
									</a>
								</div>
								<div class="ms-4 sidebar-item <?= active_menu('purchasing_order_kemas') ?>">
									<a class="sidebar-link" href="<?= base_url('admin/purchasing_order_kemas') ?>">
										</i> <span class="align-middle">Bahan Kemas</span>
									</a>
								</div>
						    </div>
						  </div>
						</div>
					</li>


					<li class="sidebar-item">
						<div class="accordion accordion-flush" id="accordionFlushExampleTwo">
						  <div class="accordion-item sidebar-item <?= active_menu('good_receiving') ?> <?= active_menu('good_receiving_kemas') ?>">
								<a class="sidebar-link accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo" style="color: rgba(233,236,239,.5); background-color: #222e3c; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important;">
									<i class="align-middle" data-feather="folder-plus"></i> <span class="align-middle" style="font-size: 14px;">Receiving</span>
								</a>
						    <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExampleTwo" style="color: rgba(233,236,239,.5); background-color: #222e3c; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important;">
								<div class="ms-4 sidebar-item <?= active_menu('good_receiving') ?>">
									<a class="sidebar-link" href="<?= base_url('admin/good_receiving') ?>">
										<span class="align-middle">Bahan Baku</span>
									</a>
								</div>
								<div class="ms-4 sidebar-item <?= active_menu('good_receiving_kemas') ?>">
									<a class="sidebar-link" href="<?= base_url('admin/good_receiving_kemas') ?>">
										<span class="align-middle">Bahan Kemas</span>
									</a>
								</div>
						    </div>
						  </div>
						</div>
					</li>
					
					<?php if($this->session->level == 0 || $this->session->level == 1 || $this->session->level == 3 || $this->session->level == 4 ): ?>
					<li class="sidebar-item">
						<div class="accordion accordion-flush" id="accordionFlushExampleThree">
						  <div class="accordion-item sidebar-item <?= active_menu('putaway') ?> <?= active_menu('putaway_kemas') ?>">
								<a class="sidebar-link accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree" style="color: rgba(233,236,239,.5); background-color: #222e3c; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important;">
									<i class="align-middle" data-feather="log-in"></i> <span class="align-middle" style="font-size: 14px;">Putaway</span>
								</a>
						    <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExampleThree" style="color: rgba(233,236,239,.5); background-color: #222e3c; border-bottom-right-radius: 0px !important; border-bottom-left-radius: 0px !important;">
								<div class="ms-4 sidebar-item <?= active_menu('putaway') ?>">
									<a class="sidebar-link" href="<?= base_url('admin/putaway') ?>">
										<span class="align-middle">Bahan Baku</span>
									</a>
								</div>
								<div class="ms-4 sidebar-item <?= active_menu('putaway_kemas') ?>">
									<a class="sidebar-link" href="<?= base_url('admin/putaway_kemas') ?>">
										<span class="align-middle">Bahan Kemas</span>
									</a>
								</div>
						    </div>
						  </div>
						</div>
					</li>
					<?php endif; ?>
					
					<li class="sidebar-item <?= active_menu('cycle_count') ?>">
						<a class="sidebar-link" href="<?= base_url('admin/cycle_count') ?>">
							<i class="align-middle" data-feather="activity"></i> <span
								class="align-middle">Material Items</span>
						</a>
					</li>

					<li class="sidebar-item <?= active_menu('expenditure') ?>">
						<a class="sidebar-link" href="<?= base_url('admin/expenditure') ?>">
							<i class="align-middle" data-feather="truck"></i> <span
								class="align-middle">Expenditure</span>
						</a>
					</li>

					<li class="sidebar-item <?= active_menu('FG_Items') ?>">
						<a class="sidebar-link" href="<?= base_url('admin/FG_Items') ?>">
							<i class="align-middle" data-feather="activity"></i> <span
								class="align-middle">Stock Material</span>
						</a>
					</li>

					<li class="sidebar-item <?= active_menu('labelling') ?>">
						<a class="sidebar-link" href="<?= base_url('admin/labelling') ?>">
							<i class="align-middle" data-feather="package"></i> <span class="align-middle">Labelling Activity</span>
						</a>
					</li>

					<li class="sidebar-item <?= active_menu('filling') ?>">
						<a class="sidebar-link" href="<?= base_url('admin/filling') ?>">
							<i class="align-middle" data-feather="codesandbox"></i> <span class="align-middle">Filling Activity</span>
						</a>
					</li>

					<?php endif; ?>

					<!-- Jika level tidak sama dengan Warehouse -->

					<li class="sidebar-header">
						Production
					</li>
					<?php if($this->session->level == 0 || $this->session->level == 1 || $this->session->level == 3 || $this->session->level == 4 ): ?>
					
					<li class="sidebar-item <?= active_menu('work_order') ?>">
						<a class="sidebar-link" href="<?= base_url('admin/work_order') ?>">
							<i class="align-middle" data-feather="clipboard"></i> <span class="align-middle">Work
								Order</span>
						</a>
					</li>

					<?php endif; ?>


					<?php if($this->session->level == 0 || $this->session->level == 1 || $this->session->level == 3 || $this->session->level == 4 || $this->session->level == 5 ): ?>

					<li class="sidebar-item <?= active_menu('replenishment') ?>">
						<a class="sidebar-link" href="<?= base_url('admin/replenishment') ?>">
							<i class="align-middle" data-feather="folder-minus"></i> <span
								class="align-middle">Material Release</span>
						</a>
					</li>

					<li class="sidebar-item <?= active_menu('production') ?>">
						<a class="sidebar-link" href="<?= base_url('admin/production') ?>">
							<i class="align-middle" data-feather="trello"></i> <span
								class="align-middle">Production</span>
						</a>
					</li>
					<?php endif; ?>


					<!-- Jika level adalah superadmin  -->
					<?php if($this->session->level == 0): ?>

					<li class="sidebar-header">
						Master Data
					</li>

					<li class="sidebar-item  <?= active_menu('master_material') ?>">
						<a class="sidebar-link" href="<?= base_url('admin/master_material') ?>">
							<i class="align-middle" data-feather="archive"></i> <span class="align-middle">Master
								Material</span>
						</a>
					</li>

					<li class="sidebar-item <?= active_menu('master_packaging') ?>">
						<a class="sidebar-link" href="<?= base_url('admin/master_packaging') ?>">
							<i class="align-middle" data-feather="box"></i> <span class="align-middle">Master
								Packaging</span>
						</a>
					</li>

					<li class="sidebar-item <?= active_menu('master_trial') ?>">
						<a class="sidebar-link" href="<?= base_url('admin/master_trial') ?>">
							<i class="align-middle" data-feather="activity"></i> <span class="align-middle">Master
								Trial</span>
						</a>
					</li>

					<li class="sidebar-item <?= active_menu('master_fg') ?>">
						<a class="sidebar-link" href="<?= base_url('admin/master_fg') ?>">
							<i class="align-middle" data-feather="check-square"></i> <span class="align-middle">Bill of Material (BoM)</span>
						</a>
					</li>

					<li class="sidebar-item <?= active_menu('master_supplier') ?>">
						<a class="sidebar-link" href="<?= base_url('admin/master_supplier') ?>">
							<i class="align-middle" data-feather="truck"></i> <span class="align-middle">Master
								Supplier</span>
						</a>
					</li>

					<li class="sidebar-item <?= active_menu('master_customer') ?>">
						<a class="sidebar-link" href="<?= base_url('admin/master_customer') ?>">
							<i class="align-middle" data-feather="users"></i> <span class="align-middle">Master
								Customer</span>
						</a>
					</li>

					<li class="sidebar-item <?= active_menu('master_location') ?>">
						<a class="sidebar-link" href="<?= base_url('admin/master_location') ?>">
							<i class="align-middle" data-feather="map-pin"></i> <span class="align-middle">Master
								Location</span>
						</a>
					</li>

					<li class="sidebar-item <?= active_menu('master_user') ?>">
						<a class="sidebar-link" href="<?= base_url('admin/master_user') ?>">
							<i class="align-middle" data-feather="users"></i> <span class="align-middle">Master
								User</span>
						</a>
					</li>

					<?php endif; ?>

					<li class="sidebar-header">
						Config
					</li>

					<li class="sidebar-item <?= active_menu('profile') ?>">
						<a class="sidebar-link" href="<?= base_url('admin/profile') ?>">
							<i class="align-middle" data-feather="user"></i> <span class="align-middle">Profile</span>
						</a>
					</li>

					<li class="sidebar-item mb-4 <?= active_menu('profile') ?>">
						<a class="sidebar-link" href="<?= base_url('login/logout') ?>">
							<i class="align-middle" data-feather="log-out"></i> <span class="align-middle">Log out</span>
						</a>
					</li>

				</ul>
			</div>
		</nav>

		<div class="main">
			<nav class="navbar navbar-expand navbar-light navbar-bg">
				<a class="sidebar-toggle js-sidebar-toggle">
					<i class="hamburger align-self-center"></i>
				</a>

				<div class="navbar-collapse collapse">
					<ul class="navbar-nav navbar-align">
						<li class="nav-item dropdown">
							<a class="nav-icon dropdown-toggle d-inline-block d-sm-none" href="#" data-bs-toggle="dropdown">
								<i class="align-middle" data-feather="settings"></i>
							</a>

							<a class="nav-link dropdown-toggle d-none d-sm-inline-block" href="#" data-bs-toggle="dropdown">
								<img src="<?= base_url() ?>assets/uploads/profile/<?= $this->session->image; ?>" style="object-fit: cover" class="avatar img-fluid rounded me-1" alt="<?= $this->session->name ?>" /> <span class="text-dark"><?= $this->session->name ?> 
									<?php if($this->session->level == 0): ?>
										(<em>Superadmin</em>)
									<?php elseif($this->session->level == 1): ?>
										(<em>Supervisor</em>)
									<?php elseif($this->session->level == 2): ?>
										(<em>Warehouse</em>)
									<?php elseif($this->session->level == 3): ?>
										(<em>Production</em>)
									<?php elseif($this->session->level == 4): ?>
										(<em>Approval</em>)
									<?php endif; ?>
								</span>
							</a>
							<div class="dropdown-menu dropdown-menu-end">
								<a class="dropdown-item" href="<?= base_url('admin/profile') ?>"><i class="align-middle me-1" data-feather="user"></i> Profile</a>
								<!--<a class="dropdown-item" href="#"><i class="align-middle me-1"-->
								<!--		data-feather="pie-chart"></i> Analytics</a>-->
								<!--<div class="dropdown-divider"></div>-->
								<!--<a class="dropdown-item" href="index.html"><i class="align-middle me-1"-->
								<!--		data-feather="settings"></i> Settings & Privacy</a>-->
								<!--<a class="dropdown-item" href="#"><i class="align-middle me-1"-->
								<!--		data-feather="help-circle"></i> Help Center</a>-->
								<div class="dropdown-divider"></div>
								<a class="dropdown-item" href="<?= base_url('login/logout') ?>">Log out</a>
							</div>
						</li>
					</ul>
				</div>
			</nav>
