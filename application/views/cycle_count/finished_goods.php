<main class="content">
	<div class="container-fluid p-0">

		<h1 class="h3 mb-3">Stock Material</h1>

		<div class="row">
			<div class="col-12 col-xl-12">
				<div class="card">
					<div class="card-header">
						<h5 class="card-title">Stock Material
							
						</h5>
						<h6 class="card-subtitle text-muted">List data Stock Material</h6>
					</div>
					<div class="card-body">
					<?= $this->session->flashdata('msg') ?>
					<table class="table table-striped dataTable">
						<thead>
							<tr>
								<th>No.</th>
								<th>Material No.</th>
								<th>Material Code</th>
								<th>Material Name</th>
								<th>Stock</th>
								<th>Created at</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$no = 1;
							foreach ($api as $row) { ?>
								<tr>
									<td><?= $no++; ?></td>
									<td><?= $row->material_id ?></td>
									<td>
										<?php foreach($material as $c){ 
											if($row->material_id  == $c->id){
												echo $c->material_code;
											} else{
												echo "";
											}
										 }  
										?>	
									</td>
									<td>
										<?php foreach($material as $c){ 
											if($row->material_id  == $c->id){
												echo $c->material_name;
											} else{
												echo "";
											}
										 }  
										?>	
									</td>
									<td><?= $row->quantity.' g' ?></td>
									<td><?= $row->created_at ?></td>
									<td>
										<a href="#!" data-bs-toggle="modal" data-bs-target="#detailModal" data-id="<?= $row->id ?>"><i data-feather="eye"></i></a>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
					</div>
				</div>
			</div>

		</div>

	</div>
</main>

<div class="modal fade" id="detailModal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Detail Stock Material</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div id="edit_data"></div>
		</div>
	</div>
</div>

<div class="modal fade" id="historyModal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">History Cycle Count</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div id="history_data"></div>
		</div>
	</div>
</div>

<script>

	$("#detailModal").on('shown.bs.modal', function(e) {
		var id = $(e.relatedTarget).data('id');
		$.ajax({
			type: "POST",
			url: "<?= base_url() ?>admin/FG_Items/get",
			data: {
				id: id
			}
		}).done(function(response) {
			console.log(response);
			$("#edit_data").html(response);
		});
	});

	$("#historyModal").on('shown.bs.modal', function(e) {
		var id = $(e.relatedTarget).data('id');
		$.ajax({
			type: "POST",
			url: "<?= base_url() ?>admin/cycle_count/history",
			data: {
				id: id
			}
		}).done(function(response) {
			$("#history_data").html(response);
		});
	});

</script>
