	<div class="modal-body m-3">
        <div class="col-md-12">
            <table class="table table-bordered table-inverse table-responsive overflow-auto">
                <thead class="thead-default">
                    <tr>
                        <!-- <th>#</th> -->
                        <th>Matrial Code</th>
                        <th>Material Name</th>
                        <th>Cas Num</th>
                        <th>Inci Name</th>
                        <th>Price</th>
                        <th>Category</th>
                        <th>Halal</th>
                        <th>Stock Minimum</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><?= $api_detail != null ? $api_detail->material_code : "-"?></td>
                        <td><?= $api_detail != null ? $api_detail->material_name : "-"?></td>
                        <td><?= $api_detail != null ? $api_detail->cas_num : "-"?></td>
                        <td><?= $api_detail != null ? $api_detail->inci_name : "-"?></td>
                        <td><?= $api_detail != null ? $api_detail->price : "-"?></td>
                        <td><?= $api_detail != null ? $api_detail->category : "-"?></td>
                        <td>
                            <?php if($api_detail != null && $api_detail->halal == 1): ?>
                                <span class="badge bg-success">Halal</span>
                            <?php elseif($api_detail != null && $api_detail->halal != 1): ?>
                                <span class="badge bg-warning">Non halal</span>
                            <?php else: ?>
                                <span>-</span>
                            <?php endif; ?>
                        </td>
                        <td><?= $api_detail->stock_minimum ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
	</div>
