<main class="content">
	<div class="container-fluid p-0">

		<h1 class="h3 mb-3">Material Items</h1>

		<div class="row">
			<div class="col-12 col-xl-12">
				<div class="card">
					<div class="card-header">
						<h5 class="card-title">Material Items
							<!-- <?= var_dump($api) ?> -->
						</h5>
						<h6 class="card-subtitle text-muted">List data Material Items</h6>
						<!-- <?= $api ?> -->
					</div>
					<div class="card-body">
					<?= $this->session->flashdata('msg') ?>
					<table class="table table-striped dataTable responsive">
						<thead>
							<tr>
								<th>No.</th>
								<th>Material Code</th>
								<th>Material Name</th>
								<th>Inci Name</th>
								<th>Category</th>
								<th>Halal</th>
								<th>Quantity</th>
								<th>Stock Minimum</th>
								<th>Created at</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$no = 1;
							foreach ($api as $row) { ?>
								<tr>
									<td><?= $no++; ?></td>
									<td><?= $row->material_code ?></td>
									<td><?= $row->material_name ?></td>
									<td><?= $row->inci_name ?></td>
									<td><?= $row->category ?></td>
									<td>
										<?php if($row != null && $row->halal == 1): ?>
											<span class="badge bg-success">Halal</span>
										<?php elseif($row != null && $row->halal != 1): ?>
											<span class="badge bg-warning">Non halal</span>
										<?php else: ?>
											<span>-</span>
										<?php endif; ?>
									</td>
									<td>
										<?php 
										foreach ($stock as $s) {
										 if($row->id == $s->material_id){
											echo $s->quantity." g";
										 }
										}
										?>
									</td>
									<td><?= $row->stock_minimum ?></td>
									<td><?= $row->created_at ?></td>
									<td class="table-action">
										<a href="#!" data-bs-toggle="modal" data-bs-target="#detailModal" data-id="<?= $row->id ?>"><i data-feather="eye"></i></a>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
					</div>
				</div>
			</div>

		</div>

	</div>
</main>

<div class="modal fade" id="detailModal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Detail Material Items</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div id="detail_modal"></div>
		</div>
	</div>
</div>

<script>
	$("#detailModal").on('shown.bs.modal', function(e) {
		var id = $(e.relatedTarget).data('id');
		$.ajax({
			type: "POST",
			url: "<?= base_url() ?>admin/cycle_count/get",
			data: {
				id: id
			}
		}).done(function(response) {
			console.log(response);
			$("#detail_modal").html(response);
		});
	});

</script>
