	<div class="modal-body m-3">
        <div class="col-md-12">
            <table class="table table-bordered table-inverse table-responsive overflow-auto">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Material No.</th>
                        <th>Material Code</th>
                        <th>Material Name</th>
                        <th>Inci Name</th>
                        <th>Category</th>
                        <th>Price</th>
                        <th>Halal</th>
                        <th>Stock</th>
                        <th>Created at</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no = 1;
                    ?>
                        <tr>
                            <td><?= $no++; ?></td>
                            <td><?= $api->material_id ?></td>
                            <td>
                                <?php foreach($material as $c){ 
                                    if($api->material_id  == $c->id){
                                        echo $c->material_code;
                                    } else{
                                        echo "";
                                    }
                                 }  
                                ?>  
                            </td>
                            <td>
                                <?php foreach($material as $c){ 
                                    if($api->material_id  == $c->id){
                                        echo $c->material_name;
                                    } else{
                                        echo "";
                                    }
                                 }  
                                ?>  
                            </td>
                            <td>
                                <?php foreach($material as $c){ 
                                    if($api->material_id  == $c->id){
                                        echo $c->inci_name;
                                    } else{
                                        echo "";
                                    }
                                 }  
                                ?>  
                            </td>
                            <td>
                                <?php foreach($material as $c){ 
                                    if($api->material_id  == $c->id){
                                        echo $c->category;
                                    } else{
                                        echo "";
                                    }
                                 }  
                                ?>  
                            </td>
                            <td>
                                <?php foreach($material as $c){ 
                                    if($api->material_id  == $c->id){
                                        echo 'Rp '.$c->price;
                                    } else{
                                        echo "";
                                    }
                                 }  
                                ?>  
                            </td>
                            <td>
                                <?php foreach($material as $c){ 
                                    if($api->material_id  == $c->id){
                                ?>
                                <?php if($c != null && $c->halal == 1): ?>
                                    <span class="badge bg-success">Halal</span>
                                <?php elseif($c != null && $c->halal != 1): ?>
                                    <span class="badge bg-warning">Non halal</span>
                                <?php else: ?>
                                    <span>-</span>
                                <?php endif; ?>
                                <?php
                                    } else{
                                        echo "";
                                    }
                                 }  
                                ?>  
                            </td>
                            <td><?= $api->quantity ?></td>
                            <td><?= $api->created_at ?></td>
                        </tr>
                </tbody>
            </table>
        </div>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
	</div>
