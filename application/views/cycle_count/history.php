<form action="<?= base_url() ?>admin/cycle_count/edit/<?= $get->stock_id ?>" method="POST">
	<div class="modal-body m-3">
		<div class="mb-3">
			<label class="form-label">Material Name</label> <br>
			<b><?= $get->nama ?></b>
		</div>

		<div class="mb-3">
			<label class="form-label">Cas No.</label> <br>
			<b><?= $get->cas_num ?></b>
		</div>

		<div class="mb-3">
			<label class="form-label">Minimum Stock</label> <br>
			<b><?= "$get->stok_minimum $get->unit" ?></b>
		</div>

		<div class="mb-3">
			<label class="form-label">Kategori</label> <br>
			<b><?= "$get->kategori" ?></b>
		</div>		

		<div class="mb-3">
			<label class="form-label">Halal</label> <br>
			<?php if($get->status == 'halal'): ?>
				<span class="badge bg-success">HALAL</span>
			<?php else: ?>
				<span class="badge bg-danger">HARAM</span>
			<?php endif; ?>
		</div>

		<div class="mb-3">
			<label class="form-label">Stock</label> <br>
			<b><?= "$get->stok $get->unit" ?></b>
		</div>

		<div class="overflow-auto" style="height: 350px;">
		<table class="table table-bordered table-inverse table-responsive">
			<thead class="thead-default">
				<tr>
					<th>#</th>
					<th>Date</th>
					<th>Status</th>
					<th>Previous Qty</th>
					<th>Qty</th>
					<th>Current Qty</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				$no = 1;
				foreach($history as $row): ?>
				<tr>
					<td scope="row"><?= $no++ ?></td>
					<td><?= date('d M Y', strtotime($row->created_at)) ?></td>
					<td>
						<?php if($row->type == 'in'): ?>
							<span class="badge bg-success">IN</span>
						<?php else: ?>
							<span class="badge bg-danger">OUT</span>
						<?php endif; ?>
					</td>

					<td><?= $row->previous_qty ?></td>
					<td><?= $row->total ?></td>
					<td><?= $row->current_qty ?></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
		</div>
	</div>

	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
	</div>
</form>
