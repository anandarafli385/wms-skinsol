<form action="<?= base_url() ?>admin/approval/edit_approval/<?= $get['work_order_id'] ?>" method="post">
<div class="modal-body m-3">
	<input type="hidden" name="work_order_id" value="<?= $get['work_order_id'] ?>">
	<div class="mb-3">
        <label class="form-label">Work Order No.</label><br>
        <b><?= $get['no_work_order'] ?></b>
    </div>

	<div class="mb-3">
        <label class="form-label">Date</label><br>
        <b><?= dateID($get['date']) ?></b>
    </div>

	<div class="form-group mb-3">
	  <label for="status_approval">Status Approval Work Order</label>
	  <select class="form-control" name="status_approval">
		<option value="approved" selected>Approved</option>
	  </select>
	</div>
	
    <!-- <div class="mb-3 mt-3">
        <label class="form-label">FG List</label>
        <div class="table-responsive">
            <table class="table table-sm table-striped">
                <thead>
                    <tr>
                        <th>Material</th>
                        <th>Description</th>
                        <th>Qty Total</th>
						<th>Qty Left</th>
                        <th>Unit</th>
						<th>Good</th>
						<th>Reject</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
					$data_no = 0;
					foreach ($detail as $index => $row) { 

						$man_sql = "SELECT sum(good) as good, sum(reject) as reject FROM `tbl_work_order_delivery` where work_order_detail_id = '". $data_wo[$index]['work_order_detail_id'] . "'";

						$delivery = $this->sql->manual_query($man_sql)->row_array();

						if(isset($delivery)){
							$qty_left = $qty[$index] - ($delivery['good'] + $delivery['reject']);
						}else{
							$qty_left = $qty[$index];
						}

						?>
                        <tr>
                            <td><?= $row['nama'] ?></td>
                            <td><?= $row['deskripsi'] ?></td>
                            <td><?= $qty[$index] ?></td>
                            <td><?= $qty_left  ?></td>
                            <td><?= $row['unit'] ?></td>
							<td>
								<input type="hidden" class="form-control" value="<?= $data_wo[$index]['work_order_detail_id'] ?>" name="work_order_detail_id[]">
								<input type="hidden" class="form-control" value="<?= $qty[$index] ?>" name="qty[]">
								<input type="number" name="good[]" required placeholder="Good" data-number="<?= $data_no ?>" class="form-control good" max="<?= $qty_left ?>" min="0">
							</td>
							<td> -->
								<!-- <span class="text-reject-<?= $data_no ?>">0</span> -->
								<!-- <input type="number" name="reject[]" required placeholder="Reject" data-number="<?= $data_no ?>" class="form-control reject" max="<?= $qty_left ?>" value="0" min="0">

							</td>
                        </tr>
                    <?php 
						$data_no++;
					} ?>
                </tbody>
            </table>
        </div>
    </div> -->
</div>
<div class="modal-footer">
	<button type="submit" class="btn btn-primary">Update</button>
    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
</div>
</form>

<script>
	$(".good").keyup(function () {
		val = $(this).val()
		no = $(this).data('number')
		reject = $(this).attr('max') - val
		console.log(no)

		$('.text-reject-'+no).text(reject)
		if(reject == 0){
			$('.text-reject-'+no).css({
				'color':'black'
			})
		}else{
			$('.text-reject-'+no).css({
				'color':'red'
			})
		}
	});

	$(".good").change(function () {
		val = $(this).val()
		no = $(this).data('number')
		reject = $(this).attr('max') - val
		console.log(no)

		$('.text-reject-'+no).text(reject)
		if(reject == 0){
			$('.text-reject-'+no).css({
				'color':'black'
			})
		}else{
			$('.text-reject-'+no).css({
				'color':'red'
			})
		}
	});
</script>
