<main class="content">
	<div class="container-fluid p-0">

		<h1 class="h3 mb-3">Sales Order</h1>

		<div class="row">
			<div class="col-12 col-xl-12">
				<div class="card">
					<div class="card-header">
						<h5 class="card-title">Sales Order
						</h5>
						<h6 class="card-subtitle text-muted">List data Sales Order
						</h6>
					</div>
					<div class="card-body">
						<?= $this->session->flashdata('msg') ?>
						<table class="table table-striped dataTable">
							<thead>
								<tr>
									<th>#</th>
									<th>Sales order date</th>
									<th>Sales order en</th>
									<th>PO no.</th>
									<th>Keterangan</th>
									<th>Customer Code</th>
									<th>Customer Name</th>
									<th>Customer Brand</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$no = 1;
									foreach($get as $row):
								?>
								<tr>
									<td><?= $no++ ?></td>
									<td><?= date($row->date) ?></td>
									<td><?= date($row->date_end) ?></td>
									<td><?= $row->po_num ?></td>
									<td><?= $row->keterangan != null ? $row->keterangan : "-" ?></td>
									<td>
										<?php foreach($cust as $c){ 
											if($row->customer_id == $c->id){
												echo $c->customer_code;
											} else{
												echo "";
											}
										 }  
										?>
									</td>
									<td>
										<?php foreach($cust as $c){ 
											if($row->customer_id == $c->id){
												echo $c->customer_name;
											} else{
												echo "";
											}
										 }  
										?>
									</td>
									<td>
										<?php foreach($cust as $c){ 
											if($row->customer_id == $c->id){
												echo $c->customer_brand;
											} else{
												echo "";
											}
										 }  
										?>
									</td>
									<td class="table-action">
										<a href="#!" data-bs-toggle="modal" data-bs-target="#detailModal"
											data-id="<?= $row->id ?>"><i data-feather="eye"></i></a>
									</td>
								</tr>

								<?php
									endforeach;
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>

		</div>

	</div>
</main>

<div class="modal fade" id="detailModal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Detail Data Sales Order</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div id="detail_data">

			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="editModal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Edit Data Sales Order</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div id="edit_data">

			</div>
		</div>
	</div>
</div>

<script>
	$("#detailModal").on('shown.bs.modal', function (e) {
		var id = $(e.relatedTarget).data('id');
		$.ajax({
			type: "POST",
			url: "<?= base_url() ?>admin/shipping/detail",
			data: {
				id: id
			}
		}).done(function (response) {
			$("#detail_data").html(response);
		});
	});


	$("#editModal").on('shown.bs.modal', function (e) {
		var id = $(e.relatedTarget).data('id');
		$.ajax({
			type: "POST",
			url: "<?= base_url() ?>admin/shipping/get",
			data: {
				id: id
			}
		}).done(function (response) {
			$("#edit_data").html(response);
		});
	});

	function delete_data(id) {
		Swal.fire({
			title: 'Konfirmasi ?',
			text: "Apakah kamu yakin ?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#28a745',
			cancelButtonColor: '#dc3545',
			confirmButtonText: 'Yes!',
			cancelButtonText: 'No!',
			showLoaderOnConfirm: true,
			preConfirm: function () {
				return new Promise(function (resolve, reject) {
					$.ajax({
						type: 'POST',
						url: "<?= base_url() ?>admin/shipping/delete",
						data: {
							id: id
						}
					}).done(function (msg) {
						if (msg == "ok") {
							swal.fire("OK!", "Data berhasil dihapus!", "success").then(
								function () {
									location.reload();
								})
						} else {
							swal.fire("Gagal!", msg, "error").then(function () {
								location.reload();
							})
						}
					})
				})
			},
			allowOutsideClick: () => !Swal.isLoading()
		})
	}

	let index = 0;

	function tambah_fg(){
		let raw_html = `
		<div class="row mb-3" id="row_`+index+`">
			<div class="col-9">
				<select name="fg_id[]" class="form-control select2"
					required>
					<option value="">- Select One -</option>
					<?php foreach ($fg as $row) { ?>
						<option value="<?= $row['material_id'] ?>" data-qty="<?= $row['stok'] ?>">
							<?= $row['material_no']  . ' - ' . $row['nama'] ?></option>
					<?php } ?>
				</select>
			</div>
			<div class="col-2">
				<input type="text" class="form-control form-control-lg" name="qty[]" placeholder="Quantity" required>

			</div>
			<div class="col-1">
				<button type="button" onclick="hapus_fg(`+index+`)"
					class="btn btn-danger btn-block"><i class="fa fa-times-circle"
						aria-hidden="true"></i></button>
			</div>
		</div>
		`;

		$('#row_putaway_fg').append(raw_html);

		$(".select2").select2({
			theme: 'bootstrap-5',
			dropdownParent: $("#addModal"),
			width: 'auto'
		});

		index++;

	}

	function hapus_fg(no){
		console.log(no)
		$('#row_'+no).remove()
	}
</script>
