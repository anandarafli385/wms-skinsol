<div class="modal-body m-3">
	<div class="row">
		<div class="col-6">
			<div class="mb-3">
				<label class="form-label">Sales Order ID</label><br>
				<b><?= $api->id ?></b>
			</div>

			<div class="mb-3">
				<label class="form-label">Sales Order Date</label><br>
				<b><?= dateID($api->date) ?> - <?= dateID($api->date_end) ?></b>
			</div>

			<div class="mb-3">
				<label class="form-label">PO No.</label><br>
				<b><?= $api->po_num ?></b>
			</div>

			<div class="mb-3">
				<label class="form-label">Sales Order Desc</label><br>
				<b><?= $api->keterangan != null ? $api->keterangan : "-" ?></b>
			</div>

			<div class="mb-3">
				<label class="form-label">Customer Code</label><br>
				<b><?= $api->customer != null ?$api->customer->customer_code : "-"?></b>
			</div>

			<div class="mb-3">
				<label class="form-label">Customer Name</label><br>
				<b><?= $api->customer != null ?$api->customer->customer_name : "-"?></b>
			</div>
		</div>
		<div class="col-6">
			<div class="mb-3">
				<label class="form-label">Customer Brand</label><br>
				<b><?= $api->customer != null ?$api->customer->customer_brand : "-"?></b>
			</div>

			<div class="mb-3">
				<label class="form-label">Customer Mobile</label><br>
				<b><?= $api->customer != null ?$api->customer->customer_mobile : "-"?></b>
			</div>

			<div class="mb-3">
				<label class="form-label">Customer Address</label><br>
				<b><?= $api->customer != null ?$api->customer->customer_address : "-"?></b>
			</div>

			<div class="mb-3">
				<label class="form-label">Created At</label><br>
				<b><?= $api->created_at ?></b>
			</div>
		</div>
	</div>



	<!-- <div class="mb-3">
		<label class="form-label">Finished Good List</label>
		<div class="table-responsive">
			<table class="table table-sm table-striped dataTable">
				<thead>
					<tr>
						<th>Item No.</th>
						<th>Item Name</th>
						<th>Type</th>
						<th>Qty</th>
						<th>Unit</th>
						<th>Price</th>
						<th>Discount</th>
						<th>Total Price</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($fg as $row) { ?>
					<tr>
						<td><?= $row['material_no'] ?></td>
						<td><?= $row['nama'] ?></td>
						<td><?= $row['inventory_part_type'] ?></td>
						<td><?= $row['qty'] ?></td>
						<td><?= $row['unit'] ?></td>
						<td><?= 'Rp. ' . number_format($row['price'], 0, ',', '.') ?></td>
						<td><?= $row['discount'] . ' %' ?></td>
						<td><?= 'Rp. ' . number_format($row['total'], 0, ',', '.') ?></td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div> -->
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
</div>
