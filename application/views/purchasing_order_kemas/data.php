<main class="content">
	<div class="container-fluid p-0">

		<h1 class="h3 mb-3">Purchasing Order Kemas</h1>
		<div class="row">
			<div class="col-12 col-xl-12">
				<div class="card">
					<div class="card-header">
						<h5 class="card-title">Purchasing Order Kemas
						</h5>
						<h6 class="card-subtitle text-muted">List data Purchasing Order Kemas</h6>
					</div>
					<div class="card-body">
					<?= $this->session->flashdata('msg') ?>
					<table class="table table-striped dataTable responsive">
						<thead>
							<tr>
								<th>No.</th>
								<th>Purchasing Order No.</th>
								<th>Purchasing Order date</th>
								<th>Receipt Code</th>
								<th>Term</th>
								<th>Status</th>
								<th>Address</th>
								<th>Created at</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$no = 1;
							foreach ($api as $row) { ?>
								<tr>
									<td><?= $no++; ?></td>
									<td><?= $row->po_num ?></td>
									<td><?= $row->date ?></td>
									<td><?= $row->supplier->supplier_name ?></td>
									<td><?= $row->terms ?></td>
									<td><?= $row->status ?></td>
									<td><?= $row->supplier->supplier_address ?></td>
									<td><?= $row->created_at ?></td>
									<td class="text-center">
										<a href="#!" data-bs-toggle="modal" data-bs-target="#detailModal" data-id="<?= $row->id ?>"><i data-feather="eye"></i></a>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
					</div>
				</div>
			</div>

		</div>

	</div>
</main>

<div class="modal fade" id="detailModal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Detail Data Purchase Order Kemas</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div id="detail_data"></div>
		</div>
	</div>
</div>
<script>
	$("#detailModal").on('shown.bs.modal', function(e) {
		var id = $(e.relatedTarget).data('id');
		$.ajax({
			type: "POST",
			url: "<?= base_url() ?>admin/purchasing_order_kemas/detail",
			data: {
				id: id
			}
		}).done(function(response) {
			$("#detail_data").html(response);
		});
	});
</script>
