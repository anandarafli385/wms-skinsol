<form action="<?= base_url() ?>admin/master_material/edit/<?= $get['material_id'] ?>" method="POST" enctype="multipart/form-data">
    <div class="modal-body m-3">
        <div class="mb-3">
            <label class="form-label">Material <span class="text-danger">*</span></label>
            <input type="text" class="form-control" name="nama" placeholder="Material" value="<?= $get['nama'] ?>" required>
        </div>
		<div class="mb-3">
            <label class="form-label">Kode <span class="text-danger">*</span></label>
            <input type="text" class="form-control" name="kode_material" placeholder="kode_material" value="<?= $get['kode_material'] ?>" required maxlength="3">
        </div>
        <div class="mb-3">
            <label class="form-label">Price <span class="text-danger">*</span></label>
            <input type="number" class="form-control" name="harga" placeholder="Price" value="<?= $get['harga'] ?>" required>
        </div>
        <div class="mb-3">
            <label class="form-label">Unit <span class="text-danger">*</span></label>
            <input type="text" class="form-control" name="unit" placeholder="Unit" value="<?= $get['unit'] ?>" required>
        </div>
        <div class="mb-3">
            <label class="form-label">Status <span class="text-danger">*</span></label>
            <select name="status" class="form-control">
                <option value="halal" <?= $get['status'] == 'halal' ? 'selected' : null ?>>Halal</option>
                <option value="non halal" <?= $get['status'] == 'non halal' ? 'selected' : null ?>>Non Halal</option>
            </select>
        </div>
        <div class="mb-3">
            <label class="form-label">Material type <span class="text-danger">*</span></label>
            <input type="text" class="form-control" name="material_type" placeholder="Material Type" value="<?= $get['material_type'] ?>" required>
        </div>
        <div class="mb-3">
            <label class="form-label">Inventory Part Type <span class="text-danger">*</span></label>
            <select name="inventory_part_type" class="form-control">
                <option value="Raw Material" <?= $get['status'] == 'Raw Material' ? 'selected' : null ?>>Raw Material</option>
                <option value="Subsidiary Material" <?= $get['status'] == 'Subsidiary Material' ? 'selected' : null ?>>Subsidiary Material</option>
                <option value="Work in Process" <?= $get['status'] == 'Work in Process' ? 'selected' : null ?>>Work in Process</option>
                <option value="Other Material" <?= $get['status'] == 'Other Material' ? 'selected' : null ?>>Other Material</option>
                <option value="Finished Goods" <?= $get['status'] == 'Finished Goods' ? 'selected' : null ?>>Finished Goods</option>
            </select>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
    </div>
</form>
