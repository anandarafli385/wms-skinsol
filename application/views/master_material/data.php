<main class="content">
	<div class="container-fluid p-0">

		<h1 class="h3 mb-3">Master Material</h1>

		<div class="row">
			<div class="col-12 col-xl-12">
				<div class="card">
					<div class="card-header">
						<h5 class="card-title">Master Material
						</h5>
						<h6 class="card-subtitle text-muted">List data master material
						</h6>
					</div>
					<div class="card-body">
						<?= $this->session->flashdata('msg') ?>
						<table class="table table-striped dataTable">
							<thead>
								<tr>
									<th>No.</th>
									<th>Material No.</th>
									<th>Material</th>
									<th>Inci Name</th>
									<th>Price</th>
									<th>Stock</th>
									<th>Category</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
								<?php
                            $no = 1;
                            foreach ($api as $row) { ?>
								<tr>
									<td><?= $no++; ?></td>
									<td><?= $row->material != null ? $row->material->material_code : "-" ?></td>
									<td><?= $row->material != null ? $row->material->material_name : "-" ?></td>
									<td><?= $row->material != null ? $row->material->inci_name : "-" ?></td>
									<td><?= $row->material != null ? $row->material->price : "-" ?></td>
									<td><?= $row->quantity != null ? $row->quantity  : "-" ?></td>
									<td><?= $row->material != null ? $row->material->category : "-" ?></td>
									<td>
										<?php if($row->material != null && $row->material->halal == 1): ?>
											<span class="badge bg-success">Halal</span>
										<?php elseif($row->material != null && $row->material->halal != 1): ?>
											<span class="badge bg-warning">Non halal</span>
										<?php else: ?>
											<span>-</span>
										<?php endif; ?>
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>

		</div>

	</div>
</main>
<div class="modal fade" id="addModal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Add Data Material</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<form action="<?= base_url() ?>admin/master_material/add" method="POST" enctype="multipart/form-data">
				<div class="modal-body m-3">
					<div class="mb-3">
						<label class="form-label">Material Id<span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="material_id" placeholder="Material" required>
					</div>
					<div class="form-group mb-3">
					  <label for="">Material Code <span class="text-danger">*</span></label>
					  <input type="text" name="kode_material" maxlength="3" class="form-control" placeholder="Material Code" aria-describedby="helpId">
					  <small id="helpId" class="text-muted">Material code should be unique to identify specific material.</small>
					</div>
					<div class="mb-3">
						<label class="form-label">Material No<span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="material_no" placeholder="Material" required>
					</div>
					<div class="mb-3">
						<label class="form-label">Cas Num<span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="cas_num" placeholder="Material" required>
					</div>
					<div class="mb-3">
						<label class="form-label">Material Name<span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="nama" placeholder="Material" required>
					</div>
					<div class="mb-3">
						<label class="form-label">Inci Name</label>
						<input type="text" class="form-control" name="inci" placeholder="Description">
					</div>
					<div class="mb-3">
						<label class="form-label">Supplier <span class="text-danger">*</span></label>
						<select name="supplier_id" class="form-control">
							<option value="">- Select One -</option>
							<?php foreach ($supp as $s): ?>
							<option value="<?= $s['supplier_id'] ?>"><?= $s['supplier_id'] ?> - <?= $s['nama'] ?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="mb-3">
						<label class="form-label">Stock Minimum <span class="text-danger">*</span></label>
						<input type="number" class="form-control" name="stok_minimum" placeholder="Price" required>
					</div>
					<div class="mb-3">
						<label class="form-label">Kategori <span class="text-danger">*</span></label>
						<input type="number" class="form-control" name="kategori" placeholder="Price" required>
					</div>
					<div class="mb-3">
						<label class="form-label">Price <span class="text-danger">*</span></label>
						<input type="number" class="form-control" name="harga" placeholder="Price" required>
					</div>
					<div class="mb-3">
						<label class="form-label">Unit <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="unit" placeholder="Unit" required>
					</div>
					<div class="mb-3">
						<label class="form-label">Status <span class="text-danger">*</span></label>
						<select name="status" class="form-control">
							<option value="halal">Halal</option>
							<option value="non halal">Non Halal</option>
						</select>
					</div>
<!-- 					<div class="mb-3">
						<label class="form-label">Material type <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="material_type" placeholder="Material Type" required>
					</div>
					<div class="mb-3">
						<label class="form-label">Inventory Part Type <span class="text-danger">*</span></label>
						<select name="inventory_part_type" class="form-control">
							<option value="Raw Material">Raw Material</option>
							<option value="Subsidiary Material">Subsidiary Material</option>
							<option value="Work in Process">Work in Process</option>
							<option value="Other Material">Other Material</option>
							<option value="Finished Goods">Finished Goods</option>
						</select>
					</div> -->
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Save changes</button>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="modal fade" id="editModal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Edit Data Material</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div id="edit_data"></div>
		</div>
	</div>
</div>


<!-- Modal -->
<div class="modal fade" id="modalExcel" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Import Master Material from Excel</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<form action="<?= base_url() . 'admin/master_material/upload_excel' ?>" method="post" enctype="multipart/form-data">
			<div class="modal-body">
				<div class="form-group">
				  <label for="">Import File Excel</label> <br>
				  <input id="fileSelect" name="upload_file" type="file" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" />  
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Save</button>
			</div>
			</form>
		</div>
	</div>
</div>

<script>
	$("#editModal").on('shown.bs.modal', function (e) {
		var id = $(e.relatedTarget).data('id');
		$.ajax({
			type: "POST",
			url: "<?= base_url() ?>admin/master_material/get",
			data: {
				id: id
			}
		}).done(function (response) {
			$("#edit_data").html(response);
		});
	});

	function delete_data(id) {
		Swal.fire({
			title: 'Konfirmasi ?',
			text: "Apakah kamu yakin ?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#28a745',
			cancelButtonColor: '#dc3545',
			confirmButtonText: 'Yes!',
			cancelButtonText: 'No!',
			showLoaderOnConfirm: true,
			preConfirm: function () {
				return new Promise(function (resolve, reject) {
					$.ajax({
						type: 'POST',
						url: "<?= base_url() ?>admin/master_material/delete",
						data: {
							id: id
						}
					}).done(function (msg) {
						if (msg == "ok") {
							swal.fire("OK!", "Data berhasil dihapus!", "success").then(
								function () {
									location.reload();
								})
						} else {
							swal.fire("Gagal!", msg, "error").then(function () {
								location.reload();
							})
						}
					})
				})
			},
			allowOutsideClick: () => !Swal.isLoading()
		})
	}

</script>
