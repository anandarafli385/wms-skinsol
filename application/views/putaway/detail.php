<div class="modal-body m-3">
    <div class="row">
        <div class="col-md-4">
            <div class="mb-3">
                <label class="form-label">Puchase Order No.</label><br>
                <b><?= $get->purchase_id ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">Material No.</label><br>
                <b><?= $get->material_id ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">Batch No.</label><br>
                <b><?= $get->batch_num ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">Expired Date</label><br>
                <b><?= $get->expired_date ?></b>
            </div>
        </div>
        <div class="col-md-4">
            <div class="mb-3">
                <label class="form-label">GR Date</label><br>
                <b><?= $get->purchase->date ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">Purchase No.</label><br>
                <b><?= $get->purchase->purchase_num ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">Delivery Order No.</label><br>
                <b><?= $get->purchase->delivery_orders_num ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">PO Material ID</label><br>
                <b><?= $get->purchase->po_material_id ?></b>
            </div>
        </div>
            <?php if($get->purchase->po_material != null){ ?>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-6">
                        <div class="mb-3">
                            <label class="form-label">PO Material No.</label><br>
                            <b><?= $get->purchase->po_material->po_num ?></b>
                        </div>
                        <div class="mb-3">
                            <label class="form-label">PO Material Date</label><br>
                            <b><?= $get->purchase->po_material->po_date ?></b>
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Currency</label><br>
                            <b><?= $get->purchase->po_material->currency ?></b>
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Terms</label><br>
                            <b><?= $get->purchase->po_material->terms ?></b>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="mb-3">
                            <label class="form-label">Status</label><br>
                            <b><?= $get->purchase->po_material->status ?></b>
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Supplier Code</label><br>
                            <b><?= $get->purchase->po_material->supplier->supplier_code ?></b>
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Supplier Name</label><br>
                            <b><?= $get->purchase->po_material->supplier->supplier_name ?></b>
                        </div>
                    </div>
                </div>
            </div>
            <?php } else { ?>
                <div class="col-md-4"></div>
            <?php } ?>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered table-inverse table-responsive overflow-auto">
                    <thead class="thead-default">
                        <tr>
                            <th>Material No.</th>
                            <th>Matrial Code</th>
                            <th>Material Name</th>
                            <th>Stock Minimum</th>
                            <th>Category</th>
                            <th>Halal</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><?= $get->material->id ?></td>
                            <td><?= $get->material->material_code ?></td>
                            <td><?= $get->material->material_name ?></td>
                            <td><?= $get->material->stock_minimum?></td>
                            <td><?= $get->material->category?></td>
                            <td>
                                <?php if($get->material != null && $get->material->halal == 1): ?>
                                    <span class="badge bg-success">Halal</span>
                                <?php elseif($get->material != null && $get->material->halal != 1): ?>
                                    <span class="badge bg-warning">Non halal</span>
                                <?php else: ?>
                                    <span>-</span>
                                <?php endif; ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
</div>
