<div class="modal-body m-3">
	<div class="row">
		<div class="col-6">
			<div class="mb-3">
				<label class="form-label">Sales Order ID</label><br>
				<b><?= $api->id ?></b>
			</div>

			<div class="mb-3">
				<label class="form-label">Sales Order Date</label><br>
				<b><?= dateID($api->date) ?> - <?= dateID($api->date_end) ?></b>
			</div>

			<div class="mb-3">
				<label class="form-label">PO No.</label><br>
				<b><?= $api->po_num ?></b>
			</div>

			<div class="mb-3">
				<label class="form-label">Sales Order Desc</label><br>
				<b><?= $api->keterangan != null ? $api->keterangan : "-" ?></b>
			</div>

			<div class="mb-3">
				<label class="form-label">Customer Code</label><br>
				<b><?= $api->customer != null ?$api->customer->customer_code : "-"?></b>
			</div>

			<div class="mb-3">
				<label class="form-label">Customer Name</label><br>
				<b><?= $api->customer != null ?$api->customer->customer_name : "-"?></b>
			</div>
		</div>
		<div class="col-6">
			<div class="mb-3">
				<label class="form-label">Customer Brand</label><br>
				<b><?= $api->customer != null ?$api->customer->customer_brand : "-"?></b>
			</div>

			<div class="mb-3">
				<label class="form-label">Customer Mobile</label><br>
				<b><?= $api->customer != null ?$api->customer->customer_mobile : "-"?></b>
			</div>

			<div class="mb-3">
				<label class="form-label">Customer Address</label><br>
				<b><?= $api->customer != null ?$api->customer->customer_address : "-"?></b>
			</div>

			<div class="mb-3">
				<label class="form-label">Created At</label><br>
				<b><?= $api->created_at ?></b>
			</div>
		</div>
	</div>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
</div>
