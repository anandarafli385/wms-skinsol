<!DOCTYPE html>
<html>

<head>
    <title>Work Order. <?= $wo['no_work_order'] ?></title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inter&display=swap" rel="stylesheet">
    <style type="text/css">
        @page {
            margin: 0cm 0cm;
			padding: 3cm 4cm;
            font-family: 'Inter', sans-serif;
        }

        body {
            margin: 0px;
            font-size: 9px;
            padding: 14px;
        }

        .text-center {
            text-align: center;
        }

        .text-right {
            text-align: right;
        }

        .text-left {
            text-align: left;
        }

        h2 {
            margin-top: 5px;
            margin-bottom: 2px;
        }

        .table {
            margin: 0;
            border-collapse: collapse;
            border: 0;
        }

        .table>tbody>tr>td {
            border-top: 0;
        }

        .table>tbody>tr>.br {
            border-right: 0;
        }

        .table>tbody>tr>.bl {
            border-left: 0;
        }

        .td-valign-top>tbody>tr>td {
            vertical-align: top;
        }

        .bl {
            border-left: 1px solid #000 !important;
        }

        .bt {
            border-top: 1px solid #000 !important;
        }

        .nowrap {
            white-space: nowrap !important;
        }

        .page_break {
            page-break-after: auto;
        }

		.body{
			margin: 45px 80px;
		}
    </style>
</head>

<body>
    <div class="header" style="margin-top: 50px;">
		<h1 class="text-center">Work Order</h1>
		<p class="text-center">No. Work Order. <?= $wo['no_work_order'] ?></p>
	</div>
	
	<?php 

		$fg_materials = $this->sql->select_table('tbl_fg_material', [
			'fg_id' => $detail[0]->fg_id
		])->result();
	?>

	<div class="body">
		<table style="width: 100%;" >
			<tr>
				<td width="30%">Date</td>
				<td width="5%">:</td>
				<td width="65%"><?= date('d M Y', strtotime($wo['date'])) ?></td>
			</tr>
			<tr>
				<td>Material count</td>
				<td>:</td>
				<td><?= count($fg_materials) . " material" ?></td>
			</tr>
			<tr>
				<td width="30%">Status Approval</td>
				<td width="5%">:</td>
				<td width="65%"><?= $wo['status_approval'] ?></td>
			</tr>
			<tr>
				<td width="30%">Approved By</td>
				<td width="5%">:</td>
				<td width="65%"><?= $wo['approved_by'] ?></td>
			</tr>
		</table>

		<h3 style="margin-top: 35px"> Finish Good </h3>
		<ol>
			<?php 
				foreach($detail as $item): 
					$fg = $this->sql->select_table('tbl_material', [
						'material_no' => $item->fg_id
					])->row_array();
			?>
				<li><?= $fg['nama'] ?></li>
					<table style="margin-bottom: 20px;">
						<tr>
							<td width="60px">Cas Num</td>
							<td>:</td>
							<td><?= $fg['cas_num'] ?></td>
						</tr>
						<tr>
							<td>Unit</td>
							<td>:</td>
							<td><?= $fg['unit'] ?></td>
						</tr>
						<tr>
							<td>Quantity</td>
							<td>:</td>
							<td><?= $item->qty ?></td>
						</tr>
					</table>
					
					<h4>Material</h4>
					<table width="100%" border="1px" style="margin-bottom: 40px; border-collapse: collapse">
						<tr>
							<th width="10%">No</th>
							<th width="20%">Material Code</th>
							<th width="20%">Material Name</th>
							<th width="30%">Cas Num</th>
							<th width="20%">Quantity</th>
						</tr>

						<?php 

							$no = 1;
							foreach($fg_materials as $mtr):
								$material = $this->sql->select_table('tbl_material', [
									'material_no' => $mtr->material_id
								])->row();
						?>
							<tr>
								<td class="text-center"><?= $no++ ?></td>
								<td class="text-center"><?= $material->kode_material ?></td>
								<td style="padding-left: 10px"><?= $material->nama ?></td>
								<td style="padding-left: 10px"><?= $material->cas_num ?></td>
								<!-- <td><?= $material->harga ?></td> -->
								<td class="text-center"><?= $mtr->qty ?></td>
							</tr>
						<?php endforeach; ?>
					</table>
			
			<?php endforeach; ?>
		</ol>
	</div>
</body>

</html>
