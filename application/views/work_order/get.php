<form action="<?= base_url() ?>admin/work_order/update" method="POST">
    <div class="modal-body m-3">
		<div class="mb-3">
            <label class="form-label">Work Order ID</label>
            <input type="text" class="form-control" name="work_order_id" placeholder="Work Order No" value="<?= $api->id ?>" readonly required>
        </div>

        <div class="mb-3">
            <label class="form-label">Work Order No.</label>
            <input type="text" class="form-control" name="work_order_num" placeholder="Work Order No" value="<?= $api->po_num ?>" readonly required>
        </div>

        <div class="mb-3">
            <label class="form-label">Customer Code</label>
            <input type="text" class="form-control" name="customer_code" placeholder="Work Order No" value="<?= $api->customer != null ? $api->customer->customer_code : '-' ?>" readonly required>
        </div>

        <div class="mb-3">
            <label class="form-label">Customer Name</label>
            <input type="text" class="form-control" name="customer_name" placeholder="Work Order No" value="<?= $api->customer != null ? $api->customer->customer_name : '-' ?>" readonly required>
        </div>

        <div class="form-group mb-3">
          <label for="status_approval">Status Work Order</label>
          <select class="form-control" name="approval">
            <option value="approved">Approved</option>
            <option value="not approved">Not Approved</option>
          </select>
        </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
    </div>
</form>

<script>
	$(function(){
		$(".select2-edit").select2({
			theme: 'bootstrap-5',
			dropdownParent: $("#editModal"),
			width: 'auto'
		});
	});
</script>
