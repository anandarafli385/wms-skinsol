<main class="content">
	<div class="container-fluid p-0">

		<h1 class="h3 mb-3">Sales Order</h1>
		<div class="row">
			<div class="col-12 col-xl-12">
				<div class="card">
					<div class="card-header">
						<h5 class="card-title">Sales Order
						</h5>
						<h6 class="card-subtitle text-muted">List data Sales Order</h6>
					</div>
					<div class="card-body">
					<?= $this->session->flashdata('msg') ?>
					<table class="table table-striped dataTable">
						<thead>
							<tr>
								<th>No.</th>
								<th>Sales Order No.</th>
								<th>Sales Order date</th>
								<th>Supplier Name</th>
								<th>Total Material</th>
								<th>Status Receiving</th>
								<!-- <th>Created By</th> -->
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$no = 1;
							foreach ($get as $row) { ?>
								<tr>
									<td><?= $no++; ?></td>
									<td><?= $row['sales_order_no'] ?></td>
									<td><?= date('d M Y', strtotime($row['gr_date'])) ?></td>
									<td><?= $row['nama'] ?></td>
									<td><?= $row['count_material'] ?></td>
									<td>
										<?php if($row['status_receiving'] == "done"): ?>
											<span class="badge bg-success">OK</span>
										<?php elseif($row['status_receiving'] == "on progress"): ?>
											<span class="badge bg-warning">On Progress</span>
										<?php else: ?>
											<span class="badge bg-danger">Reject</span>
										<?php endif; ?>
									</td>
									<!-- <td><?= $row['name'] ?></td> -->
									<td>
										<a href="#!" data-bs-toggle="modal" data-bs-target="#detailModal" data-id="<?= $row['gr_id'] ?>"><i data-feather="eye"></i></a>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
					</div>
				</div>
			</div>

		</div>

	</div>
</main>

<div class="modal fade" id="addModal" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Add Data Good Receiving</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<form action="<?= base_url() ?>admin/sales_order/add" method="POST">
				<div class="modal-body m-3">
					<div class="mb-3">
						<label class="form-label">Supplier Name <span class="text-danger">*</span></label>
						<select name="supplier_id" class="form-control select2" required>
							<option value="">- Select One -</option>
							<?php foreach ($supplier as $row) { ?>
								<option value="<?= $row['supplier_id'] ?>"><?= $row['nama'] ?></option>
							<?php } ?>
						</select>
					</div>
					<div class="mb-3">
						<label class="form-label">GR Date</label>
						<input type="date" class="form-control" name="gr_date" placeholder="GR Date" required>
					</div>

					<div class="form-group mb-3">
					  <label for="status">Status Quality Control</label>
					  <select class="form-control select2" name="status">
						<option value="not yet">Not yet</option>
						<option value="ok">OK</option>
						<option value="reject">Reject</option>
					  </select>
					</div>

					<div class="mb-3">
						<label class="form-label">Material List</label>
						<div class="table-responsive">
							<table class="table table-sm table-striped">
								<thead>
									<tr>
										<th width="25%">Material</th>
										<th>Batch No.</th>
										<th>QC No.</th>
										<th>Manufacture</th>
										<th>Country</th>
										<th width="10%">Qty</th>
										<th width="10%">Unit</th>
										<th width="20%">Expired Date</th>
										<th width="5%"></th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>
											<input type="number" class="form-control" name="qty_box[]" placeholder="Qty Box" required>
										</td>
										<td>
											<select name="material_id[]" class="form-control select2 material_id_1" required>
												<option value="">- Select One -</option>
												<?php foreach ($material as $row) { ?>
													<option value="<?= $row['material_id'] ?>" data-description="<?= $row['deskripsi'] ?>" data-unit="<?= $row['unit'] ?>"><?= $row['nama'] ?></option>
												<?php } ?>
											</select>
										</td>
										<td>
											<input type="text" class="form-control" name="batch_no[]" placeholder="Batch Number" required>
										</td>
										<td>
											<input type="text" class="form-control" name="qc_no[]" placeholder="QC Number" required>
										</td>
										<td>
											<input type="text" class="form-control" name="manufacture[]" placeholder="Manufacture" required>
										</td>
										<td>
											<input type="text" class="form-control" name="country[]" placeholder="Country" required>
										</td>
										<td>
											<input type="number" class="form-control" name="qty[]" placeholder="Qty" required>
										</td>
										<td id="material_unit_1">-</td>
										<td>
											<input type="date" class="form-control" name="expired_date[]" placeholder="Expired Date" required>
										</td>
										<td><button type="button" onclick="addrow(this)" class="btn btn-success">+</button></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Save changes</button>
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="modal fade" id="editModal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Edit Data Good Receiving</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div id="edit_data"></div>
		</div>
	</div>
</div>
<div class="modal fade" id="detailModal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Detail Data Good Receiving</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div id="detail_data"></div>
		</div>
	</div>
</div>
<script>
	let nilai_awal = 2;

	$("#editModal").on('shown.bs.modal', function(e) {
		var id = $(e.relatedTarget).data('id');
		$.ajax({
			type: "POST",
			url: "<?= base_url() ?>admin/sales_order/get",
			data: {
				id: id
			}
		}).done(function(response) {
			console.log(response);
			$("#edit_data").html(response);
		});
	});

	$("#detailModal").on('shown.bs.modal', function(e) {
		var id = $(e.relatedTarget).data('id');
		$.ajax({
			type: "POST",
			url: "<?= base_url() ?>admin/sales_order/detail",
			data: {
				id: id
			}
		}).done(function(response) {
			$("#detail_data").html(response);
		});
	});

	$(".material_id_1").on('select2:select', function (e) {
		var description = $(this).find(":selected").data('description');
		var unit = $(this).find(":selected").data('unit');

		$('#material_description_1').text(description)
		$('#material_unit_1').text(unit)
	})

	

	function delete_data(id) {
		Swal.fire({
			title: 'Konfirmasi ?',
			text: "Apakah kamu yakin ?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#28a745',
			cancelButtonColor: '#dc3545',
			confirmButtonText: 'Yes!',
			cancelButtonText: 'No!',
			showLoaderOnConfirm: true,
			preConfirm: function() {
				return new Promise(function(resolve, reject) {
					$.ajax({
						type: 'POST',
						url: "<?= base_url() ?>admin/sales_order/delete",
						data: {
							id: id
						}
					}).done(function(msg) {
						if (msg == "ok") {
							swal.fire("OK!", "Data berhasil dihapus!", "success").then(function() {
								location.reload();
							})
						} else {
							swal.fire("Gagal!", msg, "error").then(function() {
								location.reload();
							})
						}
					})
				})
			},
			allowOutsideClick: () => !Swal.isLoading()
		})
	}

	function addrow(btn) {
		var row_copy = `
			<td>
				<input type="number" class="form-control" name="qty_box[]" placeholder="Qty Box" required>
			</td>
			<td>
				<select name="material_id[]" class="form-control select2 material_id" required>
					<option value="">- Select One -</option>
					<?php foreach ($material as $row) { ?>
						<option value="<?= $row['material_id'] ?>" data-description="<?= $row['deskripsi'] ?>" data-unit="<?= $row['unit'] ?>"><?= $row['nama'] ?></option>
					<?php } ?>
				</select>
			</td>
			<td class="material_description">-</td>
			<td>
				<input type="number" class="form-control" name="qty[]" placeholder="Qty" required>
			</td>
			<td class="material_unit">-</td>
			<td>
				<input type="date" class="form-control" name="expired_date[]" placeholder="Expired Date" required>
			</td>
			<td></td>
		`;
		$(btn).closest("tbody").append("<tr>" + row_copy + "</tr>");
		var btn_delete = '<button type="button" class="btn btn-danger" onclick="deleterow(this)">-</button>';
		$(btn).closest("tbody").find("tr:last").find("td:last").html(btn_delete);

		$(".select2").select2({
			theme: 'bootstrap-5',
			dropdownParent: $("#addModal")
		});

		$(".material_id").on('select2:select', function (e) {
			// console.log("nilai berubah")
			let description = $(this).find(":selected").data('description');
			var unit = $(this).find(":selected").data('unit');

			$(this).closest('tr').children('td.material_description').text(description);
			$(this).closest('tr').children('td.material_unit').text(unit);
		})
	}

	function deleterow(btn) {
		$(btn).closest("tr").remove();
	}
</script>
