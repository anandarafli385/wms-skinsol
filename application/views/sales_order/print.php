<!DOCTYPE html>
<html>

<head>
    <title>Print Good Receiving</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inter&display=swap" rel="stylesheet">
    <style type="text/css">
        @page {
            margin: 0cm 0cm;
            font-family: 'Inter', sans-serif;
        }

        body {
            margin: 0px;
            font-size: 9px;
            padding: 4px;
        }

        .text-center {
            text-align: center;
        }

        .text-right {
            text-align: right;
        }

        .text-left {
            text-align: left;
        }

        h2 {
            margin-top: 5px;
            margin-bottom: 2px;
        }

        h3 {
            line-height: 0%;
        }

        span {
            font-size: 14px;
        }

        .table {
            margin: 0;
            border-collapse: collapse;
            border: 0;
        }

        .table>tbody>tr>td {
            border-top: 0;
        }

        .table>tbody>tr>.br {
            border-right: 0;
        }

        .table>tbody>tr>.bl {
            border-left: 0;
        }

        .td-valign-top>tbody>tr>td {
            vertical-align: top;
        }

        .bl {
            border-left: 1px solid #000 !important;
        }

        .bt {
            border-top: 1px solid #000 !important;
        }

        .nowrap {
            white-space: nowrap !important;
        }

        .page_break {
            page-break-after: auto;
        }
    </style>
</head>

<body>
    <?php foreach ($get as $row) { ?>
        <div class="page_break">
            <!-- Barcode (Set page to A8) -->
            <table class="table td-valign-top" width="100%" border="0" cellspacing="0" cellpadding="4">
                <tbody>
                    <tr>
                        <td class="text-center">
                            <img src="data:image/png;base64,<?= set_barcode($row['box_code']) ?>" height="60"><br>
                            <b><?= $row['box_code'] ?></b>
                        </td>
                    </tr>
                </tbody>
            </table>
            <hr>
            <table class="table td-valign-top" width="100%" cellspacing="0" cellpadding="6">
                <tr>
                    <td style="width: 50%">
                        <table class="table td-valign-top" width="100%" cellspacing="0">
                            <tr>
                                <td>Material</td>
                                <td>:</td>
                                <td><?= $row['nama'] ?></td>
                            </tr>
                            <tr>
                                <td>Qty</td>
                                <td>:</td>
                                <td><?= $row['qty'] . ' ' . $row['unit'] ?></td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 50%">
                        <table class="table td-valign-top" width="100%" cellspacing="0">
                            <tr>
                                <td>Expired</td>
                                <td>:</td>
                                <td><?= date("d/m/Y", strtotime($row['expired_date'])) ?></td>
                            </tr>
							<tr>
								<td>Description</td>
								<td>:</td>
								<td><?= substr($row['deskripsi'], 0, 20) ?></td>
							</tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    <?php } ?>
</body>

</html>
