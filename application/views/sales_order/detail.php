<div class="modal-body m-3">
    <div class="mb-3">
        <label class="form-label">Sales Order No.</label><br>
        <b><?= $get['sales_order_no'] ?></b>
    </div>

    <div class="mb-3">
        <label class="form-label">GR Date</label><br>
        <b><?= dateID($get['gr_date']) ?></b>
    </div>

    <div class="mb-3">
        <label class="form-label">Supplier</label><br>
        <b><?php 
        $supplier = $this->sql->select_table('tbl_supplier', [
            'supplier_id' => $get['supplier_id']
        ])->row();
        
        echo $supplier->nama; 
        ?></b>
    </div>

    <div class="mb-3">
        <label class="form-label">Quality Control Status</label><br>
        <b><?= ucfirst($get['status']) ?></b>
    </div>
    <!-- <div class="mb-3">
        <label class="form-label">Material GR List</label>
        <div class="table-responsive">
            <table class="table table-sm table-striped">
                <thead>
                    <tr>
                        <th>Qty Box</th>
                        <th>Material</th>
                        <th>Description</th>
                        <th>Qty</th>
                        <th>Unit</th>
                        <th>Expired Date</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($detail as $row) { ?>
                        <tr>
                            <td><?= $row['qty_box'] ?></td>
                            <td><?= $row['nama'] ?></td>
                            <td><?= $row['deskripsi'] ?></td>
                            <td><?= $row['qty'] ?></td>
                            <td><?= $row['unit'] ?></td>
                            <td><?= dateID($row['expired_date']) ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div> -->
    <div class="mb-3">
        <label class="form-label">Material Box List</label>
        <div class="table-responsive">
            <table class="table table-sm table-striped dataTable">
                <thead>
                    <tr>
                        <th>Material</th>
                        <th>Type</th>
                        <th>Box Code</th>
                        <th>Batch No.</th>
                        <th>QC No.</th>
                        <th>Manufacture</th>
                        <th>Country</th>
                        <th>Qty</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($box as $row) { ?>
                        <tr>
                            <td><?= $row['nama'] ?></td>
                            <td><?= $row['inventory_part_type'] ?></td>
                            <td><?= $row['box_code'] ?></td>
                            <td><?= $row['batch_no'] ?></td>
                            <td><?= $row['qc_no'] ?></td>
                            <td><?= $row['manufacture'] ?></td>
                            <td><?= $row['country'] ?></td>
                            <td><?= number_format($row['qty'], 0, ',', '.') . ' ' . $row['unit'] ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
</div>
