<main class="content">
	<div class="container-fluid p-0">

		<div class="mb-3">
			<h1 class="h3 d-inline align-middle">Profile</h1>
		</div>
		<div class="row">
			<div class="col-md-4 col-xl-3">
				<div class="card mb-3">
					<div class="card-header">
						<h5 class="card-title mb-0">Profile Details</h5>
					</div>
					<div class="card-body text-center">
						<a href="<?= base_url() ?>assets/uploads/profile/<?= $this->session->image; ?>" target="_blank" rel="noopener noreferrer">
						<img src="<?= base_url() ?>assets/uploads/profile/<?= $this->session->image; ?>" alt="<?= $this->session->name; ?>"
							class="img-fluid rounded-circle mb-2"  style="width: 128px; height: 128px; object-fit: cover">
							</a>
						<h5 class="card-title mb-0"><?= $this->session->name; ?></h5>
						<div class="text-muted mb-2"> 
									<?php if($this->session->level == 0): ?>
										<b>Superadmin</b>
									<?php elseif($this->session->level == 1): ?>
										<b>Supervisor</b>
									<?php elseif($this->session->level == 2): ?>
										<b>Warehouse</b>
									<?php elseif($this->session->level == 3): ?>
										<b>Production</b>
									<?php endif; ?></div>

						<!-- <div>
							<a class="btn btn-primary btn-sm" href="#">Follow</a>
							<a class="btn btn-primary btn-sm" href="#"><svg xmlns="http://www.w3.org/2000/svg"
									width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor"
									stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
									class="feather feather-message-square">
									<path d="M21 15a2 2 0 0 1-2 2H7l-4 4V5a2 2 0 0 1 2-2h14a2 2 0 0 1 2 2z"></path>
								</svg> Message</a>
						</div> -->
					</div>
					<hr class="my-0">
					<!-- <div class="card-body">
						<h5 class="h6 card-title">Skills</h5>
						<a href="#" class="badge bg-primary me-1 my-1">HTML</a>
						<a href="#" class="badge bg-primary me-1 my-1">JavaScript</a>
						<a href="#" class="badge bg-primary me-1 my-1">Sass</a>
						<a href="#" class="badge bg-primary me-1 my-1">Angular</a>
						<a href="#" class="badge bg-primary me-1 my-1">Vue</a>
						<a href="#" class="badge bg-primary me-1 my-1">React</a>
						<a href="#" class="badge bg-primary me-1 my-1">Redux</a>
						<a href="#" class="badge bg-primary me-1 my-1">UI</a>
						<a href="#" class="badge bg-primary me-1 my-1">UX</a>
					</div> -->
					<!-- <hr class="my-0">
					<div class="card-body">
						<h5 class="h6 card-title">About</h5>
						<ul class="list-unstyled mb-0">
							<li class="mb-1"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
									viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
									stroke-linecap="round" stroke-linejoin="round"
									class="feather feather-home feather-sm me-1">
									<path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
									<polyline points="9 22 9 12 15 12 15 22"></polyline>
								</svg> Lives in <a href="#">San Francisco, SA</a></li>

							<li class="mb-1"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
									viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
									stroke-linecap="round" stroke-linejoin="round"
									class="feather feather-briefcase feather-sm me-1">
									<rect x="2" y="7" width="20" height="14" rx="2" ry="2"></rect>
									<path d="M16 21V5a2 2 0 0 0-2-2h-4a2 2 0 0 0-2 2v16"></path>
								</svg> Works at <a href="#">GitHub</a></li>
							<li class="mb-1"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
									viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
									stroke-linecap="round" stroke-linejoin="round"
									class="feather feather-map-pin feather-sm me-1">
									<path d="M21 10c0 7-9 13-9 13s-9-6-9-13a9 9 0 0 1 18 0z"></path>
									<circle cx="12" cy="10" r="3"></circle>
								</svg> From <a href="#">Boston</a></li>
						</ul>
					</div>
					<hr class="my-0">
					<div class="card-body">
						<h5 class="h6 card-title">Elsewhere</h5>
						<ul class="list-unstyled mb-0">
							<li class="mb-1"><a href="#">staciehall.co</a></li>
							<li class="mb-1"><a href="#">Twitter</a></li>
							<li class="mb-1"><a href="#">Facebook</a></li>
							<li class="mb-1"><a href="#">Instagram</a></li>
							<li class="mb-1"><a href="#">LinkedIn</a></li>
						</ul>
					</div> -->
				</div>
			</div>

			<div class="col-md-8 col-xl-9">
				<!-- Flash data -->
				<?php if($this->session->alert_danger !== null): ?>
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
						<h3 class="text-danger"><i class="feather" data-feather="alert-triangle"></i>
						<strong>Error!</strong></h3>
						<?= $this->session->alert_danger ?>
						<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
					</div>
				<?php endif; ?>

				<?php if($this->session->alert_success !== null): ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						<h3 class="text-success"><i class="feather" data-feather="check"></i>
						<strong>Success!</strong></h3>
						<?= $this->session->alert_success ?>
						<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
					</div>
				<?php endif; ?>


				<div class="card">
					<div class="card-header">

						<!-- <h5 class="card-title mb-0">Activities</h5> -->
						<ul class="nav nav-pills">
							<!-- <li class="nav-item">
								<a class="nav-link <?= $this->session->tab_active == null ? "active" : null ?>" aria-current="page" id="activity_link" data-toggle="tab"
									onclick="tekan('activity')">Active</a>
							</li> -->
							<li class="nav-item">
								<a class="nav-link <?= $this->session->tab_active == null ? "active" : null ?>" aria-current="page" id="timeline_link" data-toggle="tab"
									onclick="tekan('timeline')">Change Profile</a>
							</li>
							<li class="nav-item">
								<a class="nav-link <?= $this->session->tab_active == "password" ? "active" : null ?>" id="settings_link" data-toggle="tab"
									onclick="tekan('settings')">Settings</a>
							</li>
						</ul>
					</div>
					<div class="card-body h-100">
						<div class="tab-content">
							<!-- <div class="tab-pane  <?= $this->session->tab_active == null ? "active" : null ?>" id="activity">

								<div class="d-flex align-items-start">
									<img src="<?= base_url() ?>assets/static/img/avatars/avatar-5.jpg" width="36"
										height="36" class="rounded-circle me-2" alt="Vanessa Tucker">
									<div class="flex-grow-1">
										<small class="float-end text-navy">5m ago</small>
										<strong>Vanessa Tucker</strong> started following <strong>Christina
											Mason</strong><br />
										<small class="text-muted">Today 7:51 pm</small><br />

									</div>
								</div>

								<hr />
								<div class="d-flex align-items-start">
									<img src="<?= base_url() ?>assets/static/img/avatars/avatar.jpg" width="36"
										height="36" class="rounded-circle me-2" alt="Charles Hall">
									<div class="flex-grow-1">
										<small class="float-end text-navy">30m ago</small>
										<strong>Charles Hall</strong> posted something on <strong>Christina
											Mason</strong>'s timeline<br />
										<small class="text-muted">Today 7:21 pm</small>

										<div class="border text-sm text-muted p-2 mt-1">
											Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam
											semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc,
											blandit vel, luctus
											pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus.
											Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante.
										</div>

										<a href="#" class="btn btn-sm btn-danger mt-1"><i class="feather-sm"
												data-feather="heart"></i> Like</a>
									</div>
								</div>

								<hr />
								<div class="d-flex align-items-start">
									<img src="<?= base_url() ?>assets/static/img/avatars/avatar-4.jpg" width="36"
										height="36" class="rounded-circle me-2" alt="Christina Mason">
									<div class="flex-grow-1">
										<small class="float-end text-navy">1h ago</small>
										<strong>Christina Mason</strong> posted a new blog<br />

										<small class="text-muted">Today 6:35 pm</small>
									</div>
								</div>

								<hr />
								<div class="d-flex align-items-start">
									<img src="<?= base_url() ?>assets/static/img/avatars/avatar-2.jpg" width="36"
										height="36" class="rounded-circle me-2" alt="William Harris">
									<div class="flex-grow-1">
										<small class="float-end text-navy">3h ago</small>
										<strong>William Harris</strong> posted two photos on <strong>Christina
											Mason</strong>'s timeline<br />
										<small class="text-muted">Today 5:12 pm</small>

										<div class="row g-0 mt-1">
											<div class="col-6 col-md-4 col-lg-4 col-xl-3">
												<img src="<?= base_url() ?>assets/static/img/photos/unsplash-1.jpg"
													class="img-fluid pe-2" alt="Unsplash">
											</div>
											<div class="col-6 col-md-4 col-lg-4 col-xl-3">
												<img src="<?= base_url() ?>assets/static/img/photos/unsplash-2.jpg"
													class="img-fluid pe-2" alt="Unsplash">
											</div>
										</div>

										<a href="#" class="btn btn-sm btn-danger mt-1"><i class="feather-sm"
												data-feather="heart"></i> Like</a>
									</div>
								</div>

								<hr />
								<div class="d-flex align-items-start">
									<img src="<?= base_url() ?>assets/static/img/avatars/avatar-2.jpg" width="36"
										height="36" class="rounded-circle me-2" alt="William Harris">
									<div class="flex-grow-1">
										<small class="float-end text-navy">1d ago</small>
										<strong>William Harris</strong> started following <strong>Christina
											Mason</strong><br />
										<small class="text-muted">Yesterday 3:12 pm</small>

										<div class="d-flex align-items-start mt-1">
											<a class="pe-3" href="#">
												<img src="<?= base_url() ?>assets/static/img/avatars/avatar-4.jpg"
													width="36" height="36" class="rounded-circle me-2"
													alt="Christina Mason">
											</a>
											<div class="flex-grow-1">
												<div class="border text-sm text-muted p-2 mt-1">
													Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem.
													Maecenas nec odio et ante tincidunt tempus.
												</div>
											</div>
										</div>
									</div>
								</div>

								<hr />
								<div class="d-flex align-items-start">
									<img src="<?= base_url() ?>assets/static/img/avatars/avatar-4.jpg" width="36"
										height="36" class="rounded-circle me-2" alt="Christina Mason">
									<div class="flex-grow-1">
										<small class="float-end text-navy">1d ago</small>
										<strong>Christina Mason</strong> posted a new blog<br />
										<small class="text-muted">Yesterday 2:43 pm</small>
									</div>
								</div>

								<hr />
								<div class="d-flex align-items-start">
									<img src="<?= base_url() ?>assets/static/img/avatars/avatar.jpg" width="36"
										height="36" class="rounded-circle me-2" alt="Charles Hall">
									<div class="flex-grow-1">
										<small class="float-end text-navy">1d ago</small>
										<strong>Charles Hall</strong> started following <strong>Christina
											Mason</strong><br />
										<small class="text-muted">Yesterdag 1:51 pm</small>
									</div>
								</div>

								<hr />
								<div class="d-grid">
									<a href="#" class="btn btn-primary">Load more</a>
								</div>

							</div> -->
							<!-- /.tab-pane -->
							<div class="tab-pane  <?= $this->session->tab_active == null ? "active" : null ?>" id="timeline">
								<form action="<?= base_url() ?>admin/profile/change_profile" method="post" enctype="multipart/form-data">
									<input type="hidden" name="user_id" readonly value="<?= $this->session->user_id; ?>">

									<!-- <div class="mb-3">
										<label class="form-label">Username</label>
										<input type="text" name="username" class="form-control" value="<?= $this->session->name; ?>" required>
									</div> -->

									<div class="mb-3">
										<label class="form-label">Name <span class="text-danger">*</span></label>
										<input type="text" name="name" class="form-control" value="<?= $this->session->name; ?>" required>
									</div>

									<div class="mb-3">
										<label for="formFile" class="form-label">Profile Image</label>
										<input class="form-control" type="file" accept="image/*" id="formFile" name="image">
									</div>

									<button type="submit" class="d-flex pull-right btn btn-danger mt-1"><i class="feather m-1" data-feather="edit-3"></i> Change Profile</button>
								</form>
							</div>
							<!-- /.tab-pane -->

							<div class="tab-pane  <?= $this->session->tab_active == 'password' ? "active" : null ?>" id="settings">
								<form class="form-horizontal" method="POST" action="<?= base_url() ?>admin/profile/change_password">
								<input type="hidden" readonly name="user_id" value="<?= $this->session->user_id ?>">

								<div class="mb-3">
									<label class="form-label">Current Password <span class="text-danger">*</span></label>
									<input type="password" name="current_pass" maxlength="16" required class="form-control" placeholder="Current password...">
								</div>

								<div class="mb-3">
									<label class="form-label">New Password <span class="text-danger">*</span></label>
									<input type="password" name="new_pass" maxlength="16" required class="form-control" placeholder="New Password...">
								</div>

								<div class="mb-3">
									<label class="form-label">New Password Confirmation <span class="text-danger">*</span></label>
									<input type="password" name="new_pass_confirm" maxlength="16" required class="form-control" placeholder="New Password Confirmation...">
								</div>
								
								<button type="submit" class="d-flex pull-right btn btn-danger mt-1"><i class="feather m-1" data-feather="edit-3"></i> Change Password</button>
								</form>
							</div>
							<!-- /.tab-pane -->
						</div>
						<!-- /.tab-content -->
					</div>
				</div>
			</div>
		</div>

	</div>
</main>

<script>
	function tekan(id) {
		$('#activity').removeClass('active')
		$('#timeline').removeClass('active')
		$('#settings').removeClass('active')

		$('#activity_link').removeClass('active')
		$('#timeline_link').removeClass('active')
		$('#settings_link').removeClass('active')

		$('#' + id).addClass('active')
		$('#' + id + '_link').addClass('active')
	}

</script>
