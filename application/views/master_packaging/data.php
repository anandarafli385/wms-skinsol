<main class="content">
	<div class="container-fluid p-0">

		<h1 class="h3 mb-3">Master Packaging</h1>

		<div class="row">
			<div class="col-12 col-xl-12">
				<div class="card">
					<div class="card-header">
						<h5 class="card-title">Master Packaging
							<!-- <div class="float-end">
								<button type="button" class="btn btn-primary" data-bs-toggle="modal"
									data-bs-target="#addModal"><i class="fa fa-plus"></i> Add Packaging</button>
							</div> -->
						</h5>
						<h6 class="card-subtitle text-muted">List data master Packaging
						</h6>
					</div>
					<div class="card-body">
						<?= $this->session->flashdata('msg') ?>
						<table class="table table-striped dataTable">
							<thead>
								<tr>
									<th>No.</th>
									<th>Packaging Code</th>
									<th>Packaging Name</th>
									<th>Packaging Type</th>
									<th>Customer Name</th>
									<th>Supplier Name</th>
									<th>Category</th>
									<th>Stock Minimum</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
								<?php
                            $no = 1;
                            foreach ($api as $row) { ?>
								<tr>
									<td><?= $no++; ?></td>
									<td><?= $row->packaging_code ?></td>
									<td><?= $row->packaging_name ?></td>
									<td><?= $row->packaging_type ?></td>
									<td>
										<?php foreach($cust as $c){ 
											if($row->customer_id == $c->id){
												echo $c->customer_name;
											} else{
												echo "";
											}
										 }  
										?>
									</td>
									<td>
										<?php foreach($supp as $s){ 
											if($row->supplier_id == $s->id){
												echo $s->supplier_name;
											} else{
												echo "";
											}
										 }  
										?>
									</td>
									<td>
									<?php 
									if(strpos($row->packaging_code,'BKP') !== false){
										echo 'Primer';
									}else if(strpos($row->packaging_code,'BKS') !== false){
										echo 'Sekunder';
									}else{
										echo 'Tersier';
									}
									?></td>
									<td><?= $row->stock_minimum ?></td>
									<td><?= $row->status ?></td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>

		</div>

	</div>
</main>
<div class="modal fade" id="addModal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Add Data Salesman</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<form action="<?= base_url() ?>admin/master_salesman/add" method="POST">
				<div class="modal-body m-3">
					<div class="mb-3">
						<label class="form-label">Sales No <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="salesman_no" placeholder="Sales No" required>
					</div>
					<div class="mb-3">
						<label class="form-label">Sales Name <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="name" placeholder="Sales Name" required>
					</div>
					<div class="mb-3">
						<label class="form-label">Sales Email <span class="text-danger">*</span></label>
						<input type="email" class="form-control" name="email" placeholder="Sales Email" required>
					</div>
					<div class="mb-3">
						<label class="form-label">Address <span class="text-danger">*</span></label>
						<textarea name="address" class="form-control" placeholder="Address"></textarea>
					</div>
					<div class="mb-3">
						<label class="form-label">Phone <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="phone" placeholder="Phone" required>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Save changes</button>
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="modal fade" id="editModal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Edit Data Salesman</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div id="edit_data"></div>
		</div>
	</div>
</div>
<script>
	$("#editModal").on('shown.bs.modal', function (e) {
		var id = $(e.relatedTarget).data('id');
		$.ajax({
			type: "POST",
			url: "<?= base_url() ?>admin/master_salesman/get",
			data: {
				id: id
			}
		}).done(function (response) {
			$("#edit_data").html(response);
		});
	});

	function delete_data(id) {
		Swal.fire({
			title: 'Konfirmasi ?',
			text: "Apakah kamu yakin ?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#28a745',
			cancelButtonColor: '#dc3545',
			confirmButtonText: 'Yes!',
			cancelButtonText: 'No!',
			showLoaderOnConfirm: true,
			preConfirm: function () {
				return new Promise(function (resolve, reject) {
					$.ajax({
						type: 'POST',
						url: "<?= base_url() ?>admin/master_salesman/delete",
						data: {
							id: id
						}
					}).done(function (msg) {
						if (msg == "ok") {
							swal.fire("OK!", "Data berhasil dihapus!", "success").then(
								function () {
									location.reload();
								})
						} else {
							swal.fire("Gagal!", msg, "error").then(function () {
								location.reload();
							})
						}
					})
				})
			},
			allowOutsideClick: () => !Swal.isLoading()
		})
	}

</script>
