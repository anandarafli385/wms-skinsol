<form action="<?= base_url() ?>admin/master_salesman/edit/<?= $get['salesman_id'] ?>" method="POST">
    <div class="modal-body m-3">
        <div class="mb-3">
            <label class="form-label">Sales No <span class="text-danger">*</span></label>
            <input type="text" class="form-control" name="salesman_no" placeholder="Sales No" value="<?= $get['salesman_no'] ?>" required>
        </div>
        <div class="mb-3">
            <label class="form-label">Sales Name <span class="text-danger">*</span></label>
            <input type="text" class="form-control" name="name" placeholder="Sales Name" value="<?= $get['name'] ?>" required>
        </div>
        <div class="mb-3">
            <label class="form-label">Email <span class="text-danger">*</span></label>
            <input type="email" class="form-control" name="email" placeholder="Email" value="<?= $get['email'] ?>" required>
        </div>
        <div class="mb-3">
            <label class="form-label">Address <span class="text-danger">*</span></label>
            <textarea name="address" class="form-control" placeholder="Address"><?= $get['address'] ?></textarea>
        </div>
        <div class="mb-3">
            <label class="form-label">Phone <span class="text-danger">*</span></label>
            <input type="text" class="form-control" name="phone" placeholder="Phone" value="<?= $get['phone'] ?>" required>
        </div>
        
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
    </div>
</form>