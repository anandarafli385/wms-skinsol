<div class="modal-body m-3">
    <div class="row">
        <div class="col-md-6">
            <div class="mb-3">
                <label class="form-label">Formula No.</label><br>
                <b><?= $formula->formula_num ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">Trial Revision No.</label><br>
                <b><?= $formula->trial_revision->revision_num ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">Trial Revision No.</label><br>
                <b><?= $formula->trial_revision->trial->trial_num ?></b>
            </div>            
            <div class="mb-3">
                <label class="form-label">Keterangan</label><br>
                <b><?= $formula->trial_revision->keterangan ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">Created At</label><br>
                <b><?= substr($formula->created_at,0,10) ?></b>
            </div>
        </div>
        <div class="col-md-12">
            <table class="table table-bordered table-inverse table-responsive overflow-auto">
                <thead class="thead-default">
                    <tr>
                        <th>#</th>
                        <th>Matrial Code</th>
                        <th>Material Name</th>
                        <th>Inci Name</th>
                        <th>Quantity</th>
                        <th>Weighing</th>
                        <th>Price</th>
                        <th>Category</th>
                        <th>Halal</th>
                        <th>Stock Minimum</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    $no = 1;
                    foreach($api_detail as $row){
                    if($id == $row->formula_id){ ?>
                    <tr>
                        <td><?= $no++ ?></td>
                        <td><?= $row->material != null ? $row->material->material_code : "-"?></td>
                        <td><?= $row->material != null ? $row->material->material_name : "-"?></td>
                        <td><?= $row->material != null ? $row->material->inci_name : "-"?></td>
                        <td><?= $row->material != null ? $row->quantity : "-"?></td>
                        <td><?= $row->material != null ? $row->weighing : "-"?></td>
                        <td><?= $row->material != null ? $row->material->price : "-"?></td>
                        <td><?= $row->material != null ? $row->material->category : "-"?></td>
                        <td>
                            <?php if($row->material != null && $row->material->halal == 1): ?>
                                <span class="badge bg-success">Halal</span>
                            <?php elseif($row != null && $row->material->halal != 1): ?>
                                <span class="badge bg-warning">Non halal</span>
                            <?php else: ?>
                                <span>-</span>
                            <?php endif; ?>
                        </td>
                        <td><?= $row->material->stock_minimum ?></td>
                    </tr>
                    <?php 
                        }
                    } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
</div>
<script>
    show_item();
    $(".select2-view").select2({
		theme: 'bootstrap-5',
		dropdownParent: $("#viewModal")
	});

</script>