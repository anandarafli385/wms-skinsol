<main class="content">
	<div class="container-fluid p-0">

		<h1 class="h3 mb-3">Bill of Materials</h1>

		<div class="row">
			<div class="col-12 col-xl-12">
				<div class="card">
					<div class="card-header">
						<h5 class="card-title">Bill of Materials
						</h5>
						<h6 class="card-subtitle text-muted">List data Bill of Materials
						</h6>
					</div>
					<div class="card-body">
						<?= $this->session->flashdata('msg') ?>
						<table class="table table-striped dataTable responsive">
							<thead>
								<tr>
									<th>No.</th>
									<th>Formula No.</th>
									<th>Trial Revision No.</th>
									<th>PO No.</th>
									<th>Prosedur Revisi</th>
									<th>Cust. Name</th>
									<th>Cust. Brand</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php
                            $no = 1;
                            foreach ($api as $row) { ?>
								<tr>
									<td><?= $no++; ?></td>
									<td><?= $row->formula_num ?></td>
									<td><?= $row->trial_revision->revision_num ?></td>
									<td><?= $row->trial_revision->trial->po_customer->po_num ?></td>
									<td style="width: 850px" ><?= $row->trial_revision->prosedur ?></td>
									<td><?= $row->trial_revision->trial->po_customer->customer->customer_name ?></td>
									<td><?= $row->trial_revision->trial->po_customer->customer->customer_brand ?></td>
									<td class="table-action">
										<a href="#!" data-bs-toggle="modal" data-bs-target="#viewModal"
											data-id="<?= $row->id ?>" data-fg_id="<?= $row->id ?>"><i data-feather="eye"></i></a>
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>

		</div>

	</div>
</main>
<div class="modal fade" id="addModal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Add Data Material</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<form action="<?= base_url() ?>admin/master_fg/add" method="POST" enctype="multipart/form-data">
				<div class="modal-body m-3">
					<div class="mb-3">
						<label class="form-label">Material <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="nama" placeholder="Material" required>
					</div>
					<div class="form-group mb-3">
					  <label for="">Material Code <span class="text-danger">*</span></label>
					  <input type="text" name="kode_material" maxlength="3" class="form-control" placeholder="3 Digit Material Code" aria-describedby="helpId">
					  <small id="helpId" class="text-muted">Material code should be unique to identody specific material.</small>
					</div>
					<div class="mb-3">
						<label class="form-label">Description</label>
						<input type="text" class="form-control" name="deskripsi" placeholder="Description">
					</div>
					<div class="mb-3">
						<label class="form-label">Price <span class="text-danger">*</span></label>
						<input type="number" class="form-control" name="harga" placeholder="Price" required>
					</div>
					<div class="mb-3">
						<label class="form-label">Unit <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="unit" placeholder="Unit" required>
					</div>
					<div class="mb-3">
						<label class="form-label">Status <span class="text-danger">*</span></label>
						<select name="status" class="form-control">
							<option value="halal">Halal</option>
							<option value="non halal">Non Halal</option>
						</select>
					</div>
					<div class="mb-3">
						<label class="form-label">Material type <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="material_type" placeholder="Material Type" required>
					</div>
					<div class="mb-3">
						<label class="form-label">Inventory Part Type <span class="text-danger">*</span></label>
						<select name="inventory_part_type" class="form-control">
							<!-- <option value="Raw Material">Raw Material</option>
							<option value="Subsidiary Material">Subsidiary Material</option> -->
							<option value="Work in Process">Work in Process</option>
							<!-- <option value="Other Material">Other Material</option> -->
							<option value="Finished Goods">Finished Goods</option>
						</select>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Save changes</button>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="modal fade" id="editModal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Edit Data FG</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div id="edit_data"></div>
		</div>
	</div>
</div>
<div class="modal fade" id="viewModal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">View Data FG</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div id="view_data"></div>
		</div>
	</div>
</div>
<script>

	$("#viewModal").on('shown.bs.modal', function (e) {
		var fg_id = $(e.relatedTarget).data('fg_id');
		console.log(fg_id)
		$.ajax({
			type: "POST",
			url: "<?= base_url() ?>admin/master_fg/view",
			data: {
				id: fg_id
			}
		}).done(function (response) {
			$("#view_data").html(response);
		});
	});

	// function delete_data(id) {
	// 	Swal.fire({
	// 		title: 'Konfirmasi ?',
	// 		text: "Apakah kamu yakin ?",
	// 		type: 'warning',
	// 		showCancelButton: true,
	// 		confirmButtonColor: '#28a745',
	// 		cancelButtonColor: '#dc3545',
	// 		confirmButtonText: 'Yes!',
	// 		cancelButtonText: 'No!',
	// 		showLoaderOnConfirm: true,
	// 		preConfirm: function () {
	// 			return new Promise(function (resolve, reject) {
	// 				$.ajax({
	// 					type: 'POST',
	// 					url: "<?= base_url() ?>admin/master_fg/delete",
	// 					data: {
	// 						id: id
	// 					}
	// 				}).done(function (msg) {
	// 					if (msg == "ok") {
	// 						swal.fire("OK!", "Data berhasil dihapus!", "success").then(
	// 							function () {
	// 								location.reload();
	// 							})
	// 					} else {
	// 						swal.fire("Gagal!", msg, "error").then(function () {
	// 							location.reload();
	// 						})
	// 					}
	// 				})
	// 			})
	// 		},
	// 		allowOutsideClick: () => !Swal.isLoading()
	// 	})
	// }

</script>
