<div class="modal-body m-3">
    <div class="row">
        <div class="col-md-6">
            <div class="mb-3">
                <label class="form-label">Labelling No.</label><br>
                <b><?= $api->labelling_code ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">Date</label><br>
                <b><?= $api->date ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">Packaging Result</label><br>
                <b><?= $api->packaging_result ?></b>
            </div>            
            <div class="mb-3">
                <label class="form-label">Result</label><br>
                <b><?= $api->result ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">Used Quantity</label><br>
                <b><?= $api->used_quantity ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">Release Quantity</label><br>
                <b><?= $api->release_quantity ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">Used Packaging</label><br>
                <b><?= $api->used_packaging ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">Description</label><br>
                <b><?= $api->description ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">Status</label><br>
                <b><?= $api->status ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">Created At</label><br>
                <b><?= $api->created_at ?></b>
            </div>
        </div>
        <div class="col-md-6">
            <div class="mb-3">
                <label class="form-label">Packaging Activity No.</label><br>
                <b><?= $api->packaging_activity->activity_code ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">Date</label><br>
                <b><?= $api->packaging_activity->date ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">Production Result</label><br>
                <b><?= $api->packaging_activity->production_result ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">Ruahan</label><br>
                <b><?= $api->packaging_activity->ruahan ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">Status</label><br>
                <b><?= $api->packaging_activity->status ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">Description</label><br>
                <b><?= $api->packaging_activity->description ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">Created At</label><br>
                <b><?= $api->packaging_activity->created_at ?></b>
            </div>
        </div>
        <div class="col-md-12">
            <table class="table table-bordered table-inverse table-responsive overflow-auto">
                <thead class="thead-default">
                    <tr>
                        <th>Product Activity Code</th>
                        <th>Product Code</th>
                        <th>Product Name</th>
                        <th>Sale Pricing</th>
                        <th>Date Start</th>
                        <th>Status</th>
                        <th>Keterangan</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><?= $api->packaging_activity->product_activity->activity_code?></td>
                        <td><?= $api->packaging_activity->product->product_code?></td>
                        <td><?= $api->packaging_activity->product->product_name?></td>
                        <td><?= $api->packaging_activity->product->sale_price?></td>
                        <td><?= $api->packaging_activity->product_activity->date_start?></td>
                        <td><?= $api->packaging_activity->product_activity->status?></td>
                        <td><?= $api->packaging_activity->product_activity->keterangan?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
</div>
<script>
    show_item();
    $(".select2-view").select2({
		theme: 'bootstrap-5',
		dropdownParent: $("#viewModal")
	});

</script>