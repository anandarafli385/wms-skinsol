<main class="content">
	<div class="container-fluid p-0">

		<h1 class="h3 mb-3">Labelling Activity</h1>

		<div class="row">
			<div class="col-12 col-xl-12">
				<div class="card">
					<div class="card-header">
						<h5 class="card-title">Labelling Activity
						</h5>
						<h6 class="card-subtitle text-muted">List data Labelling Activity
						</h6>
					</div>
					<div class="card-body">
						<?= $this->session->flashdata('msg') ?>
						<table class="table table-striped dataTable responsive">
							<thead>
								<tr>
									<th>No.</th>
									<th>Labelling No.</th>
									<th>Date Labelling</th>
									<th>Packaging Result</th>
									<th>Result</th>
									<th>Used Qty</th>
									<th>Release Qty</th>
									<th>Used Packaging Qty</th>
									<th>Status</th>
									<th>Desc</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php
                            $no = 1;
                            foreach ($api as $row) { ?>
								<tr>
									<td><?= $no++; ?></td>
									<td><?= $row->labelling_code ?></td>
									<td><?= $row->date ?></td>
									<td><?= $row->packaging_result.' Pcs' ?></td>
									<td><?= $row->result.' Pcs' ?></td>
									<td><?= $row->used_quantity.' Pcs' ?></td>
									<td><?= $row->release_quantity.' Pcs' ?></td>
									<td><?= $row->used_packaging.' Pcs' ?></td>
									<td><?= $row->status ?></td>
									<td><?= $row->description ?></td>
									<td class="table-action">
										<a href="#!" data-bs-toggle="modal" data-bs-target="#viewModal"
											data-id="<?= $row->id ?>"><i data-feather="eye"></i></a>
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>

		</div>

	</div>
</main>
<div class="modal fade" id="viewModal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content" style="overflow:auto">
			<div class="modal-header">
				<h5 class="modal-title">View Data Filling Activity</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div id="view_data"></div>
		</div>
	</div>
</div>
<script>

	$("#viewModal").on('shown.bs.modal', function (e) {
		var fg_id = $(e.relatedTarget).data('id');
		$.ajax({
			type: "POST",
			url: "<?= base_url() ?>admin/labelling/view",
			data: {
				id: fg_id
			}
		}).done(function (response) {
			$("#view_data").html(response);
		});
	});

</script>
