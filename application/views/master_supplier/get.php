<!-- <form action="<?= base_url() ?>admin/master_supplier/edit/<?= $get['supplier_id'] ?>" method="POST"> -->
    <div class="modal-body m-3">
        <div class="mb-3">
            <label class="form-label">Supplier Name <span class="text-danger">*</span></label>
            <input type="text" class="form-control" name="nama" placeholder="Supplier Name" value="<?= $get['nama'] ?>" required>
        </div>
        <div class="mb-3">
            <label class="form-label">Address <span class="text-danger">*</span></label>
            <textarea name="alamat" class="form-control" placeholder="Address"><?= $get['alamat'] ?></textarea>
        </div>
        <div class="row mb-3">
            <div class="col">
                <input type="text" class="form-control" name="kota" placeholder="City" value="<?= $get['kota'] ?>" required>
            </div>
            <div class="col">
                <input type="text" class="form-control" name="provinsi" placeholder="Province" value="<?= $get['provinsi'] ?>" required>
            </div>
        </div>
        <div class="row mb-3">
            <div class="col">
                <input type="text" class="form-control" name="negara" placeholder="Country" value="<?= $get['negara'] ?>" required>
            </div>
            <div class="col">
                <input type="text" class="form-control" name="kode_pos" placeholder="Postal Code" value="<?= $get['kode_pos'] ?>" required>
            </div>
        </div>
        <div class="mb-3">
            <label class="form-label">Phone <span class="text-danger">*</span></label>
            <input type="text" class="form-control" name="telp" placeholder="Phone" value="<?= $get['telp'] ?>" required>
        </div>
        <div class="mb-3">
            <label class="form-label">Fax</label>
            <input type="text" class="form-control" name="fax" placeholder="Fax" value="<?= $get['fax'] ?>">
        </div>
        <div class="mb-3">
            <label class="form-label">Email <span class="text-danger">*</span></label>
            <input type="email" class="form-control" name="email" placeholder="Email" value="<?= $get['email'] ?>" required>
        </div>
        <div class="mb-3">
            <label class="form-label">Contact Person <span class="text-danger">*</span></label>
            <input type="text" class="form-control" name="contact_person" placeholder="Contact Person" value="<?= $get['contact_person'] ?>" required>
        </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
    </div>
<!-- </form> -->