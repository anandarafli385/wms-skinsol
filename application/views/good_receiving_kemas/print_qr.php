<!DOCTYPE html>
<html>

<head>
	<title>Print Good Receiving</title>
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Inter&display=swap" rel="stylesheet">
	<style type="text/css">
		@page {
			margin: 0cm 0cm;
			font-family: 'Inter', sans-serif;
		}

		body {
			margin: 0px;
			font-size: 9px;
			padding: 4px;
		}

		.text-center {
			text-align: center;
		}

		.text-right {
			text-align: right;
		}

		.text-left {
			text-align: left;
		}

		h2 {
			margin-top: 5px;
			margin-bottom: 2px;
		}

		h3 {
			line-height: 0%;
			font-size: 18px;
		}

		span {
			font-size: 16px;
		}

		.table {
			margin: 0;
			border-collapse: collapse;
			border: 0;
		}

		.table>tbody>tr>td {
			border-top: 0;
		}

		.table>tbody>tr>.br {
			border-right: 0;
		}

		.table>tbody>tr>.bl {
			border-left: 0;
		}

		.td-valign-top>tbody>tr>td {
			vertical-align: top;
		}

		.bl {
			border-left: 1px solid #000 !important;
		}

		.bt {
			border-top: 1px solid #000 !important;
		}

		.nowrap {
			white-space: nowrap !important;
		}

		.page_break {
			page-break-after: auto;
		}

	</style>
</head>

<body>
        <?php 
        foreach($api->packaging_receipt_detail as $row){
            foreach($pkg as $list){
               if($row->packaging_id == $list->id){
         ?>
	<div class="page_break">
		<!-- QR Code (A6 Page) -->
		<table class="table td-valign-top" height="100%" width="100%" border="0" cellspacing="0" cellpadding="4">
			<tr>
				<td width="30%">
					<div class="text-center">
						<img src="<?php echo 'assets/barcode/kemas/'.$api->id.'.png' ?>" width="80%" /> <br>
						<!-- <img src="<? base_url('assets/barcode/kemas/'.$api->id.'.png') ?>" width="80%" /> <br> -->
						<center>
							<!-- <h3 style="margin-top: 3%;font-weight: bold"><?= $row->id ?></h3> -->
							<h3 style="margin-top: 3%;font-weight: bold"><?= $api->id ?></h3>
						</center>
					</div>
				</td>
				<td width="70%" style="vertical-align: top; padding-top: 5%">
					<table class="table" border="1" cellspacing="0" cellpadding="4" width="100%">
						<?php 
                                print_r('<tr><td width="40%"><span>Packaging Code</span></td><td><h4 style="font-size: 16px">'.$list->packaging_code.'</h4></td></tr>');
                                print_r('<tr><td width="40%"><span>Packaging Name</span></td><td><h4 style="font-size: 16px">'.$list->packaging_name.'</h4></td></tr>');
                                print_r('<tr><td width="40%"><span>Packaging Type</span></td><td><h4 style="font-size: 16px">'.$list->packaging_type.'</h4></td></tr>');
                                print_r('<tr><td width="40%"><span>Packaging Category</span></td><td><h4 style="font-size: 16px">'.$list->category.'</h4></td></tr>');
                                print_r('<tr><td width="40%"><span>Quantity</span></td><td><h4 style="font-size: 16px">'.$row->quantity.'</h4></td></tr>');
                                print_r('<tr><td width="40%"><span>Stock Minimum</span></td><td><h4 style="font-size: 16px">'.$list->stock_minimum.'</h4></td></tr>');
						 ?>
					</table>
				</td>
			</tr>
		</table>
	</div>
        <?php 
                }
            }
        }
     	?>
</body>

</html>
