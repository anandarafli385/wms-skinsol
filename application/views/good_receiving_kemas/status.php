<form action="<?= base_url('admin/good_receiving_kemas/update_status') ?>" method="POST">
	<div class="modal-body m-3">
		<div class="mb-3">
			<div class="form-group">
				<input type="hidden" name="id" value="<?= $get->id ?>">
			</div>
		</div>

		<div class="mb-3">
		    <label class="form-label" for="purchase_id">Receipt Code</label>
			<input type="text" class="form-control" name="rc_id" value="<?= $get->receipt_code ?>" required readonly>
		</div>

		<div class="mb-3">
		    <label class="form-label" for="material_id">Package Type</label>
			<input type="text" class="form-control" name="pkg_type" value="<?= $get->packaging_type ?>" required readonly>
		</div>

		<div class="mb-3">
		    <label class="form-label" for="batch_num">Tgl Receipt</label>
			<input type="text" class="form-control" name="tgl_rc" value="<?= $get->tanggal_recep ?>" required readonly>
		</div>

		<div class="mb-3">
		    <label class="form-label" for="batch_num">Tgl Receipt</label>
			<input type="text" class="form-control" name="supplier_code" value="<?= $get->customer_id == 0 || $get->customer_id == null ? print_r($get->supplier->supplier_code) : ($get->customer != null ? $get->customer->customer_code : "-")   ?>" required readonly>
		</div>


		<div class="mb-3">
		  <label class="form-label" for="status">Status QC Good Receiving</label>
		  <select class="form-control select2-edit" name="status" id="status">
			<option value="ok">Ok</option>
			<option value="not ok">Not Ok</option>
		  </select>
		</div>

		<div class="mb-3" id="desc" style="display: none">
		    <label class="form-label" for="desc">Description</label>
			<textarea class="form-control" name="desc"></textarea>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
		<button type="submit" class="btn btn-primary">Save changes</button>
	</div>
</form>

<script>
	$(function(){
		$(".select2-edit").select2({
			theme: 'bootstrap-5',
			dropdownParent: $("#statusModal"),
			width: 'auto'
		});

	$("#status").on('select2:select', function(e) {
		let value = e.params.data.id
		let url = "<?= base_url() ?>"
		if(value === "not ok"){
			$("#desc").css({
				"display": "block"
			})
		} else {
			value = "OK"
			$("#desc").css({
				"display": "none"
			})
		}
	})

	})
</script>
