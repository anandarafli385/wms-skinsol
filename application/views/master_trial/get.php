<div class="modal-body m-3">
    <div class="row">
        <h3><b>TRIAL REVISION DETAIL</b></h3>
        <b><?= $api_detail ?></b>
        <!-- <div class="col-12">
            <div class="row">
                <div class="col-6">
                    <div class="mb-3">
                        <label class="form-label">ID Trial Revision</label><br>
                        <b><?= $api_detail->id ?></b>
                    </div>

                    <div class="mb-3">
                        <label class="form-label">Revision No.</label><br>
                        <b><?= $api_detail->revision_num ?></b>
                    </div>

                    <div class="mb-3">
                        <label class="form-label">Keterangan</label><br>
                        <b><?= $api_detail->keterangan ?></b>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Created From</label><br>
                        <b><?= $api_detail->created_from?></b>
                    </div>

                    <div class="mb-3">
                        <label class="form-label">Created To</label><br>
                        <b><?= $api_detail->created_to ?></b>
                    </div>
                </div>
                <div class="col-6">
                    <div class="mb-3">
                        <label class="form-label">Prosedur Revisi</label><br>
                        <b><?= $api_detail->prosedur ?></b>
                    </div>
                </div>
            </div>
        </div> -->

        <h3><b>TRIAL DETAIL</b></h3>

        <!-- <div class="row">
            <div class="col-6">
                <div class="mb-3">
                    <label class="form-label">Trial No.</label><br>
                    <b><?= $api_detail->trial->trial_num ?></b>
                </div>

                <div class="mb-3">
                    <label class="form-label">ID PO Customer</label><br>
                    <b><?= $api_detail->trial->po_customer_id ?></b>
                </div>

                <div class="mb-3">
                    <label class="form-label">ID PO Customer Detail</label><br>
                    <b><?= $api_detail->trial->po_customer_detail_id ?></b>
                </div>

                <div class="mb-3">
                    <label class="form-label">PO Num</label><br>
                    <b><?= $api_detail->trial->po_customer->po_num ?></b>
                </div>

                <div class="mb-3">
                    <label class="form-label">PO Status</label><br>
                    <b><?= $api_detail->trial->po_customer->status ?></b>
                </div>
            </div>
            <div class="col-6">
                <div class="mb-3">
                    <label class="form-label">Customer Code</label><br>
                    <b><?= $api_detail->trial->po_customer->customer->customer_code ?></b>
                </div>

                <div class="mb-3">
                    <label class="form-label">Customer Name</label><br>
                    <b><?= $api_detail->trial->po_customer->customer->customer_name ?></b>
                </div>

                <div class="mb-3">
                    <label class="form-label">Customer Brand</label><br>
                    <b><?= $api_detail->trial->po_customer->customer->customer_brand ?></b>
                </div>

                <div class="mb-3">
                    <label class="form-label">Customer Email</label><br>
                    <b><?= $api_detail->trial->po_customer->customer->customer_email ?></b>
                </div>

                <div class="mb-3">
                    <label class="form-label">Customer Address</label><br>
                    <b><?= $api_detail->trial->po_customer->customer->customer_address ?></b>
                </div>
            </div>
        </div> -->
    </div>

</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
</div>
