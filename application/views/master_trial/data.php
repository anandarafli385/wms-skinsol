


<main class="content">
	<div class="container-fluid p-0">

		<h1 class="h3 mb-3">Master Trial</h1>

		<div class="row">
			<div class="col-12 col-xl-12">
				<div class="card">
					<div class="card-header">
						<h5 class="card-title">Master Trial
							<!-- <div class="float-end">
								<button type="button" class="btn btn-primary" data-bs-toggle="modal"
									data-bs-target="#addModal"><i class="fa fa-plus"></i> Add Trial</button>
							</div> -->
						</h5>
						<h6 class="card-subtitle text-muted">List data master Trial
						</h6>
					</div>
					<div class="card-body">
						<?= $this->session->flashdata('msg') ?>
						<table class="table table-striped dataTable responsive">
							<thead>
								<tr>
									<th>No.</th>
									<th>Trial No.</th>
									<th>PO No.</th>
									<th>Willingness </th>
									<th>Keterangan</th>
									<th>Type</th>
									<th>Status</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php
                            $no = 1;
                            foreach ($api as $row) { ?>
								<tr>
									<td><?= $no++; ?></td>
									<td><?= $row->trial_num ?></td>
									<td><?= $row->po_customer->po_num ?></td>
									<td><?= $row->willingness ?></td>
									<td><?= $row->keterangan ?></td>
									<td><?= $row->type ?></td>
									<td><?= $row->po_customer->status ?></td>
									<td class="table-action">
										<a href="#!" data-bs-toggle="modal" data-bs-target="#detailModal" data-id="<?= $row->id ?>"><i data-feather="eye"></i></a>
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>

		</div>

	</div>
</main>
<div class="modal fade" id="detailModal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Detail Data Trial</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div id="edit_data"></div>
		</div>
	</div>
</div>
<script>
	$("#detailModal").on('shown.bs.modal', function (e) {
		var id = $(e.relatedTarget).data('id');
		$.ajax({
			type: "POST",
			url: "<?= base_url() ?>admin/master_trial/get",
			data: {
				id: id
			}
		}).done(function (response) {
			$("#edit_data").html(response);
		});
	});


</script>
