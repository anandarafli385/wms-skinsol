<div class="modal-body m-3">
	<?php
		$pembuat = $this->db->where([
			'user_id' => $get['user_id']
		])->get('tbl_user')->row();

		$finish = $this->db->where([
			'user_id' => $get['finished_by']
		])->get('tbl_user')->row();
	?>
	<div class="mb-3">
        <label class="form-label">Work Order No.</label><br>
        <b><?= $get['no_work_order'] ?></b>
    </div>

	<div class="mb-3">
        <label class="form-label">Created by</label><br>
        <b><?= $pembuat->name ?></b>
    </div>

	<div class="mb-3">
        <label class="form-label">Approved by</label><br>
        <b><?= $get['approved_by'] ?></b>
    </div>

	<div class="mb-3">
        <label class="form-label">Date Created</label><br>
        <b><?= dateID($get['date']) ?></b>
    </div>

	<div class="mb-3">
        <label class="form-label">Finished by</label><br>
        <b><?= $finish->name ?></b>

    </div>

	<div class="mb-3">
        <label class="form-label">Date Finished</label><br>
        <b><?= dateID($get['finished_at']) ?></b>
    </div>
	
    <div class="mb-3">
        <label class="form-label">FG List</label>
        <div class="table-responsive">
            <table class="table table-sm table-striped">
                <thead>
                    <tr>
                        <th>Material</th>
                        <th>Description</th>
                        <th>Unit</th>
                        <th>Qty</th>
                        <th>Good</th>
                        <th>Reject</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
					$index = 0;
					foreach ($detail as $row) { ?>
                        <tr>
                            <td><?= $row['nama'] ?></td>
                            <td><?= $row['deskripsi'] ?></td>
                            <td><?= $row['unit'] ?></td>
                            <td><?= $qty[$index] ?></td>
                            <td><?= $good[$index] ?></td>
                            <td>
								<?php if($reject[$index] != 0): ?>
								<span class="text-danger"><?= $reject[$index] ?></span>
								<?php else: ?>
									<?= $reject[$index] ?>
								<?php endif; $index++; ?>
							</td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    
	<div class="mb-3">
		<label class="form-label">Operation Log</label>
		<div class="table-responsive">
			<table class="table table-sm table-striped">
				<thead>
					<tr>
						<th>No</th>
						<th>Material</th>
						<th>Good</th>
						<th>Reject</th>
						<th>Created at</th>
					</tr>
				</thead>

				<tbody>
					<?php 
						$no = 1;
						foreach($delivery as $row): 
							$detail_wo = $this->sql->select_table('tbl_work_order_detail', [
								'work_order_detail_id' => $row->work_order_detail_id
							])->row();

							$fg = $this->sql->select_table('tbl_material', [
								'material_no' => $detail_wo->fg_id
							])->row();
						?>
					<tr>
						<td><?= $no++ ?></td>
						<td><?= $fg->nama ?></td>
						<td><?= $row->good ?></td>
						<td><?= $row->reject ?></td>
						<td><?= $row->created_at ?></td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
</div>
