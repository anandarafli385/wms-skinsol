<form action="<?= base_url() ?>admin/work_order/edit/<?= $get['work_order_id'] ?>" method="POST">
    <div class="modal-body m-3">
        <!-- <div class="mb-3">
            <label class="form-label">Customer Name <span class="text-danger">*</span></label>
            <select name="customer_id" class="form-control select2" required>
                <option value="">- Select One -</option>
                <?php foreach ($customer as $row) { ?>
                    <option value="<?= $row['customer_id'] ?>" <?= $row['customer_id'] == $get['customer_id'] ? 'selected' : ''; ?>><?= $row['nama'] ?></option>
                <?php } ?>
            </select>
        </div> -->

		<div class="mb-3">
            <label class="form-label">Work Order No.</label>
            <input type="text" class="form-control" name="no_work_order" placeholder="Work Order No" value="<?= $get['no_work_order'] ?>" required>
        </div>

        <div class="mb-3">
            <label class="form-label">Date</label>
            <input type="date" class="form-control" name="date" placeholder="Date" value="<?= $get['date'] ?>" required>
        </div>
        <div class="mb-3">
            <label class="form-label">FG List</label>
            <div class="table-responsive">
                <table class="table table-sm table-striped">
                    <thead>
                        <tr>
                            <th width="60%">FG</th>
                            <th width="15%">Qty</th>
                            <th width="15%">Unit</th>
                            <th width="10%"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (count($detail) > 0) {
							$first = true;
                            foreach ($detail as $res) {
                                $description = '-';
                                $unit = '-';
                        ?>
                                <tr>
                                    <td>
                                        <select name="fg_id[]" class="form-control select2-edit" required>
                                            <option value="">- Select One -</option>
                                            <?php foreach ($fg as $row) { ?>
                                                <option value="<?= $row['material_no'] ?>" data-description="<?= $row['deskripsi'] ?>" data-unit="<?= $row['unit'] ?>" 
                                                <?php if($row['material_no'] == $res['fg_id']) {
                                                        echo 'selected';
                                                        $description = $row['deskripsi'];
                                                        $unit = $row['unit']; 
                                                    }; ?>><?= $row['material_no'] . ' - ' . $row['nama'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="number" class="form-control" name="qty[]" placeholder="Qty" value="<?=$res['qty']?>" required>
                                    </td>
                                    <td id="fg_unit"><?=$unit?></td>
                                    <td>
										<?php if ($first) : ?>
											<button type="button" onclick="addrow_edit(this)" class="btn btn-success">+</button>
										<?php else: ?>
											<button type="button" class="btn btn-danger" onclick="deleterow(this)">-</button>
										<?php endif; ?>
									</td>
                                </tr>
                            <?php 
								$first = false;
							}
                        } else { ?>
                            <tr>
                                <td>
                                    <input type="number" class="form-control" name="qty_box[]" placeholder="Qty Box" required>
                                </td>
                                <td>
                                    <select name="fg_id[]" class="form-control select2_edit" required>
                                        <option value="">awdad</option>
                                        <?php foreach ($fg as $row) { ?>
                                            <option value="<?= $row['fg_id'] ?>" data-description="<?= $row['deskripsi'] ?>" data-unit="<?= $row['unit'] ?>"><?= $row['material_no'] . ' - ' . $row['nama'] ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                                <td>
                                    <input type="number" class="form-control" name="qty[]" placeholder="Qty" required>
                                </td>
                                <td id="fg_unit">-</td>
                                <td><button type="button" onclick="addrow(this)" class="btn btn-success">+ </button></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
    </div>
</form>

<script>
	$(function(){
		$(".select2-edit").select2({
			theme: 'bootstrap-5',
			dropdownParent: $("#editModal"),
			width: 'auto'
		});
	});
</script>
