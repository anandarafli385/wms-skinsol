<main class="content">
	<div class="container-fluid p-0">

		<h1 class="h3 mb-3">Approval Analisis</h1>

		<div class="row">
			<div class="col-12 col-xl-12">
				<div class="card">
					<div class="card-header">
						<h5 class="card-title">Approval Analisis
							<?php if($this->session->level != 4 ): ?>
							<div class="float-end">
								<button type="button" class="btn btn-primary" data-bs-toggle="modal"
									data-bs-target="#addModal"><i class="fa fa-plus"></i> Add Data</button>
							</div>
						<?php endif; ?>
						</h5>
						<h6 class="card-subtitle text-muted">List data Approval Analisis</h6>
					</div>
					<div class="card-body">
						<?= $this->session->flashdata('msg') ?>
						<table class="table table-striped dataTable">
							<thead>
								<tr>
									<th>No.</th>
									<th>Approval Analisis No.</th>
									<th>Description</th>
									<th>Date</th>
									<th>Total FG</th>
									<th>Percentage</th>
									<th>Status Approval Analisis</th>
									<th>Status Approval</th>
									<th>Created By</th>
									<th>Approved By</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php
                            $no = 1;
							$nomor = 1;
                            foreach ($get as $row) { 
								$sql_persentase = "SELECT sum(qty) as total_qty, sum(good) as delivered, (sum(good) * 100) / sum(qty) as percentage  from tbl_work_order_detail where work_order_id = '".$row['work_order_id'] ."'";

								$persentase = $this->sql->manual_query($sql_persentase)->row();
								?>
								<tr>
									<td><?= $no++; ?></td>
									<td><?= $row['no_work_order']; ?></td>
									<td><?= $row['description']; ?></td>
									<td><?= date('D, d M Y', strtotime($row['date'])) ?></td>
									<td><?= $row['count_fg'] ?></td>
									<td><?= $persentase->percentage . '%' ?></td>
									<td class="text-center">
										<?php if($row['status'] == 'on progress'): ?>
											<span class="badge bg-warning"><?= $row['status'] ?></span>
										<?php elseif($row['status'] == 'not progress'): ?>
											<span class="badge bg-danger"><?= $row['status'] ?></span>
										<?php elseif($row['status'] == 'finished'): ?>
											<span class="badge bg-success"><?= $row['status'] ?></span>
										<?php endif; ?>
									</td>
									<td class="text-center">
										<?php if($row['status_approval'] != 'approved'): ?>
											<span class="badge bg-danger"><?= $row['status_approval'] ?></span>
										<?php else: ?>
											<span class="badge bg-success"><?= $row['status_approval'] ?></span>
										<?php endif; ?>
									</td>
									<td><?= $row['name'] ?></td>
									<td><?= $row['approved_by'] ?></td>
									<td class="text-center">
										<a href="<?= base_url() ?>admin/work_order/print/<?= $row['work_order_id'] ?>" title="print Approval Analisis" target="_blank"><i data-feather="printer"></i></a>

										<!-- <a href="<?= base_url() ?>admin/work_order/scale_label/<?= $row['work_order_id'] ?>" title="Scale Label" target="_blank"><i data-feather="file-text"></i></a> -->

										<?php if($row['status'] != 'finished'): ?>
										<a href="#!" data-bs-toggle="modal" 		
											data-bs-target="#statusModal"
											title="update status"
											data-id="<?= $row['work_order_id'] ?>"><i data-feather="package"></i></a>
										
											<a href="#!" data-bs-toggle="modal" data-bs-target="#detailModal"
											title="detail"
											data-id="<?= $row['work_order_id'] ?>"><i data-feather="book"></i></a>
										<?php else: ?>
											<a href="#!" data-bs-toggle="modal" data-bs-target="#detailFinishedModal"
											title="detail"
											data-id="<?= $row['work_order_id'] ?>"><i data-feather="book"></i></a>
										<?php endif; ?>
										

										<?php if($row['status'] != 'finished'): ?>
										<a href="#!" data-bs-toggle="modal" data-bs-target="#editModal"
											title="edit"
											data-id="<?= $row['work_order_id'] ?>"><i data-feather="edit"></i></a>
										<?php endif; ?>


										<a href="#!" onclick="delete_data('<?= $row['work_order_id'] ?>')" title="delete"><i data-feather="trash-2"></i></a>

									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>

		</div>

	</div>
</main>

<div class="modal fade" id="addModal" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Add Data Approval Analisis</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<form action="<?= base_url() ?>admin/work_order/add" method="POST">
				<div class="modal-body m-3">
					<div class="mb-3">
						<div class="form-group">
						  <label for="no_wo">No. Approval Analisis <span class="text-danger">*</span></label>
						  <input type="text" name="no_wo" id="no_wo" class="form-control" placeholder="No. Approval Analisis" aria-describedby="">
						</div>
					</div>
					<!-- <div class="mb-3">
						<label class="form-label">Customer Name <span class="text-danger">*</span></label>
						<select name="customer_id" class="form-control select2" required>
							<option value="">- Select One -</option>
							<?php foreach ($customer as $row) { ?>
							<option value="<?= $row['customer_id'] ?>"><?= $row['nama'] ?></option>
							<?php } ?>
						</select>
					</div> -->
					<div class="mb-3">
						<label class="form-label">Date <span class="text-danger">*</span></label>
						<input type="date" class="form-control" name="date" placeholder="Date" required>
					</div>

					<div class="mb-3">
						<label class="form-label">Description</label>
						<textarea name="description" class="form-control" placeholder="Approval Analisis Description..." id="description" cols="30" rows="3"></textarea>
					</div>


					<div class="mb-3">
						<label class="form-label">FG List <span class="text-danger">*</span></label>

						<div class="fg_list" id="fg-list">
							<div class="row">
								<div class="col-6">
									<select name="fg_id[]" class="form-control select2 fg_id" data-nomor="0" required>
										<option value="">- Select One -</option>
										<?php foreach ($fg as $row) { ?>
										<option value="<?= $row['material_no'] ?>" data-description="<?= $row['inventory_part_type'] ?>"
											data-unit="<?= $row['unit'] ?>"><?= $row['material_no'] . ' - ' . $row['nama'] ?></option>
										<?php } ?>
									</select>
								</div>

								<div class="col-2">
									<h4 id="fg-description-0" class="mt-2">-</h4>
								</div>
								<div class="col-2">
									<input type="number" class="form-control form-control-lg" name="qty[]" placeholder="Qty" id="qty-0">
								</div>

								<div class="col-2">
									<button type="button" id="btnTambahFG" class="btn btn-success">+</button>
								</div>

								<div class="col-12 mt-3 kartu" id="kartu_0" style="display: none;">
									<h3>Material</h3>
									<div id="card-body-0">
									</div>

									<div id="table-responsive-0" class="table-responsive">
									<table id="table-0" class="table table-striped table-inverse">
										<thead class="thead-inverse">
											<tr>
												<th>#</th>
												<th>Material name</th>
												<th>Desc</th>
												<th>Material perItem</th>
												<th>Material needed</th>
												<th>Material at Warehouse</th>
												<th>Material left</th>
											</tr>
											</thead>
											<tbody id="tbody-0">
												
											</tbody>
									</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" id="tombol_simpan">Save changes</button>
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="editModal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Edit Data Approval Analisis</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div id="edit_data"></div>
		</div>
	</div>
</div>

<div class="modal fade" id="detailModal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Detail Data Approval Analisis</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div id="detail_data"></div>
		</div>
	</div>
</div>

<div class="modal fade" id="detailFinishedModal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Detail Data Approval Analisis</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div id="detail_finished_data"></div>
		</div>
	</div>
</div>

<div class="modal fade" id="statusModal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Status Data Approval Analisis</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div id="status_data"></div>
		</div>
	</div>
</div>

<script>
	

	$("#editModal").on('shown.bs.modal', function (e) {
		var id = $(e.relatedTarget).data('id');
		$.ajax({
			type: "POST",
			url: "<?= base_url() ?>admin/work_order/get",
			data: {
				id: id
			}
		}).done(function (response) {
			$("#edit_data").html(response);
		});
	});

	$("#detailModal").on('shown.bs.modal', function (e) {
		var id = $(e.relatedTarget).data('id');
		$.ajax({
			type: "POST",
			url: "<?= base_url() ?>admin/work_order/detail",
			data: {
				id: id
			}
		}).done(function (response) {
			$("#detail_data").html(response);
		});
	});

	$("#detailFinishedModal").on('shown.bs.modal', function (e) {
		var id = $(e.relatedTarget).data('id');
		$.ajax({
			type: "POST",
			url: "<?= base_url() ?>admin/work_order/detail_finished",
			data: {
				id: id
			}
		}).done(function (response) {
			$("#detail_finished_data").html(response);
		});
	});

	$("#statusModal").on('shown.bs.modal', function (e) {
		var id = $(e.relatedTarget).data('id');
		$.ajax({
			type: "POST",
			url: "<?= base_url() ?>admin/work_order/status",
			data: {
				id: id
			}
		}).done(function (response) {
			$("#status_data").html(response);
		});
	});

	let x = 1;
	let i = 1;


	$(".fg_id").on('select2:select', function (e) {
		var description = $(this).find(":selected").data('description');
		var unit = $(this).find(":selected").data('unit');
		var nomor = $(this).data('nomor');
		let value = e.params.data.id

        let nomor_tbl = 1;
		
		console.log(value)

		if (e.params.data.id == "") {
			$('div#kartu_' + nomor).hide()

		} else {
			let base_url = '<?= base_url() ?>'
			let val = 0;

			console.log(value)


			$.ajax({
				method: 'POST',
				url: base_url + 'admin/work_order/check_stock/' + value,
				success: (res) => {
					console.log(res)
					$('#card-body-' + nomor).empty()
					$('#fg-description-' + nomor).text(description)

					let jml_material = res.data.length
					$("#tbody-"+nomor).empty()

					res.data.forEach(element => {

						let txt_html = `
								<tr>
									<td>`+nomor_tbl+`</td>
									<td>`+element.nama_material+`</td>
									<td>`+element.deskripsi+`</td>
									<td><span class="item_`+nomor+`_` + i + `">`+element.qty+`</span></td>
									<td><span class="need tbl_need_`+nomor+`_`+i+`">`+ 0 +`</span></td>
									<td><span class="total_`+nomor+`_` + i + `">`+element.sisa+`</span></td>
									<td><span class="sisa tbl_sisa_`+nomor+`_`+i+`">`+element.sisa+`</span></td>
								</tr>
							`;


						// console.log(txt_html)
						$("#tbody-"+nomor).append(txt_html)

						nomor_tbl++;
						i++;

					});



					$("#qty-" + nomor).keyup(function () {
						val = $(this).val()
						console.log(val)
						let cukup = true;

						for (let y = 1; y <= jml_material; y++) {
							let gudang = $('.total_'+nomor+`_` + y).text()
							let each = $('.item_'+nomor+`_` + y).text()
							let need = val * each
							let sisa = gudang - need


							if(sisa < 0){
								cukup = false
								$(".tbl_sisa_"+nomor+`_` + y).css('color', 'red');
							}else{
								$(".tbl_sisa_"+nomor+`_` + y).css('color', 'black');
							}

							$(".tbl_need_"+nomor+`_` + y).text(need)
							$(".tbl_sisa_"+nomor+`_` + y).text(sisa)
						}

						if(!cukup){
							$("#tombol_simpan").prop('disabled', true);
						}else{
							$("#tombol_simpan").prop('disabled', false);
						}
					});
				}
			})

			$('div#kartu_' + nomor).show()
		}
	});

	function delete_data(id) {
		Swal.fire({
			title: 'Konfirmasi ?',
			text: "Apakah kamu yakin ?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#28a745',
			cancelButtonColor: '#dc3545',
			confirmButtonText: 'Yes!',
			cancelButtonText: 'No!',
			showLoaderOnConfirm: true,
			preConfirm: function () {
				return new Promise(function (resolve, reject) {
					$.ajax({
						type: 'POST',
						url: "<?= base_url() ?>admin/work_order/delete",
						data: {
							id: id
						}
					}).done(function (msg) {
						if (msg == "ok") {
							swal.fire("OK!", "Data berhasil dihapus!", "success").then(
								function () {
									location.reload();
								})
						} else {
							swal.fire("Gagal!", msg, "error").then(function () {
								location.reload();
							})
						}
					})
				})
			},
			allowOutsideClick: () => !Swal.isLoading()
		})
	}

	function addrow(btn) {
		var row_copy = `
			<td>
				<select name="fg_id[]" class="form-control select2 fg_id" required>
					<option value="">- Select One -</option>
					<?php foreach ($fg as $row) { ?>
						<option value="<?= $row['material_no'] ?>" data-description="<?= $row['inventory_part_type'] ?>" data-unit="<?= $row['unit'] ?>"><?= $row['material_no'] . ' - ' . $row['nama'] ?></option>
					<?php } ?>
				</select>
			</td>
			<td id="fg_description">-</td>
			<td>
				<input type="number" class="form-control" name="qty[]" placeholder="Qty" required>
			</td>
			<td id="fg_unit">-</td>
			<td></td>
		`;
		$(btn).closest("tbody").append("<tr>" + row_copy + "</tr>");
		var btn_delete = '<button type="button" class="btn btn-danger" onclick="deleterow(this)">-</button>';
		$(btn).closest("tbody").find("tr:last").find("td:last").html(btn_delete);

		$(".select2").select2({
			theme: 'bootstrap-5',
			dropdownParent: $("#addModal")
		});
	}

	$("#btnTambahFG").click(function () {
		let n = x
		console.log(n)

		$('#fg-list').append(`
		<div class="row mt-3 baris-`+n+`">
				<div class="col-6">
					<select name="fg_id[]" class="form-control select2-`+n+` fg_id" data-nomor="`+n+`" required>
						<option value="">- Select One -</option>
						<?php foreach ($fg as $row) { ?>
							<option value="<?= $row['material_no'] ?>" data-description="<?= $row['inventory_part_type'] ?>"
											data-unit="<?= $row['unit'] ?>"><?= $row['material_no'] . ' - ' . $row['nama'] ?></option>
						<?php } ?>
					</select>
				</div>

				<div class="col-2">
					<h4 class="mt-2" id="fg-description-`+n+`">-</h4>
				</div>
				<div class="col-2">
					<input type="number" class="form-control" name="qty[]" placeholder="Qty" id="qty-`+n+`">
				</div>

				<div class="col-2">
					<button type="button" id="btn_remove_` + n + `" class="btn btn-danger">-</button>
				</div>

				<div class="col-12 mt-3 kartu" id="kartu_`+ n +`" style="display: none;">
					<h3>Material</h3>
					<div id="card-body-`+ n +`">
					</div>

					<div id="table-responsive-`+ n +`" class="table-responsive">
					<table id="table-`+ n +`" class="table table-striped table-inverse">
						<thead class="thead-inverse">
							<tr>
								<th>#</th>
								<th>Material name</th>
								<th>Desc</th>
								<th>Material perItem</th>
								<th>Material needed</th>
								<th>Material at Warehouse</th>
								<th>Material left</th>
							</tr>
							</thead>
							<tbody id="tbody-`+ n +`">
												
							</tbody>
					</table>
					</div>
				</div>
			</div>
		`)


		$("#btn_remove_" + n).click(function () {
			console.log('tombol hapus ke ' + n)
			$(".baris-" + n).remove()
		})

		$(".fg_id").on('change', function (e) {
			let no = $(this).data('nomor')
			let description = $(this).find(":selected").data('description');
			let value = $(this).val()

			console.log("nomor = "+no)
			console.log("value = "+value)


			// $(this).closest('tr').children('td.fg_description').text(description);

			

			$("#table-"+no).css({
				'display' : 'block'
			})

			if (value == "") {
				$('div#kartu_' + no).hide()
				$('#table-'+no).hide()
			} else {
				let base_url = '<?= base_url() ?>'
				let val = 0;

				console.log(no)

				$.ajax({
				method: 'POST',
				url: base_url + 'admin/work_order/check_stock/' + value,
				success: (res) => {

					$('#card-body-' + no).empty()
					$('#fg-description-' + no).text(description)

					let jml_bahan = res.data.length
					$("#tbody-"+no).empty()

					let nomor_tbl = 1;
					i = 1;
					res.data.forEach(element => {

						let txt_html = `
								<tr>
									<td>`+nomor_tbl+`</td>
									<td>`+element.nama_material+`</td>
									<td>`+element.deskripsi+`</td>
									<td><span class="item_`+ no + `_` + i + `">`+element.qty+`</span></td>
									<td><span class="need tbl_need_`+ no + `_`+i+`">`+ 0 +`</span></td>
									<td><span class="total_`+ no + `_` + i + `">`+element.sisa+`</span></td>
									<td><span class="sisa tbl_sisa_`+ no + `_`+i+`">`+element.sisa+`</span></td>
								</tr>
							`;


						// console.log(txt_html)
						$("#tbody-"+no).append(txt_html)

						nomor_tbl++;
						i++;
					});



					$("#qty-" + no).keyup(function () {
						let nilai_qty = $(this).val()
						console.log("jml material = "+jml_bahan)
						let cukup = true;

						for (let y = 1; y <= jml_bahan; y++) {
							let gudang = $('.total_'+ no + '_' + y).text()
							let each = $('.item_'+ no + '_' + y).text()
							let need = nilai_qty * each
							let sisa = gudang - need

							console.log('gudang = '+gudang)
							console.log('each = '+each)


							if(sisa < 0){
								cukup = false
								$(".tbl_sisa_"+ no + `_` + y).css('color', 'red');
							}else{
								$(".tbl_sisa_"+ no + `_` + y).css('color', 'black');
							}

							$(".tbl_need_"+ no + `_` + y).text(need)
							$(".tbl_sisa_"+ no + `_` + y).text(sisa)
						}

						if(!cukup){
							$("#tombol_simpan").prop('disabled', true);
						}else{
							$("#tombol_simpan").prop('disabled', false);
						}
					});
				}
			})
			}
			$('div#kartu_' + no).show()
		})

		$(".select2-" + n).select2({
			theme: 'bootstrap-5',
			dropdownParent: $("#addModal")
		});
		
		x++;
	})

	function addrow_edit(btn) {
		var row_copy = `
			<td>
				<select name="fg_id[]" class="form-control select2-edit" required>
					<option value="">- Select One -</option>
					<?php foreach ($fg as $row) { ?>
						<option value="<?= $row['material_no'] ?>" data-description="<?= $row['deskripsi'] ?>" data-unit="<?= $row['unit'] ?>"><?= $row['material_no'] . ' - ' . $row['nama'] ?></option>
					<?php } ?>
				</select>
			</td>
			<td>
				<input type="number" class="form-control" name="qty[]" placeholder="Qty" required>
			</td>
			<td id="fg_unit">g</td>
			<td></td>
		`;
		$(btn).closest("tbody").append("<tr>" + row_copy + "</tr>");
		var btn_delete = '<button type="button" class="btn btn-danger" onclick="deleterow(this)">-</button>';
		$(btn).closest("tbody").find("tr:last").find("td:last").html(btn_delete);

		$(".select2-edit").select2({
			theme: 'bootstrap-5',
			dropdownParent: $("#editModal")
		});
	}

	function deleterow(btn) {
		$(btn).closest("tr").remove();
	}

</script>
