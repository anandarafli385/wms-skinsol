<div class="modal-body m-3">
    <div class="row">
        <div class="col-md-6">
            <div class="mb-3">
                <label class="form-label">Fillling No.</label><br>
                <b><?= $api->activity_code ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">Date</label><br>
                <b><?= $api->date ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">Packaging Result</label><br>
                <b><?= $api->packaging_result ?></b>
            </div>            
            <div class="mb-3">
                <label class="form-label">Production Result</label><br>
                <b><?= $api->production_result ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">Used Quantity</label><br>
                <b><?= $api->used_quantity ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">Release Quantity</label><br>
                <b><?= $api->release_quantity ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">Ruahan</label><br>
                <b><?= $api->ruahan ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">Status</label><br>
                <b><?= $api->status ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">Created At</label><br>
                <b><?= $api->created_at ?></b>
            </div>
        </div>
        <div class="col-md-6">
            <div class="mb-3">
                <label class="form-label">Product Activity No.</label><br>
                <b><?= $api->product_activity->activity_code ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">Date Start</label><br>
                <b><?= $api->product_activity->date_start ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">Status</label><br>
                <b><?= $api->product_activity->status ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">Keterangan</label><br>
                <b><?= $api->product_activity->keterangan ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">PO Num</label><br>
                <b><?= $api->product_activity->po_product->po_num ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">Date</label><br>
                <b><?= $api->product_activity->po_product->date ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">Date End</label><br>
                <b><?= $api->product_activity->po_product->date_end ?></b>
            </div>
        </div>
        <div class="col-md-12">
            <table class="table table-bordered table-inverse table-responsive overflow-auto">
                <thead class="thead-default">
                    <tr>
                        <th>Product Code</th>
                        <th>Product Name</th>
                        <th>Sale Pricing</th>
                        <th>Customer Code</th>
                        <th>Customer Name</th>
                        <th>Customer Brand</th>
                        <th>Customer Address</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><?= $api->product->product_code?></td>
                        <td><?= $api->product->product_name?></td>
                        <td><?= $api->product->sale_price?></td>
                        <td><?= $api->product_activity->po_product->customer->customer_code?></td>
                        <td><?= $api->product_activity->po_product->customer->customer_name?></td>
                        <td><?= $api->product_activity->po_product->customer->customer_brand?></td>
                        <td><?= $api->product_activity->po_product->customer->customer_address?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
</div>
<script>
    show_item();
    $(".select2-view").select2({
		theme: 'bootstrap-5',
		dropdownParent: $("#viewModal")
	});

</script>