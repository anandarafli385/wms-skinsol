<!DOCTYPE html>
<html>

<head>
    <title>Scale Label of <?= $wo->no_work_order ?></title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inter&display=swap" rel="stylesheet">
    <style type="text/css">
        @page {
            margin: 0cm 0cm;
            font-family: 'Inter', sans-serif;
        }

        body {
            margin: 0px;
            font-size: 9px;
            padding: 4px;
        }

        .text-center {
            text-align: center;
        }

        .text-right {
            text-align: right;
        }

        .text-left {
            text-align: left;
        }

        h2 {
            margin-top: 5px;
            margin-bottom: 2px;
        }

        .table {
            margin: 0;
            border-collapse: collapse;
            border: 0;
        }

        .table>tbody>tr>td {
            border-top: 0;
        }

        .table>tbody>tr>.br {
            border-right: 0;
        }

        .table>tbody>tr>.bl {
            border-left: 0;
        }

        .td-valign-top>tbody>tr>td {
            vertical-align: top;
        }

        .bl {
            border-left: 1px solid #000 !important;
        }

        .bt {
            border-top: 1px solid #000 !important;
        }

        .nowrap {
            white-space: nowrap !important;
        }

        .page_break {
            page-break-after: auto;
        }
    </style>
</head>

<body>
    <?php foreach ($get as $row) { ?>
        <div class="page_break">
            <table class="table td-valign-top" width="100%" border="0" cellspacing="0" cellpadding="6">
                <tbody>
                    <tr>
                        <td class="text-center">
                            <img src="data:image/png;base64,<?= set_barcode($wo->work_order_id . '-' . $row['kode_material'] ) ?>" height="50"><br>
                            <b><?= $wo->no_work_order. '-' . $row['kode_material'] ?></b>
                        </td>
                    </tr>
                </tbody>
            </table>
            <hr>
            <table class="table td-valign-top" width="100%" cellspacing="0" cellpadding="6">
                <tr>
                    <td style="width: 50%">
                        <table class="table td-valign-top" width="100%" cellspacing="0">
							<tr>
                                <td>No. WO</td>
                                <td>:</td>
                                <td><?= $wo->no_work_order ?></td>
                            </tr>
							<tr>
                                <td>Date</td>
                                <td>:</td>
                                <td><?= date('d M Y', strtotime($wo->date)) ?></td>
                            </tr>
                        </table>
                    </td>
					<td style="width: 50%">
                        <table class="table td-valign-top" width="100%" cellspacing="0">
							<tr>
                                <td>Material</td>
                                <td>:</td>
                                <td><?= $row['nama'] ?></td>
                            </tr>
                            <tr>
                                <td>Qty</td>
                                <td>:</td>
                                <td><?= $row['qty'] . ' ' . $row['unit'] ?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    <?php } ?>
</body>

</html>
