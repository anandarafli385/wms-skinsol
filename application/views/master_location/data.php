<main class="content">
	<div class="container-fluid p-0">

		<h1 class="h3 mb-3">Master Location</h1>

		<div class="row">
			<div class="col-12 col-xl-12">
				<div class="card">
					<div class="card-header">
						<h5 class="card-title">Master Location
							<div class="float-end">
								<button type="button" class="btn btn-primary" data-bs-toggle="modal"
									data-bs-target="#addModal"><i class="fa fa-plus"></i> Add Location</button>
							</div>
						</h5>
						<h6 class="card-subtitle text-muted">List data master location
						</h6>
					</div>
					<div class="card-body">
						<?= $this->session->flashdata('msg') ?>
						<table class="table table-striped dataTable">
							<thead>
								<tr>
									<th>No.</th>
									<th>Location Name</th>
									<!-- <th>Category</th> -->
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php
                            $no = 1;
                            foreach ($get as $row) { ?>
								<tr>
									<td><?= $no++; ?></td>
									<td><?= $row['location_name'] ?></td>
									<!-- <td><?= $row['category'] ?></td> -->
									<td class="table-action">
										<a href="#!" data-bs-toggle="modal" data-bs-target="#editModal"
											data-id="<?= $row['location_id'] ?>"><svg xmlns="http://www.w3.org/2000/svg"
												width="24" height="24" viewBox="0 0 24 24" fill="none"
												stroke="currentColor" stroke-width="2" stroke-linecap="round"
												stroke-linejoin="round" class="feather feather-edit-2 align-middle">
												<path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z">
												</path>
											</svg></a>
										<a href="#!" onclick="delete_data('<?= $row['location_id'] ?>')"><svg
												xmlns="http://www.w3.org/2000/svg" width="24" height="24"
												viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
												stroke-linecap="round" stroke-linejoin="round"
												class="feather feather-trash align-middle">
												<polyline points="3 6 5 6 21 6"></polyline>
												<path
													d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2">
												</path>
											</svg></a>
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>

		</div>

	</div>
</main>
<div class="modal fade" id="addModal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Add Data Location</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<form action="<?= base_url() ?>admin/master_location/add" method="POST">
				<div class="modal-body m-3">
					<div class="mb-3">
						<label class="form-label">Location Name <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="location_name" placeholder="Location Name"
							required>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Save changes</button>
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="modal fade" id="editModal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Edit Data Location</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div id="edit_data"></div>
		</div>
	</div>
</div>
<script>
	$("#editModal").on('shown.bs.modal', function (e) {
		var id = $(e.relatedTarget).data('id');
		$.ajax({
			type: "POST",
			url: "<?= base_url() ?>admin/master_location/get",
			data: {
				id: id
			}
		}).done(function (response) {
			$("#edit_data").html(response);
		});
	});

	function delete_data(id) {
		Swal.fire({
			title: 'Konfirmasi ?',
			text: "Apakah kamu yakin ?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#28a745',
			cancelButtonColor: '#dc3545',
			confirmButtonText: 'Yes!',
			cancelButtonText: 'No!',
			showLoaderOnConfirm: true,
			preConfirm: function () {
				return new Promise(function (resolve, reject) {
					$.ajax({
						type: 'POST',
						url: "<?= base_url() ?>admin/master_location/delete",
						data: {
							id: id
						}
					}).done(function (msg) {
						if (msg == "ok") {
							swal.fire("OK!", "Data berhasil dihapus!", "success").then(
								function () {
									location.reload();
								})
						} else {
							swal.fire("Gagal!", msg, "error").then(function () {
								location.reload();
							})
						}
					})
				})
			},
			allowOutsideClick: () => !Swal.isLoading()
		})
	}

</script>
