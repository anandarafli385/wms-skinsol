<form action="<?= base_url() ?>admin/master_location/edit/<?= $get['location_id'] ?>" method="POST">
	<div class="modal-body m-3">
		<div class="mb-3">
			<label class="form-label">Location Name <span class="text-danger">*</span></label>
			<input type="text" class="form-control" name="location_name" placeholder="Location Name"
				value="<?= $get['location_name'] ?>" required>
		</div>
	</div>
	<div class="modal-footer">
		<button type="submit" class="btn btn-primary">Save changes</button>
		<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
	</div>
</form>
