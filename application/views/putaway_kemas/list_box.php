<input type="hidden" name="total_box" value="<?= ($this->session->userdata('putaway') != NULL) ? count($this->session->userdata('putaway')) : '0' ?>">
<div class="table-responsive">
    <table class="table table-sm table-striped">
        <thead>
            <tr>
                <th>No.</th>
                <th>Box Code</th>
                <th>Material</th>
                <th>Description</th>
                <th>Qty</th>
                <th>Unit</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php $no = 1;
            if ($this->session->userdata('putaway') != NULL) {
                foreach ($this->session->userdata('putaway') as $key => $row) { ?>
                    <tr>
                        <td><?= $no++; ?></td>
                        <td><?= $row['box_code'] ?></td>
                        <td><?= $row['material'] ?></td>
                        <td><?= $row['description'] ?></td>
                        <td><?= $row['qty'] ?></td>
                        <td><?= $row['unit'] ?></td>
                        <td><button type="button" class="btn btn-danger" onclick="delete_box('<?= $key ?>')">Delete</button></td>
                    </tr>
            <?php }
            } ?>
        </tbody>
    </table>
</div>