<main class="content">
	<div class="container-fluid p-0">

		<h1 class="h3 mb-3">Putaway Bahan Kemas</h1>

		<div class="row">
			<div class="col-12 col-xl-12">
				<div class="card">
					<div class="card-header">
						<h5 class="card-title">Putaway Bahan Kemas
							<div class="float-end">
								<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#addModal"><i class="fa fa-plus"></i> Add Data</button>
							</div>
						</h5>
						<h6 class="card-subtitle text-muted">List data putaway bahan kemas
						</h6>
					</div>
					<div class="card-body">
						<table class="table table-striped dataTable">
							<thead>
								<tr>
									<th>#</th>
									<th>Date</th>
									<th>Batch ID</th>
									<th>Packaging ID</th>
									<th>Location</th>
									<th>Layer</th>
									<th>Created Date</th>
									<th>Putaway By</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php 
								$no = 1;
								foreach ($get as $row) { 
										
										$pembuat = $this->db->where([
											'user_id' => $row->user_id
										])->get('tbl_user')->row();

									?>
									<tr>
										<td><?= $no++ ?></td>
										<td><?= $row->putaway_date ?></td>
										<td><?= $row->batch_id ?></td>
										<td><?= $row->packaging_id ?></td>
										<td><?= $row->location ?></td>
										<td><?= $row->layer ?></td>
										<td><?= $row->created_at ?></td>
										<td><?= $pembuat->name ?></td>
										<td>
										<?php if($this->session->level == 0): ?>
											<a href="#!" data-bs-toggle="modal" data-bs-target="#editModal"
												data-id="<?= $row->putaway_id ?>"><i data-feather="edit"></i></a>
											<a href="#!" onclick="delete_data('<?= $row->putaway_id ?>')"><i data-feather="trash-2"></i></a>
										<?php endif; ?>
											<a href="#!" data-bs-toggle="modal" data-bs-target="#detailModal" data-id="<?= $row->batch_id ?>"><i data-feather="eye"></i></a>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>

		</div>

	</div>
</main>

<div class="modal fade" id="addModal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Add Data Putaway Bahan Kemas</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body m-3">
				<div class="mb-3">
					<label class="form-label">Scan ID Receiveing</label>
					<div class="row">
						<div class="col-12">
							<select class="form-control" id="camera"></select>
						</div>
						<div class="col-12 text-center">
							<canvas height="200px" width="305px"></canvas>
						</div>
						<div class="col-12">
							<input type="text" class="form-control" id="batch_num" name="batch_num" placeholder="Scan Id Receiving">
						</div>
					</div>
				</div>

				<div class="mb-3">
					<label class="form-label">Date</label>
					<input type="date" class="form-control" name="putaway_date" placeholder="Date" required>
				</div>

				<div class="mb-3">
					<div class="alert alert-info alert-dismissible fade show" role="alert" id="alert-notifikasi" style="display: none;">
						<strong><i class="fa fa-map-marker" aria-hidden="true"></i> Available Location</strong> <br>
						<span id="alert-text"></span>
					</div>
				</div>

				<div class="mb-3" id="select-add">
					<label class="form-label">Location</label>
					<select required class="form-control select2-location" name="location" id="location">
						<option>Choose Location</option>
						<?php foreach ($location as $loc) : ?>
							<option value="<?= $loc->location_name ?>"><?= $loc->location_name ?></option>
						<?php endforeach; ?>
					</select>
				</div>

				<div class="mb-3">
					<label class="form-label">Box List</label>
					<div id="box_list" style="display: none">
						<div class="table-responsive">
							<table class="table table-sm table-striped">
								<thead>
									<tr>
										<th>Batch No.</th>
										<th>Package No.</th>
										<th>PO No.</th>
										<th>Packaging Type</th>
										<th>Quantity</th>
										<th>Layer</th>
										<th></th>
									</tr>
								</thead>
								<tbody id="box_tbody">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
				<button type="button" onclick="add_putaway()" class="btn btn-primary">Save changes</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="detailModal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Detail Data Putaway Bahan Kemas</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div id="detail_data"></div>
		</div>
	</div>
</div>

<div class="modal fade" id="editModal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Edit Data Putaway Baku</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div id="edit_data">

			</div>
		</div>
	</div>
</div>
<script src="<?= base_url() ?>assets/plugins/webcodecamjs/js/DecoderWorker.js"></script>
	<script src="<?= base_url() ?>assets/plugins/webcodecamjs/audio/beep.mp3"></script>
<script>

	function userExists(arr,item) {
	  return arr.some(function(el) {
	    return el.username,arr === item;
	  }); 
	}

	$("#detailModal").on('shown.bs.modal', function(e) {
		var id = $(e.relatedTarget).data('id');
		$.ajax({
			type: "POST",
			url: "<?= base_url() ?>admin/putaway_kemas/detail",
			data: {
				id: id
			}
		}).done(function(response) {
			$("#detail_data").html(response);
		});
	});


	$("#editModal").on('shown.bs.modal', function (e) {
		var id = $(e.relatedTarget).data('id');
		$.ajax({
			type: "POST",
			url: "<?= base_url() ?>admin/putaway_kemas/get",
			data: {
				id: id
			}
		}).done(function (response) {
			$("#edit_data").html(response);
		});
	});

	let arr_batch_num = [];
	let arr_batch_id = [];
	let arr_packaging_id = [];
	var items = <?= json_encode($get) ?>;

	$("[name=batch_num]").keyup(function(e) {
		if (e.which == 13) {
			var batch_num = $("[name=batch_num]").val();
			const found = items.some(el => el.batch_id === batch_num);
			if(found){
				swal.fire("Error!", "Batch Already in Putaway!", "error").then(function () {
					$("[name=batch_num]").val('');
				});
			}
			else if(arr_batch_id.includes(batch_num)){
				console.log('batch_num already scanned')
				swal.fire("Error!", "Duplicated batch num!", "error").then(function () {
					$("[name=batch_num]").val('');
				});
			}else{
				$.ajax({
					type: "POST",
					url: "<?= base_url() ?>admin/putaway_kemas/get_box",
					data: {
						batch_num: batch_num
					}
				}).done(function (response) {
					const found = items.some(el => el.batch_id === batch_num);
					if(found){
						swal.fire("Error!", "Batch Already in Putaway!", "error").then(function () {
							$("[name=batch_num]").val('');
						});
					} else if (arr_batch_id.includes(response.data.id)) {
						swal.fire("Error!", "Duplicated batch num!", "error").then(function () {
							$("[name=batch_num]").val('');
						});
					} else if (response.message == 'OK') {
						var datas = <?= json_encode($qc) ?>;
						datas.forEach(value => {
							 	// console.log(!value.id.includes(response.data.id) == true)
							if(value.id == response.data.id && value.status === 'ok'){
								swal.fire("OK!", "Batch Num found!", "success").then(function () {
									$("[name=batch_num]").val('').focus();
									
									// Show alert
									// $(".alert").alert();
									$(".alert").css({
										'display' : 'block'
									})

									let lokasi = response.lokasi

									$("#alert-text").html('This material batch will available to put at : <br/>');
									$("#alert-text").append('<ul style="margin-left: 15px">');
									lokasi.forEach(element => {
										$("#alert-text").append('<li>'+element+'</li>')
									});
									$("#alert-text").append('</ul>');

									arr_batch_id.push(response.data.id)
									arr_batch_num.push(response.data.receipt_code)
									// Table
									// push array of batch num & material box id
									$("#box_list").css({
										'display' : 'block'
									});
									let x = 0;
									for(x ; x <= response.detail.length ; x++ ){
										let html = `
											<tr>
												<td>${response.data.id}</td>
												<td>${response.detail[x].packaging_id}</td>
												<td>${response.data.receipt_code}</td>
												<td>${response.data.packaging_type}</td>
												<td>${response.detail[x].quantity}</td>
												<td>
													<div class="mb-3">
														<label class="form-label"></label>
														<select required class="form-control" name="layer[]">
															<option>Choose Layer</option>
															<option value="Layer 1">Layer 1</option>
															<option value="Layer 2">Layer 2</option>
															<option value="Layer 3">Layer 3</option>
															<option value="Layer 4">Layer 4</option>
															<option value="Layer 5">Layer 5</option>
															<option value="Layer 6">Layer 6</option>
															<option value="Layer 7">Layer 7</option>
															<option value="Layer 8">Layer 8</option>
															<option value="Layer 9">Layer 9</option>
															<option value="Layer 10">Layer 10</option>
															<option value="Layer 11">Layer 11</option>
															<option value="Layer 12">Layer 12</option>
															<option value="Layer 13">Layer 13</option>
															<option value="Layer 14">Layer 14</option>
															<option value="Layer 15">Layer 15</option>
															<option value="Layer 16">Layer 16</option>
														</select>
													</div>

												</td>
												<td><button type="button" class="btn btn-danger" onclick="delete_box(this, '${response.detail[x].id}', '${response.detail[x].packaging_id}')">Delete</button></td>
											</tr>
										`;
										$('#box_tbody').append(html)
										arr_packaging_id.push(response.detail[x].packaging_id)
									}
								});
							}else if(value.id == response.data.id && value.status.includes('not')){
								swal.fire("Error!", "Box code status QC is not ok!", "error").then(function () {
									$("[name=batch_num]").val('');
								});
							}
							//  else if (!value.id.includes(response.data.id) == true){
							//  	console.log(!value.id.includes(response.data.id) == true)
							// 	swal.fire("Error!", "Box code not have status QC,Check again data PO!", "error").then(function () {
							// 		$("[name=batch_num]").val('');
							// 	});
							// }
						});
					} else {
						swal.fire("Error!", "Box code not found!", "error").then(function () {
							$("[name=batch_num]").val('');
						});
					}
				})
			}
		}
	});

	$("[name=location]").keyup(function(e) {
		if (e.which == 13) {
			var location = $("[name=location]").val();
			$.ajax({
				type: "POST",
				url: "<?= base_url() ?>admin/putaway_kemas/get_location",
				data: {
					location: location
				}
			}).done(function(response) {
				if (response == 'OK') {
					swal.fire("OK!", "Location found!", "success").then(function() {
						$("[name=location]").attr('readonly', 'readonly');
						$("[name=box_code]").focus().removeAttr('readonly');
					});
				} else {
					swal.fire("Error!", "Location not found!", "error").then(function() {
						$("[name=location]").val('');
						$("[name=box_code]").val('').attr('readonly', 'readonly');
					});
				}
			})
		}
	});

	function add_putaway() {
		var batch_nums = $("[name=batch_num]").val();
		var putaway_date = $("[name=putaway_date]").val();
		var location = $("[name=location]").val();
		var total_box = $("[name=total_box]").val();
		var layer = $("[name='layer[]']").map(function() {
			return $(this).val();
		}).get();
		if (putaway_date == '') {
			swal.fire("Error!", "Must input putaway date first!", 'error');
		} else if (location == '') {
			swal.fire("Error!", "Must input location first!", 'error');
		} else if (total_box == 0) {
			swal.fire("Error!", "Must input box code at least 1!", 'error');
		} else if (layer.includes('Choose Layer')) {
			swal.fire("Error!", "Must input layer location!", 'error');
		} else {
			$.ajax({
				type: "POST",
				url: "<?= base_url() ?>admin/putaway_kemas/add",
				data: {
					putaway_date: putaway_date,
					location: location,
					layer: layer,
					batch_num : arr_batch_num,
					batch_id : arr_batch_id,
					packaging_id : arr_packaging_id

				}
			}).done(function(response) {
				if (response.includes('OK')) {
					swal.fire("OK!", "Successful add putaway", "success").then(function() {
						window.location.reload();
					});

					$(".alert").css({
						'display': 'none'
					})
				} else {
					swal.fire("Error!", "Failed add putaway", "error").then(function() {
						window.location.reload();
					});
				}
			});
		}
	}

	function delete_box(btn, batch_num, batch_id) {
		let index = arr_batch_id.indexOf(batch_num)
    	arr_batch_id.splice(index, 1)

		let index_id = arr_packaging_id.indexOf(batch_id)
    	arr_packaging_id.splice(index_id, 1)

		$(btn).closest("tr").remove();
		// $.ajax({
		// 	type: "POST",
		// 	url: "<?= base_url() ?>admin/putaway_kemas/delete_box",
		// 	data: {
		// 		key: key
		// 	}
		// }).done(function (response) {
		// 	if (response.includes('OK')) {
		// 		// load_box();
		// 	} else {
		// 		swal.fire("Error!", "Delete box failed!", "error");
		// 	}
		// });
	}

	function edit_putaway() {
		var putaway_date = $("[name=expired_date]").val();
		var location = $("[name=location]").val();
		if (putaway_date == '') {
			swal.fire("Error!", "Must input putaway date first!", 'error');
		} else if (location == '') {
			swal.fire("Error!", "Must input location first!", 'error');
		} else {
			// console.log(arr_box_code)
			// console.log(arr_box_id)

			$.ajax({
				type: "POST",
				url: "<?= base_url() ?>admin/putaway_kemas/data",
				data: {
					expired_date: expired_date,
					location: location,
					box_codes : arr_box_code,
					box_id : arr_box_id,
				}
			}).done(function(response) {
				if (response == 'OK') {
					swal.fire("OK!", "Successful edit putaway", "success").then(function() {
						window.location.reload();
					});

					$(".alert").css({
						'display': 'none'
					})
				} else {
					swal.fire("Error!", "Failed edit putaway", "error").then(function() {
						window.location.reload();
					});
				}
			});
		}
	}

	// load_box();

	function delete_data(id) {
		Swal.fire({
			title: 'Konfirmasi ?',
			text: "Apakah kamu yakin ?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#28a745',
			cancelButtonColor: '#dc3545',
			confirmButtonText: 'Yes!',
			cancelButtonText: 'No!',
			showLoaderOnConfirm: true,
			preConfirm: function() {
				return new Promise(function(resolve, reject) {
					$.ajax({
						type: 'POST',
						url: "<?= base_url() ?>admin/putaway_kemas/delete",
						data: {
							id: id
						}
					}).done(function(msg) {
						if (msg.includes("ok")) {
							swal.fire("OK!", "Data berhasil dihapus!", "success").then(
								function() {
									location.reload();
								})
						} else {
							swal.fire("Gagal!", msg, "error").then(function() {
								location.reload();
							})
						}
					})
				})
			},
			allowOutsideClick: () => !Swal.isLoading()
		})
	}

	var arg = {
		resultFunction: function(result) {
			var e = $.Event( "keyup", { keyCode: 13 } );
			$("#batch_num").val(result.code).trigger(e);
			// $('body').append($('<li>' + result.format + ': ' + result.code + '</li>'));
		}
	};
	var decoder = $("canvas").WebCodeCamJQuery(arg).data().plugin_WebCodeCamJQuery;
	decoder.buildSelectMenu("#camera");
	decoder.play();
	  // Without visible select menu
	    // decoder.buildSelectMenu(document.createElement('select'), 'environment|back').init(arg).play();
	
	$('#camera').on('change', function() {
		decoder.stop().play();
	});
</script>