<form action="<?= base_url('admin/putaway/update') ?>" method="post">
	<div class="modal-body m-3">
		<div class="mb-3">
			<div class="row">
				<div class="col-12">
					<div class="form-group">
						<label for="">Putaway Id</label>
						<input type="text" class="form-control" name="putaway_id" placeholder="Expired date" required  value="<?= $putnew['putaway_id'] ?>" readonly
						>
					</div>
				</div>
			</div>
		</div>

		<div class="mb-3">
			<div class="row">
				<div class="col-12">
					<?php 
			            foreach($api as $a){ 
			                if($putdetailnew['batch_id'] == $a->id){
			       ?>
					<div class="form-group">
						<label for="">Batch Id</label>
						<input type="text" class="form-control" name="batch_id" placeholder="Batch id" required  value="<?= $putdetailnew['batch_id'] ?>
						" readonly>
					</div>
			       <?php
			                }
			             }  
					 ?>
				</div>
			</div>
		</div>

		<div class="mb-3">
			<div class="row">
				<div class="col-12">
					<?php 
			            foreach($api as $a){ 
			                if($putdetailnew['batch_id'] == $a->id){
			       ?>
					<div class="form-group">
						<label for="">Batch Id</label>
						<input type="text" class="form-control" name="material_id" placeholder="Material Id" required  value="<?= $putdetailnew['material_id'] ?>
						" readonly>
					</div>
			       <?php
			                }
			             }  
					 ?>
				</div>
			</div>
		</div>


		<div class="mb-3">
			<div class="row">
				<div class="col-12">
					<?php 
			            foreach($api as $a){ 
			                if($putdetailnew['batch_id'] == $a->id){
			       ?>
					<div class="form-group">
						<label for="">Expired date</label>
						<input type="text" class="form-control" name="expired_date" placeholder="Expired date" required  value="<?= date('m/d/Y',strtotime($a->expired_date)); ?>
						" readonly>
					</div>
			       <?php
			                }
			             }  
					 ?>
				</div>
			</div>
		</div>
		<div class="mb-3">
			<label class="form-label">Location</label>
			<!--<input type="text" class="form-control" name="location" placeholder="Location" required>-->
			<select name="location" id="location" class="select2 form-control">
				<?php foreach ($location as $loc) : ?>
				<option value="<?= $loc->location_name ?>"<?= $loc->location_name == $putnew['location'] ? "selected  required" : null ?>><?= $loc->location_name ?></option>
				<?php endforeach; ?>
			</select>
		</div>
		<div class="mb-3">
			<label class="form-label">layer</label>
			<!--<input type="text" class="form-control" name="location" placeholder="Location" required>-->
			<select name="layer" id="layer" class="select2 form-control">
				<?php 
				$no = 1;
				for($no;$no <= 16;$no++){
				?>
				<option value="<?= 'Layer '.$no ?>" <?= 'Layer '.$no == $putdetailnew['layer'] ? " required selected"  : null  ?>><?= 'Layer '.$no ?></option>
				<?php
				} 
				?>
			</select>
		</div>
	</div>

	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
		<button type="submit" class="btn btn-primary">Save changes</button>
	</div>
</form>

<script>
	$(function () {
		$(".select2-edit").select2({
			theme: 'bootstrap-5',
			dropdownParent: $("#editModal"),
			width: 'auto'
		});
	})

</script>
