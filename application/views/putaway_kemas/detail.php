<div class="modal-body m-3">
    <div class="row">
        <div class="col-md-6">
            <div class="mb-3">
                <label class="form-label">Receipt Code</label><br>
                <b><?= $get->receipt_code ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">Packaging Type</label><br>
                <b><?= $get->packaging_type ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">Date</label><br>
                <b><?= $get->tanggal_recep ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">Created Date</label><br>
                <b><?= $get->created_at ?></b>
            </div>
        </div>
        <div class="col-md-6">
            <div class="mb-3">
                <label class="form-label">Supplier Code</label><br>
                <b>
                    <?= 
                        $get->customer_id == 0 || $get->customer_id == null 
                            ? print_r($get->supplier->supplier_code) 
                            : ($get->customer != null 
                                ? $get->customer->customer_code 
                                : "-")  
                    ?>
                </b>
            </div>
            <div class="mb-3">
                <label class="form-label">Supplier Name</label><br>
                <b>
                    <?= 
                        $get->customer_id == 0 || $get->customer_id == null 
                            ? print_r($get->supplier->supplier_name) 
                            : ($get->customer != null 
                                ? $get->customer->customer_name.' '.'(Brand : '.$get->customer->customer_brand.')'
                                : "-")  
                    ?>
                </b>
            </div>
            <div class="mb-3">
                <label class="form-label">Supplier Mobile</label><br>
                <b>
                    <?= 
                        $get->customer_id == 0 || $get->customer_id == null 
                            ? print_r($get->supplier->supplier_mobile) 
                            : ($get->customer != null 
                                ? $get->customer->customer_mobile 
                                : "-")  
                    ?>
                </b>
            </div>
            <div class="mb-3">
                <label class="form-label">Supplier Email</label><br>
                <b>
                    <?= 
                        $get->customer_id == 0 || $get->customer_id == null 
                            ? print_r($get->supplier->supplier_email) 
                            : ($get->customer != null 
                                ? $get->customer->customer_email 
                                : "-")  
                    ?>
                </b>
            </div>
            <div class="mb-3">
                <label class="form-label">Supplier Address</label><br>
                <b>
                    <?= 
                        $get->customer_id == 0 || $get->customer_id == null 
                            ? print_r($get->supplier->supplier_address) 
                            : ($get->customer != null 
                                ? $get->customer->customer_address 
                                : "-")  
                    ?>
                </b>
            </div>
            <div class="mb-3">
                <label class="form-label">Contact Person</label><br>
                <b>
                    <?= 
                        $get->customer_id == 0 || $get->customer_id == null 
                            ? print_r($get->supplier->supplier_mobile) 
                            : ($get->customer != null 
                                ? $get->customer->customer_mobile 
                                : "-")  
                    ?>
                </b>
            </div>
        </div>
        <div class="row">
                <?php if(count($get->packaging_receipt_detail) != 0){ ?>
                <div class="col-md-12">
                    <h3 class="mt-5"><b>Packaging Detail</b></h3>
                    <table class="table table-striped">
                        <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Packaging Code</th>
                                    <th>Packaging Name</th>
                                    <th>Packaging Type</th>
                                    <th>Category</th>
                                    <th>Quantity</th>
                                    <th>Stock Minimum</th>
                                </tr>
                            </thead>
                            <tbody>
                                    <?php 
                                    $no = 1;
                                    foreach($get->packaging_receipt_detail as $row){
                                     ?>
                                <tr>
                                    <td><?= $no++ ?></td>
                                    <?php 
                                    foreach($pkg as $list){
                                       if($row->packaging_id == $list->id){
                                            print_r('<td>'.$list->packaging_code.'</td>');
                                            print_r('<td>'.$list->packaging_name.'</td>');
                                            print_r('<td>'.$list->packaging_type.'</td>');
                                            print_r('<td>'.$list->category.'</td>');
                                            print_r('<td>'.$row->quantity.'</td>');
                                            print_r('<td>'.$list->stock_minimum.'</td>');
                                        }
                                    }
                                    ?>
                                    <!-- </td> -->
                                    <?php 
                                    }
                                     ?>
                                </tr>
                            </tbody>
                    </table>
                </div>                <?php } else { ?>
                <div class="col-md-6"></div>
                <?php } ?>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
</div>
