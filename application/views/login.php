<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="PT Skinsol Kosmetik Industri">
    <meta name="author" content="Digital RISe Indonesia">
    <meta name="keywords" content="kosmetik apps">

    <link rel="preconnect" href="https://fonts.gstatic.com">
	<link rel="shortcut icon" href="<?= base_url() ?>assets/src/img/logo2.png" />


    <title>Sign In | PT Skinsol Kosmetik Industri</title>

    <link href="<?= base_url() ?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>assets/static/css/app.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;600&display=swap" rel="stylesheet">
</head>

<body>
	<div class="container-scroller">
        <div class="row w-100 m-0 bg-white" style="padding: 6rem;">
          <div class="col-md-8 container-fluid full-page-wrapper d-flex align-items-center auth login-bg float-right shadow-xl">
          </div>
          <div class="col-md-4 px-0">
              <div class="card mx-auto mb-5 h-100">
                <div class="card-body px-5 py-0">
                  <div class="col-lg-12 text-center">
                    <img src="<?= base_url() ?>assets/src/img/logo-skinsol.png" class="rounded-circle h-25" width="200">
                    <!-- <img src="<?= base_url() ?>assets/images/auth/logo.png" class="rounded-circle h-25" width="400"> -->
                  </div>
                  <h3 class="card-title text-dark mb-3 text-center">PT. SKINSOL KOSMETIK INDUSTRI</h3>
          				<form action="<?= base_url() ?>login/login_process" method="POST">
          					<?= $this->session->flashdata('msg'); ?>
          					<div class="mb-3">
          						<label class="form-label">Username</label>
          						<input class="form-control form-control-lg" type="text" name="username" placeholder="Enter your username" required />
          					</div>
          					<div class="mb-3">
          						<label class="form-label">Password</label>
          						<input class="form-control form-control-lg" type="password" name="password" placeholder="Enter your password" required />
          					</div>
          					<div class="checkbox text-dark mb-3 justify-content-between d-flex">
          						<label class="form-check">
          							<input class="form-check-input" type="checkbox" value="remember-me" name="remember-me" checked>
          							<span class="form-check-label">
          								Remember me next time
          							</span>
          						</label>
                      <a href="#">Lupa Kata Sandi?</a>
                    </div>
          					<div class="text-center">
          						<button type="submit" class="btn btn-lg btn-primary">Sign in</button>
          					</div>
          				</form>
                </div>
                <div class="card-footer text-dark text-center pt-0">
                  © 2022 - Digital RISe Indonesia
                </div>
              </div>
          </div>
          <!-- content-wrapper ends -->
        </div>
        <!-- row ends -->
      <!-- </div> -->
      <!-- page-body-wrapper ends -->
    </div>
    <!-- </main> -->
</body>

</html>
