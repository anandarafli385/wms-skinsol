<form action="<?= base_url('admin/putaway_fg/update') ?>" method="post">
	<?php 
	$wo = $this->sql->select_table('tbl_work_order', [
		'work_order_id' => $get['work_order_id']
	])->row_array();
	
	$fg = $this->sql->select_table('tbl_material', [
		'material_no' => $get['fg_id']
	])->row_array(); 

	?>


	<div class="modal-body m-3">
		<input type="hidden" name="putaway_fg_id" value="<?= $get['putaway_fg_id'] ?>">

		<div class="mb-3">
			<label class="form-label">Work Order</label>
			<input type="text" name="work_order_id" value="Work Order <?= $wo['no_work_order'] ?>" readonly class="form-control">
		</div>

		<div class="mb-3">
			<label class="form-label">FG</label>
			<input type="text" name="work_order_id" value="<?= $fg['material_no'] . ' - ' . $fg['nama'] ?>" readonly class="form-control">

		</div>
		<div class="mb-3">
			<div class="row">
				<div class="col-12">
					<label class="form-label">Quantity</label>
					<input type="number" class="form-control" name="qty" id="qty_res" placeholder="Quantity" required value="<?= $get['qty'] ?>" readonly>
				</div>
			</div>
		</div>

		<div class="mb-3">
			<div class="row">
				<div class="col-6">
					<div class="form-group">
						<label for="">Batch No.</label>
						<input type="text" class="form-control" name="batch_no" placeholder="Batch Number" required value="<?= $get['batch_no'] ?>" readonly
							autocomplete="off">
					</div>
				</div>
				<div class="col-6">
					<div class="form-group">
						<label for="">Quality Control No.</label>
						<input type="text" class="form-control" name="qc_no" placeholder="QC Number" required value="<?= $get['qc_no'] ?>" readonly
							autocomplete="off">
					</div>
				</div>
			</div>
		</div>
		<div class="mb-3">
			<div class="row">
				<div class="col-6">
					<div class="form-group">
						<label for="">Retest date</label>
						<input type="date" class="form-control" name="retest_date" placeholder="Retest date" required  value="<?= $get['retest_date'] ?>">
					</div>
				</div>
				<div class="col-6">
					<div class="form-group">
						<label for="">Expired date</label>
						<input type="date" class="form-control" name="expired_date" placeholder="Expired date" required  value="<?= $get['expired_date'] ?>">
					</div>
				</div>
			</div>
		</div>
		<div class="mb-3">
			<label class="form-label">Putaway FG Date</label>
			<input type="date" class="form-control" name="fg_date" placeholder="Putaway Date" required  value="<?= $get['putaway_fg_date'] ?>">
		</div>
		<div class="mb-3">
			<label class="form-label">Location</label>
			<!--<input type="text" class="form-control" name="location" placeholder="Location" required>-->
			<select name="location" id="location" class="select2 form-control">
				<?php foreach ($location as $loc) : ?>
				<option value="<?= $loc->location_name ?>"<?= $loc->location_name == $get['location'] ? "selected" : null ?>><?= $loc->location_name ?></option>
				<?php endforeach; ?>
			</select>
		</div>
	</div>

	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
		<button type="submit" class="btn btn-primary">Save changes</button>
	</div>
</form>

<script>
	$(function () {
		$(".select2-edit").select2({
			theme: 'bootstrap-5',
			dropdownParent: $("#editModal"),
			width: 'auto'
		});
	})

	$('#work_order_id_edit').on('select2:select', function (e) {
		var data = e.params.data.id;

		$.ajax({
			method: 'POST',
			data: {
				id: data
			},
			url: "<?= base_url('admin/putaway_fg/filter_fg') ?>",
		}).done(function (result) {
			// console.log(result)
			$("#fg_id_edit").html(result)
		})
	});

</script>
