<main class="content">
	<div class="container-fluid p-0">

		<h1 class="h3 mb-3">Putaway</h1>

		<div class="row">
			<div class="col-12 col-xl-12">
				<div class="card">
					<div class="card-header">
						<h5 class="card-title">Putaway
							<div class="float-end">
							</div>
						</h5>
						<h6 class="card-subtitle text-muted">List data Putaway
						</h6>
					</div>
					<div class="card-body">
						<?= $this->session->flashdata('msg') ?>
						<table class="table table-striped dataTable">
							<thead>
								<tr>
									<th>#</th>
									<th>Date Time</th>
									<th>Work Order</th>
									<th>FG</th>
									<th>Qty</th>
									<th>Unit</th>
									<th>Location</th>
									<th>Putaway By</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>

								<?php
								$no = 1;
								foreach ($putaway_fg as $row) :
									$work = $this->sql->select_table('tbl_work_order', [
										'work_order_id' => $row['work_order']
									])->row();
								?>
								<tr>
									<td><?= $no++ ?></td>
									<td><?= date('d M Y', strtotime($row['putaway_fg_date'])) ?></td>
									<td><?= $work->no_work_order ?></td>
									<td><?= $row['nama'] ?></td>
									<td><?= $row['qty'] ?></td>
									<td><?= $row['unit'] ?></td>
									<td><?= $row['location'] ?></td>
									<td><?= $row['name'] ?></td>
									<td class="table-action">
										<!-- <a href="<?= base_url() ?>admin/putaway_fg/print/<?= $row['putaway_fg_id'] ?>" target="_blank"><i data-feather="printer"></i></a> -->
										<a href="#!" data-bs-toggle="modal" data-bs-target="#editModal"
											data-id="<?= $row['putaway_fg_id'] ?>"><i data-feather="edit"></i></a>
										<!-- <a href="#!" onclick="delete_data('<?= $row['putaway_fg_id'] ?>')"><i
												data-feather="trash-2"></i></a> -->
									</td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>

		</div>

	</div>
</main>

<div class="modal fade" id="addModal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Add Data </h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<form action="<?= base_url('admin/putaway_fg/add') ?>" method="post">
				<div class="modal-body m-3">
					<div class="mb-3">
						<label class="form-label">Work Order</label>

						<select name="work_order_id" id="work_order_id" class="form-control select2" required>
							<option value="0">- Select One -</option>
							<?php
							foreach ($wo as $item) { ?>
							<option value="<?= $item->work_order_id ?>">Work Order <?= $item->no_work_order ?></option>
							<?php
							} ?>
						</select>
					</div>

					<div class="mb-3">
						<label class="form-label">FG</label>
						<select name="fg_id" id="fg_id" class="form-control select2" required>
							<option value="0">- Select One -</option>
							<!--<?php foreach ($fg as $row) { ?>
							<option value="<?= $row['fg_id'] ?>"><?= $row['nama'] ?></option>
							<?php } ?> -->
						</select>
					</div>
					<div class="mb-3">
						<div class="row">
							<div class="col-6">
								<label for="" class="form-label">Quantity Left</label>
								<input type="number" name="qty_left" id="qty_left" readonly class="form-control">
							</div>
							<div class="col-6">
								<label class="form-label">Quantity Result</label>
								<input type="number" class="form-control" name="qty" id="qty_res" placeholder="Quantity"
									required>
							</div>
						</div>
					</div>

					<div class="mb-3">
						<div class="row">
							<div class="col-6">
								<div class="form-group">
									<label for="">Batch No.</label>
									<input type="text" class="form-control" name="batch_no" placeholder="Batch Number"
										required autocomplete="off">
								</div>
							</div>
							<div class="col-6">
								<div class="form-group">
									<label for="">Quality Control No.</label>
									<input type="text" class="form-control" name="qc_no" placeholder="QC Number"
										required autocomplete="off">
								</div>
							</div>
						</div>
					</div>
					<div class="mb-3">
						<div class="row">
							<div class="col-6">
								<div class="form-group">
								<label for="">Retest date</label>
						<input type="date" class="form-control" name="retest_date" placeholder="Retest date"
							required>
								</div>
							</div>
							<div class="col-6">
								<div class="form-group">
								<label for="">Expired date</label>
						<input type="date" class="form-control" name="expired_date" placeholder="Expired date"
							required>
								</div>
							</div>
						</div>
					</div>
					<div class="mb-3">
						<label class="form-label">Putaway FG Date</label>
						<input type="date" class="form-control" name="fg_date" placeholder="Putaway Date" required>
					</div>
					<div class="mb-3">
						<label class="form-label">Location</label>
						<!--<input type="text" class="form-control" name="location" placeholder="Location" required>-->
						<select name="location" id="location" class="select2 form-control">
							<?php foreach ($location as $loc) : ?>
							<option value="<?= $loc->location_name ?>"><?= $loc->location_name ?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="mb-3">
						<label class="form-label">Putaway By</label>
						<input type="hidden" name="user_id" value="<?= $this->session->user_id ?>">
						<input type="text" readonly class="form-control" name="putaway_by" placeholder="Putaway By"
							required value="<?= $this->session->name ?>">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Save changes</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="editModal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Edit Data Results</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div id="edit_data">

			</div>
		</div>
	</div>
</div>

<script>
	let work_order_id = null;

	$('#work_order_id').on('select2:select', function (e) {
		var data = e.params.data.id;

		if (data != 0) {
			work_order_id = data
			$.ajax({
				method: 'POST',
				data: {
					id: data
				},
				url: "<?= base_url('admin/putaway_fg/filter_fg') ?>",
			}).done(function (result) {
				console.log(result)
				$("#fg_id").html(result)
				$("#wo_info").css({
					'display': 'block'
				})
			})
		} else {
			$("#wo_info").css({
				'display': 'none'
			})
		}
	});

	$('#fg_id').on('select2:select', function (e) {
		let value = e.params.data.id;

		if (value != 0 && work_order_id != null) {
			$.ajax({
				method: 'POST',
				data: {
					work_order_id: work_order_id,
					fg_id: value
				},
				url: "<?= base_url('admin/putaway_fg/check_fg_left') ?>",
			}).done(function (result) {
				$('#qty_left').val(result)
				$('#qty_res').attr({
					'max': result
				})
			})
		} else {

		}
	});

	$("#editModal").on('shown.bs.modal', function (e) {
		var id = $(e.relatedTarget).data('id');
		$.ajax({
			type: "POST",
			url: "<?= base_url() ?>admin/putaway_fg/get",
			data: {
				id: id
			}
		}).done(function (response) {
			$("#edit_data").html(response);
		});
	});

	function delete_data(id) {
		Swal.fire({
			title: 'Konfirmasi ?',
			text: "Apakah kamu yakin ?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#28a745',
			cancelButtonColor: '#dc3545',
			confirmButtonText: 'Yes!',
			cancelButtonText: 'No!',
			showLoaderOnConfirm: true,
			preConfirm: function () {
				return new Promise(function (resolve, reject) {
					$.ajax({
						type: 'POST',
						url: "<?= base_url() ?>admin/putaway_fg/delete",
						data: {
							id: id
						}
					}).done(function (msg) {
						if (msg == "ok") {
							swal.fire("OK!", "Data berhasil dihapus!", "success").then(
								function () {
									location.reload();
								})
						} else {
							swal.fire("Gagal!", msg, "error").then(function () {
								location.reload();
							})
						}
					})
				})
			},
			allowOutsideClick: () => !Swal.isLoading()
		})
	}

</script>
