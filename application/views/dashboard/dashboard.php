<main class="content">
	<div class="container-fluid p-0">

		<h1 class="h3 mb-3"><strong>Analytics</strong> Dashboard</h1>

		<div class="row">
			<div class="row mb-3">
				<div class="form-group col-sm-3">
					<label for="">Month</label>
					<select name="month" class="form-control" onchange="filter()" required="">
															<option value="1">January</option>
															<option value="2">February</option>
															<option value="3">March</option>
															<option value="4" selected="">April</option>
															<option value="5">May</option>
															<option value="6">June</option>
															<option value="7">July</option>
															<option value="8">August</option>
															<option value="9">September</option>
															<option value="10">October</option>
															<option value="11">November</option>
															<option value="12">December</option>
													</select>
				</div>
				<div class="form-group col-sm-3">
					<label for="">Year</label>
					<select name="year" class="form-control" onchange="filter()" required="">
															<option value="2022" selected="">2022</option>
															<option value="2021">2021</option>
															<option value="2020">2020</option>
															<option value="2019">2019</option>
															<option value="2018">2018</option>
															<option value="2017">2017</option>
															<option value="2016">2016</option>
															<option value="2015">2015</option>
															<option value="2014">2014</option>
															<option value="2013">2013</option>
															<option value="2012">2012</option>
															<option value="2011">2011</option>
															<option value="2010">2010</option>
															<option value="2009">2009</option>
															<option value="2008">2008</option>
															<option value="2007">2007</option>
															<option value="2006">2006</option>
															<option value="2005">2005</option>
															<option value="2004">2004</option>
															<option value="2003">2003</option>
															<option value="2002">2002</option>
															<option value="2001">2001</option>
															<option value="2000">2000</option>
													</select>
				</div>
			</div>
				<div class="row">
					<div class="col-sm-3">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col mt-0">
										<h5 class="text-dark">Good Receiving</h5>
										<h2 class="my-1 font-weight-bold">0</h2>
									</div>

									<div class="col-auto">
										<div class="stat text-primary">
											<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-truck align-middle"><rect x="1" y="3" width="15" height="13"></rect><polygon points="16 8 20 8 23 11 23 16 16 16 16 8"></polygon><circle cx="5.5" cy="18.5" r="2.5"></circle><circle cx="18.5" cy="18.5" r="2.5"></circle></svg>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col mt-0">
										<h5 class="text-dark">Put Away</h5>
										<h2 class="my-1 font-weight-bold">0</h2>
									</div>

									<div class="col-auto">
										<div class="stat text-primary">
											<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users align-middle"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="9" cy="7" r="4"></circle><path d="M23 21v-2a4 4 0 0 0-3-3.87"></path><path d="M16 3.13a4 4 0 0 1 0 7.75"></path></svg>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="card">
							<div class="card-body">
								<div class="row">

									<div class="col mt-0">
										<h6 class="text-dark" style="font-size: 14px">Replenishment </h6>
										<h2 class="my-1 font-weight-bold">0</h2>
									</div>

									<div class="col-auto">
										<div class="stat text-primary">
											<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-dollar-sign align-middle"><line x1="12" y1="1" x2="12" y2="23"></line><path d="M17 5H9.5a3.5 3.5 0 0 0 0 7h5a3.5 3.5 0 0 1 0 7H6"></path></svg>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col mt-0">
										<h5 class="text-dark">Putaway FG</h5>
										<h2 class="my-1 font-weight-bold">0</h2>
									</div>

									<div class="col-auto">
										<div class="stat text-primary">
											<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-shopping-cart align-middle"><circle cx="9" cy="21" r="1"></circle><circle cx="20" cy="21" r="1"></circle><path d="M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6"></path></svg>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="card">
						<div class="card-body">
							<div class="row">
								<div class="col mt-0">
									<h5 class="text-dark">Sales Order</h5>
									<h2 class="my-1 font-weight-bold">0</h2>
								</div>

								<div class="col-auto">
									<div class="stat text-primary">
										<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-shopping-cart align-middle"><circle cx="9" cy="21" r="1"></circle><circle cx="20" cy="21" r="1"></circle><path d="M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6"></path></svg>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="card">
						<div class="card-body">
							<div class="row">
								<div class="col mt-0">
									<h5 class="text-dark">Purchase Order</h5>
									<h2 class="my-1 font-weight-bold">0</h2>
								</div>

								<div class="col-auto">
									<div class="stat text-primary">
										<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-shopping-cart align-middle"><circle cx="9" cy="21" r="1"></circle><circle cx="20" cy="21" r="1"></circle><path d="M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6"></path></svg>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="card">
						<div class="card-body">
							<div class="row">
								<div class="col mt-0">
									<h5 class="text-dark">Work Order</h5>
									<h2 class="my-1 font-weight-bold">0</h2>
								</div>

								<div class="col-auto">
									<div class="stat text-primary">
										<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-shopping-cart align-middle"><circle cx="9" cy="21" r="1"></circle><circle cx="20" cy="21" r="1"></circle><path d="M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6"></path></svg>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-8 col-xxl-8">
					<div class="card flex-fill w-100">
						<div class="card-header">
							<h5 class="card-title mb-0">Output</h5>
						</div>
						<div class="card-body py-3">
							<div class="chart chart-sm">
								<canvas id="chartjs-dashboard-line"></canvas>
							</div>
						</div>
					</div>
				</div>
		</div>

		<div class="col-12">
			<h3 class="card-title mb-4">Monthly Employee Performance</h3>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="card flex-fill w-100">
					<div class="card-header">
						<h5>Good Receiving</h5>
					</div>
					<div class="card-body py-3">
						<div class="chart chart-sm">
							<div id="apex-employee"></div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="card flex-fill w-100">
					<div class="card-header">
						<h5>Put Away</h5>
					</div>
					<div class="card-body py-3">
						<div class="chart chart-sm">
							<div id="apex-employee2"></div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<div class="card flex-fill w-100">
					<div class="card-header">
						<h5>Replenishment</h5>
					</div>
					<div class="card-body py-3">
						<div class="chart chart-sm">
							<div id="apex-employee3"></div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-12 col-md-6 col-xxl-6 d-flex order-2 order-xxl-3">
				<div class="card flex-fill w-100">
					<div class="card-header">

						<h5 class="card-title mb-0">Monthly Target & Actual Output</h5>
					</div>
					<div class="card-body d-flex">
						<div class="align-self-center w-100">
							<div class="py-3">
								<div class="chart chart-xs">
									<div id="chart-donut"></div>
								</div>
							</div>

							<table class="table mb-0">
								<tbody>
									<tr>
										<td>Target</td>
										<td class="text-end"><?= isset($wo_qty->total) ?$wo_qty->total : 0 ?></td>
									</tr>
									<tr>
										<td>Good</td>
										<td class="text-end"><?= isset($wo_qty->good) ? $wo_qty->good : 0 ?></td>
									</tr>
									<tr>
										<td>Reject</td>
										<td class="text-end"><?= isset($wo_qty->reject) ? $wo_qty->reject : 0 ?></td>
									</tr>
									<tr>
										<td>In Progress</td>
										<td class="text-end"><?= isset($wo_qty) ? $wo_qty->total - ($wo_qty->good + $wo_qty->reject) : 0 ?>
										</td>
									</tr>

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

			<div class="col-12 col-md-6 col-xxl-6 d-flex order-1 order-xxl-1">
				<div class="card flex-fill">
					<div class="card-header">
						<h5 class="card-title">Output Trend</h5>
					</div>
					<div class="card-body d-flex">
						<div class="align-self-center w-100">
							<div class="row mb-3">
								<div class="col-6">
									<select name="selected_material" class="select-dashboard select-material" required>
									<option value="">- Select Product -</option>
									<?php foreach($list_fg as $fg): ?>
									<option value="<?= $fg->material_id ?>" <?= $fg->nama == "Bulk LM Finished Red Yarrow 03" ? 'selected' : null ?>><?= $fg->nama ?></option>
									<?php endforeach; ?>
								</select>
								</div>
								<div class="col-6">
									<select name="filter" id="filter" class="select-dashboard select-filter">
										<option value="daily" selected>Daily</option>
										<option value="weekly">Weekly</option>
										<option value="monthly">Monthly</option>
									</select>
								</div>
							</div>
							<div class="chart">
								<div id="chart-output-trend"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-12 col-lg-8 col-xxl-9 d-flex">
				<div class="card flex-fill">
					<div class="card-header">

						<h5 class="card-title mb-0">Latest Work Order</h5>
					</div>
					<div class="card-body">
						<table class="table table-hover dataTable m-2">
							<thead>
								<tr>
									<th>No.</th>
									<th>No Work Order</th>
									<th>Date</th>
									<th>Total FG</th>
									<th>Status</th>
									<th>Lead Time</th>
									<th>Created By</th>
								</tr>
							</thead>
							<tbody>
								<?php
                            $no = 1;
                            foreach ($latest_workorder as $row) { ?>
								<tr>
									<td><?= $no++; ?></td>
									<td><?= $row['no_work_order'] ?></td>
									<td><?= date('d M Y', strtotime($row['date'])) ?></td>
									<td><?= $row['count_fg'] ?></td>
									<td class="text-center">
										<?php if($row['status'] != 'finished'): ?>
										<span class="badge bg-warning"><?= $row['status'] ?></span>
										<?php else: ?>
										<span class="badge bg-success"><?= $row['status'] ?></span>
										<?php endif; ?>
									</td>
									<td>
										<?php 
										if($row['finished_at'] !== null): 
											$datetime1 = date_create($row['date']);
											$datetime2 = date_create($row['finished_at']);
										
											$interval = date_diff($datetime1, $datetime2);
											if($interval->format('%m') == '0'){
												echo $interval->format('%d Days');
											}else{
												echo $interval->format('%m Month %d Days');

											}
										?>

										<?php else: 
										$datetime1 = date_create($row['date']);
										$datetime2 = date_create(date('Y-m-d H:i:s'));
									
										$interval = date_diff($datetime1, $datetime2);
										if($interval->format('%m') == '0'){
											echo $interval->format('%d Days');
										}else{
											echo $interval->format('%m Month %d Days');

										}
										endif; ?>
									</td>
									<td><?= $row['name'] ?></td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col-12 col-lg-4 col-xxl-3 d-flex">
				<div class="card flex-fill w-100">
					<div class="card-header">

						<h5 class="card-title mb-0">Work Order per Month</h5>
					</div>
					<div class="card-body d-flex w-100">
						<div class="align-self-center chart chart-lg">
							<canvas id="chartjs-dashboard-bar"></canvas>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</main>


<script>
	document.addEventListener("DOMContentLoaded", function () {
		var ctx = document.getElementById("chartjs-dashboard-line").getContext("2d");
		var gradient = ctx.createLinearGradient(0, 0, 0, 225);
		gradient.addColorStop(0, "rgba(215, 227, 244, 1)");
		gradient.addColorStop(1, "rgba(215, 227, 244, 0)");
		// Line chart
		new Chart(document.getElementById("chartjs-dashboard-line"), {
			type: "line",
			data: {
				labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov",
					"Dec"
				],
				datasets: [{
					label: "Jumlah Output",
					fill: true,
					backgroundColor: gradient,
					borderColor: window.theme.primary,
					data: [
						<?= $jan->qty == null ? 0 : $jan->qty ?>,
						<?= $feb->qty == null ? 0 : $feb->qty ?>,
						<?= $mar->qty == null ? 0 : $mar->qty ?>,
						<?= $apr->qty == null ? 0 : $apr->qty ?>,
						<?= $mei->qty == null ? 0 : $mei->qty ?>,
						<?= $jun->qty == null ? 0 : $jun->qty ?>,
						<?= $jul->qty == null ? 0 : $jul->qty ?>,
						<?= $agu->qty == null ? 0 : $agu->qty ?>,
						<?= $sep->qty == null ? 0 : $sep->qty ?>,
						<?= $okt->qty == null ? 0 : $okt->qty ?>,
						<?= $nov->qty == null ? 0 : $nov->qty ?>,
						<?= $des->qty == null ? 0 : $des->qty ?>,

					]
				}]
			},
			options: {
				maintainAspectRatio: false,
				legend: {
					display: false
				},
				tooltips: {
					intersect: false
				},
				hover: {
					intersect: true
				},
				plugins: {
					filler: {
						propagate: false
					}
				},
				scales: {
					xAxes: [{
						reverse: true,
						gridLines: {
							color: "rgba(0,0,0,0.0)"
						}
					}],
					yAxes: [{
						ticks: {
							stepSize: 1000
						},
						display: true,
						borderDash: [3, 3],
						gridLines: {
							color: "rgba(0,0,0,0.0)"
						}
					}]
				}
			}
		});
	});

</script>
<script>
	document.addEventListener("DOMContentLoaded", function () {
		// Pie chart
		new Chart(document.getElementById("chartjs-dashboard-pie"), {
			type: "pie",
			data: {
				labels: ["Good", "Reject", "On Process"],
				datasets: [{
					data: [<?= $wo_qty->good ?>, <?= $wo_qty->reject ?>, <?= $wo_qty->total - ($wo_qty->good + $wo_qty->reject) ?>],
					backgroundColor: [
						window.theme.success,
						window.theme.danger,
						window.theme.warning,
					],
					borderWidth: 5
				}]
			},
			options: {
				responsive: !window.MSInputMethodContext,
				maintainAspectRatio: false,
				legend: {
					display: false
				},
				cutoutPercentage: 75
			}
		});
	});

</script>
<script>
	let index_bulan = []
	let data_wo_bulan = []
	let bulan = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov",
					"Dec"
				]

	<?php 
		foreach($wo_monthly as $wo):
	?>
		index_bulan.push(bulan['<?= $wo->bulan-1  ?>'])
		data_wo_bulan.push("<?= $wo->jumlah ?>")

	<?php endforeach; ?>


	document.addEventListener("DOMContentLoaded", function () {
		// Bar chart
		new Chart(document.getElementById("chartjs-dashboard-bar"), {
			type: "bar",
			data: {
				labels: index_bulan,
				datasets: [{
					label: "This year",
					backgroundColor: window.theme.primary,
					borderColor: window.theme.primary,
					hoverBackgroundColor: window.theme.primary,
					hoverBorderColor: window.theme.primary,
					// data: [54, 67, 41, 55, 62, 45, 55, 73, 60, 76, 48, 79],
					data: data_wo_bulan,
					barPercentage: .75,
					categoryPercentage: .5
				}]
			},
			options: {
				maintainAspectRatio: false,
				legend: {
					display: false
				},
				scales: {
					yAxes: [{
						gridLines: {
							display: false
						},
						stacked: false,
						ticks: {
							stepSize: 20
						}
					}],
					xAxes: [{
						stacked: false,
						gridLines: {
							color: "transparent"
						}
					}]
				}
			}
		});
	});

</script>

<!-- Apex Chart -->
<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
<!-- Apex Chart Configuration -->
<script>
	var options = {
		chart: {
			type: 'line'
		},
		series: [{
			name: 'sales',
			data: [30, 40, 35, 50, 49, 60, 70, 91, 125]
		}],
		xaxis: {
			categories: [1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999]
		}
	}

	var chart = new ApexCharts(document.querySelector("#chart-apex"), options);

	chart.render();
</script>

<script>
	//Employee 
	var colors = [
      '#008FFB',
      '#00E396',
      '#FEB019',
      '#FF4560',
      '#775DD0',
      '#546E7A',
      '#26a69a',
      '#D10CE8'
    ];
  
	// Good Receiving   
	let emp_gr = [];
	let emp_gr_name = [];
	// value
	<?php 
		foreach($emp_gr as $emp):
	?>
		emp_gr.push("<?= $emp->jumlah ?>")
		emp_gr_name.push("<?= $emp->name ?>")

	<?php endforeach; ?>

	// Putaway
	let emp_putaway = [];
	let emp_putaway_name = [];
	// value
	<?php 
		foreach($emp_putaway as $emp):
	?>
		emp_putaway.push("<?= $emp->jumlah ?>")
		emp_putaway_name.push("<?= $emp->name ?>")

	<?php endforeach; ?>

	// Replenishment
	let emp_replenishment = [];
	let emp_replenishment_name = [];
	// value
	<?php foreach($emp_replenishment as $emp): ?>
		emp_replenishment.push("<?= $emp->jumlah ?>")
		emp_replenishment_name.push("<?= $emp->name ?>")
	<?php endforeach; ?>
	
	let arr = [];
	emp_gr.forEach(element => {
		arr.push({
			data : element
		})
	})

	var options = {
		series: [{
			data: emp_gr
		}],
          chart: {
          type: 'bar',
          width: '100%',
        },
        plotOptions: {
          bar: {
            borderRadius: 4,
            horizontal: true,
          },
		//   colors: colors
        },
		colors: '#1CBB8C',
        dataLabels: {
          enabled: false
        },
        xaxis: {
          categories: emp_gr_name,
        }
    };


	var options2 = {
		series: [{
          data: emp_putaway
        }],
          chart: {
          type: 'bar',
          width: '100%',

        },
        plotOptions: {
          bar: {
            borderRadius: 4,
            horizontal: true,
          }
        },
		colors :'#FCB92C',
        dataLabels: {
          enabled: false
        },
        xaxis: {
          categories: emp_putaway_name,
        }
    };

	var options3 = {
		series: [{
          data: emp_replenishment
        }],
          chart: {
          type: 'bar',
          width: '100%',

        },
        plotOptions: {
          bar: {
            borderRadius: 4,
            horizontal: true,
          }
        },
        dataLabels: {
          enabled: false
        },
        xaxis: {
          categories: emp_replenishment_name,
        }
    };

    var chart = new ApexCharts(document.querySelector("#apex-employee"), options);
    var chart2 = new ApexCharts(document.querySelector("#apex-employee2"), options2);
    var chart3 = new ApexCharts(document.querySelector("#apex-employee3"), options3);
        
	chart.render();
	chart2.render();
	chart3.render();

	var options_trend = {
          series: [{
            name: "Quantity",
            data: [
				<?= isset($day1['qty']) ? $day1['qty'] : 0 ?>,
				<?= isset($day2['qty']) ? $day2['qty'] : 0 ?>,
				<?= isset($day3['qty']) ? $day3['qty'] : 0 ?>,
				<?= isset($day4['qty']) ? $day4['qty'] : 0 ?>,
				<?= isset($day5['qty']) ? $day5['qty'] : 0 ?>,
				<?= isset($day6['qty']) ? $day6['qty'] : 0 ?>,
				<?= isset($today['qty']) ? $today['qty'] : 0 ?>,
			]
        }],
          chart: {
          height: 350,
          type: 'line',
          zoom: {
            enabled: false
          }
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          curve: 'straight'
        },
        title: {
          text: 'Trend <?= $list_fg[0]->nama ?>' ,
          align: 'left'
        },
        grid: {
          row: {
            colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
            opacity: 0.5
          },
        },
        xaxis: {
          categories: [
			  "6 days ago",
			  "5 days ago",
			  "4 days ago",
			  "3 days ago",
			  "2 days ago",
			  "Yesterday",
			  "Today"
		  ],
        }
        };

        var chart_trend = new ApexCharts(document.querySelector("#chart-output-trend"), options_trend);
        chart_trend.render();

		var options_donut = {
		  colors: ['#00E396', '#FF4560', '#FEB019'],
          series: [<?= isset($wo_qty->good) ? $wo_qty->good : 0 ?>, <?= isset($wo_qty->reject) ? $wo_qty->reject : 0 ?>, <?= isset($wo_qty) ? $wo_qty->total - ($wo_qty->good + $wo_qty->reject) : 0 ?>],
		  labels: ['Good', 'Reject', 'In Progress'],
          chart: {
          type: 'donut',
        },
        responsive: [{
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: 'bottom'
            }
          }
        }]
        };

        var chart_donut = new ApexCharts(document.querySelector("#chart-donut"), options_donut);
        chart_donut.render();
</script>

<script>

	//output dashboard
	var options = {
		series: [{
			name: 'Jumlah Output',
			data: [500, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
		}],
		chart: {
			height: 230,
			type: 'area'
		},
		dataLabels: {
			enabled: false
		},
		stroke: {
			curve: 'smooth'
		},
		xaxis: {
			categories: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
		},
	};

	var chart = new ApexCharts(document.querySelector("#chartjs-dashboard-line"), options);
	chart.render();

	//work order per month
	var options = {
		series: [{
			name: 'Net Profit',
			data: data_wo_bulan
		}],
		chart: {
			type: 'bar',
			height: 300
		},
		plotOptions: {
			bar: {
				horizontal: false,
				columnWidth: '55%',
				endingShape: 'rounded'
			},
		},
		dataLabels: {
			enabled: false
		},
		stroke: {
			show: true,
			width: 2,
			colors: ['transparent']
		},
		xaxis: {
			categories: index_bulan,
		},
		yaxis: {
			title: {
				text: 'Work Order'
			}
		},
		fill: {
			opacity: 1
		},
		tooltip: {
			y: {
				formatter: function(val) {
					return "$ " + val + " thousands"
				}
			}
		}
	};

	var chart = new ApexCharts(document.querySelector("#chartjs-dashboard-bar"), options);
	chart.render();

	// output good receiving employee
	var options = {
		series: emp_gr,
		chart: {
			width: 300,
			type: 'pie',
		},
		legend: {
			position: 'bottom'
		},
		labels: emp_gr_name,
		dataLabels: {
			formatter: function(val, opts) {
				return opts.w.config.series[opts.seriesIndex]
			},
		},
		responsive: [{
			breakpoint: 480,
			options: {
				chart: {
					width: 200
				},
				legend: {
					position: 'bottom'
				}
			}
		}]
	};

	var chart = new ApexCharts(document.querySelector("#apex-employee"), options);
	chart.render();


	// output putaway employee
	var options = {
		series: emp_putaway,
		chart: {
			width: 300,
			type: 'pie',
		},
		legend: {
			position: 'bottom'
		},
		labels: emp_putaway_name,
		responsive: [{
			breakpoint: 480,
			options: {
				chart: {
					width: 200
				},
				legend: {
					position: 'bottom'
				}
			}
		}]
	};

	var chart = new ApexCharts(document.querySelector("#apex-employee2"), options);
	chart.render();


	// output replenishment employee
	var options = {
		series: emp_replenishment,
		chart: {
			width: 300,
			type: 'pie',
		},
		legend: {
			position: 'bottom'
		},
		labels: emp_replenishment_name,
		responsive: [{
			breakpoint: 480,
			options: {
				chart: {
					width: 200
				},
				legend: {
					position: 'bottom'
				}
			}
		}]
	};

	var chart = new ApexCharts(document.querySelector("#apex-employee3"), options);
	chart.render();

	// output sales order employee
	var options = {
		series: emp_so,
		chart: {
			width: 300,
			type: 'pie',
		},
		legend: {
			position: 'bottom'
		},
		labels: emp_so_name,
		dataLabels: {
			formatter: function(val, opts) {
				return opts.w.config.series[opts.seriesIndex]
			},
		},
		responsive: [{
			breakpoint: 480,
			options: {
				chart: {
					width: 200
				},
				legend: {
					position: 'bottom'
				}
			}
		}]
	};

	var chart = new ApexCharts(document.querySelector("#apex-employee4"), options);
	chart.render();

	// output purchase order employee
	var options = {
		series: emp_po,
		chart: {
			width: 300,
			type: 'pie',
		},
		legend: {
			position: 'bottom'
		},
		labels: emp_po_name,
		dataLabels: {
			formatter: function(val, opts) {
				return opts.w.config.series[opts.seriesIndex]
			},
		},
		responsive: [{
			breakpoint: 480,
			options: {
				chart: {
					width: 200
				},
				legend: {
					position: 'bottom'
				}
			}
		}]
	};

	var chart = new ApexCharts(document.querySelector("#apex-employee5"), options);
	chart.render();

	// output work order employee
	var options = {
		series: emp_wo,
		chart: {
			width: 300,
			type: 'pie',
		},
		legend: {
			position: 'bottom'
		},
		labels: emp_wo_name,
		dataLabels: {
			formatter: function(val, opts) {
				return opts.w.config.series[opts.seriesIndex]
			},
		},
		responsive: [{
			breakpoint: 480,
			options: {
				chart: {
					width: 200
				},
				legend: {
					position: 'bottom'
				}
			}
		}]
	};

	var chart = new ApexCharts(document.querySelector("#apex-employee6"), options);
	chart.render();

	// output trend
	var option = {
		series: [{
			name: "Quantity",
			data: [
				0,
				0,
				0,
				0,
				0,
				0,
				0,
			]
		}],
		chart: {
			height: 350,
			type: 'line',
			zoom: {
				enabled: false
			}
		},
		dataLabels: {
			enabled: false
		},
		stroke: {
			curve: 'straight'
		},
		title: {
			text: 'Trend Bulk LM Finished Orange Poppy 01',
			align: 'left'
		},
		grid: {
			row: {
				colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
				opacity: 0.5
			},
		},
		xaxis: {
			categories: [
				"6 days ago",
				"5 days ago",
				"4 days ago",
				"3 days ago",
				"2 days ago",
				"Yesterday",
				"Today"
			],
		}
	};

	var chart_trend = new ApexCharts(document.querySelector("#chart-output-trend"), option);
	chart_trend.render();

	// monthly target & actual output	
	var options_donut = {
		colors: ['#00E396', '#FF4560', '#FEB019'],
		series: [0, 0, 0],
		labels: ['Good', 'Reject', 'In Progress'],
		chart: {
			type: 'donut',
		},
		responsive: [{
			breakpoint: 480,
			options: {
				chart: {
					width: 200
				},
				legend: {
					position: 'bottom'
				}
			}
		}]
	};

	var chart_donut = new ApexCharts(document.querySelector("#chart-donut"), options_donut);
	chart_donut.render();

	$(".select-material").on('select2:select', function(e) {
		let base_url = 'https://production-spj.com/'
		let filter = $(".select-filter").val();
		var material = e.params.data.id;
		if (material == '') {
			material = '0';
		}

		$.ajax({
			url: base_url + 'admin/dashboard/complete_api_trend/' + material + '/' + filter,
			method: 'GET',
		}).done(function(result) {
			chart_trend.updateSeries([{
				data: [
					result.data.day1,
					result.data.day2,
					result.data.day3,
					result.data.day4,
					result.data.day5,
					result.data.day6,
					result.data.today,
				],
			}])

			console.log(result)
			chart_trend.updateOptions({
				title: {
					text: 'Trend ' + result.data.material_name
				},
				labels: result.data.labels

			})
		})
	})

	function filter() {
		var month = $("[name=month]").val();
		var year = $("[name=year]").val();

		window.location.href = 'https://production-spj.com/admin/dashboard?year=' + year + '&month=' + month;
	}


	// $(".select-filter").on('select2:select', function(e) {
	// 	let base_url = 'https://production-spj.com/'
	// 	let material = $(".select-material").val()

	// 	$.ajax({
	// 		url: base_url + 'admin/dashboard/complete_api_trend/' + material + '/' + e.params.data.id,
	// 		method: 'GET',
	// 	}).done(function(result) {
	// 		chart_trend.updateSeries([{
	// 			data: [
	// 				result.data.day1,
	// 				result.data.day2,
	// 				result.data.day3,
	// 				result.data.day4,
	// 				result.data.day5,
	// 				result.data.day6,
	// 				result.data.today,
	// 			],
	// 		}])

	// 		console.log(result)
	// 		chart_trend.updateOptions({
	// 			title: {
	// 				text: 'Trend ' + result.data.material_name
	// 			},
	// 			labels: result.data.labels

	// 		})
	// 	})
	// });
</script>
