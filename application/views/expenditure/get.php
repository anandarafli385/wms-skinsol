<form action="<?= base_url('admin/expenditure/update') ?>" method="post">
	<div class="modal-body m-3">
		<div class="mb-3">
			<label class="form-label">Expenditure ID</label>
			<input type="text" class="form-control" name="expenditure_id" required  value="<?=$get->id ?>" readonly>
		</div>

		<div class="mb-3">
			<label class="form-label">Expenditure No.</label>
			<input type="text" class="form-control" name="expenditure_num" required  value="<?=$get->code ?>" readonly>
		</div>

		<div class="mb-3">
			<label class="form-label">Expenditure Date</label>
			<input type="text" class="form-control" name="expenditure_date" required  value="<?= $get->date ?>" readonly>
		</div>

		<div class="mb-3">
			<label class="form-label">Expenditure Material ID</label>
			<input type="text" class="form-control" name="material_id" required  value="<?=$get->material_id ?>" readonly>
		</div>

		<div class="mb-3">
			<label class="form-label">Expenditure Material Name</label>
			<input type="text" class="form-control" name="material_name" required  value="<?= $get->material != null ? $get->material->material_name : '-' ?>" readonly>
		</div>

		<div class="mb-3" id="select-loc">
			<label class="form-label">Location</label>
			<select name="location" id="location" class="select2-locate">
				<option>Choose Location</option>
				<?php 
				foreach($putdetailnew as $put):
					if($put->material_id == $get->material_id):
						foreach($putnew as $p):
							if($put->putaway_id == $p->putaway_id):
								 echo "<option value=$p->location.'.'.$put->batch_id>$p->location (Batch : $put->batch_id)</option>";			
							endif;
						endforeach;
					endif;
				endforeach; 
				?>
			</select>
		</div>

		<div class="mb-3" id="select-lay">
			<label class="form-label">Layer</label>
			<select name="layer_loc" id="layer_loc" class="select2-layer">
				<option>Choose Layer</option>
				<?php 
				foreach($putdetailnew as $put):
					if($put->material_id == $get->material_id):
						foreach($putnew as $p):
							if($put->putaway_id == $p->putaway_id):
								 echo "<option value=$p->layer.'.'.$p->location>$p->location (Layer : $put->layer)</option>";			
							endif;
						endforeach;
					endif;
				endforeach; 
				?>
			</select>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
		<button type="submit" class="btn btn-primary">Save changes</button>
	</div>
</form>

<script type="text/javascript">
$(function () {
	$(".select2").select2({
		theme: 'bootstrap-5',
		dropdownParent: $("#editModal"),
		width: 'auto'
	});

	$(".select2-locate").select2({
		theme: 'bootstrap-5',
		dropdownParent: $("#select-loc"),
		width: 'auto'
	});

	$(".select2-layer").select2({
		theme: 'bootstrap-5',
		dropdownParent: $("#select-lay"),
		width: 'auto'
	});
})

</script>