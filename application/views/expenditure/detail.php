    <div class="modal-body m-3">
    	<div class="row">
            <div class="col-md-12">
                <table class="table table-bordered table-inverse table-responsive">
                    <thead class="thead-default">
                        <tr>
                            <th>Matrial Code</th>
                            <th>Material Name</th>
                            <th>Inci Name</th>
                            <th>Quantity</th>
                            <th>Stock Minimum</th>
                            <th>Category</th>
                            <th>Price</th>
                            <th>Halal</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><?= $api->material != null ? $api->material->material_code : "-"?></td>
                            <td><?= $api->material != null ? $api->material->material_name : "-"?></td>
                            <td><?= $api->material != null ? $api->material->inci_name : "-"?></td>
                            <td><?= $api->material != null ? $api->quantity : "-"?></td>
                            <td><?= $api->material != null ? $api->material->price : "-"?></td>
                            <td><?= $api->material != null ? $api->material->category : "-"?></td>
                            <td>
                                <?php if($api->material != null && $api->material->halal == 1): ?>
                                    <span class="badge bg-success">Halal</span>
                                <?php elseif($api != null && $api->material->halal != 1): ?>
                                    <span class="badge bg-warning">Non halal</span>
                                <?php else: ?>
                                    <span>-</span>
                                <?php endif; ?>
                            </td>
                            <td><?= $api->material->stock_minimum ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            </div>
    	</div>
    </div>
    <div class="modal-footer">
    	<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
    </div>