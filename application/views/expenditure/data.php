<main class="content">
	<div class="container-fluid p-0">

		<h1 class="h3 mb-3">Expenditure</h1>

		<div class="row">
			<div class="col-12 col-xl-12">
				<div class="card">
					<div class="card-header">
						<h5 class="card-title">Expenditure

						</h5>
						<h6 class="card-subtitle text-muted">List data Expenditure
						</h6>
					</div>
					<div class="card-body">
						<?= $this->session->flashdata('msg') ?>
						<table class="table table-striped dataTable">
							<thead>
								<tr>
									<th>#</th>
									<th>Expenditure No.</th>
									<th>Expenditure Date</th>
									<th>Quantity</th>
									<th>Desc</th>
									<th>Location</th>
									<th>Layer</th>
									<th>Created At</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$no = 1;
									foreach($get as $row):
								?>
								<tr>
									<td><?= $no++ ?></td>
									<td><?= $row->code ?></td>
									<td><?= date($row->date) ?></td>
									<td><?= $row->quantity.' g' ?></td>
									<td><?= $row->keterangan ?></td>
									<td>
										<?php foreach($expenditure as $c){ 
											if($row->id == $c->expenditure_id){
												echo substr($c->location, 0, strpos($c->location, '.'));
											} else{
												echo "";
											}
										 }  
										?>
									</td>
									<td>
										<?php foreach($expenditure as $c){ 
											if($row->id == $c->expenditure_id){
												echo substr($c->layer_loc, 0, strpos($c->layer_loc, '.'));
											} else{
												echo "";
											}
										 }  
										?>
									</td>
									<td><?= $row->created_at ?></td>
									<td class="table-action">
										<a href="#!" data-bs-toggle="modal" data-bs-target="#detailModal"
											data-id="<?= $row->id ?>"><i data-feather="eye"></i></a>
										<a href="#!" data-bs-toggle="modal" data-bs-target="#editModal"
											data-id="<?= $row->id ?>"><i data-feather="edit"></i></a>
									</td>
								</tr>

								<?php
									endforeach;
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>

		</div>

	</div>
</main>

<div class="modal fade" id="detailModal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Detail Data Expenditure</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div id="detail_data">

			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="editModal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Edit Data Expenditure</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div id="edit_data">

			</div>
		</div>
	</div>
</div>

<script>
	$("#detailModal").on('shown.bs.modal', function (e) {
		var id = $(e.relatedTarget).data('id');
		$.ajax({
			type: "POST",
			url: "<?= base_url() ?>admin/expenditure/detail",
			data: {
				id: id
			}
		}).done(function (response) {
			$("#detail_data").html(response);
		});
	});


	$("#editModal").on('shown.bs.modal', function (e) {
		var id = $(e.relatedTarget).data('id');
		$.ajax({
			type: "POST",
			url: "<?= base_url() ?>admin/expenditure/get",
			data: {
				id: id
			}
		}).done(function (response) {
			$("#edit_data").html(response);
		});
	});

	function add_putaway() {
		var expenditure_num = $("[name=expenditure_num]").val();
		var location = $("[name=location]").val();
		var total_box = $("[name=total_box]").val();
		if (expenditure_num == '') {
			swal.fire("Error!", "Must input expenditure nu first!", 'error');
		} else {
			// console.log(arr_box_code)
			// console.log(arr_box_id)

			$.ajax({
				type: "POST",
				url: "<?= base_url() ?>admin/expenditure/add",
				data: {
					expenditure_num: expenditure_num,
					location: location,
				}
			}).done(function(response) {
				// console.log(response);
				if (response.includes('OK')) {
					swal.fire("OK!", "Successful add expenditure", "success").then(function() {
						window.location.reload();
					});

					$(".alert").css({
						'display': 'none'
					})
				} else {
					swal.fire("Error!", "Failed add expenditure", "error").then(function() {
						window.location.reload();
					});
				}
			});
		}
	}


	function delete_data(id) {
		Swal.fire({
			title: 'Konfirmasi ?',
			text: "Apakah kamu yakin ?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#28a745',
			cancelButtonColor: '#dc3545',
			confirmButtonText: 'Yes!',
			cancelButtonText: 'No!',
			showLoaderOnConfirm: true,
			preConfirm: function () {
				return new Promise(function (resolve, reject) {
					$.ajax({
						type: 'POST',
						url: "<?= base_url() ?>admin/shipping/delete",
						data: {
							id: id
						}
					}).done(function (msg) {
						if (msg == "ok") {
							swal.fire("OK!", "Data berhasil dihapus!", "success").then(
								function () {
									location.reload();
								})
						} else {
							swal.fire("Gagal!", msg, "error").then(function () {
								location.reload();
							})
						}
					})
				})
			},
			allowOutsideClick: () => !Swal.isLoading()
		})
	}

	let index = 0;

</script>
