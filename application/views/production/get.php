<form action="<?= base_url() ?>admin/production/edit" method="POST">
    <input type='hidden' value="<?= $production['id'] ?>" name="id">
    <div class="modal-body m-3">
        <div class="mb-3">
            <label class="form-label">Batch Num <span class="text-danger">*</span></label>
            <select name="replenishment_id" class="form-control select2" required>
                <option value="">- Select One -</option>
                <?php foreach ($replenishment as $row) { ?>
                    <option value="<?= $row->replenishment_id ?>" <?= $row->replenishment_id == $production['replenishment_id'] ? 'selected' : ''; ?>><?= $row->wo_formula ?></option>
                <?php } ?>
            </select>
        </div>

		<div class="mb-3">
            <label class="form-label">QTY</label>
            <input type="text" class="form-control" name="qty" placeholder="QTY" value="<?= $production['qty'] ?>" required>
        </div>

        <div class="mb-3">
            <label class="form-label">Status</label>
            <select class="form-control" name="status" required>
                <option value="No Started" <?= ($production['status']=='No Started') ? 'selected' : ''; ?>>No Started </option>
                <option value="Production" <?= ($production['status']=='Production') ? 'selected' : ''; ?>>Production</option>
                <option value="Done" <?= ($production['status']=='Done') ? 'selected' : ''; ?>>Done</option>
            </select>
        </div>

        <div class="mb-3">
            <label class="form-label">Date</label>
            <input type="date" class="form-control" name="date" placeholder="Date" value="<?= $production['date'] ?>" required>
        </div>

    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
    </div>
</form>

<script>
	$(function(){
		$(".select2").select2({
			theme: 'bootstrap-5',
			dropdownParent: $("#editModal"),
			width: 'auto'
		});
	});
</script>
