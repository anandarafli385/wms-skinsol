<!DOCTYPE html>
<html>

<head>
    <title>Material Release. <?= $get['no_dokumen'] ?></title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inter&display=swap" rel="stylesheet">
    <style type="text/css">
        @page {
            margin: 0cm 0cm;
			padding: 3cm 4cm;
            font-family: 'Inter', sans-serif;
        }

        body {
            margin: 0px;
            font-size: 9px;
            padding: 14px;
        }

        .text-center {
            text-align: center;
        }

        .text-right {
            text-align: right;
        }

        .text-left {
            text-align: left;
        }

        .my-0 {
			margin-bottom: 0px; 
			margin-top: 0px;
        }

        h2 {
            margin-top: 5px;
            margin-bottom: 2px;
        }

        .table {
            margin: 0;
            border-collapse: collapse;
            border: 0;
        }

        .table>tbody>tr>td {
            border-top: 0;
        }

        .table>tbody>tr>.br {
            border-right: 0;
        }

        .table>tbody>tr>.bl {
            border-left: 0;
        }

        .td-valign-top>tbody>tr>td {
            vertical-align: top;
        }

        .bl {
            border-left: 1px solid #000 !important;
        }

        .bt {
            border-top: 1px solid #000 !important;
        }

        .nowrap {
            white-space: nowrap !important;
        }

        .page_break {
            page-break-after: auto;
        }

		.body{
			margin: 45px 80px;
		}
    </style>
</head>

<body>
    <div class="header" style="margin-top: 50px;">
		<h1 class="text-center">Material Release</h1>
		<p class="text-center">No. Dokumen Material Release <?= $get['no_dokumen'] ?></p>
	</div>
	
	<?php 

		$fg_materials = $this->sql->select_table('tbl_fg_material', [
			'fg_id' => $detail[0]->fg_id
		])->result();

		$pembuat = $this->db->where([
			'user_id' => $get['user_id']
		])->get('tbl_user')->row();

		$approved = $this->db->where([
			'user_id' => $get['approved_by']
		])->get('tbl_user')->row();
	?>
	<table width="100%" border="1px" style="margin-bottom: 0px; border-collapse: collapse">
		<tr>
			<td width="35%" class="text-center" style="border-bottom: 1px solid #fff !important;">
				<img width="125" src="http://localhost/skinsol-wms/assets/src/img/logo-skinsol.png">
			</td>
			<td width="60%" style="border-bottom: 1px solid #fff !important;">
				<h1 class="text-center" style="margin-top: 40px;">CATATAN PENGOLAHAN BATCH<br/> PROSES PRODUKSI </h1>
			</td>
			<td width="35%" style="border-bottom: 1px solid #fff !important;">
				<div style="margin-left:5px;" class="my-0">
					<h3 class="text-left my-0">Standar : CPKB ISO 9001,GMP</h3>
				</div>
			</td>
		</tr>
	</table>

	<table width="100%" border="1px" style="margin-bottom: 0px; border-collapse: collapse">
		<tr>
			<td width="35%" style="border-bottom: 1px solid #fff !important;">
				<h3 class="text-center my-0">No. Dokumen. <?= $get['no_dokumen'] ?></h3>
				<h3 class="text-center my-0">No Revisi : 01</h3>
				<h3 class="text-center my-0">Berlaku : <?= $get['tgl_berlaku'] ?></h3>
			</td>					
			<td width="30%" style="border-bottom: 1px solid #fff !important;">
				<h3 class="text-center my-0">Besar Bets : <br> <?= $get['besar_bets'] .'Kg'?> / <?= $detail[0]->qty .'pcs'  ?></h3>
			</td>
			<td width="30%" style="border-bottom: 1px solid #fff !important;">
				<h3 class="text-center my-0">Kode Produk : <?= $detail[0]->fg_id ?> </h3>
				<h3 class="text-center my-0">No.Batch : <?= $get['no_batch'].'.'.$detail[0]->fg_id ?></h3>
			</td>
			<td width="35%" style="border-bottom: 1px solid #fff !important;">
				<div style="margin-left:5px" class="my-0">
					<h3 class="text-center my-0"> Halaman </h3>
					<h3 class="text-center my-0"> 1 dari 1</h3>
				</div>
			</td>
		</tr>
	</table>


	<table width="100%" border="1px" style="margin-bottom: 20px; border-collapse: collapse">
		<tr>
			<td width="35%">
				<h3 class="text-center my-0">Bentuk Sediaan : <br> <?= $get['bentuk_sediaan'] ?></h3>
				<h3 class="text-center my-0">Kemasan Primer : <br> <?= $get['kemasan_primer'] ?></h3>
			</td>					
			<td width="30%">
				<h3 class="text-center my-0">Disusun Oleh : <br> <?= $pembuat->name?></h3>
			</td>
			<td width="30%">
				<h3 class="text-center my-0">Disetujui Oleh : <br> <?= $approved->name ?></h3>
			</td>
			<td width="35%">
				<div style="margin-left:5px" class="my-0">
					<h3 class="text-center my-0"> Tanggal Pengolahan </h3>
					<h3 class="text-center my-0"> Tanggal Mulai : <?= $get['tgl_mulai'] ?></h3>
					<h3 class="text-center my-0"> Tanggal Selesai : <?= $get['tgl_selesai'] ?></h3>
				</div>
			</td>
		</tr>
	</table>

	<h1><b>PENGEMBANGAN BAHAN</b></h1>

	<table width="100%" border="1px" style="margin-bottom: 40px; border-collapse: collapse">
		<tr>
			<th width="20%">Nama Bahan</th>
			<th width="20%">No Batch</th>
			<th width="30%">Jumlah Yang Dibutuhkan</th>
			<th width="30%">Jumlah Yang Ditimbang</th>
		</tr>

		<?php 

			foreach($fg_materials as $mtr):
				$material = $this->sql->select_table('tbl_material', [
					'material_no' => $mtr->material_id
				])->row();
		?>
			<tr>
				<td style="padding-left: 10px"><?= $material->nama ?></td>
				<td class="text-center"><?= $get['no_batch'] ?></td>
				<td class="text-center"><?= $mtr->qty . $material->unit ?> </td>
				<td class="text-center"><?= $mtr->qty . $material->unit ?> </td>
				<!-- <td><?= $material->harga ?></td> -->
				<!-- <td class="text-center"><?= $mtr->qty ?></td> -->
			</tr>
		<?php endforeach; ?>
	</table>
</body>

</html>
