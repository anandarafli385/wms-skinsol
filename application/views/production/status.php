<form action="<?= base_url() ?>admin/material_release/edit_status/<?= $get['material_release_id'] ?>" method="post">
<div class="modal-body m-3">
	<input type="hidden" name="material_release_id" value="<?= $get['material_release_id'] ?>">
	<div class="mb-3">
        <label class="form-label">No. Dokumen Material Release </label><br>
        <b><?= $get['no_dokumen'] ?></b>
    </div>

	<div class="mb-3">
        <label class="form-label">Tanggal Berlaku</label><br>
        <b><?= dateID($get['tgl_berlaku']) ?></b>
    </div>


	<div class="form-group mb-3">
	  <label for="status_approval">Status Material Release</label>
	  <select class="form-control" name="status_approval">
		<option value="approved">Approved</option>
		<option value="not approved">Not Approved</option>
	  </select>
	</div>

</div>
<div class="modal-footer">
	<button type="submit" class="btn btn-primary">Update</button>
    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
</div>
</form>

<script>
	$(".good").keyup(function () {
		val = $(this).val()
		no = $(this).data('number')
		reject = $(this).attr('max') - val
		console.log(no)

		$('.text-reject-'+no).text(reject)
		if(reject == 0){
			$('.text-reject-'+no).css({
				'color':'black'
			})
		}else{
			$('.text-reject-'+no).css({
				'color':'red'
			})
		}
	});

	$(".good").change(function () {
		val = $(this).val()
		no = $(this).data('number')
		reject = $(this).attr('max') - val
		console.log(no)

		$('.text-reject-'+no).text(reject)
		if(reject == 0){
			$('.text-reject-'+no).css({
				'color':'black'
			})
		}else{
			$('.text-reject-'+no).css({
				'color':'red'
			})
		}
	});
</script>
