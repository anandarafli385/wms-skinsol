<main class="content">
	<div class="container-fluid p-0">

		<h1 class="h3 mb-3">Production</h1>

		<div class="row">
			<div class="col-12 col-xl-12">
				<div class="card">
					<div class="card-header">
						<h5 class="card-title">Production
						<!-- <div class="float-end">
								<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#addModal"><i class="fa fa-plus"></i> Add Data</button>
							</div>
						</h5> -->
						<h6 class="card-subtitle text-muted">List data Production</h6>
					</div>
					<div class="card-body">
						<?= $this->session->flashdata('msg') ?>
						<table class="table table-striped dataTable">
							<thead>
								<tr>
									<th>No. Work Order</th>
									<th>Batch Number</th>
									<th>QTY</th>
									<th>Persentase</th>
									<th>Status</th>
									<th>Tanggal</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$no = 1;
									$nomor = 1;
									foreach ($production as $row) { 
								?>
									<tr>
										<td><?= $row['wo_num'] ?></td>
										<td><?= $row['replenishment'] ?></td>
										<td><?= $row['qty'] ?></td>
										<td>
										<?php 
											$query = $this->db->query("SELECT sum(qty) AS `total` FROM `tbl_production_logs` WHERE `production_id` = ".$row['id']." ORDER BY id DESC LIMIT 1");
											$result = $query->result(); 
											$persentase = (intval($result[0]->total)/intval($row['qty']))*100; 
											echo $persentase.'%';
										?>
										</td>
										<td>
											<?php if($row['status'] == 'No Started'): ?>
												<span class="badge bg-danger"><?= $row['status'] ?></span>
											<?php elseif($row['status'] == 'Done'): ?>
												<span class="badge bg-success"><?= $row['status'] ?></span>
											<?php elseif($row['status'] == 'Production'): ?>
												<span class="badge bg-warning"><?= $row['status'] ?></span>
											<?php endif; ?>		
										</td>
										<td><?= date('d M Y H:i', strtotime($row['created_at'])) ?></td>
										<td class="table-action">
											<a href="#!" data-bs-toggle="modal" data-bs-target="#detailModal" data-id="<?= $row['id'] ?>" title="Detail" ><i data-feather="eye"></i></a>
											<a href="#!" data-bs-toggle="modal" data-bs-target="#addLogModal" data-id="<?= $row['id'] ?>" title="Log Data" class="mx-1"><i data-feather="edit"></i></a>
											<!-- <?php if($row['status'] != 'done'): ?> -->
												<!-- <a href="#!" data-bs-toggle="modal" data-bs-target="#editModal" data-id="<?= $row['id'] ?>" class="mx-1"><i data-feather="edit" title="Edit Data"></i></a> -->
											<!-- <?php endif; ?> -->
											<?php if($this->session->level == 0 || $this->session->level == 3): ?>
												<?php if($row['status'] != 'done'): ?>
													<a href="#!" onclick="delete_data('<?= $row['id'] ?>')"><i data-feather="trash-2" class="text-danger"></i></a>
												<?php endif; ?>
											<?php endif; ?>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>

		</div>

	</div>
</main>

<div class="modal fade" id="editModal" tabindex="-2" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-md" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Edit Data Production</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div id="edit_data"></div>
		</div>
	</div>
</div>

<div class="modal fade" id="detailModal" tabindex="-3"  aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Detail Data Production</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div id="detail_data"></div>
		</div>
	</div>
</div>


<div class="modal fade" id="addModal" tabindex="-1" aria-hidden="true">
	<div class="modal-dialog modal-md" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Add Data Production</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body m-3">
				<form action="<?= base_url() ?>admin/production/add" method="POST">
				<div class="mb-3" id="select-add">
					<label class="form-label">Pilih Batch Num</label>
					<select required class="form-control select2" name="replenishment_id" id="replenishment_add">
						<option>Choose Batch Num</option>
						<?php foreach ($replenishment as $val) : ?>
							<option value="<?= $val->replenishment_id ?>"><?= $val->wo_formula ?></option>
						<?php endforeach; ?>
					</select>
				</div>

				<div class="mb-3">
					<label class="form-label">QTY</label>
					<input type="number" class="form-control" name="qty" id="qty" placeholder="QTY" readonly required>
				</div>

				<div class="mb-3">
					<label class="form-label">Date</label>
					<input type="date" class="form-control" name="date" placeholder="Date" required>
				</div>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Save changes</button>
			</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="addLogModal" tabindex="-4" aria-hidden="true">
	<div class="modal-dialog modal-md" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Add Log Production</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body m-3">
				<form action="<?= base_url() ?>admin/production/add_log" method="POST">
				<input type="hidden" name="production_id" value="" id="production_id">

				<div class="mb-3">
					<label class="form-label">Formula</label>
					<input type="text" class="form-control" name="formula" id="formula" readonly required>
				</div>
				<div class="mb-3" id="select-log">
					<label class="form-label">Status</label>
					<select class="form-control selects2" name="status" id="status" required>
						<option value="Production">Production</option>
						<option value="Done">Done</option>
					</select>
				</div>
					

				<div class="mb-3">
					<label class="form-label">QTY</label>
					<input type="number" class="form-control" name="qty" id="qty_log" placeholder="QTY" required>
				</div>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Save changes</button>
			</div>
			</form>
		</div>
	</div>
</div>

<script>
	
	$("#editModal").on('shown.bs.modal', function (e) {
		var id = $(e.relatedTarget).data('id');
		$.ajax({
			type: "GET",
			url: "<?= base_url() ?>admin/production/get/"+id,
			dataType: "html",
			success: function (data) {
                $('#edit_data').html(data);
            },
		});
	});


	$("#replenishment_add").on('select2:select', function(e) {
		var id = $(e.relatedTarget).data('id');
		$.ajax({
			type: "GET",
			url: "<?= base_url() ?>admin/production/detail_product/"+id,
			success: function (data) {
				$("#qty").val('');
				qty = data.replenishment[0].wo_pack
				total = data.persentase[0].total
				$("#qty").val(parseInt(qty) - parseInt(total));
            },
		});
	});

	$("#status").on('select2:select', function(e) {
		value = e.params.data.id
		var id = $(e.relatedTarget).data('id');
		if(value == "Done"){
			$('#qty_log').attr('readonly',true)
			$.ajax({
				type: "GET",
				url: "<?= base_url() ?>admin/production/detail_product/"+id,
				success: function (data) {
					$("#qty_log").val('');
					qty = data.replenishment[0].wo_pack
					$("#qty_log").val(parseInt(qty));
				},
			});

		}else{
			$("#qty_log").val('');
			$('#qty_log').attr('readonly',false)
		}	
	});

	$('#editModal').on('hidden.bs.modal', function () {
		$('#edit_data').html('');
	})

	$("#detailModal").on('shown.bs.modal', function (e) {
		var id = $(e.relatedTarget).data('id');
		$.ajax({
			type: "GET",
			url: "<?= base_url() ?>admin/production/detail/"+id,
			dataType: "html",
			success: function (data) {
                $('#detail_data').html(data);
            },
		});
	});

	$('#detailModal').on('hidden.bs.modal', function () {
		$('#detail_data').html('');
	})

	$("#addLogModal").on('shown.bs.modal', function (e) {
		var id = $(e.relatedTarget).data('id');
		$('#production_id').val(id);
		$.ajax({
			type: "GET",
			url: "<?= base_url() ?>admin/production/detail_product/"+id,
			success: function (data) {
				console.log(data)
				$("#formula").val('');
				formula = data.replenishment[0].wo_formula
				$("#formula").val(formula);
				
				qty = data.replenishment[0].wo_pack
				total = data.persentase[0].total
				$('#qty_log').on('input', function () {
					var values = $(this).val();
					if ((values !== '') && (values.indexOf('.') === -1)) {
						$(this).val(Math.max(Math.min(values, (parseInt(qty) - parseInt(total))), 0));
					}
				});
            },
		});
	});

	$('#addLogModal').on('hidden.bs.modal', function (e) {
		var id = $(e.relatedTarget).data('id');
		$('#production_id').val('');
	})



	function delete_data(id) {
		Swal.fire({
			title: 'Konfirmasi ?',
			text: "Apakah kamu yakin ?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#28a745',
			cancelButtonColor: '#dc3545',
			confirmButtonText: 'Yes!',
			cancelButtonText: 'No!',
			showLoaderOnConfirm: true,
			preConfirm: function () {
				return new Promise(function (resolve, reject) {
					$.ajax({
						type: 'POST',
						url: "<?= base_url() ?>admin/production/delete",
						data: {
							id: id
						}
					}).done(function (msg) {
						if (msg.includes("ok")) {
							swal.fire("OK!", "Data berhasil dihapus!", "success").then(
								function () {
									location.reload();
								})
						} else {
							swal.fire("Gagal!", msg, "error").then(function () {
								location.reload();
							})
						}
					})
				})
			},
			allowOutsideClick: () => !Swal.isLoading()
		})
	}



</script>
