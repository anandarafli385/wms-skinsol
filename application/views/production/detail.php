<div class="modal-body m-3">
	<div class="row">
		<div class="col-lg-6">
			<div class="mb-3">
				<label class="form-label">Batch Number</label><br>
				<b><?= $production['replenishment'] ?></b>
			</div>
	
			<div class="mb-3">
				<label class="form-label">QTY</label><br>
				<b><?= $production['qty'] ?></b>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="mb-3">
				<label class="form-label">Tanggal</label><br>
				<b><?= dateID($production['date']) ?></b>
			</div>
			<div class="mb-3">
				<label class="form-label">Status</label><br>
				<b><?= $production['status'] ?></b>
			</div>
		</div>

	</div>

	<div class="mb-3 mt-4">
		<h5>PRODUCTION LOG</h5>
		<div class="table-responsive">
			<table class="table table-sm table-striped">
				<thead>
					<tr>
						<th>No</th>
						<th>Quantity</th>
						<th>Status</th>
						<th>Created at</th>
					</tr>
				</thead>

				<tbody>
					<?php 
						$no = 1;
						foreach($logs as $row): 
						?>
						<tr>
							<td><?= $no++ ?></td>
							<td><?= $row['qty'] ?></td>
							<td><?= $row['status'] ?></td>
							<td><?= date('d M Y (H:i)', strtotime($production['created_at'])) ?></td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
</div>
