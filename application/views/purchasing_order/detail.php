<div class="modal-body m-3">
	<div class="row">
		<div class="col-6">
			<div class="mb-3">
				<label class="form-label">ID PO.</label><br>
				<b><?= $api_detail->id ?></b>
			</div>
			<div class="mb-3">
				<label class="form-label">PO No.</label><br>
				<b><?= $api_detail->po_num ?></b>
			</div>
			<div class="mb-3">
				<label class="form-label">Date</label><br>
				<b><?= $api_detail->po_date ?></b>
			</div>
			<div class="mb-3">
				<label class="form-label">PPN</label><br>
				<b><?= $api_detail->ppn ?></b>
			</div>
			<div class="mb-3">
				<label class="form-label">terms</label><br>
				<b><?= $api_detail->terms ?></b>
			</div>
			<div class="mb-3">
				<label class="form-label">Status</label><br>
				<b><?= $api_detail->status ?></b>
			</div>
			<div class="mb-3">
				<label class="form-label">Created At</label><br>
				<b><?= $api_detail->created_at ?></b>
			</div>
		</div>
		<div class="col-6">
			<div class="mb-3">
				<label class="form-label">Supplier ID</label><br>
				<b><?= $api_detail->supplier->id ?></b>
			</div>
			<div class="mb-3">
				<label class="form-label">Supplier Code</label><br>
				<b><?= $api_detail->supplier->supplier_code ?></b>
			</div>
			<div class="mb-3">
				<label class="form-label">Supplier Name</label><br>
				<b><?= $api_detail->supplier->supplier_name ?></b>
			</div>
			<div class="mb-3">
				<label class="form-label">Supplier Mobile</label><br>
				<b><?= $api_detail->supplier->supplier_mobile ?></b>
			</div>
			<div class="mb-3">
				<label class="form-label">Supplier Email</label><br>
				<b><?= $api_detail->supplier->supplier_email ?></b>
			</div>
			<div class="mb-3">
				<label class="form-label">Supplier Address</label><br>
				<b><?= $api_detail->supplier->supplier_address ?></b>
			</div>
			<div class="mb-3">
				<label class="form-label">Contact Person</label><br>
				<b><?= $api_detail->supplier->contact_person ?></b>
			</div>
		</div>
	</div>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
</div>
