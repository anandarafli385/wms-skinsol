<main class="content">
	<div class="container-fluid p-0">

		<h1 class="h3 mb-3">Master Customer</h1>

		<div class="row">
			<div class="col-12 col-xl-12">
				<div class="card">
					<div class="card-header">
						<h5 class="card-title">Master Customer
						</h5>
						<h6 class="card-subtitle text-muted">List data master customer
						</h6>
					</div>
					<div class="card-body">
						<?= $this->session->flashdata('msg') ?>
						<table class="table table-striped dataTable">
							<thead>
								<tr>
									<th>No.</th>
									<th>Customer Code</th>
									<th>Customer Name</th>
									<th>Customer Brand</th>
									<th>Address</th>
									<th>Phone</th>
									<th>Email</th>
								</tr>
							</thead>
							<tbody>
								<?php
                            $no = 1;
                            foreach ($api as $row) { ?>
								<tr>
									<td><?= $no++; ?></td>
									<td><?= $row->customer_code ?></td>
									<td><?= $row->customer_name ?></td>
									<td><?= $row->customer_brand ?></td>
									<td><?= $row->customer_address ?></td>
									<td><?= $row->customer_mobile ?></td>
									<td><?= $row->customer_email ?></td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>

		</div>

	</div>
</main>
<div class="modal fade" id="addModal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Add Data Customer</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<form action="<?= base_url() ?>admin/master_customer/add" method="POST">
				<div class="modal-body m-3">
					<div class="mb-3">
						<label class="form-label">Customer Name <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="nama" placeholder="Customer Name" required>
					</div>
					<div class="mb-3">
						<label class="form-label">Address <span class="text-danger">*</span></label>
						<textarea name="alamat" class="form-control" placeholder="Address"></textarea>
					</div>
					<div class="row mb-3">
						<div class="col">
							<input type="text" class="form-control" name="kota" placeholder="City" required>
						</div>
						<div class="col">
							<input type="text" class="form-control" name="provinsi" placeholder="Province" required>
						</div>
					</div>
					<div class="row mb-3">
						<div class="col">
							<input type="text" class="form-control" name="negara" placeholder="Country" required>
						</div>
						<div class="col">
							<input type="text" class="form-control" name="kode_pos" placeholder="Postal Code" required>
						</div>
					</div>
					<div class="mb-3">
						<label class="form-label">Phone <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="telp" placeholder="Phone" required>
					</div>
					<div class="mb-3">
						<label class="form-label">Fax</label>
						<input type="text" class="form-control" name="fax" placeholder="Fax">
					</div>
					<div class="mb-3">
						<label class="form-label">Email <span class="text-danger">*</span></label>
						<input type="email" class="form-control" name="email" placeholder="Email" required>
					</div>
					<div class="mb-3">
						<label class="form-label">Contact Person <span class="text-danger">*</span></label>
						<input type="text" class="form-control" name="contact_person" placeholder="Contact Person"
							required>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Save changes</button>
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="modal fade" id="editModal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Edit Data Customer</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div id="edit_data"></div>
		</div>
	</div>
</div>
<script>
	$("#editModal").on('shown.bs.modal', function (e) {
		var id = $(e.relatedTarget).data('id');
		$.ajax({
			type: "POST",
			url: "<?= base_url() ?>admin/master_customer/get",
			data: {
				id: id
			}
		}).done(function (response) {
			$("#edit_data").html(response);
		});
	});

	function delete_data(id) {
		Swal.fire({
			title: 'Konfirmasi ?',
			text: "Apakah kamu yakin ?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#28a745',
			cancelButtonColor: '#dc3545',
			confirmButtonText: 'Yes!',
			cancelButtonText: 'No!',
			showLoaderOnConfirm: true,
			preConfirm: function () {
				return new Promise(function (resolve, reject) {
					$.ajax({
						type: 'POST',
						url: "<?= base_url() ?>admin/master_customer/delete",
						data: {
							id: id
						}
					}).done(function (msg) {
						if (msg == "ok") {
							swal.fire("OK!", "Data berhasil dihapus!", "success").then(
								function () {
									location.reload();
								})
						} else {
							swal.fire("Gagal!", msg, "error").then(function () {
								location.reload();
							})
						}
					})
				})
			},
			allowOutsideClick: () => !Swal.isLoading()
		})
	}

</script>
