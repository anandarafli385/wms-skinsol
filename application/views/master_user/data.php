<main class="content">
    <div class="container-fluid p-0">

        <h1 class="h3 mb-3">Master User</h1>

        <div class="row">
            <div class="col-12 col-xl-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Master User
                            <div class="float-end">
                                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#addModal"><i class="fa fa-plus"></i> Add User</button>
                            </div>
                        </h5>
                        <h6 class="card-subtitle text-muted">List data master user
                        </h6>
                    </div>
                    <div class="card-body">
					<?= $this->session->flashdata('msg') ?>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Username</th>
                                <th>Level</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            foreach ($get as $row) { ?>
                                <tr>
                                    <td><?= $no++; ?></td>
                                    <td><?= $row['name'] ?></td>
                                    <td><?= $row['username'] ?></td>
                                    <td>
                                        <?php if ($row['level'] == 0) { ?>
                                            <span class="badge bg-success">Super Admin</span>
                                        <?php }else if ($row['level'] == 1){ ?>
                                            <span class="badge bg-primary">Supervisor</span>
                                        <?php }else if ($row['level'] == 2){ ?>
                                            <span class="badge bg-warning">Warehouse</span>
                                        <?php }else if ($row['level'] == 4){ ?>
                                            <span class="badge bg-secondary">Approval</span>
                                        <?php }else{ ?>
                                            <span class="badge bg-info">Production</span>
                                        <?php } ?>
                                    </td>
                                    <td class="table-action">
                                        <a href="#!" data-bs-toggle="modal" data-bs-target="#editModal" data-id="<?= $row['user_id'] ?>"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2 align-middle">
                                                <path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path>
                                            </svg></a>
                                        <a href="#!" onclick="delete_data('<?= $row['user_id'] ?>')"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash align-middle">
                                                <polyline points="3 6 5 6 21 6"></polyline>
                                                <path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path>
                                            </svg></a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
					</div>
                </div>
            </div>

        </div>

    </div>
</main>
<div class="modal fade" id="addModal" tabindex="-1" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Data User</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="<?= base_url() ?>admin/master_user/add" method="POST">
                <div class="modal-body m-3">
                    <div class="mb-3">
                        <label class="form-label">Name <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" name="name" placeholder="Name" required>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Username <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" name="username" placeholder="Username" required>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Password <span class="text-danger">*</span></label>
                        <input type="password" class="form-control" name="password" placeholder="Password" required>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Level <span class="text-danger">*</span></label>
                        <select name="level" class="form-control" required>
                            <option value="">- Select One -</option>
                            <option value="0">Super Admin</option>
                            <option value="1">Supervisor</option>
                            <option value="2">Warehouse</option>
                            <option value="3">Production</option>
                            <option value="4">Approval</option>
                            <option value="5">Quality Control</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Save changes</button>
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="editModal" tabindex="-1" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Data User</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div id="edit_data"></div>
        </div>
    </div>
</div>
<script>
    $("#editModal").on('shown.bs.modal', function(e) {
        var id = $(e.relatedTarget).data('id');
        $.ajax({
            type: "POST",
            url: "<?= base_url() ?>admin/master_user/get",
            data: {
                id: id
            }
        }).done(function(response) {
            $("#edit_data").html(response);
        });
    });

    function delete_data(id) {
        Swal.fire({
            title: 'Konfirmasi ?',
            text: "Apakah kamu yakin ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#28a745',
            cancelButtonColor: '#dc3545',
            confirmButtonText: 'Yes!',
            cancelButtonText: 'No!',
            showLoaderOnConfirm: true,
            preConfirm: function() {
                return new Promise(function(resolve, reject) {
                    $.ajax({
                        type: 'POST',
                        url: "<?= base_url() ?>admin/master_user/delete",
                        data: {
                            id: id
                        }
                    }).done(function(msg) {
                        if (msg == "ok") {
                            swal.fire("OK!", "Data berhasil dihapus!", "success").then(function() {
                                location.reload();
                            })
                        } else {
                            swal.fire("Gagal!", msg, "error").then(function() {
                                location.reload();
                            })
                        }
                    })
                })
            },
            allowOutsideClick: () => !Swal.isLoading()
        })
    }
</script>
