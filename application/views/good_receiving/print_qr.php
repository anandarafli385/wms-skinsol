<!DOCTYPE html>
<html>

<head>
	<title>Print Good Receiving</title>
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Inter&display=swap" rel="stylesheet">
	<style type="text/css">
		@page {
			margin: 0cm 0cm;
			font-family: 'Inter', sans-serif;
		}

		body {
			margin: 0px;
			font-size: 9px;
			padding: 4px;
		}

		.text-center {
			text-align: center;
		}

		.text-right {
			text-align: right;
		}

		.text-left {
			text-align: left;
		}

		h2 {
			margin-top: 5px;
			margin-bottom: 2px;
		}

		h3 {
			line-height: 0%;
			font-size: 18px;
		}

		span {
			font-size: 20px;
		}

		.table {
			margin: 0;
			border-collapse: collapse;
			border: 0;
		}

		.table>tbody>tr>td {
			border-top: 0;
		}

		.table>tbody>tr>.br {
			border-right: 0;
		}

		.table>tbody>tr>.bl {
			border-left: 0;
		}

		.td-valign-top>tbody>tr>td {
			vertical-align: top;
		}

		.bl {
			border-left: 1px solid #000 !important;
		}

		.bt {
			border-top: 1px solid #000 !important;
		}

		.nowrap {
			white-space: nowrap !important;
		}

		.page_break {
			page-break-after: auto;
		}

	</style>
</head>

<body>
	<div class="page_break">
		<!-- QR Code (A6 Page) -->

		<table class="table td-valign-top" height="100%" width="100%" border="0" cellspacing="0" cellpadding="4">
			<tr>
				<td width="30%">
					<div class="text-center">
						 <img src="<?php echo '/assets/barcode/baku/'.$api->id.'.png' ?>" width="80%" /> <br> 
						<!--<img src="<?php echo '/home/wms/app/public/assets/barcode/'.$api->id.'.png' ?>" width="80%" /> <br>-->
						<center>
							<h3 style="margin-top: 3%;font-weight: bold"><?= $api->id ?></h3>
						</center>
					</div>
				</td>
				<td width="70%" style="vertical-align: top; padding-top: 5%">
					<table class="table" border="1" cellspacing="0" cellpadding="4" width="100%">
						<tr>
							<td width="40%">
								<span>No. Material</span>
							</td>
							<td>
								<span> <?= $api->material != null ? $api->material->material_code : "-"?></span>
							</td>
						</tr>
						<tr>
							<td width="40%">
								<span>Material</span>
							</td>
							<td>
								<span> <?= $api->material != null ? $api->material->material_name : "-"?></span>
							</td>
						</tr>
                        <tr>
							<td width="40%">
								<span>Material Status</span>
							</td>
							<td>
								<?php if($api->material != null && $api->material->halal == 1): ?>
									<span class="badge bg-success">Halal</span>
								<?php elseif($api->material != null && $api->material->halal != 1): ?>
									<span class="badge bg-warning">Non halal</span>
								<?php else: ?>
									<span>-</span>
								<?php endif; ?>
							</td>
						</tr>
						<tr>
							<td width="40%">
								<span>Category</span>
							</td>
							<td>
								<span> <?= $api->material != null ? $api->material->category : "-" ?></span>
							</td>
						</tr>
                        
						<tr>
							<td width="40%">
								<span>Batch No.</span>
							</td>
							<td>
								<span> <?= $api->batch_num ?></span>
							</td>
						</tr>
						<tr>
							<td width="40%">
								<span>QC No.</span>
							</td>
							<td>
								<span> <?= $api->analis_num ?></span>
							</td>
						</tr>
						<tr>
							<td width="40%">
								<span>Expired Date</span>
							</td>
							<td>
								<span style="border: 1px solid black;">&nbsp; &nbsp; </span>
								<span> &nbsp; <?=  date('d M Y', strtotime($api->expired_date)) ?></span>
							</td>
						</tr>
						<tr>
							<td width="40%">
								<span>Received Qty</span>
							</td>
							<td>
								<span> <?= number_format($api->quantity, 0, ',','.') ?></span>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
</body>

</html>
