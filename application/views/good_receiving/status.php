<form action="<?= base_url('admin/good_receiving/update_status') ?>" method="POST">
	<div class="modal-body m-3">
		<div class="mb-3">
			<div class="form-group">
				<input type="hidden" name="gr_id" value="<?= $get->id ?>">
			</div>
		</div>

		<div class="mb-3">
		    <label class="form-label" for="purchase_id">Purchase Id</label>
			<input type="text" class="form-control" name="purchase_id" value="<?= $get->purchase_id ?>" required readonly>
		</div>

		<div class="mb-3">
		    <label class="form-label" for="material_id">Material Id</label>
			<input type="text" class="form-control" name="material_id" value="<?= $get->material_id ?>" required readonly>
		</div>

		<div class="mb-3">
		    <label class="form-label" for="batch_num">Batch Num</label>
			<input type="text" class="form-control" name="batch_num" value="<?= $get->batch_num ?>" required readonly>
		</div>

		<div class="mb-3">
		    <label class="form-label" for="analisis_num">Analisis Num</label>
			<input type="text" class="form-control" name="analis_num" value="<?= $get->analis_num ?>" required readonly>
		</div>

		<div class="mb-3">
		  <label class="form-label" for="status">Status QC Good Receiving</label>
		  <select class="form-control select2-edit" name="status" id="status">
			<option value="ok">Ok</option>
			<option value="not ok">Not Ok</option>
		  </select>
		</div>

		<div class="mb-3" id="desc" style="display: none">
		    <label class="form-label" for="desc">Description</label>
			<textarea class="form-control" name="desc"></textarea>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
		<button type="submit" class="btn btn-primary">Save changes</button>
	</div>
</form>

<script>
	$(function(){
		$(".select2-edit").select2({
			theme: 'bootstrap-5',
			dropdownParent: $("#statusModal"),
			width: 'auto'
		});

	$("#status").on('select2:select', function(e) {
		let value = e.params.data.id
		let url = "<?= base_url() ?>"
		if(value === "not ok"){
			$("#desc").css({
				"display": "block"
			})
		} else {
			value = "OK"
			$("#desc").css({
				"display": "none"
			})
		}
	})

	})
</script>
