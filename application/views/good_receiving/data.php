<main class="content">
	<div class="container-fluid p-0">

		<h1 class="h3 mb-3">Good Receiving Bahan Baku</h1>

		<div class="row">
			<div class="col-12 col-xl-12">
				<div class="card">
					<div class="card-header">
						<h5 class="card-title">Good Receiving Bahan Baku
						</h5>
						<h6 class="card-subtitle text-muted">List data good receiving Bahan Baku</h6>
					</div>
					<div class="card-body">
					<?= $this->session->flashdata('msg') ?>
					<table class="table table-striped dataTable responsive">
						<thead>
							<tr>
								<th>No.</th>
								<th>Purchase Order No.</th>
								<th>Delivery Order No.</th>
								<th>Supplier Name</th>
								<th>Supplier Code</th>
								<th>Supplier Address</th>
								<th>GR Date</th>
								<th>Material Name</th>
								<th>Total Material</th>
								<th>Analisis No.</th>
								<th>Status QC</th>
								<th>Desc QC</th>
								<th>Expired Date</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$no = 1;
							foreach($api as $row){
								?>
								<tr class="item-model-number">
									<td><?= $no++; ?></td>
									<td><?= $row->purchase->purchase_num ?></td>
									<td><?= $row->purchase->delivery_orders_num ?></td>
									<td><?= $row->purchase->po_material != null ? $row->purchase->po_material->supplier->supplier_name : "-" ?></td>
									<td><?= $row->purchase->po_material != null ? $row->purchase->po_material->supplier->supplier_code : "-" ?></td>
									<td><?= $row->purchase->po_material != null ? $row->purchase->po_material->supplier->supplier_address : "-" ?></td>
									<td><?= $row->purchase->date ?></td>
									<td><?= $row->material != null ? $row->material->material_name : "-" ?></td>
									<td><?= $row->quantity ?></td>
									<td><?= $row->analis_num != null ? $row->analis_num : "-"?></td>
									<td class="status"> 
										<?php foreach ($qc as $q) {
										 if($q->gr_id == $row->id && $q->status == 'not checked'): ?>
											<span class="badge bg-danger"><?= $q->status ?></span>
										<?php elseif($q->gr_id == $row->id && $q->status == 'ok'): ?>
											<span class="badge bg-success"><?= $q->status ?></span>
										<?php elseif($q->gr_id == $row->id && $q->status == 'not ok'): ?>
											<span class="badge bg-warning"><?= $q->status ?></span>
										<?php endif; }?>			
									</td>
									<td class="desc_status">
										<?php foreach ($qc as $q) {
										 if($q->gr_id == $row->id):
										 	echo $q->desc; 
										 endif; }?>	
									</td>
									<td><?= $row->expired_date != null ? $row->expired_date : "-" ?></td>
									<td class="table-action">
										<a href="<?= base_url() ?>admin/good_receiving/print/<?= $row->id ?>" target="_blank"><i data-feather="printer"></i></a>
										<?php if($this->session->level == 0 ||  $this->session->level == 4 || $this->session->level == 5): ?>
												<a href="#!" data-bs-toggle="modal" data-bs-target="#detailModal" data-id="<?= $row->id ?>"><i data-feather="eye"></i></a>
										<?php endif; ?>
										<a href="#!" id="statusAction-<?= $no ?>" class="statusAction" data-bs-toggle="modal" data-bs-target="#statusModal" data-id="<?= $row->id ?>"><i data-feather="edit"></i></a>
									</td>
								</tr>
							<?php
							}?>
						</tbody>
					</table>
					</div>
				</div>
			</div>

		</div>

	</div>
</main>
<div class="modal fade" id="detailModal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Detail Data Good Receiving Bahan Baku</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div id="detail_data"></div>
		</div>
	</div>
</div><div class="modal fade" id="statusModal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Status QC Receiving Bahan Baku</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div id="status_data"></div>
		</div>
	</div>
</div>
<script>
	// let nilai_awal = 2;

	// $("#editModal").on('shown.bs.modal', function(e) {
	// 	var id = $(e.relatedTarget).data('id');
	// 	$.ajax({
	// 		type: "POST",
	// 		url: "<?= base_url() ?>admin/good_receiving/get",
	// 		data: {
	// 			id: id
	// 		}
	// 	}).done(function(response) {
	// 		console.log(response);
	// 		$("#edit_data").html(response);
	// 	});
	// });
	var no = 2;
	$(".item-model-number .status").each(function() {
	  var value = $(this).text();
		  if(value.includes('ok')){
	  	    $('#statusAction-'+no++).hide();
		  }else{
			$('#statusAction-'+no++).show();
		  }
	})

	$(".item-model-number .desc_status").each(function() {
	  var value = $(this).text();
		  if(value.includes('ok')){
	  	    $('#statusAction-'+no++).hide();
		  }else{
			$('#statusAction-'+no++).show();
		  }
	})


	$("#statusModal").on('shown.bs.modal', function(e) {
		var id = $(e.relatedTarget).data('id');
		$.ajax({
			type: "POST",
			url: "<?= base_url() ?>admin/good_receiving/status",
			data: {
				id: id
			}
		}).done(function(response) {
			$("#status_data").html(response);
		});
	});


	$("#detailModal").on('shown.bs.modal', function(e) {
		var id = $(e.relatedTarget).data('id');
		$.ajax({
			type: "POST",
			url: "<?= base_url() ?>admin/good_receiving/detail",
			data: {
				id: id
			}
		}).done(function(response) {
			$("#detail_data").html(response);
		});
	});
	// $(".material_id_1").on('select2:select', function (e) {
	// 	var description = $(this).find(":selected").data('description');
	// 	var unit = $(this).find(":selected").data('unit');

	// 	$('#material_description_1').text(description)
	// 	$('#material_unit_1').text(unit)
	// })

	

	// function delete_data(id) {
	// 	Swal.fire({
	// 		title: 'Konfirmasi ?',
	// 		text: "Apakah kamu yakin ?",
	// 		type: 'warning',
	// 		showCancelButton: true,
	// 		confirmButtonColor: '#28a745',
	// 		cancelButtonColor: '#dc3545',
	// 		confirmButtonText: 'Yes!',
	// 		cancelButtonText: 'No!',
	// 		showLoaderOnConfirm: true,
	// 		preConfirm: function() {
	// 			return new Promise(function(resolve, reject) {
	// 				$.ajax({
	// 					type: 'POST',
	// 					url: "<?= base_url() ?>admin/good_receiving/delete",
	// 					data: {
	// 						id: id
	// 					}
	// 				}).done(function(msg) {
	// 					if (msg == "ok") {
	// 						swal.fire("OK!", "Data berhasil dihapus!", "success").then(function() {
	// 							location.reload();
	// 						})
	// 					} else {
	// 						swal.fire("Gagal!", msg, "error").then(function() {
	// 							location.reload();
	// 						})
	// 					}
	// 				})
	// 			})
	// 		},
	// 		allowOutsideClick: () => !Swal.isLoading()
	// 	})
	// }

	// 	$(".material_id").on('select2:select', function (e) {
	// 		// console.log("nilai berubah")
	// 		let description = $(this).find(":selected").data('description');
	// 		var unit = $(this).find(":selected").data('unit');

	// 		$(this).closest('tr').children('td.material_description').text(description);
	// 		$(this).closest('tr').children('td.material_unit').text(unit);
	// 	})
	// }

	// function deleterow(btn) {
	// 	$(btn).closest("tr").remove();
	// }
</script>
