<div class="modal-body m-3">
    <div class="row">
        <div class="col-md-6">
            <div class="mb-3">
                <label class="form-label">Puchase Order No.</label><br>
                <b><?= $get->purchase_id ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">Material No.</label><br>
                <b><?= $get->material_id ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">Batch No.</label><br>
                <b><?= $get->batch_num ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">Expired Date</label><br>
                <b><?= $get->expired_date ?></b>
            </div>
        </div>
        <div class="col-md-6">
            <div class="mb-3">
                <label class="form-label">GR Date</label><br>
                <b><?= $get->purchase->date ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">Purchase No.</label><br>
                <b><?= $get->purchase->purchase_num ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">Delivery Order No.</label><br>
                <b><?= $get->purchase->delivery_orders_num ?></b>
            </div>
            <div class="mb-3">
                <label class="form-label">PO Material ID</label><br>
                <b><?= $get->purchase->po_material_id ?></b>
            </div>
        </div>
        <div class="row">
                <div class="col-md-12">
                    <h3 class="mt-5"><b>Material Detail</b></h3>
                    <table class="table table-striped">
                        <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Material No.</th>
                                    <th>Material Code</th>
                                    <th>Material Name</th>
                                    <th>Stock Minimum</th>
                                    <th>Category</th>
                                    <th>Price</th>
                                    <th>Halal</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <?php 
                                    $no = 1;
                                     ?>
                                    <td><?= $no++ ?></td>
                                    <td>
                                        <?= $get->material->id ?>
                                    </td>
                                    <td>
                                        <?= $get->material->material_code ?>
                                    </td>
                                    <td>
                                        <?= $get->material->material_name ?>
                                    </td>
                                    <td>
                                        <?= $get->material->stock_minimum ?>
                                    </td>
                                    <td>
                                        <?= $get->material->category ?>
                                    </td>
                                    <td>
                                        <?= $get->material->price ?>
                                    </td>
                                    <td>
                                        <?php if($get->material != null && $get->material->halal == 1): ?>
                                            <span class="badge bg-success">Halal</span>
                                        <?php elseif($get->material != null && $get->material->halal != 1): ?>
                                            <span class="badge bg-warning">Non halal</span>
                                        <?php else: ?>
                                            <span>-</span>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            </tbody>
                    </table>
                </div>
                <?php if($get->purchase->po_material != null){ ?>
                <div class="col-md-12">
                    <h3 class="mt-5"><b>PO Material Detail</b></h3>
                    <table class="table table-striped">
                        <thead>
                                <tr>
                                    <th>#</th>
                                    <th>PO Material No.</th>
                                    <th>PO Material Date</th>
                                    <th>Currency</th>
                                    <th>Terms</th>
                                    <th>Status</th>
                                    <th>Supplier Code</th>
                                    <th>Supplier Name</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <?php 
                                    $no = 1;
                                     ?>
                                    <td><?= $no++ ?></td>
                                    <td>
                                        <?= $get->purchase->po_material->po_num ?>
                                    </td>
                                    <td>
                                        <?= $get->purchase->po_material->po_date ?>
                                    </td>
                                    <td>
                                        <?= $get->purchase->po_material->currency ?>
                                    </td>
                                    <td>
                                        <?= $get->purchase->po_material->terms ?>
                                    </td>
                                    <td>
                                        <?= $get->purchase->po_material->status ?>
                                    </td>
                                    <td>
                                        <?= $get->purchase->po_material->supplier->supplier_name ?>
                                    </td>
                                    <td>
                                        <?= $get->purchase->po_material->supplier->supplier_code ?>
                                    </td>
                                </tr>
                            </tbody>
                    </table>
                </div>
                <?php } else { ?>
                    <div class="col-md-6"></div>
                <?php } ?>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
</div>
