<main class="content">
	<div class="container-fluid p-0">

		<h1 class="h3 mb-3">Approval Pengujian</h1>

		<div class="row">
			<div class="col-12 col-xl-12">
				<div class="card">
					<div class="card-header">
						<h5 class="card-title">Approval Pengujian
							<?php if($this->session->level != 4 ): ?>
							<div class="float-end">
								<button type="button" class="btn btn-primary" data-bs-toggle="modal"
									data-bs-target="#addModal"><i class="fa fa-plus"></i> Add Data</button>
							</div>
						<?php endif; ?>
						</h5>
						<h6 class="card-subtitle text-muted">List data Approval Pengujian</h6>
					</div>
					<div class="card-body">
						<?= $this->session->flashdata('msg') ?>
						<table class="table table-striped dataTable" style="width:100%">
							<thead>
								<tr>
									<th>No.</th>
									<th>No. Dokumen</th>
									<th>Departement</th>
									<th>Merk</th>
									<th>Kemasan</th>
									<th>Nama Produk</th>
									<th>Netto</th>
									<th>Tgl Kedatangan Sample</th>
									<th>No. Analisa</th>
									<th>No. Batch</th>
									<th>Tgl Pengujian</th>
									<th>Jumlah Sample</th>
									<th>Tgl Selesai</th>
									<th>Pengujian</th>
									<th>Spesifikasi 1</th>
									<th>Syarat 1</th>
									<th>Hasil 1</th>
									<th>Spesifikasi 2</th>
									<th>Syarat 2</th>
									<th>Hasil 2</th>
									<th>Spesifikasi 3</th>
									<th>Syarat 3</th>
									<th>Hasil 3</th>
									<th>Spesifikasi 4</th>
									<th>Syarat 4</th>
									<th>Hasil 4</th>
									<th>Spesifikasi 5</th>
									<th>Syarat 5</th>
									<th>Hasil 5</th>
									<th>Uji Homogenitas</th>
									<th>Homogen</th>
									<th>Hasil Honogenitas</th>
									<th>Syarat Kebocotan</th>
									<th>Hasil Kebocotan</th>
									<th>Diuji Oleh</th>
									<th>Diperiksa Oleh</th>
									<th>Disposisi</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php
		                            $no = 1;
		                            foreach ($get as $row) { 
										?>
										<tr>
											<td><?= $no++; ?></td>
											<td><?= $row['no_dokumen']; ?></td>
											<td><?= $row['departement'];?></td>
											<td><?= $row['merk'];?></td>
											<td><?= $row['kemasan'];?></td>
											<td><?= $row['nama_produk'];?></td>
											<td><?= $row['netto'];?></td>
											<td><?= date('D, d M Y', strtotime($row['tgl_kedatangan_sample']))?></td>
											<td><?= $row['no_analisa'];?></td>
											<td><?= $row['no_batch'];?></td>
											<td><?= date('D, d M Y', strtotime($row['tgl_pengujian']))?></td>
											<td><?= $row['jml_sample'];?></td>
											<td><?= date('D, d M Y', strtotime($row['tgl_selesai']))?></td>
											<td><?= $row['pengujian'];?></td>
											<td><?= $row['spesifikasi_1'];?></td>
											<td><?= $row['syarat_1'];?></td>
											<td><?= $row['hasil_1'];?></td>
											<td><?= $row['spesifikasi_2'];?></td>
											<td><?= $row['syarat_2'];?></td>
											<td><?= $row['hasil_2'];?></td>
											<td><?= $row['spesifikasi_3'];?></td>
											<td><?= $row['syarat_3'];?></td>
											<td><?= $row['hasil_3'];?></td>
											<td><?= $row['spesifikasi_4'];?></td>
											<td><?= $row['syarat_4'];?></td>
											<td><?= $row['hasil_4'];?></td>
											<td><?= $row['spesifikasi_5'];?></td>
											<td><?= $row['syarat_5'];?></td>
											<td><?= $row['hasil_5'];?></td>
											<td><?= $row['uji_homogenitas'];?></td>
											<td><?= $row['homogen'];?></td>
											<td><?= $row['hasil_homogenitas'];?></td>
											<td><?= $row['syarat_kebocoran'];?></td>
											<td><?= $row['hasil_kebocoran'];?></td>
											<td><?= $row['diuji_oleh'];?></td>
											<td><?= $row['diperiksa_oleh'];?></td>
											<td class="text-center">
												<?php if($row['disposisi'] != 'DITERIMA'): ?>
													<span class="badge bg-danger"><?= $row['disposisi'] ?></span>
												<?php else: ?>
													<span class="badge bg-success"><?= $row['disposisi'] ?></span>
												<?php endif; ?>
											</td>
											<td class="text-center">
												<a href="<?= base_url() ?>admin/approval_pengujian/print/<?= $row['approval_pengujian_id'] ?>" title="print Approval Pengujian" target="_blank"><i data-feather="printer"></i></a>

												 <!-- <a href="<?= base_url() ?>admin/approval_pengujian/scale_label/<?= $row['approval_pengujian_id'] ?>" title="Scale Label" target="_blank"><i data-feather="file-text"></i></a> -->

												<?php if($row['disposisi'] != 'DITERIMA'): ?>
												<!-- <a href="#!" data-bs-toggle="modal" 		
													data-bs-target="#statusModal"
													title="update status"
													data-id="<?= $row['approval_pengujian_id'] ?>"><i data-feather="package"></i></a>
												 -->
													<a href="#!" data-bs-toggle="modal" data-bs-target="#detailModal"
													title="detail"
													data-id="<?= $row['approval_pengujian_id'] ?>"><i data-feather="book"></i></a>
												<?php else: ?>
													<a href="#!" data-bs-toggle="modal" data-bs-target="#detailFinishedModal"
													title="detail"
													data-id="<?= $row['approval_pengujian_id'] ?>"><i data-feather="book"></i></a>
												<?php endif; ?>
												

												<?php if($row['disposisi'] != 'DITERIMA'): ?>
												<a href="#!" data-bs-toggle="modal" data-bs-target="#editModal"
													title="edit disposisi"
													data-id="<?= $row['approval_pengujian_id'] ?>"><i data-feather="edit"></i></a>
												<?php endif; ?>


												<!-- <a href="#!" onclick="delete_data('<?= $row['approval_pengujian_id'] ?>')" title="delete"><i data-feather="trash-2"></i></a> -->
											</td>
										</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>

		</div>

	</div>
</main>

<div class="modal fade" id="addModal" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Add Data Approval Pengujian</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<form action="<?= base_url() ?>admin/Approval_pengujian/add" method="POST">
				<div class="modal-body m-3">
					<div class="row">
						<div class="col-md-6 mb-3">
							<div class="form-group">
							  <label class="form-label" for="no_dokumen">No. Dokumen <span class="text-danger">*</span></label>
							  <input type="text" name="no_dokumen" id="no_dokumen" class="form-control" placeholder="No. Dokumen Pengujian" aria-describedby="">
							</div>
						</div>
						<div class="col-md-6 mb-3">
							<label class="form-label">Departemen : <span class="text-danger">*</span></label>
							<select name="departement" class="form-control" required>
								<option value="">- Select Departement -</option>
								<option value="Quality Control">Quality Control</option>
							</select>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6 mb-3">
							<div class="form-group">
							  <label class="form-label" for="merk">Merk : <span class="text-danger">*</span></label>
							  <input type="text" name="merk" id="merk" class="form-control" placeholder="Merk Produk" aria-describedby="">
							</div>
						</div>
						<div class="col-md-6 mb-3">
							<div class="form-group">
							  <label class="form-label" for="kemasan">Kemasan : <span class="text-danger">*</span></label>
							  <input type="text" name="kemasan" id="kemasan" class="form-control" placeholder="Kemasan Produk" aria-describedby="">
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6 mb-3">
							<div class="form-group">
							  <label class="form-label" for="nama_produk">Nama Produk : <span class="text-danger">*</span></label>
							  <input type="text" name="nama_produk" id="nama_produk" class="form-control" placeholder="Nama Produk" aria-describedby="">
							</div>
						</div>
						<div class="col-md-6 mb-3">
							<div class="form-group">
							  <label class="form-label" for="netto">Netto : <span class="text-danger">*</span></label>
							  <input type="text" name="netto" id="netto" class="form-control" placeholder="Netto" aria-describedby="">
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6 mb-3">
							<div class="form-group">
							  <label class="form-label" for="tgl_kedatangan_sample">Tgl Kedatangan Sample : <span class="text-danger">*</span></label>
							  <input type="date" class="form-control" name="tgl_kedatangan_sample" placeholder="Tgl Kedatangan Sample" required>
							</div>
						</div>
						<div class="col-md-6 mb-3">
							<div class="form-group">
							  <label class="form-label" for="no_analisa">No. Analisa : <span class="text-danger">*</span></label>
							  <input type="text" name="no_analisa" id="no_analisa" class="form-control" placeholder="No. Analisa" aria-describedby="">
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6 mb-3">
							<div class="form-group">
							  <label class="form-label" for="no_batch">No.Batch : <span class="text-danger">*</span></label>
							  <input type="text" name="no_batch" id="no_batch" class="form-control" placeholder="No. Batch" aria-describedby="">
							</div>
						</div>
						<div class="col-md-6 mb-3">
							<div class="form-group">
							  <label class="form-label" for="tgl_pengujian">Tgl Pengujian : <span class="text-danger">*</span></label>
							  <input type="date" class="form-control" name="tgl_pengujian" placeholder="Tgl Pengujian" required>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6 mb-3">
							<div class="form-group">
							  <label for="jumlah_sample">Jumlah Sample : <span class="text-danger">*</span></label>
							  <input type="text" name="jml_sample" id="jumlah_sample" class="form-control" placeholder="Jumlah Sample" aria-describedby="">
							</div>
						</div>
						<div class="col-md-6 mb-3">
							<div class="form-group">
							  <label for="tgl_selesai">Tgl Selesai : <span class="text-danger">*</span></label>
							  <input type="date" class="form-control" name="tgl_selesai" placeholder="Tgl Selesai" required>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-8 mb-3">
							<label class="form-label">PENGUJIAN : <span class="text-danger">*</span></label>
							<select name="pengujian" class="form-control" required>
								<option value="">- Select One -</option>
								<option value="PRODUK RUAHAN">PRODUK RUAHAN</option>
								<option value="PRODUK JADI">PRODUK JADI</option>
							</select>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<table class="table table-striped">
							  <thead class="thead-dark table-dark">
							    <tr>
							      <th scope="col">Spesifikasi</th>
							      <th scope="col">Syarat</th>
							      <th scope="col">Hasil</th>
							    </tr>
							  </thead>
							  <tbody>
							    <tr>
							      <td>
									<div class="form-floating">
									  <textarea class="form-control" name="spesifikasi_1" style="height: 40px"></textarea>
									</div>
							      </td>
							      <td>
									<div class="form-floating">
									  <textarea class="form-control" name="syarat_1" style="height: 40px"></textarea>
									</div>
							      </td>
							      <td>
									<div class="form-floating">
									  <textarea class="form-control" name="hasil_1" style="height: 40px"></textarea>
									</div>
							      </td>
							    </tr>
							    <tr>
							      <td>
									<label class="form-label">Identifikasi Sediaan Krim <span class="text-danger">*</span></label>
									<div class="form-floating">
									  <textarea class="form-control" name="spesifikasi_2" style="height: 40px"></textarea>
									</div>
							      </td>
							      <td>
									<div class="form-floating">
									  <textarea class="form-control mt-4" name="syarat_2" style="height: 45px"></textarea>
									</div>
							      </td>
							      <td>
									<div class="form-floating">
									  <textarea class="form-control mt-4" name="hasil_2" style="height: 45px"></textarea>
									</div>
							      </td>
							    </tr>
							    <tr>
							      <td>
									 <label class="form-label">Identifikasi Sediaan Cair/Cairan Kental/Gel <span class="text-danger">*</span></label>
									<div class="form-floating">
									  <textarea class="form-control" name="spesifikasi_3" style="height: 40px"></textarea>
									</div>
							      </td>
							      <td>
									<div class="form-floating">
									  <textarea class="form-control mt-4" name="syarat_3" style="height: 45px"></textarea>
									</div>
							      </td>
							      <td>
									<div class="form-floating">
									  <textarea class="form-control mt-4" name="hasil_3" style="height: 45px"></textarea>
									</div>
							      </td>
							    </tr>
							    <tr>
							      <td>
									 <label class="form-label">Identifikasi Sediaan Serbuk <span class="text-danger">*</span></label>
									<div class="form-floating">
									  <textarea class="form-control" name="spesifikasi_4" style="height: 40px"></textarea>
									</div>
							      </td>
							      <td>
									<div class="form-floating">
									  <textarea class="form-control mt-4" name="syarat_4" style="height: 45px"></textarea>
									</div>
							      </td>
							      <td>
									<div class="form-floating">
									  <textarea class="form-control mt-4" name="hasil_4" style="height: 45px"></textarea>
									</div>
							      </td>
							    </tr>
							    <tr>
							      <td>
									 <label class="form-label">Identifikasi Sediaan Padat <span class="text-danger">*</span></label>
									<div class="form-floating">
									  <textarea class="form-control" name="spesifikasi_5" style="height: 40px"></textarea>
									</div>
							      </td>
							      <td>
									<div class="form-floating">
									  <textarea class="form-control mt-4" name="syarat_5" style="height: 45px"></textarea>
									</div>
							      </td>
							      <td>
									<div class="form-floating">
									  <textarea class="form-control mt-4" name="hasil_5" style="height: 45px"></textarea>
									</div>
							      </td>
							    </tr>
							  </tbody>
							  <thead class="thead-dark table-dark">
							    <tr>
							      <th scope="col">Uji Homogenitas</th>
							      <th scope="col">Homogen</th>
							      <th scope="col">Hasil</th>
							    </tr>
							  </thead>
							  <tbody>
							    <tr>
								    <tr>
								      <td>
										<div class="form-floating">
										  <textarea class="form-control" name="uji_homogenitas" style="height: 40px"></textarea>

										</div>
								      </td>
								      <td>
										<div class="form-floating">
										  <textarea class="form-control" name="homogen" style="height: 40px"></textarea>

										</div>
								      </td>
								      <td>
										<div class="form-floating">
										  <textarea class="form-control" name="hasil_homogenitas" style="height: 40px"></textarea>

										</div>
								      </td>
								    </tr>
							    </tr>
							  </tbody>
							  <thead class="thead-dark table-dark">
							    <tr>
							      <th scope="col">Uji Kebocoran</th>
							      <th scope="col">
									<div class="form-floating">
									  <textarea class="form-control" name="syarat_kebocoran" style="height: 40px"></textarea>
									</div>
							      </th>
							      <th scope="col">
									<div class="form-floating">
									  <textarea class="form-control" name="hasil_kebocoran" style="height: 40px"></textarea>
									</div>
							      </th>
							    </tr>
							  </thead>
							</table>
		
							<div class="row">
								<div class="col-md-4 mb-3">
									<label class="form-label">Diuji Oleh : <span class="text-danger">*</span></label>
									<select name="diuji_oleh" class="form-control" required>
										<option value="">- Select One -</option>
										<option value="Ami Efiani(Quality Control Analisis)">Ami Efiani(Quality Control Analisis)</option>
									</select>
								</div>
								<div class="col-md-4 mb-3">
									<label class="form-label">Disposisi : <span class="text-danger">*</span></label>
									<select name="disposisi" class="form-control" required>
										<option value="">- Select One -</option>
										<option value="DITERIMA">Diterima</option>
										<option value="DITOLAK">Ditolak</option>
									</select>
								</div>
								<div class="col-md-4 mb-3">
									<label class="form-label">Diperiksa Oleh : <span class="text-danger">*</span></label>
									<select name="diperiksa_oleh" class="form-control" required>
										<option value="">- Select One -</option>
										<option value="Abdul (SPV Quality Control Analisis)">Abdul (SPV Quality Control Analisis)</option>
									</select>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" id="tombol_simpan">Save changes</button>
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="editModal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Edit Data Approval Pengujian</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div id="edit_data"></div>
		</div>
	</div>
</div>

<div class="modal fade" id="detailModal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Detail Data Approval Pengujian</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div id="detail_data"></div>
		</div>
	</div>
</div>

<div class="modal fade" id="detailFinishedModal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Detail Data Approval Pengujian</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div id="detail_finished_data"></div>
		</div>
	</div>
</div>

<div class="modal fade" id="statusModal" tabindex="-1" style="display: none;" aria-hidden="true">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Status Data Approval Pengujian</h5>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div id="status_data"></div>
		</div>
	</div>
</div>

<script>
	

	$("#editModal").on('shown.bs.modal', function (e) {
		var id = $(e.relatedTarget).data('id');
		$.ajax({
			type: "POST",
			url: "<?= base_url() ?>admin/approval_pengujian/get",
			data: {
				id: id
			}
		}).done(function (response) {
			$("#edit_data").html(response);
		});
	});

	$("#detailModal").on('shown.bs.modal', function (e) {
		var id = $(e.relatedTarget).data('id');
		$.ajax({
			type: "POST",
			url: "<?= base_url() ?>admin/approval_pengujian/detail",
			data: {
				id: id
			}
		}).done(function (response) {
			$("#detail_data").html(response);
		});
	});

	$("#detailFinishedModal").on('shown.bs.modal', function (e) {
		var id = $(e.relatedTarget).data('id');
		$.ajax({
			type: "POST",
			url: "<?= base_url() ?>admin/approval_pengujian/detail_approved",
			data: {
				id: id
			}
		}).done(function (response) {
			$("#detail_finished_data").html(response);
		});
	});

	// $("#statusModal").on('shown.bs.modal', function (e) {
	// 	var id = $(e.relatedTarget).data('id');
	// 	$.ajax({
	// 		type: "POST",
	// 		url: "<?= base_url() ?>admin/approval_pengujian/status",
	// 		data: {
	// 			id: id
	// 		}
	// 	}).done(function (response) {
	// 		$("#status_data").html(response);
	// 	});
	// });

	// let x = 1;
	// let i = 1;


	// $(".fg_id").on('select2:select', function (e) {
	// 	var description = $(this).find(":selected").data('description');
	// 	var unit = $(this).find(":selected").data('unit');
	// 	var nomor = $(this).data('nomor');
	// 	let value = e.params.data.id

 //        let nomor_tbl = 1;
		
	// 	console.log(value)

	// 	if (e.params.data.id == "") {
	// 		$('div#kartu_' + nomor).hide()

	// 	} else {
	// 		let base_url = '<?= base_url() ?>'
	// 		let val = 0;

	// 		console.log(value)


	// 		$.ajax({
	// 			method: 'POST',
	// 			url: base_url + 'admin/approval_pengujian/check_stock/' + value,
	// 			success: (res) => {
	// 				console.log(res)
	// 				$('#card-body-' + nomor).empty()
	// 				$('#fg-description-' + nomor).text(description)

	// 				let jml_material = res.data.length
	// 				$("#tbody-"+nomor).empty()

	// 				res.data.forEach(element => {

	// 					let txt_html = `
	// 							<tr>
	// 								<td>`+nomor_tbl+`</td>
	// 								<td>`+element.nama_material+`</td>
	// 								<td>`+element.deskripsi+`</td>
	// 								<td><span class="item_`+nomor+`_` + i + `">`+element.qty+`</span></td>
	// 								<td><span class="need tbl_need_`+nomor+`_`+i+`">`+ 0 +`</span></td>
	// 								<td><span class="total_`+nomor+`_` + i + `">`+element.sisa+`</span></td>
	// 								<td><span class="sisa tbl_sisa_`+nomor+`_`+i+`">`+element.sisa+`</span></td>
	// 							</tr>
	// 						`;


	// 					// console.log(txt_html)
	// 					$("#tbody-"+nomor).append(txt_html)

	// 					nomor_tbl++;
	// 					i++;

	// 				});



	// 				$("#qty-" + nomor).keyup(function () {
	// 					val = $(this).val()
	// 					console.log(val)
	// 					let cukup = true;

	// 					for (let y = 1; y <= jml_material; y++) {
	// 						let gudang = $('.total_'+nomor+`_` + y).text()
	// 						let each = $('.item_'+nomor+`_` + y).text()
	// 						let need = val * each
	// 						let sisa = gudang - need


	// 						if(sisa < 0){
	// 							cukup = false
	// 							$(".tbl_sisa_"+nomor+`_` + y).css('color', 'red');
	// 						}else{
	// 							$(".tbl_sisa_"+nomor+`_` + y).css('color', 'black');
	// 						}

	// 						$(".tbl_need_"+nomor+`_` + y).text(need)
	// 						$(".tbl_sisa_"+nomor+`_` + y).text(sisa)
	// 					}

	// 					if(!cukup){
	// 						$("#tombol_simpan").prop('disabled', true);
	// 					}else{
	// 						$("#tombol_simpan").prop('disabled', false);
	// 					}
	// 				});
	// 			}
	// 		})

	// 		$('div#kartu_' + nomor).show()
	// 	}
	// });

	function delete_data(id) {
		Swal.fire({
			title: 'Konfirmasi ?',
			text: "Apakah kamu yakin ?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#28a745',
			cancelButtonColor: '#dc3545',
			confirmButtonText: 'Yes!',
			cancelButtonText: 'No!',
			showLoaderOnConfirm: true,
			preConfirm: function () {
				return new Promise(function (resolve, reject) {
					$.ajax({
						type: 'POST',
						url: "<?= base_url() ?>admin/approval_pengujian/delete",
						data: {
							id: id
						}
					}).done(function (msg) {
						if (msg == "ok") {
							swal.fire("OK!", "Data berhasil dihapus!", "success").then(
								function () {
									location.reload();
								})
						} else {
							swal.fire("Gagal!", msg, "error").then(function () {
								location.reload();
							})
						}
					})
				})
			},
			allowOutsideClick: () => !Swal.isLoading()
		})
	}

	

</script>
