<form action="<?= base_url() ?>admin/approval_pengujian/edit/<?= $get['approval_pengujian_id'] ?>" method="POST">
    <div class="modal-body m-3">

		<div class="mb-3">
            <label class="form-label">No. Dokumen</label>
            <input type="text" class="form-control" name="no_dokumen" placeholder="Work Order No" value="<?= $get['no_dokumen'] ?>" required>
        </div>

        <div class="mb-3">
            <label class="form-label">Tgl Pengujian</label>
            <input type="date" class="form-control" name="tgl_pengujian" placeholder="tgl_pengujian" value="<?= $get['tgl_pengujian'] ?>" required>
        </div>

        <div class="mb-3">
            <label class="form-label">Disposisi : <span class="text-danger">*</span></label>
            <select name="disposisi" class="form-control" required>
                <option value="">- Select One -</option>
                <option value="DITERIMA">Diterima</option>
                <option value="DITOLAK">Ditolak</option>
            </select>
        </div>
        <div class="mb-3">
            <label class="form-label">Diperiksa Oleh : <span class="text-danger">*</span></label>
            <select name="diperiksa_oleh" class="form-control" required>
                <option value="">- Select One -</option>
                <option value="Abdul (SPV Quality Control Analisis)">Abdul (SPV Quality Control Analisis)</option>
            </select>
        </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
    </div>
</form>

<script>
	$(function(){
		$(".select2-edit").select2({
			theme: 'bootstrap-5',
			dropdownParent: $("#editModal"),
			width: 'auto'
		});
	});
</script>
