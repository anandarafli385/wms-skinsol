<!DOCTYPE html>
<html>

<head>
    <title>Approval Pengujian No. <?= $get['no_dokumen'] ?></title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inter&display=swap" rel="stylesheet">
    <style type="text/css">
        @page {
            margin: 0cm 0cm;
			padding: 3cm 4cm;
            font-family: 'Inter', sans-serif;
        }

        body {
            margin: 0px;
            font-size: 9px;
            padding: 14px;
        }

        .text-center {
            text-align: center;
        }

        .text-right {
            text-align: right;
        }

        .text-left {
            text-align: left;
        }

        .my-0 {
			margin-bottom: 0px; 
			margin-top: 0px;
        }

        h2 {
            margin-top: 5px;
            margin-bottom: 2px;
        }

        .table {
            margin: 0;
            border-collapse: collapse;
            border: 0;
        }

        .table>tbody>tr>td {
            border-top: 0;
        }

        .table>tbody>tr>.br {
            border-right: 0;
        }

        .table>tbody>tr>.bl {
            border-left: 0;
        }

        .td-valign-top>tbody>tr>td {
            vertical-align: top;
        }

        .bl {
            border-left: 1px solid #000 !important;
        }

        .bt {
            border-top: 1px solid #000 !important;
        }

        .nowrap {
            white-space: nowrap !important;
        }

        .page_break {
            page-break-after: auto;
        }

		.body{
			margin: 45px 80px;
		}
    </style>
</head>

<body>
    <!-- <div class="header" style="margin-top: 50px;"> -->
	<!-- </div> -->
	<div class="body">
		<table width="100%" border="1px" style="margin-bottom: 20px; border-collapse: collapse">
			<tr>
				<td width="35%" class="text-center">
					<img width="125" src="http://localhost/skinsol-wms/assets/src/img/logo-skinsol.png">
				</td>
				<td width="60%" style="border-bottom: 1px solid #fff !important;">
					<h1 class="text-center" style="margin-top: 40px;">FORM PENGUJIAN <br/> <?= $get['pengujian'] ?></h1>
				</td>
				<td width="35%">
					<div style="margin-left:5px;" class="my-0">
						<h3 class="text-left my-0">No. Dokumen. <?= $get['no_dokumen'] ?></h3>
						<h3 class="text-left my-0">Standar : CPKB ISO 9001,GMP</h3>
					</div>
				</td>
			</tr>
			<tr>
				<td width="35%">
					<h3 class="text-left my-0" style="margin-left:5px">Departemen : <?= $get['departement'] ?></h3>
				</td>
				<td width="60%" style="border-top: 1px solid #fff !important;">
				</td>
				<td width="35%">
					<div style="margin-left:5px" class="my-0">
						<h3 class="text-left my-0">Berlaku : 12 Januari 2022</h3>
						<h3 class="text-left my-0">No Revisi : 01</h3>
					</div>
				</td>
			</tr>
		</table>
		<table width="100%" border="1px" style="margin-bottom: 10px; border-collapse: collapse">
			<tr>
				<div style="margin-left:5px" class="my-0">
					<td width="20%" style="border-right: 1px solid #fff !important;">
						<h2 class="text-left my-0">Merk</h2>
					</td>
					<td width="5%" style="border-left: 1px solid #fff !important; border-right: 1px solid #fff !important;">:</td>
					<td width="25%" style="border-left: 1px solid #fff !important;"><h3 class="my-0"><?= $get['merk'] ?></h3></td>
					<td width="20%" style="border-right: 1px solid #fff !important;">
						<h2 class="text-left my-0">Kemasan</h2>
					</td>
					<td width="5%" style="border-left: 1px solid #fff !important; border-right: 1px solid #fff !important;">:</td>
					<td width="25%" style="border-left: 1px solid #fff !important;"><h3 class="my-0"><?= $get['kemasan'] ?></h3></td>
				</div>
			</tr>
			<tr>
				<div style="margin-left:5px" class="my-0">
					<td width="20%" style="border-right: 1px solid #fff !important;">
						<h2 class="text-left my-0">Nama Produk</h2>
					</td>
					<td width="5%" style="border-left: 1px solid #fff !important; border-right: 1px solid #fff !important;">:</td>
					<td width="25%" style="border-left: 1px solid #fff !important;"><h3 class="my-0"><?= $get['nama_produk'] ?></h3></td>
					<td width="20%" style="border-right: 1px solid #fff !important;">
						<h2 class="text-left my-0">Netto</h2>
					</td>
					<td width="5%" style="border-left: 1px solid #fff !important; border-right: 1px solid #fff !important;">:</td>
					<td width="25%" style="border-left: 1px solid #fff !important;"><h3 class="my-0"><?= $get['netto'] ?></h3></td>
				</div>
			</tr>
		</table>
		<table style="width: 100%;">
			<tr>
				<td width="40%">Tgl Kedatangan Sample</td>
				<td width="5%">:</td>
				<td width="55%"><?= date('d M Y', strtotime($get['tgl_kedatangan_sample'])) ?></td>
				<td width="40%">No.Analisa</td>
				<td width="5%">:</td>
				<td width="55%"><?= $get['no_analisa']?></td>
			</tr>
			<tr>
				<td width="40%">No.Batch</td>
				<td width="5%">:</td>
				<td width="55%"><?= $get['no_batch']?></td>
				<td width="40%">Tgl Pengujian</td>
				<td width="5%">:</td>
				<td width="55%"><?= date('d M Y', strtotime($get['tgl_pengujian'])) ?></td>
			</tr>
			<tr>
				<td width="40%">Jumlah Sample</td>
				<td width="5%">:</td>
				<td width="55%"><?= $get['jml_sample']?></td>
				<td width="40%">Tgl Selesai</td>
				<td width="5%">:</td>
				<td width="55%"><?= date('d M Y', strtotime($get['tgl_selesai'])) ?></td>
			</tr>
		</table>
		<table style="width: 100%;">
			<h1>PENGUJIAN : <?= $get['pengujian'] ?></h1>
		</table>
		<table width="100%" border="1px" style="margin-bottom: 10px; border-collapse: collapse">
			<thead>
				<tr>
					<th width="33%">Spesifikasi</th>
					<th width="33%">Syarat</th>
					<th width="33%">Hasil</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><h3 style="margin-left:5px"><?= $get['spesifikasi_1'] ?></h3></td>
					<td><h3 style="margin-left:5px"><?= $get['syarat_1'] ?></h3></td>
					<td><h3 style="margin-left:5px"><?= $get['hasil_1'] ?></h3></td>
				</tr>
				<tr>
					<td>
						<h3 style="margin-left:5px; margin-bottom: 0px;">Identifikasi Sediaan Krim</h3> <br>
						<h3 style="margin-left:20px;margin-top: 0px;"><?= $get['spesifikasi_2'] ?></h3></td>
					<td><h3 style="margin-left:5px"><?= $get['syarat_2'] ?></h3></td>
					<td><h3 style="margin-left:5px"><?= $get['hasil_2'] ?></h3></td>
				</tr>
				<tr>
					<td>
						<h3 style="margin-left:5px; margin-bottom: 0px;">Identifikasi Sediaan Cair/Cairan Kental</h3> <br>
						<h3 style="margin-left:20px;margin-top: 0px;"><?= $get['spesifikasi_3'] ?></h3></td>
					<td><h3 style="margin-left:5px"><?= $get['syarat_3'] ?></h3></td>
					<td><h3 style="margin-left:5px"><?= $get['hasil_3'] ?></h3></td>
				</tr>
				<tr>
					<td>
						<h3 style="margin-left:5px; margin-bottom: 0px;">Identifikasi Sediaan Serbuk</h3> <br>
						<h3 style="margin-left:20px;margin-top: 0px;"><?= $get['spesifikasi_4'] ?></h3></td>
					<td><h3 style="margin-left:5px"><?= $get['syarat_4'] ?></h3></td>
					<td><h3 style="margin-left:5px"><?= $get['hasil_4'] ?></h3></td>
				</tr>
			</tbody>
			<thead>
				<tr>
					<th width="33%">Uji Homogenitas</th>
					<th width="33%">Homogen</th>
					<th width="33%">Hasil Homogenitas</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><h3 style="margin-left:5px"><?= $get['uji_homogenitas'] ?></h3></td>
					<td><h3 style="margin-left:5px"><?= $get['homogen'] ?></h3></td>
					<td class="text-center"><h3 style="margin-left:5px"><?= $get['hasil_homogenitas'] ?></h3></td>
				</tr>
			</tbody>
			<thead>
				<tr>
					<th width="33%">Uji Kebocoran</th>
					<th width="33%"><?= $get['syarat_kebocoran'] ?></th>
					<th width="33%"><?= $get['hasil_kebocoran'] ?></th>
				</tr>
			</thead>
		</table>
		<table width="100%">
			<tr>
				<td width="33%" class="text-center">Diuji Oleh</td>
				<td width="33%"></td>
				<td width="33%" class="text-center">Diperiksa Oleh</td>
			</tr>
			<tr>
				<td width="33%"></td>
				<td width="33%" class="text-center"><h3>Disposisi</h3><br><h3><?= $get['disposisi'] ?></h3></td>
				<td width="33%"></td>
			</tr>
			<tr>
				<td width="33%" class="text-center"><h3><?= $get['diuji_oleh'] ?></h3></td>
				<td width="33%"></td>
				<td width="33%" class="text-center"><h3><?= $get['diperiksa_oleh'] ?></h3></td>
			</tr>
		</table>
	</div>
</body>

</html>
