<div class="modal-body m-3">
	<div class="row">
		<div class="col-6">
			<div class="mb-3">
				<label class="form-label">No. Dokumen Formulir Approval Pengujian</label><br>
				<b><?= $get['no_dokumen'] ?></b>
			</div>

			<div class="mb-3">
				<label class="form-label">Standar</label><br>
				<b>CPKB ISO 9001,GMP</b>
			</div>

			<div class="mb-3">
				<label class="form-label">Departemen</label><br>
				<b><?= $get['departement'] ?></b>
			</div>

			<div class="mb-3">
				<label class="form-label">Berlaku</label><br>
				<b>12 Januari 2022</b>
			</div>

			<div class="mb-3">
				<label class="form-label">No. Revisi</label><br>
				<b>01</b>
			</div>
		</div>
		<div class="col-6">
			<div class="mb-3">
				<label class="form-label">Merk</label><br>
				<b><?= $get['merk'] ?></b>
			</div>

			<div class="mb-3">
				<label class="form-label">Kemasan</label><br>
				<b><?= $get['kemasan'] ?></b>
			</div>

			<div class="mb-3">
				<label class="form-label">Nama Produk</label><br>
				<b><?= $get['nama_produk'] ?></b>
			</div>

			<div class="mb-3">
				<label class="form-label">Netto</label><br>
				<b><?= $get['netto'] ?></b>
			</div>

			<div class="mb-3">
				<label class="form-label">PENGUJIAN</label><br>
				<b><?= $get['pengujian'] ?></b>
			</div>

		</div>
	</div>
		<table style="width: 100%;">
			<tr>
				<td width="20%">Tgl Kedatangan Sample<b></td>
				<td width="5%">:</td>
				<td width="26%"><b><?= date('d M Y', strtotime($get['tgl_kedatangan_sample'])) ?></b></td>

				<td width="20%">No.Analisa<b></td>
				<td width="5%">:</td>
				<td width="26%"><b><?= $get['no_analisa']?></b></td>
			</tr>
			<tr>
				<td width="20%">No.Batch<b></td>
				<td width="5%">:</td>
				<td width="26%"><b><?= $get['no_batch']?></b></td>

				<td width="20%">Tgl Pengujian<b></td>
				<td width="5%">:</td>
				<td width="26%"><b><?= date('d M Y', strtotime($get['tgl_pengujian'])) ?></b></td>
			</tr>
			<tr>
				<td width="20%">Jumlah Sample<b></td>
				<td width="5%">:</td>
				<td width="26%"><b><?= $get['jml_sample']?></b></td>

				<td width="20%">Tgl Selesai<b></td>
				<td width="5%">:</td>
				<td width="26%"><b><?= date('d M Y', strtotime($get['tgl_selesai'])) ?></b></td>
			</tr>
		</table>
		<table width="100%" class="table table-striped table-bordered mt-3">
			<thead class="table-dark">
				<tr>
					<th width="33%">Spesifikasi</th>
					<th width="33%">Syarat</th>
					<th width="33%">Hasil</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><h6 style="margin-left:5px"><?= $get['spesifikasi_1'] ?></h6></td>
					<td><h6 style="margin-left:5px"><?= $get['syarat_1'] ?></h6></td>
					<td><h6 style="margin-left:5px"><?= $get['hasil_1'] ?></h6></td>
				</tr>
				<tr>
					<td>
						<h6 style="margin-left:5px; margin-bottom: 0px;">Identifikasi Sediaan Krim</h6> <br>
						<h6 style="margin-left:20px;margin-top: 0px;"><?= $get['spesifikasi_2'] ?></h6></td>
					<td><h6 style="margin-left:5px"><?= $get['syarat_2'] ?></h6></td>
					<td><h6 style="margin-left:5px"><?= $get['hasil_2'] ?></h6></td>
				</tr>
				<tr>
					<td>
						<h6 style="margin-left:5px; margin-bottom: 0px;">Identifikasi Sediaan Cair/Cairan Kental</h6> <br>
						<h6 style="margin-left:20px;margin-top: 0px;"><?= $get['spesifikasi_3'] ?></h6></td>
					<td><h6 style="margin-left:5px"><?= $get['syarat_3'] ?></h6></td>
					<td><h6 style="margin-left:5px"><?= $get['hasil_3'] ?></h6></td>
				</tr>
				<tr>
					<td>
						<h6 style="margin-left:5px; margin-bottom: 0px;">Identifikasi Sediaan Serbuk</h6> <br>
						<h6 style="margin-left:20px;margin-top: 0px;"><?= $get['spesifikasi_4'] ?></h6></td>
					<td><h6 style="margin-left:5px"><?= $get['syarat_4'] ?></h6></td>
					<td><h6 style="margin-left:5px"><?= $get['hasil_4'] ?></h6></td>
				</tr>
			</tbody>
			<thead class="table-dark">
				<tr>
					<th width="33%">Uji Homogenitas</th>
					<th width="33%">Homogen</th>
					<th width="33%">Hasil Homogenitas</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><h6 style="margin-left:5px"><?= $get['uji_homogenitas'] ?></h6></td>
					<td><h6 style="margin-left:5px"><?= $get['homogen'] ?></h6></td>
					<td class="text-center"><h6 style="margin-left:5px"><?= $get['hasil_homogenitas'] ?></h6></td>
				</tr>
			</tbody>
			<thead class="table-dark">
				<tr>
					<th width="33%">Uji Kebocoran</th>
					<th width="33%"><?= $get['syarat_kebocoran'] ?></th>
					<th width="33%"><?= $get['hasil_kebocoran'] ?></th>
				</tr>
			</thead>
		</table>
		<table width="100%">
			<tr>
				<td width="33%" class="text-center"><h2>Diuji Oleh</h2></td>
				<td width="33%"></td>
				<td width="33%" class="text-center"><h2>Diperiksa Oleh</h2></td>
			</tr>
			<tr>
				<td width="33%"></td>
				<td width="33%" class="text-center">
					<h3>Disposisi</h3>
					<br>
					<?php if($get['disposisi'] != 'DITERIMA'): ?>
						<span class="badge bg-danger"><h3 class="my-2 mx-2 text-white"><b><?= $get['disposisi'] ?></b></h3></span>
					<?php else: ?>
						<span class="badge bg-success"><h3 class="my-2 mx-2 text-white"><b><?= $get['disposisi'] ?></b></h3></span>
					<?php endif; ?>
				</td>
				<td width="33%"></td>
			</tr>
			<tr>
				<td width="33%" class="text-center"><h3><?= $get['diuji_oleh'] ?></h3></td>
				<td width="33%"></td>
				<td width="33%" class="text-center"><h3><?= $get['diperiksa_oleh'] ?></h3></td>
			</tr>
		</table>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
</div>
