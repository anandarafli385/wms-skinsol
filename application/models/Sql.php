<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sql extends CI_Model
{
    public function select_table($table, $where = null, $order_by = null, $order = 'asc')
    {
        if (isset($where)) {
            $this->db->where($where);
        }

		if (isset($order_by)) {
            $this->db->order_by($order_by, $order);
        }

        $query = $this->db->get($table);
        return $query;
    }


    public function manual_query($query)
    {
        return $this->db->query($query);
    }

	public function select_table_in($table, $column, $in = [], $order_by = null, $order = 'asc')
	{
		$this->db->where_in($column, $in);

		if (isset($order_by)) {
            $this->db->order_by($order_by, $order);
        }

		$query = $this->db->get($table);
        return $query;
	}

    function insert_table($table, $data)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    function update_table($table, $data, $where)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
        return 'OK';
    }

    function select_table_join($table, $column, $join_table, $join_on, $join, $where = array(1 => 1), $order_by = null, $order = 'asc')
    {
        if ($order_by != null) {
            $this->db->order_by($order_by, $order);
        }
        $this->db->select($column);
        $this->db->from($table);
        $this->db->where($where);
        $this->db->join($join_table, $join_on, $join);
        return $this->db->get();
    }

    public function delete_table($table, $where)
    {
        $this->db->where($where);
        $this->db->delete($table);
        return 'OK';
    }

    public function good_receiving($where = null, $order_by = null, $order = 'asc')
    {
        if (isset($where)) {
            $this->db->where($where);
        }
        if (isset($order_by)) {
            $this->db->order_by($order_by, $order);
        }
        $this->db->select('a.gr_id, a.purchase_order_no, d.nama,a.status, a.status_receiving, a.gr_date, a.created_at as so_date, a.receiving_date, count(b.gr_detail_id) as count_material, c.name');
        $this->db->from('tbl_gr a');
        $this->db->join('tbl_gr_detail b', 'a.gr_id = b.gr_id', 'left outer');
        $this->db->join('tbl_user c', 'a.user_id = c.user_id');
        $this->db->join('tbl_supplier d', 'a.supplier_id = d.supplier_id');
        $this->db->group_by("a.gr_id, d.nama, a.gr_date, c.name");
        return $this->db->get();
    }

    public function gr_detail_list($where = null)
    {
        if (isset($where)) {
            $this->db->where($where);
        }

        $this->db->select("a.*, b.*");
        $this->db->from('tbl_gr_detail a');
        $this->db->join('tbl_material b', 'a.material_id = b.material_id', 'left outer');
        return $this->db->get();
    }

    public function material_box_list($where = null)
    {
        if (isset($where)) {
            $this->db->where($where);
        }

        $this->db->select("a.*, b.*, c.*, d.*, b.status as halal");
        $this->db->from('tbl_material_box a');
        $this->db->join('tbl_material b', 'a.material_id = b.material_id', 'left outer');
        $this->db->join('tbl_gr c', 'a.gr_id = c.gr_id', 'left outer');
        $this->db->join('tbl_gr_detail d', 'a.gr_detail_id = d.gr_detail_id', 'left outer');

        return $this->db->get();
    }

    public function putaway_list_new($where = null)
    {
        if (isset($where)) {
            $this->db->where($where);
        }
        $this->db->select("a.*, b.*, e.*");
        $this->db->from('tbl_putaway_detail_new b');
        $this->db->join('tbl_putaway_new a', 'a.putaway_id = b.putaway_id', 'left outer');
        $this->db->join('tbl_user e', 'a.user_id = e.user_id', 'left outer');
        return $this->db->get();
    }

    public function putaway_list_kemas_new($where = null)
    {
        if (isset($where)) {
            $this->db->where($where);
        }
        $this->db->select("a.*, b.*, e.*");
        $this->db->from('tbl_putaway_kemas_detail_new b');
        $this->db->join('tbl_putaway_kemas_new a', 'a.putaway_id = b.putaway_id', 'left outer');
        $this->db->join('tbl_user e', 'a.user_id = e.user_id', 'left outer');
        return $this->db->get();
    }

    public function work_order($where = null, $order_by = null, $limit = null)
    {
        if (isset($where)) {
            $this->db->where($where);
        }
        if (isset($order_by)) {
            $this->db->order_by($order_by);
        }
		if (isset($limit)) {
            $this->db->limit($limit);
        }
        $this->db->select('a.work_order_id, a.description, a.date, a.no_work_order, a.created_at, a.finished_at, a.status, a.status_approval, a.approved_by, a.approved_at, count(b.work_order_id) as count_fg, c.name');
        $this->db->from('tbl_work_order a');
        $this->db->join('tbl_work_order_detail b', 'a.work_order_id = b.work_order_id', 'left outer');
        $this->db->join('tbl_user c', 'a.user_id = c.user_id');
        $this->db->group_by("a.work_order_id, a.date, c.name");
		$this->db->order_by('a.date', 'desc');
        return $this->db->get();
    }

    public function material_release($where = null, $order_by = null, $limit = null)
    {
        if (isset($where)) {
            $this->db->where($where);
        }
        if (isset($order_by)) {
            $this->db->order_by($order_by);
        }
        if (isset($limit)) {
            $this->db->limit($limit);
        }
        $this->db->select('a.material_release_id, a.no_dokumen, a.tgl_berlaku, a.besar_bets, a.no_batch, a.bentuk_sediaan, a.kemasan_primer, a.status_approval, a.tgl_mulai, a.tgl_selesai, a.created_at,  a.approved_by, a.approved_at, count(b.material_release_id) as count_fg, c.name');
        $this->db->from('tbl_material_release a');
        $this->db->join('tbl_material_release_detail b', 'a.material_release_id = b.material_release_id', 'left outer');
        $this->db->join('tbl_user c', 'a.user_id = c.user_id');
        // $this->db->join('tbl_user d', 'a.approved_by = c.user_id');
        $this->db->group_by("a.material_release_id, a.tgl_berlaku, c.name");
        $this->db->order_by('a.tgl_berlaku', 'desc');
        return $this->db->get();
    }

    public function approval_pengujian($where = null, $order_by = null, $limit = null)
    {
        if (isset($where)) {
            $this->db->where($where);
        }
        if (isset($order_by)) {
            $this->db->order_by($order_by);
        }
        if (isset($limit)) {
            $this->db->limit($limit);
        }
        $this->db->select('*');
        $this->db->from('tbl_approval_pengujian');
        $this->db->order_by('approval_pengujian_id', 'desc');
        return $this->db->get();
    }

	// Count table data
	public function count_table($table, $where = [])
	{
		if (isset($where)) {
            $this->db->where($where);
        }

		return $this->db->count_all_results($table);
	}

	// Sum table data
	public function sum_table_in($table, $sum, $column_where_in ,$where_in = [], $where = [])
	{
		$this->db->select_sum($sum);
		$this->db->where_in($column_where_in, $where_in);

		if (isset($where)) {
            $this->db->where($where);
        }

		return $this->db->get($table);
	}

    // Inner join
    public function inner_join($select, $table, $inner_join, $table_fk, $inner_pk, $where = [], $order_by = null, $order = 'asc')
    {
        if (isset($where)) {
            $this->db->where($where);
        }

        if (isset($order_by)) {
            $this->db->order_by($order_by, $order);
        }

        $this->db->select($select);
		$this->db->from($table);
		$this->db->join($inner_join, $table_fk . ' = ' . $inner_pk);
		$query = $this->db->get();

		return $query;
    }
    
    // Select like
	public function select_like($table, $column, $keyword, $where = [], $order_by = null, $order = 'asc')
	{
		if (isset($where)) {
            $this->db->where($where);
        }

		if($order_by){
			$this->db->order_by($order_by, $order);
		}

		$this->db->like($column, $keyword);
		$query = $this->db->get($table);

		return $query;
	}

    public function full_select($select, $table, $where = [], $order_by = null, $order = 'asc')
    {
        if (isset($where)) {
            $this->db->where($where);
        }

        if (isset($order_by)) {
            $this->db->order_by($order_by, $order);
        }

        $this->db->select($select);
		$this->db->from($table);
		$query = $this->db->get();

		return $query;
    }

    // Extends
    public function generate_qr($kodeqr)
    {
        if($kodeqr){
            $filename = 'image/qr_box/'.$kodeqr;
            if (!file_exists($filename)) { 
                    $this->load->library('ciqrcode');
                    $params['data'] = $kodeqr;
                    $params['level'] = 'H';
                    $params['size'] = 10;
                    $params['savename'] = FCPATH.'image/qr_box/'.$kodeqr.".png";
                    return  $this->ciqrcode->generate($params);
            }
        }
    }

    public function save_log($param)
    {
        $sql        = $this->db->insert_string('tb_log_work_order',$param);
        $ex         = $this->db->query($sql);
        return $this->db->affected_rows($sql);
    }

    public function save_log_pengujian($param)
    {
        $sql        = $this->db->insert_string('tbl_log_approval_pengujian',$param);
        $ex         = $this->db->query($sql);
        return $this->db->affected_rows($sql);
    }

    public function save_log_material_release($param)
    {
        $sql        = $this->db->insert_string('tbl_log_material_release',$param);
        $ex         = $this->db->query($sql);
        return $this->db->affected_rows($sql);
    }

}
