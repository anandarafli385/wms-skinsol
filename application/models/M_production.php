<?php 
 
class M_production extends CI_Model{
	function data($number = null,$offset = null){
		$query = $this->db->select('tbl_productions.*,tbl_replenishment.wo_formula AS replenishment,tbl_replenishment.wo_pack AS qty_replenishment,tbl_replenishment.work_order_batch AS wo_num')
		->from('tbl_productions')
		->join('tbl_replenishment', 'tbl_replenishment.replenishment_id = tbl_productions.replenishment_id', 'left')
		->get();
		return 	$query->result_array();
	}
 
	function jumlah_data(){
		return $this->db->get('tbl_productions')->num_rows();
	}

	function getReplenishment(){
		return $this->db->select('tbl_replenishment.*')
		->from('tbl_replenishment')->where('approval','approved')->get()->result();
	}
 
}