<?php
defined('BASEPATH') or exit('No direct script access allowed');

class master_trial extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model("sql");
		$this->middleware();
    }

    public function index()
    {
        // $data['get'] = $this->sql->select_table('tbl_salesman')->result_array();
        $data['api'] = $this->get_api();
        $data['site_title'] = "Master Salesman";
        $data['subview'] = "master_trial/data";
        $this->load->view('index', $data);
	}

    // public function add()
    // {
    //     if ($this->input->post()) {
    //         $post = $this->input->post();

    //         $form_data = [
    //             'salesman_no' => $post['salesman_no'],
    //             'name' => $post['name'],
    //             'address' => $post['address'],
    //             'phone' => $post['phone'],
    //             'email' => $post['email'],
    //         ];

    //         $id = $this->sql->insert_table('tbl_salesman', $form_data);

    //         $message = '<div class="alert alert-success alert-dismissible" role="alert">
				// 			<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
				// 			<div class="alert-message">
				// 				<strong>Success!</strong> New data has been created!
				// 			</div>
				// 		</div>';
    //         $this->session->set_flashdata('msg', $message);
    //         redirect('admin/master_salesman');
    //     } else {
    //         $message = '<div class="alert alert-danger alert-dismissible" role="alert">
				// 			<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
				// 			<div class="alert-message">
				// 				<strong>Error!</strong> Error!
				// 			</div>
				// 		</div>';
    //         $this->session->set_flashdata('msg', $message);
    //         redirect('admin/master_salesman');
    //     }
    // }

    public function get_api()
    {
        $url = "http://api.skinsolution.co.id/trial";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );

        $resp = curl_exec($curl);
        curl_close($curl);

        $return = json_decode($resp);
        return $return;
    }
    
    public function get_api_detail($id){

        // $id_trial = $this->get_api();

        // $url = "http://api.skinsolution.co.id/trial_revision/";

        // $curl = curl_init($url);
        // curl_setopt($curl, CURLOPT_URL, $url);
        // curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        // $headers = array(
        //    "Content-Type: application/json",
        // );
        // curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        // //for debug only!
        // curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        // curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        // $resp = curl_exec($curl);
        // curl_close($curl);

        // $return = json_decode($resp);
        // // var_dump($id_trial);
        // foreach($return as $trial_rev){
        //     foreach ($id_trial as $trial){
        //         if($trial->id == $trial_rev->trial_data_id){
        //             var_dump($return);            
        //         }
        //     }
        // }

        $id_trial = $this->get_api();

        $url = "http://api.skinsolution.co.id/trial_revision/".$id;

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );

        $resp = curl_exec($curl);
        curl_close($curl);

        $return = json_decode($resp);
        return $return;
    }

    public function get()
    {
        if ($this->input->post()) {
            $id = $this->input->post('id');
            $data['api_detail'] = $this->get_api_detail($id);
            $this->load->view('master_trial/get', $data);
        } else {
            echo "error";
        }
    }

    public function edit($id)
    {
        if ($this->input->post()) {
            $post = $this->input->post();

            $form_data = [
                'salesman_no' => $post['salesman_no'],
                'name' => $post['name'],
                'address' => $post['address'],
                'phone' => $post['phone'],
                'email' => $post['email'],
            ];

            $where = ['salesman_id' => $id];

            $id = $this->sql->update_table('tbl_salesman', $form_data, $where);

            $message = '<div class="alert alert-success alert-dismissible" role="alert">
							<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
							<div class="alert-message">
								<strong>Success!</strong> Data has been updated!
							</div>
						</div>';
            $this->session->set_flashdata('msg', $message);
            redirect('admin/master_salesman');
        } else {
            $message = '<div class="alert alert-danger alert-dismissible" role="alert">
							<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
							<div class="alert-message">
								<strong>Error!</strong> Error!
							</div>
						</div>';
            $this->session->set_flashdata('msg', $message);
            redirect('admin/master_salesman');
        }
    }

    // public function delete()
    // {
    //     if($this->input->post()){
    //         $id = $this->input->post('id');
    //         $where = ['salesman_id' => $id];
            
    //         $this->sql->delete_table('tbl_salesman', $where);
    //         echo "ok";
    //     }else{
    //         echo "error";
    //     }
    // }

	// middleware
	public function middleware()
	{
		if(!$this->session->name){
			redirect(base_url());
		}
	}
}
