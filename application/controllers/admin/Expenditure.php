<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Expenditure extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model("sql");
		$this->middleware();
    }

    public function index()
    {
		// Load view
        $data['subview'] = "expenditure/data";
        $data['site_title'] = "Expenditure";
		$data['expenditure'] = $this->sql->select_table('tbl_expenditure')->result();
        $data['get'] = $this->get_api();
        $data['cust'] = $this->get_api_customer();

        $this->load->view('index', $data);
    }


    public function get()
    {
        if ($this->input->post()) {
            $id = $this->input->post('id');
            $data['get'] = $this->get_api_detail($id);
	        $data['location'] = $this->sql->select_table('tbl_location')->result();
            $data['putdetailnew'] = $this->sql->select_table('tbl_putaway_detail_new')->result();
            $data['putnew'] = $this->sql->select_table('tbl_putaway_new')->result();
            $this->load->view('expenditure/get', $data);
        } else {
            echo "error";
        }
    }	

    public function get_api(){
        $url = "http://api.skinsolution.co.id/pengeluaran_material";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );

        $resp = curl_exec($curl);
        curl_close($curl);

        $return = json_decode($resp);
        return $return;
    }

    public function get_api_detail($id){
        $url = "http://api.skinsolution.co.id/pengeluaran_material/".$id;

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );

        $resp = curl_exec($curl);
        curl_close($curl);

        $return = json_decode($resp);
        return $return;
    }

    public function get_api_customer(){
        $url = "http://api.skinsolution.co.id/customer";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );

        $resp = curl_exec($curl);
        curl_close($curl);

        $return = json_decode($resp);
        return $return;
    }

	// Get Modal Detail
	public function detail()
	{
		if ($this->input->post()) {
            $id = $this->input->post('id');
            $data['api'] = $this->get_api_detail($id);
            $this->load->view('expenditure/detail', $data);
        } else {
            echo "error";
        }
	}

    public function update()
    {
        if($this->input->post()){
            $post = $this->input->post();

            $form_data = [
                'expenditure_id' => $post['expenditure_id'],
                'expenditure_num' => $post['expenditure_num'],
                'expenditure_date' => $post['expenditure_date'],
                'material_id' => $post['material_id'],
                'material_name' => $post['material_name'],
                'location' => $post['location'],
                'layer_loc' => $post['layer_loc'],
            ];

            $this->sql->insert_table('tbl_expenditure', $form_data);
            $message = '<div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            <div class="alert-message">
                                <strong>Success!</strong> Data has been updated!
                            </div>
                        </div>';
            $this->session->set_flashdata('msg', $message);
            redirect('admin/expenditure');

        } else {
            $message = '<div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            <div class="alert-message">
                                <strong>Error!</strong> Error!
                            </div>
                        </div>';
            $this->session->set_flashdata('msg', $message);
            redirect('admin/expenditure');
        }
    }

	// middleware
	public function middleware()
	{
		if(!$this->session->name){
			redirect(base_url());
		}
	}
}
