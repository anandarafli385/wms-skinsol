<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_supplier extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        // $this->load->model("sql");
		// $this->middleware();
    }

    public function index()
    {
        // $data['get'] = $this->sql->select_table('tbl_supplier_old')->result_array();
        $data['api'] = $this->get_api();
        $data['site_title'] = "Master Supplier";
        $data['subview'] = "master_supplier/data";
        $this->load->view('index', $data);
    }

    public function get_api()
    {
        $url = "http://api.skinsolution.co.id/supplier";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );

        $resp = curl_exec($curl);
        curl_close($curl);

        $return = json_decode($resp);
        return $return;
    }
    
	// middleware
	public function middleware()
	{
		if(!$this->session->name){
			redirect(base_url());
		}
	}
}
