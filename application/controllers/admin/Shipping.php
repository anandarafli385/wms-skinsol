<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Shipping extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model("sql");
		$this->middleware();
    }

    public function index()
    {
		// Sql
		// $shipping_sql = "SELECT s.*, c.nama as customer_name, u.name as shipping_by, fg.nama as fg, fg.unit FROM tbl_shipping s 
		// INNER JOIN tbl_customer c on s.customer_id = c.customer_id 
		// inner join tbl_user u on s.user_id = u.user_id 
		// inner join tbl_material fg on s.fg_id = fg.material_id order by created_at desc";

		// $so_sql = "SELECT s.*, c.nama as customer_name, count(d.fg_id) as jml_fg FROM tbl_sales_order s
		// INNER JOIN tbl_customer c on s.customer_id = c.customer_id
  //       inner join tbl_sales_order_detail d on d.so_id = s.id
  //       GROUP by d.so_id";

		// $query_cycle = "SELECT s.*, m.material_no, m.nama, m.unit FROM tbl_stock s inner join tbl_material m on m.material_id = s.material_id where m.inventory_part_type = 'Finished Goods'";

		// // Page data
  //       $data['site_title'] = "Sales Order";
		// $data['sales_order'] = $this->sql->manual_query($so_sql)->result_array();
		// $data['customers'] = $this->sql->select_table('tbl_customer', null, 'nama', 'asc')->result_array();
		// $data['fg'] = $this->sql->manual_query($query_cycle)->result_array();
		// $data['shipping'] = $this->sql->manual_query($shipping_sql)->result_array();

		// Load view
        $data['subview'] = "shipping/data";
        $data['get'] = $this->get_api();
        $data['cust'] = $this->get_api_customer();
        $this->load->view('index', $data);
    }


    public function get_api(){
        $url = "http://api.skinsolution.co.id/so";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);

        $return = json_decode($resp);
        return $return;
    }

    public function get_api_detail($id){
        $url = "http://api.skinsolution.co.id/so/".$id;

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);

        $return = json_decode($resp);
        return $return;
    }


    public function get_api_customer(){
        $url = "http://api.skinsolution.co.id/customer";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);

        $return = json_decode($resp);
        return $return;
    }

	// Get Modal Detail
	public function detail()
	{
		if ($this->input->post()) {
            $id = $this->input->post('id');
            $data['api'] = $this->get_api_detail($id);
            $this->load->view('shipping/detail', $data);
        } else {
            echo "error";
        }
	}

	// // Filter Customer name by Putaway_FG Id
	// public function filter_putaway_fg()
	// {
	// 	if ($this->input->post()) {
 //            $id = $this->input->post('id');
			
	// 		$sql = "SELECT f.*, fg.nama as fg, fg.unit FROM tbl_putaway_fg f INNER JOIN tbl_work_order w on f.work_order_id = w.work_order_id  inner join tbl_material fg on f.fg_id = fg.material_id where fg.material_id = '" . $id . "'";

	// 		// Get specified sql data
	// 		$customer_name = $this->sql->manual_query($sql)->row_array();

	// 		// print customer name 
	// 		$return = [
	// 			'id' => $customer_name['customer_id'],
	// 			'customer_name' => $customer_name['customer_name'],
	// 			'unit' => $customer_name['unit']
	// 		];

	// 		echo json_encode($return);

 //        } else {
 //            echo "error";
 //        }
	// }


	// // Add
	// public function create()
	// {
	// 	if($this->input->post()){
	// 		$post = $this->input->post();

	// 		$so_data = [
	// 			'sales_order_no' => $post['sales_order_no'],
	// 			'sales_order_description' => $post['sales_order_description'],
	// 			'sales_order_date' => $post['sales_order_date'],
	// 			'salesman_id' => $post['salesman_id'],
	// 			'customer_id' => $post['customer_id'],
	// 			'ship_date' => $post['ship_date'],
	// 			'fob' => $post['fob'],
	// 			'term' => $post['term'],
	// 			'dp_account' => $post['dp_account'],
	// 			'ship_via' => $post['ship_via'],
	// 		];

	// 		$so_id = $this->sql->insert_table('tbl_sales_order', $so_data);


	// 		foreach($post['fg_id'] as $index => $value){
	// 			$form_data = [
	// 				'shipping_no' => $post['sales_order_no'],
	// 				'fg_id' => $value,
	// 				'qty' => $post['qty'][$index],
	// 				// 'unit' => $post['unit'][$index],
	// 				'shipping_date' => $post['sales_order_date'],
	// 				'customer_id' => $post['customer_id'],
	// 				'user_id' => $this->session->userdata('user_id')

	// 			];

	// 			$so_detail_data = [
	// 				'so_id' => $so_id,
	// 				'fg_id' => $value,
	// 				'qty' => $post['qty'][$index],
	// 				'unit' => $post['unit'][$index],
	// 				'price' => $post['price'][$index],
	// 				'discount' => $post['discount'][$index],
	// 				'total' => $post['total'][$index],
	// 			];
	
	// 			$this->sql->insert_table('tbl_shipping', $form_data);
	// 			$this->sql->insert_table('tbl_sales_order_detail', $so_detail_data);

	// 			// Get data stok
	// 			$data_stok = $this->sql->select_table('tbl_stock', [
	// 				'material_id' => $value
	// 			])->row();

	// 			// Kurangi stok di cycle count
	// 			$this->sql->update_table('tbl_stock', [
	// 				'stok' => $data_stok->stok - $post['qty'][$index]
	// 			], [
	// 				'material_id' => $value
	// 			]);

	// 			// Add History Stock
	// 			$this->sql->insert_table('tbl_stock_history', [
	// 				'material_id' => $value,
	// 				'type' => 'out',
	// 				'previous_qty' => $data_stok->stok,
	// 				'total' => $post['qty'][$index],
	// 				'current_qty' => $data_stok->stok - $post['qty'][$index],
	// 				'created_by' => $this->session->userdata('user_id'),
	// 			]);
	// 		}

	// 		$message = '<div class="alert alert-success alert-dismissible" role="alert">
	// 						<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
	// 						<div class="alert-message">
	// 							<strong>Success!</strong> New data has been created!
	// 						</div>
	// 					</div>';
 //            $this->session->set_flashdata('msg', $message);
 //            redirect('admin/shipping');

	// 	} else {
	// 		$message = '<div class="alert alert-danger alert-dismissible" role="alert">
	// 						<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
	// 						<div class="alert-message">
	// 							<strong>Error!</strong> Error!
	// 						</div>
	// 					</div>';
	// 		$this->session->set_flashdata('msg', $message);
	// 		redirect('admin/shipping');
	// 	}
	// }

	// // Update
	// public function update()
	// {
	// 	if($this->input->post()){
	// 		$post = $this->input->post();

	// 		$where = [
	// 			'shipping_id' => $post['shipping_id'],
	// 		];

	// 		$form_data = [
	// 			'fg_id' => $post['fg_id'],
	// 			'qty' => $post['qty'],
	// 			'shipping_date' => $post['shipping_date'],
 //                'customer_id' => $post['customer_id'],
 //            ];

 //            $this->sql->update_table('tbl_shipping', $form_data, $where);
	// 		$message = '<div class="alert alert-success alert-dismissible" role="alert">
	// 						<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
	// 						<div class="alert-message">
	// 							<strong>Success!</strong> Data has been updated!
	// 						</div>
	// 					</div>';
 //            $this->session->set_flashdata('msg', $message);
 //            redirect('admin/shipping');

	// 	} else {
	// 		$message = '<div class="alert alert-danger alert-dismissible" role="alert">
	// 						<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
	// 						<div class="alert-message">
	// 							<strong>Error!</strong> Error!
	// 						</div>
	// 					</div>';
	// 		$this->session->set_flashdata('msg', $message);
	// 		redirect('admin/shipping');
	// 	}
	// }

	// // Delete
	// public function delete(){
 //        if ($this->input->post()) {
 //            $id = $this->input->post('id');
 //            $where = ['shipping_id' => $id];

 //            $this->sql->delete_table('tbl_sales_order', [
	// 			'id' => $id
	// 		]);
	// 		$this->sql->delete_table('tbl_sales_order_detail', [
	// 			'so_id' => $id
	// 		]);
 //            echo "ok";
 //        } else {
 //            echo "error";
 //        }
 //    }

	// // Shipping select2 putaway fg data
	// public function detail_putaway_fg()
	// {
	// 	# code...
	// }

	// middleware
	public function middleware()
	{
		if(!$this->session->name){
			redirect(base_url());
		}
	}
}
