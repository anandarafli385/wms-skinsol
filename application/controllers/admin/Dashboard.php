<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Sql');
		$this->middleware();

	}

	public function index()
	{
		$date = date('Y-m-d');
		$month = date('Y-m');
		$year = date('Y');

		// Sql
		$sql_wo_monthly = "SELECT *, count(*) as jumlah, month(date) as bulan FROM `tbl_work_order` where date like '2021%' group by month(date) order by bulan";

		$sql_emp_gr = "SELECT g.*, u.name, count(*) as jumlah FROM tbl_gr g inner join tbl_user u on g.user_id = u.user_id where gr_date like '$month%' group by g.user_id limit 4";

		$sql_emp_replenishment = "SELECT g.*, u.name, count(*) as jumlah FROM tbl_replenishment g inner join tbl_user u on g.user_id = u.user_id where g.created_at like '$month%' group by g.user_id limit 4";
		
		$sql_emp_putaway = "SELECT d.*, u.name, count(*) as jumlah FROM tbl_putaway_detail d inner join tbl_putaway p on d.putaway_id = p.putaway_id inner join tbl_user u on p.user_id = u.user_id where putaway_date like '$month%' group by p.user_id limit 4";

		$sql_qty = "SELECT sum(qty) as total, sum(good) as good, sum(reject) as reject from tbl_work_order_detail where work_order_id in (SELECT work_order_id FROM `tbl_work_order` where date like '$month%')";

		$sql_day = "SELECT fg.fg_id, f.nama, fg.putaway_fg_date, sum(fg.qty) as qty FROM tbl_putaway_fg fg inner join tbl_fg f on fg.fg_id = f.fg_id where fg.putaway_fg_date = '$date' GROUP by fg.fg_id ORDER by qty desc";

		$sql_week = "SELECT fg.fg_id, f.nama, fg.putaway_fg_date, sum(fg.qty) as qty, week(fg.putaway_fg_date) as minggu FROM tbl_putaway_fg fg inner join tbl_fg f on fg.fg_id = f.fg_id where week(fg.putaway_fg_date) = week('$date') GROUP by fg.fg_id order by qty desc";

		$sql_month = "SELECT fg.fg_id, f.nama, fg.putaway_fg_date, sum(fg.qty) as qty FROM tbl_putaway_fg fg inner join tbl_fg f on fg.fg_id = f.fg_id where fg.putaway_fg_date like '$date%' GROUP by fg.fg_id ORDER by qty desc";

		$sql_fg = "SELECT * from tbl_material where inventory_part_type = 'Work in Process' or inventory_part_type = 'Finished Goods' order by material_no";

		// data dashboard
		$data = [
			'list_material' => $this->Sql->select_table('tbl_material')->result(),
			'list_fg' => $this->Sql->manual_query($sql_fg)->result(),
			'count_gr' => $this->Sql->count_table('tbl_gr'),
			'count_putaway' => $this->Sql->count_table('tbl_putaway_detail'),
			'count_replenishment' => $this->Sql->count_table('tbl_replenishment'),
			'count_fg' => $this->Sql->count_table('tbl_putaway_fg'),
			'count_material' => $this->Sql->count_table('tbl_material'),
			'count_workorder' => $this->Sql->count_table('tbl_work_order'),

			'latest_workorder' => $this->Sql->work_order(null, null, 5)->result_array(),

			// Employee
			'emp_gr' => $this->Sql->manual_query($sql_emp_gr)->result(),
			'emp_replenishment' => $this->Sql->manual_query($sql_emp_replenishment)->result(),
			'emp_putaway' => $this->Sql->manual_query($sql_emp_putaway)->result(),

			// Work Order
			'wo_monthly' => $this->Sql->manual_query($sql_wo_monthly)->result(),
			'wo_qty' => $this->Sql->manual_query($sql_qty)->row(),

			// Trend
			'trend_day' => $this->Sql->manual_query($sql_day)->result(),
			'trend_week' => $this->Sql->manual_query($sql_week)->result(),
			'trend_month' => $this->Sql->manual_query($sql_month)->result(),


			//year
			'jan' =>  $this->Sql->manual_query("SELECT sum(qty) as qty from tbl_putaway_fg where putaway_fg_date like '$year-01%'")->row(),
			'feb' =>  $this->Sql->manual_query("SELECT sum(qty) as qty from tbl_putaway_fg where putaway_fg_date like '$year-02%'")->row(),
			'mar' =>  $this->Sql->manual_query("SELECT sum(qty) as qty from tbl_putaway_fg where putaway_fg_date like '$year-03%'")->row(),
			'apr' =>  $this->Sql->manual_query("SELECT sum(qty) as qty from tbl_putaway_fg where putaway_fg_date like '$year-04%'")->row(),
			'mei' =>  $this->Sql->manual_query("SELECT sum(qty) as qty from tbl_putaway_fg where putaway_fg_date like '$year-05%'")->row(),
			'jun' =>  $this->Sql->manual_query("SELECT sum(qty) as qty from tbl_putaway_fg where putaway_fg_date like '$year-06%'")->row(),
			'jul' =>  $this->Sql->manual_query("SELECT sum(qty) as qty from tbl_putaway_fg where putaway_fg_date like '$year-07%'")->row(),
			'agu' =>  $this->Sql->manual_query("SELECT sum(qty) as qty from tbl_putaway_fg where putaway_fg_date like '$year-08%'")->row(),
			'sep' =>  $this->Sql->manual_query("SELECT sum(qty) as qty from tbl_putaway_fg where putaway_fg_date like '$year-09%'")->row(),
			'okt' =>  $this->Sql->manual_query("SELECT sum(qty) as qty from tbl_putaway_fg where putaway_fg_date like '$year-10%'")->row(),
			'nov' =>  $this->Sql->manual_query("SELECT sum(qty) as qty from tbl_putaway_fg where putaway_fg_date like '$year-11%'")->row(),
			'des' =>  $this->Sql->manual_query("SELECT sum(qty) as qty from tbl_putaway_fg where putaway_fg_date like '$year-12%'")->row(),

			// Trend 
			'today' =>  $this->Sql->manual_query("SELECT * FROM `tbl_putaway_fg` where putaway_fg_date = DATE(NOW()) and fg_id = 385")->row_array(),
			'day6' =>  $this->Sql->manual_query("SELECT * FROM `tbl_putaway_fg` where putaway_fg_date = DATE(NOW()) - INTERVAL 1 day  and fg_id = 385")->row_array(),
			'day5' =>  $this->Sql->manual_query("SELECT * FROM `tbl_putaway_fg` where putaway_fg_date = DATE(NOW()) - INTERVAL 2 day  and fg_id = 385")->row_array(),
			'day4' =>  $this->Sql->manual_query("SELECT * FROM `tbl_putaway_fg` where putaway_fg_date = DATE(NOW()) - INTERVAL 3 day  and fg_id = 385")->row_array(),
			'day3' =>  $this->Sql->manual_query("SELECT * FROM `tbl_putaway_fg` where putaway_fg_date = DATE(NOW()) - INTERVAL 4 day  and fg_id = 385")->row_array(),
			'day2' =>  $this->Sql->manual_query("SELECT * FROM `tbl_putaway_fg` where putaway_fg_date = DATE(NOW()) - INTERVAL 5 day  and fg_id = 385")->row_array(),
			'day1' =>  $this->Sql->manual_query("SELECT * FROM `tbl_putaway_fg` where putaway_fg_date = DATE(NOW()) - INTERVAL 6 day  and fg_id = 385")->row_array(),
			
		];

		$data['site_title'] = "Dashboard";
        $data['subview'] = "dashboard/dashboard";

		$this->load->view('index', $data);
		$this->middleware();
	}

	public function api_trend($material_id)
	{
		$material = $this->Sql->select_table('tbl_material', [
			'material_id' => $material_id
		])->row();

		$raw = [
			'today' =>  $this->Sql->manual_query("SELECT * FROM `tbl_putaway_fg` where putaway_fg_date = DATE(NOW()) and fg_id = $material_id")->row_array(),
			'day_6' =>  $this->Sql->manual_query("SELECT * FROM `tbl_putaway_fg` where putaway_fg_date = DATE(NOW()) - INTERVAL 1 day  and fg_id = $material_id")->row_array(),
			'day_5' =>  $this->Sql->manual_query("SELECT * FROM `tbl_putaway_fg` where putaway_fg_date = DATE(NOW()) - INTERVAL 2 day  and fg_id = $material_id")->row_array(),
			'day_4' =>  $this->Sql->manual_query("SELECT * FROM `tbl_putaway_fg` where putaway_fg_date = DATE(NOW()) - INTERVAL 3 day  and fg_id = $material_id")->row_array(),
			'day_3' =>  $this->Sql->manual_query("SELECT * FROM `tbl_putaway_fg` where putaway_fg_date = DATE(NOW()) - INTERVAL 4 day  and fg_id = $material_id")->row_array(),
			'day_2' =>  $this->Sql->manual_query("SELECT * FROM `tbl_putaway_fg` where putaway_fg_date = DATE(NOW()) - INTERVAL 5 day  and fg_id = $material_id")->row_array(),
			'day_1' =>  $this->Sql->manual_query("SELECT * FROM `tbl_putaway_fg` where putaway_fg_date = DATE(NOW()) - INTERVAL 6 day  and fg_id = $material_id")->row_array(),
		];

		$data = [
			'material_name' => $material->nama,

			// Trend
			'today' => ($raw['today'] == null) ? '0' : $raw['today']['qty'],
			'day6' => ($raw['day_6'] == null) ? 0 : $raw['day_6']['qty'],
			'day5' => ($raw['day_5'] == null) ? 0 : $raw['day_5']['qty'],
			'day4' => ($raw['day_4'] == null) ? 0 : $raw['day_4']['qty'],
			'day3' => ($raw['day_3'] == null) ? 0 : $raw['day_3']['qty'],
			'day2' => ($raw['day_2'] == null) ? 0 : $raw['day_2']['qty'],
			'day1' => ($raw['day_1'] == null) ? 0 : $raw['day_1']['qty'],
		];

		$return = [
			'status' => 'success',
			'message' => 'data fetch successfully',
			'data' => $data,
		];

		$json = $this->output
			->set_content_type('application/json')
			->set_output(json_encode($return));

		return $json;
		
	}

	public function complete_api_trend($fg_id, $filter)
	{
		if($filter == 'daily'){
			$fg = $this->Sql->select_table('tbl_material', [
				'material_id' => $fg_id
			])->row();
	
			$raw = [
				'today' =>  $this->Sql->manual_query("SELECT * FROM `tbl_putaway_fg` where putaway_fg_date = DATE(NOW()) and fg_id = $fg_id")->row_array(),
				'day_6' =>  $this->Sql->manual_query("SELECT * FROM `tbl_putaway_fg` where putaway_fg_date = DATE(NOW()) - INTERVAL 1 day  and fg_id = $fg_id")->row_array(),
				'day_5' =>  $this->Sql->manual_query("SELECT * FROM `tbl_putaway_fg` where putaway_fg_date = DATE(NOW()) - INTERVAL 2 day  and fg_id = $fg_id")->row_array(),
				'day_4' =>  $this->Sql->manual_query("SELECT * FROM `tbl_putaway_fg` where putaway_fg_date = DATE(NOW()) - INTERVAL 3 day  and fg_id = $fg_id")->row_array(),
				'day_3' =>  $this->Sql->manual_query("SELECT * FROM `tbl_putaway_fg` where putaway_fg_date = DATE(NOW()) - INTERVAL 4 day  and fg_id = $fg_id")->row_array(),
				'day_2' =>  $this->Sql->manual_query("SELECT * FROM `tbl_putaway_fg` where putaway_fg_date = DATE(NOW()) - INTERVAL 5 day  and fg_id = $fg_id")->row_array(),
				'day_1' =>  $this->Sql->manual_query("SELECT * FROM `tbl_putaway_fg` where putaway_fg_date = DATE(NOW()) - INTERVAL 6 day  and fg_id = $fg_id")->row_array(),
			];
	
			$data = [
				'material_name' => $fg->nama,
	
				// Trend
				'today' => ($raw['today'] == null) ? '0' : $raw['today']['qty'],
				'day6' => ($raw['day_6'] == null) ? 0 : $raw['day_6']['qty'],
				'day5' => ($raw['day_5'] == null) ? 0 : $raw['day_5']['qty'],
				'day4' => ($raw['day_4'] == null) ? 0 : $raw['day_4']['qty'],
				'day3' => ($raw['day_3'] == null) ? 0 : $raw['day_3']['qty'],
				'day2' => ($raw['day_2'] == null) ? 0 : $raw['day_2']['qty'],
				'day1' => ($raw['day_1'] == null) ? 0 : $raw['day_1']['qty'],

				'labels' => [
					'6 days ago',
					'5 days ago',
					'4 days ago',
					'3 days ago',
					'2 days ago',
					'yesterday',
					'today'
				]
			];
		}else if($filter == 'weekly'){
			$fg = $this->Sql->select_table('tbl_material', [
				'material_id' => $fg_id
			])->row();
	
			$raw = [
				'today' =>  $this->Sql->manual_query("SELECT sum(qty) as qty, week(putaway_fg_date) from tbl_putaway_fg where week(putaway_fg_date) = week(now()) and fg_id = $fg_id")->row_array(),
				'day_6' =>  $this->Sql->manual_query("SELECT sum(qty) as qty from tbl_putaway_fg where week(putaway_fg_date) = week(now()) - 1 and fg_id = $fg_id")->row_array(),
				'day_5' =>  $this->Sql->manual_query("SELECT sum(qty) as qty from tbl_putaway_fg where week(putaway_fg_date) = week(now()) - 2 and fg_id = $fg_id")->row_array(),
				'day_4' =>  $this->Sql->manual_query("SELECT sum(qty) as qty from tbl_putaway_fg where week(putaway_fg_date) = week(now()) -3  and fg_id = $fg_id")->row_array(),
				'day_3' =>  $this->Sql->manual_query("SELECT sum(qty) as qty from tbl_putaway_fg where week(putaway_fg_date) = week(now()) -4 and fg_id = $fg_id")->row_array(),
				'day_2' =>  $this->Sql->manual_query("SELECT sum(qty) as qty from tbl_putaway_fg where week(putaway_fg_date) = week(now()) -5 and fg_id = $fg_id")->row_array(),
				'day_1' =>  $this->Sql->manual_query("SELECT sum(qty) as qty from tbl_putaway_fg where week(putaway_fg_date) = week(now()) -6 and fg_id = $fg_id")->row_array(),
			];
	
			$data = [
				'material_name' => $fg->nama,
	
				// Trend
				'today' => ($raw['today']['qty'] == null) ? '0' : $raw['today']['qty'],
				'day6' => ($raw['day_6']['qty'] == null) ? 0 : $raw['day_6']['qty'],
				'day5' => (!isset($raw['day_5']['qty'])) ? 0 : $raw['day_5']['qty'],
				'day4' => ($raw['day_4']['qty'] == null) ? 0 : $raw['day_4']['qty'],
				'day3' => ($raw['day_3']['qty'] == null) ? 0 : $raw['day_3']['qty'],
				'day2' => ($raw['day_2']['qty'] == null) ? 0 : $raw['day_2']['qty'],
				'day1' => ($raw['day_1']['qty'] == null) ? 0 : $raw['day_1']['qty'],

				'labels' => [
					'6 weeks ago',
					'5 weeks ago',
					'4 weeks ago',
					'3 weeks ago',
					'2 weeks ago',
					'last week',
					'this week'
				],
			];
		}else{
			$fg = $this->Sql->select_table('tbl_material', [
				'material_id' => $fg_id
			])->row();
	
			$raw = [
				'today' =>  $this->Sql->manual_query("SELECT sum(qty) as qty, month(putaway_fg_date) from tbl_putaway_fg where month(putaway_fg_date) = month(now()) and fg_id = $fg_id")->row_array(),
				'day_6' =>  $this->Sql->manual_query("SELECT sum(qty) as qty from tbl_putaway_fg where month(putaway_fg_date) = month(now()) - 1 and fg_id = $fg_id")->row_array(),
				'day_5' =>  $this->Sql->manual_query("SELECT sum(qty) as qty from tbl_putaway_fg where month(putaway_fg_date) = month(now()) - 2 and fg_id = $fg_id")->row_array(),
				'day_4' =>  $this->Sql->manual_query("SELECT sum(qty) as qty from tbl_putaway_fg where month(putaway_fg_date) = month(now()) -3  and fg_id = $fg_id")->row_array(),
				'day_3' =>  $this->Sql->manual_query("SELECT sum(qty) as qty from tbl_putaway_fg where month(putaway_fg_date) = month(now()) -4 and fg_id = $fg_id")->row_array(),
				'day_2' =>  $this->Sql->manual_query("SELECT sum(qty) as qty from tbl_putaway_fg where month(putaway_fg_date) = month(now()) -5 and fg_id = $fg_id")->row_array(),
				'day_1' =>  $this->Sql->manual_query("SELECT sum(qty) as qty from tbl_putaway_fg where month(putaway_fg_date) = month(now()) -6 and fg_id = $fg_id")->row_array(),
			];
	
			$data = [
				'material_name' => $fg->nama,
	
				// Trend
				'today' => ($raw['today']['qty'] == null) ? '0' : $raw['today']['qty'],
				'day6' => ($raw['day_6']['qty'] == null) ? 0 : $raw['day_6']['qty'],
				'day5' => (!isset($raw['day_5']['qty'])) ? 0 : $raw['day_5']['qty'],
				'day4' => ($raw['day_4']['qty'] == null) ? 0 : $raw['day_4']['qty'],
				'day3' => ($raw['day_3']['qty'] == null) ? 0 : $raw['day_3']['qty'],
				'day2' => ($raw['day_2']['qty'] == null) ? 0 : $raw['day_2']['qty'],
				'day1' => ($raw['day_1']['qty'] == null) ? 0 : $raw['day_1']['qty'],

				'labels' => [
					'6 months ago',
					'5 months ago',
					'4 months ago',
					'3 months ago',
					'2 months ago',
					'last month',
					'this month'
				],
			];
		}

		$return = [
			'status' => 'success',
			'message' => 'data fetch successfully',
			'data' => $data,
		];

		$json = $this->output
			->set_content_type('application/json')
			->set_output(json_encode($return));

		return $json;
	}

	// middleware
	public function middleware()
	{
		if(!$this->session->name){
			redirect(base_url());
		}
	}
}
