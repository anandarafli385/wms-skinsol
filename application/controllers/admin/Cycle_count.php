<?php 
        
defined('BASEPATH') OR exit('No direct script access allowed');
        
class Cycle_count extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
		$this->load->model("sql");
		$this->middleware();

    }

    public function index()
    {
        $data['api'] = $this->get_api();
        $data['stock'] = $this->get_stock();
        $data['site_title'] = "Material Items";
        $data['subview'] = "cycle_count/data";

        $this->load->view('index', $data);
		$this->middleware(); 
    }

    public function get_api()
    {
        $url = "http://api.skinsolution.co.id/material";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );

        $resp = curl_exec($curl);
        curl_close($curl);

        $return = json_decode($resp);
        return $return;
    }

    public function get_stock()
    {
        $url = "http://api.skinsolution.co.id/stock";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );

        $resp = curl_exec($curl);
        curl_close($curl);

        $return = json_decode($resp);
        return $return;
    }

    public function get_api_detail($id)
    {
        $url = "http://api.skinsolution.co.id/material/".$id;

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );

        $resp = curl_exec($curl);
        curl_close($curl);

        $return = json_decode($resp);
        return $return;
    }

	public function get()
    {
        if ($this->input->post()) {
            $id = $this->input->post('id');
            $data['api_detail'] = $this->get_api_detail($id);
            $this->load->view('cycle_count/get', $data);
        } else {
            echo "error";
        }
    }


	// middleware
	public function middleware()
	{
		if(!$this->session->name){
			redirect(base_url());
		}
	}
}

/* End of file Cycle_count.php and path /application/controllers/admin/Cycle_count.php */


