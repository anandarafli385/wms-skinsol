<?php 
        
defined('BASEPATH') OR exit('No direct script access allowed');
        
class FG_Items extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
		$this->load->model("sql");
		$this->middleware();

    }

    public function index()
    {
        $data['site_title'] = "Finished Goods Items";
        $data['subview'] = "cycle_count/finished_goods";
        $data['api'] = $this->get_api();
        // $daata['api'] = $this->get_api_detail();
        $data['material'] = $this->get_api_material();
        $this->load->view('index', $data);
		$this->middleware(); 
    }

    public function get_api()
    {
        $url = "http://api.skinsolution.co.id/stock";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );

        $resp = curl_exec($curl);
        curl_close($curl);

        $return = json_decode($resp);
        return $return;
    }

    public function get_api_detail($id)
    {
        $url = "http://api.skinsolution.co.id/stock/".$id;

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );

        $resp = curl_exec($curl);
        curl_close($curl);

        $return = json_decode($resp);
        return $return;
    }


    public function get_api_material()
    {
        $url = "http://api.skinsolution.co.id/material";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );

        $resp = curl_exec($curl);
        curl_close($curl);

        $return = json_decode($resp);
        return $return;
    }

    public function get_api_material_detail($id)
    {
        $url = "http://api.skinsolution.co.id/material/".$id;

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );

        $resp = curl_exec($curl);
        curl_close($curl);

        $return = json_decode($resp);
        return $return;
    }

	public function get()
    {
        if ($this->input->post()) {
            $id = $this->input->post('id');
            $data['material'] = $this->get_api_material();
            $data['api'] = $this->get_api_detail($id);
            $this->load->view('cycle_count/detail_fg', $data);
        } else {
            echo "error";
        }
    }

	public function edit($id)
    {
        if ($this->input->post()) {
            $post = $this->input->post();

            $form_data = [
                'stok' => $post['stok'],
				'updated_by' => $this->session->user_id,
				'updated_at' => date('Y-m-d H:i:s')
            ];

            $where = ['stock_id' => $id];

            $this->sql->update_table('tbl_stock', $form_data, $where);


            $message = '<div class="alert alert-success alert-dismissible" role="alert">
							<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
							<div class="alert-message">
								<strong>Success!</strong> Data has been updated!
							</div>
						</div>';
            $this->session->set_flashdata('msg', $message);
            redirect('admin/fg_items');
        } else {
            $message = '<div class="alert alert-danger alert-dismissible" role="alert">
							<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
							<div class="alert-message">
								<strong>Error!</strong> Error!
							</div>
						</div>';
            $this->session->set_flashdata('msg', $message);
            redirect('admin/fg_items');
        }
    }

	// middleware
	public function middleware()
	{
		if(!$this->session->name){
			redirect(base_url());
		}
	}
}

/* End of file Cycle_count.php and path /application/controllers/admin/Cycle_count.php */


