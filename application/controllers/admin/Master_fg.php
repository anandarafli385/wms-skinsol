<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_fg extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model("sql");
// 		$this->middleware();
    }

    public function index()
    {
        // $sql = "SELECT * from tbl_material where inventory_part_type = 'Work in Process' or inventory_part_type = 'Finished Goods' order by material_no";

        // $data['get'] = $this->sql->manual_query($sql)->result_array();
        $data['api'] = $this->get_api();
        $data['site_title'] = "Master FG";
        $data['subview'] = "master_fg/data";
        $this->load->view('index', $data);
    }

    public function get_api(){
        $url = "http://api.skinsolution.co.id/formula";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);

        $return = json_decode($resp);
        return $return;
    }

    public function get_api_formula($id){
        $url = "http://api.skinsolution.co.id/formula/".$id;

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);

        $return = json_decode($resp);
        return $return;
    }

    public function get_api_detail(){
        $url = "http://api.skinsolution.co.id/formula_detail";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);

        $return = json_decode($resp);
        return $return;
    }

    public function add()
    {
        if ($this->input->post()) {
            $post = $this->input->post();

            $form_data = [
                'nama' => $post['nama'],
                'kode_material' => $post['kode_material'],
                'harga' => $post['harga'],
                'unit' => $post['unit'],
                'status' => $post['status'],
                'material_type' => $post['material_type'],
                'inventory_part_type' => $post['inventory_part_type'],
                'user_id' => $this->session->userdata('user_id')
            ];

            if(isset($post['deskripsi'])){
                $form_data['deskripsi'] = $post['deskripsi'];
            }

            $id = $this->sql->insert_table('tbl_material', $form_data);

            if($id){
                $message = '<div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                <div class="alert-message">
                                    <strong>Success!</strong> New data has been created!
                                </div>
                            </div>';
                $this->session->set_flashdata('msg', $message);
                redirect('admin/master_fg');
            }else{
                $message = '<div class="alert alert-danger alert-dismissible" role="alert">
                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                                <div class="alert-message">
                                    <strong>Error!</strong> Error!
                                </div>
                            </div>';
                $this->session->set_flashdata('msg', $message);
                redirect('admin/master_fg');
            }
        } else {
            $message = '<div class="alert alert-danger alert-dismissible" role="alert">
							<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
							<div class="alert-message">
								<strong>Error!</strong> Error!
							</div>
						</div>';
            $this->session->set_flashdata('msg', $message);
            redirect('admin/master_fg');
        }
    }

    public function view()
    {
        if ($this->input->post()) {
            $data['id'] = $this->input->post('id');
            $data['api_detail'] = $this->get_api_detail();
            $data['formula'] = $this->get_api_formula($id);
            $this->load->view('master_fg/view', $data);
        } else {
            echo "error";
        }
    }

	// middleware
	public function middleware()
	{
		if(!$this->session->name){
			redirect(base_url());
		}
	}
}
