<?php

use phpDocumentor\Reflection\Types\This;

defined('BASEPATH') OR exit('No direct script access allowed');
        
class Profile extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model("sql");
		$this->middleware();
    }

    public function index()
    {
        $data['site_title'] = "Profile";
        $data['subview'] = "profile/profile";
        $this->load->view('index', $data);
    }

	public function change_password()
	{
		if ($this->input->post()) {
			$post = $this->input->post();
			
			if($post['new_pass'] !== $post['new_pass_confirm']){
				$this->session->set_flashdata('alert_danger', 'New password and new password confirmation does`t match.');
				$this->session->set_flashdata('tab_active', 'password');
				redirect(base_url('admin/profile'));
			}else{
				$query = $this->sql->select_table('tbl_user', array('user_id' => $post['user_id']));
				if ($query->num_rows() > 0) {
					$get = $query->row_array();
					if (password_verify($post['current_pass'], $get['password'])) {
						$new_password = password_hash($post['new_pass'], PASSWORD_DEFAULT);

						$this->sql->update_table('tbl_user', [
							'password' => $new_password
						], array('user_id' => $post['user_id']));

						$this->session->set_flashdata('alert_success', 'Password successfully changed');
						redirect(base_url('admin/profile'));
					}else{
						$this->session->set_flashdata('alert_danger', 'Your Current password is wrong.');
						$this->session->set_flashdata('tab_active', 'password');
						redirect(base_url('admin/profile'));
					}
				}else{
					$this->session->set_flashdata('alert_danger', 'Invalid User data');
					$this->session->set_flashdata('tab_active', 'password');
					redirect(base_url('admin/profile'));
				}
			}
		}else{
			redirect(base_url('admin/profile'));
		}
		
	}

	public function change_profile()
	{
		if ($this->input->post()) {
			$post = $this->input->post();
			$data = [
				'name' => $post['name'],
			];

			// Generate random string 16 digit
            $unique = random_string('alnum', 16);

            // Konfigurasi Upload Image
            $config['upload_path']          = './assets/uploads/profile/';
            $config['allowed_types']        = 'gif|jpg|png|jpeg|PNG|JPG|JPEG';
            $config['file_name']            = $unique . '-' . date('dmY');
            $config['overwrite']            = true;
            $config['max_size']             = 1024 * 8; // 8MB

            // Load library upload
            $this->load->library('upload', $config);

			// Jika berhasil upload gambar
			if($this->upload->do_upload('image')){
				// ambil data profil saat ini
				$current_image = $this->session->image;

				// Jika data image sebelumnya bukan file default.jpg
				if($current_image != 'default.png'){
					// Hapus file dari folder profile
					unlink('./assets/uploads/profile/'. $current_image);
				}

				$data['image'] = $this->upload->data('file_name');
				$this->session->set_userdata([
					'image' => $data['image']
				]);
			}

			$this->session->set_userdata([
				'name' => $data['name']
			]);

			$update = $this->sql->update_table('tbl_user', $data, ['user_id' => $post['user_id']]);
			if($update){
				$this->session->set_flashdata('alert_success', 'Your Profile is up to date');
				$this->session->set_flashdata('tab_active', 'profile');
				redirect(base_url('admin/profile'));
			}else{
				$this->session->set_flashdata('alert_danger', 'Something went wrong, please check your data');
				$this->session->set_flashdata('tab_active', 'profile');
				redirect(base_url('admin/profile'));
			}

		}else{
			redirect(base_url('admin/profile'));
		}
	}

	// middleware
	public function middleware()
	{
		if(!$this->session->name){
			redirect(base_url());
		}
	}
}

/* End of file Profile.php and path /application/controllers/admin/Profile.php */


