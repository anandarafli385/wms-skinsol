<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Putaway extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model("sql");
		$this->middleware();
    }

    public function index()
    {
        // $data['get'] = $this->sql->putaway_list()->result_array();
        $data['get'] = $this->sql->putaway_list_new()->result();
        $data['api'] = $this->get_api();
        $data['site_title'] = "Putaway";
        $data['subview'] = "putaway/data";
        $data['qc'] = $this->sql->select_table('tbl_gr_qc')->result();
        $data['location'] = $this->sql->select_table('tbl_location')->result();

        $this->load->view('index', $data);
		$this->middleware();

    }

    public function detail()
    {
        if ($this->input->post()) {
            $id = $this->input->post('id');
            $data['get'] = $this->get_api_detail($id);
            $this->load->view('putaway/detail', $data);
        } else {
            echo "error";
        }
    }

    public function get_api()
    {
        $url = "http://api.skinsolution.co.id/penerimaan_material_detail";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );

        $resp = curl_exec($curl);
        curl_close($curl);

        $return = json_decode($resp);
        return $return;
    }

    public function get_api_detail($id)
    {
        $url = "http://api.skinsolution.co.id/penerimaan_material_detail/".$id;

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );

        $resp = curl_exec($curl);
        curl_close($curl);

        $return = json_decode($resp);
        return $return;
    }


    public function get_location()
    {
        if ($this->input->post()) {
            $location = $this->input->post('location');
            $where['location_name'] = $location;

            $get = $this->sql->select_table('tbl_location', $where)->num_rows();
            if ($get > 0) {
                echo "OK";
            } else {
                echo "notfound";
            }
        } else {
            echo "error";
        }
    }

    public function get_box()
    {
        $data['get'] = $this->sql->putaway_list_new()->result();
        if ($this->input->post()) {
            $batch_nums = $this->input->post('batch_num');
            $api_detail = $this->get_api_detail($batch_nums);
            $batch = $api_detail->batch_num;
        

            // $get = $this->sql->material_box_list($where);
            if (count((array)$api_detail) > 0) {
                
                $session = [];
                $batch_num = [];

                $locations = $this->sql->select_table('tbl_location')->result();

                $lokasi = [];
                foreach($locations as $location){
                    array_push($lokasi, $location->location_name);
                }

                if (!in_array($batch, $batch_num)) {
                    $session[] = [
                        'batch_num' => $api_detail->batch_num,
                        'purchase_num' => $api_detail->purchase->purchase_num,
                        'material' => $api_detail->material != null ? $api_detail->material->material_name :"-",
                        'qty' => $api_detail->quantity,
                        'category' => $api_detail->material != null ? $api_detail->material->category : "-"
                    ];

                    $this->session->set_userdata('putaway', $session);


                    $return = [
    					'status' => 'success',
    					'message' => 'OK',
    					'data' => $api_detail,
    					'lokasi' => $lokasi,
                        // 'html' => $html,
    				];
                } else {
                    $this->session->set_userdata('putaway', $session);

    				$return = [
    					'status' => 'warning',
    					'message' => 'duplicate'
    				];
                }
            } else {
                $return = [
    				'status' => 'error',
    				'message' => 'error'
    			];
            }
        } else {
            $return = [
                'status' => 'error',
                'message' => 'error'
            ];
        }

		// set content as JSON
		$val = $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($return));

		// return statement
		return $val;
    }


    public function get()
    {
        if ($this->input->post()) {
            $id = $this->input->post('id');
            
            // $sql_wo = "SELECT w.*, c.nama FROM tbl_work_order w inner join tbl_customer c on w.customer_id = c.customer_id order by w.work_order_id desc";
            
            // $sql_fg = "SELECT * from tbl_material where inventory_part_type = 'Work in Process' or inventory_part_type = 'Finished Goods' order by material_no";

            $data['putnew'] = $this->sql->select_table('tbl_putaway_new', ['putaway_id' => $id])->row_array();
            $data['putdetailnew'] = $this->sql->select_table('tbl_putaway_detail_new', ['putaway_id' => $id])->row_array();
            // $data['wo'] = $this->sql->select_table('tbl_work_order',[
            //     'status' => 'on progress'
            // ])->result_array();
            $data['api'] = $this->get_api();
            $data['location'] = $this->sql->select_table('tbl_location', null, 'location_name')->result();



            $this->load->view('putaway/get', $data);
        } else {
            echo "error";
        }
    }
    public function list_box()
    {
        $this->load->view('putaway/list_box');
    }

    public function delete_box()
    {
        if ($this->input->post()) {
            $key = $this->input->post('key');

            $array = $this->session->userdata('putaway');
            $this->session->unset_userdata('putaway');
            unset($array[$key]);
            $this->session->set_userdata('putaway', $array);

            echo "OK";
		} else {
            echo "error";
        }
    }

    public function add()
    {
        if($this->input->post()){
            $post = $this->input->post();
            
            $form_data = [
                'putaway_date' => $post['putaway_date'],
                'location' => $post['location'],
                'user_id' => $this->session->user_id
            ];

            // Insert new Putaway data & get newest Id
            $putaway_id = $this->sql->insert_table('tbl_putaway_new', $form_data);

            foreach($post['batch_id'] as $key => $value){
                $form_data = [
                    'putaway_id' => $putaway_id,
                    'batch_id' => $value,
                    'material_id' => $post['material_id'][$key],
                    'layer' => $post['layer'][$key],
                ];

                // Insert putaway detail for each material scanned
                $this->sql->insert_table('tbl_putaway_detail_new', $form_data);

                // // Update location in material box table
                // $form_data = ['location' => $post['location']];
                // $this->sql->update_table('tbl_material_box', $form_data, ['batch_id' => $value]);
            }

            $this->session->unset_userdata('putaway');

            echo "OK";
        }else{
            echo "error";
        }
    }

    public function update()
    {
        if($this->input->post()){
            $post = $this->input->post();

            $where1 = [
                'putaway_id' => $post['putaway_id'],
                'batch_id' => $post['batch_id'],
                'material_id' => $post['material_id']
            ];

            $form_data1 = [
                'layer' => $post['layer'],
            ];

            $where2 = [
                'putaway_id' => $post['putaway_id'],
            ];

            $form_data2 = [
                'location' => $post['location'],
            ];

            $this->sql->update_table('tbl_putaway_detail_new', $form_data1, $where1);
            $this->sql->update_table('tbl_putaway_new', $form_data2, $where2);
            $message = '<div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            <div class="alert-message">
                                <strong>Success!</strong> Data has been updated!
                            </div>
                        </div>';
            $this->session->set_flashdata('msg', $message);
            redirect('admin/putaway');

        } else {
            $message = '<div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            <div class="alert-message">
                                <strong>Error!</strong> Error!
                            </div>
                        </div>';
            $this->session->set_flashdata('msg', $message);
            redirect('admin/putaway');
        }
    }

    public function delete()
    {
        if ($this->input->post()) {
            $id = $this->input->post('id');
            $where = ['batch_id' => $id];

            $this->sql->delete_table('tbl_putaway_detail_new', $where);
            echo "ok";
        } else {
            echo "error";
        }
    }

	// middleware
	public function middleware()
	{
		if(!$this->session->name){
			redirect(base_url());
		}
	}
}
