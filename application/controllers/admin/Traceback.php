<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Traceback extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model("sql");
        $this->middleware();
    }

    // Index page
    public function index()
    {
        $data['site_title'] = "Traceback";
        $data['subview'] = "traceback/data";

        $this->load->view('index', $data);
    }

    // API Trace FG
    public function trace()
    {
        $qrcode = $this->input->post('qrcode');
        
        $data_wo = $this->sql->inner_join('fg.*, wo.*', 'tbl_putaway_fg fg', 'tbl_work_order wo', 'fg.work_order_id','wo.work_order_id', [
            'fg.qr_code' => $qrcode
        ])->row();

        if($data_wo){
            $fg = $this->sql->select_table('tbl_material', [
                'material_id' => $data_wo->fg_id
            ])->row();

            $return = [
				'status' => 'success',
				'message' => 'qr found',
                'data' => [
                    'wo' => $data_wo,
                    'fg' => $fg,
                    'date' => [
                        'putaway_fg_date' => date('d M Y', strtotime($data_wo->putaway_fg_date)),
                        'retest_date' => date('d M Y', strtotime($data_wo->retest_date)),
                        'expired_date' => date('d M Y', strtotime($data_wo->expired_date)),
                    ] 
                ]
			];
        }else{
            $return = [
				'status' => 'error',
				'message' => 'error',
                'data' => []
			];
        }

        // set content as JSON
		$val = $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($return));

		// return statement
		return $val;
    }

    // middleware
	public function middleware()
	{
		if(!$this->session->name){
			redirect(base_url());
		}
	}
}
