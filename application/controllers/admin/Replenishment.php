<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Replenishment extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model("sql");
		
		$this->middleware();
    }

    public function index()
    {
        $data['site_title'] = "Replenishment";
        $data['subview'] = "replenishment/data";

		$data['replenishment'] = $this->sql->select_table('tbl_replenishment')->result_array();
        $data['putaway'] = $this->sql->putaway_list_new()->result();
		$data['api_wo'] = $this->get_api_wo();
        $data['location'] = $this->sql->select_table('tbl_location')->result();

		$this->middleware();
        $this->load->view('index', $data);
    }

	public function add()
	{
		if ($this->input->post()) {
            $post = $this->input->post();

			$replenishment_data = [
				'replenishment_no' => $_POST['replenishment_no'],
				'work_order_id' => $_POST['work_order_id'],
				'work_order_batch' => $_POST['work_order_batch'],
				'wo_formula'=> $_POST['wo_formula'],
				'wo_po_num'=> $_POST['wo_po_num'],
				'wo_pack'=> $_POST['wo_pack'],
				'wo_status'=> $_POST['wo_status'],
				'wo_product_name'=> $_POST['wo_product_name'],
				'wo_product_code'=> $_POST['wo_product_code'],
				'tgl_berlaku'=> $_POST['tgl_berlaku'],
				'tgl_mulai'=> $_POST['tgl_mulai'],
                'packaging_name'=> $_POST['packaging_name'],
				'packaging_code'=> $_POST['packaging_code'],
				'revision_num'=> $_POST['revision_num'],
				'user_id' => $_POST['user_id'],
			];
			$replenishment_id = $this->sql->insert_table('tbl_replenishment', $replenishment_data);
			// Insert replenishment

			// Looping for each material data
			$key = 0;
			foreach($post['batch_num'] as $key => $value) {
				$replenishment_detail = [
					'replenishment_id' => $replenishment_id,
					'wo_po_num' => $_POST['wo_po_num'],
					'batch_id' => $post['batch_location'][$key],
					'material_box_id' => $post['batch_id'][$key],
					'material_name' => $post['batch_name'][$key],
					'qty' => $post['batch_qty'][$key],
					'user_id' =>  $this->session->user_id,
				];
				
				$this->sql->insert_table('tbl_replenishment_detail', $replenishment_detail);
			}

            echo "OK";
        }else{
            echo "error";
        }
	}

    public function get_api_wo(){
        $url = "http://api.skinsolution.co.id/so";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);

        $return = json_decode($resp);
        return $return;
    }

    public function get_api_detail_wo($id){
        $url = "http://api.skinsolution.co.id/so/".$id;

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);

        // $return = ;
		$return = [
			'status' => 'success',
			'message' => 'Data successfully fetch',
			'data' => json_decode($resp)
		];

		$val = $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($return));
        return $val;
    }

    public function get_api_product(){
        $url = "http://api.skinsolution.co.id/product";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);

        // $return = ;
		$return = [
			'status' => 'success',
			'message' => 'Data successfully fetch',
			'data' => json_decode($resp)
		];

		$val = $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($return));
        return $val;
    }

    public function get_api_packaging($id){
        $url = "http://api.skinsolution.co.id/packaging/".$id;

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);

        // $return = ;
		$return = [
			'status' => 'success',
			'message' => 'Data successfully fetch',
			'data' => json_decode($resp)
		];

		$val = $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($return));
        return $val;
    }

    public function get_trial_revision($id){
        $url = "http://api.skinsolution.co.id/trial_revision/".$id;

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);

        // $return = ;
		$return = [
			'status' => 'success',
			'message' => 'Data successfully fetch',
			'data' => json_decode($resp)
		];

		$val = $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($return));
        return $val;
    }

    public function get_api_formula($id){
        $url = "http://api.skinsolution.co.id/formula/".$id;

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);

        // $return = ;
		$return = [
			'status' => 'success',
			'message' => 'Data successfully fetch',
			'data' => json_decode($resp)
		];

		$val = $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($return));
        return $val;
    }

    public function get_product_activity(){
        $url = "http://api.skinsolution.co.id/product_activity";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);

        // $return = ;
		$return = [
			'status' => 'success',
			'message' => 'Data successfully fetch',
			'data' => json_decode($resp),
		];

		$val = $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($return));
        return $val;
    }

    public function get_api_detail_formula(){
        $url = "http://api.skinsolution.co.id/formula_detail";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);

        $location = $this->sql->select_table('tbl_location')->result();

        // $return = ;
		$return = [
			'status' => 'success',
			'message' => 'Data successfully fetch',
			'data' => json_decode($resp),
			'location' => $location
		];

		$val = $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($return));
        return $val;
    }

    public function get_api_detail_material()
    {
        $url = "http://api.skinsolution.co.id/penerimaan_material_detail";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );

        $resp = curl_exec($curl);
        curl_close($curl);

        $putaway = $this->sql->putaway_list_new()->result();

		$return = [
			'status' => 'success',
			'message' => 'Data successfully fetch',
			'data' => json_decode($resp),
			'putaway' => $putaway,
		];

		$val = $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($return));
        return $val;
    }

    public function get_api_detail_box($id)
    {
        $url = "http://api.skinsolution.co.id/penerimaan_material_detail/".$id;

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );

        $resp = curl_exec($curl);
        curl_close($curl);

        $return = json_decode($resp);
        return $return;
    }

    public function get_box()
    {
        if ($this->input->post()) {
            $batch_nums = $this->input->post('batch_num');
            $api_detail = $this->get_api_detail_box($batch_nums);
            $where['location'] = 'Raw Material';
        

            // $get = $this->sql->material_box_list($where);
            if (count((array)$api_detail) > 0) {
                
                $session = [];
                $batch_num = [];

                $locations = $this->sql->select_table('tbl_location',[
                    'category' => 'Raw Material'
                ], 'location_name')->result();

                $lokasi = [];
                foreach($locations as $location){
                    array_push($lokasi, $location->location_name);
                }

                if (!in_array($api_detail->batch_num, $batch_num)) {
                    $session[] = [
                        'batch_num' => $api_detail->batch_num,
                        'purchase_num' => $api_detail->purchase->purchase_num,
                        'material' => $api_detail->material != null ? $api_detail->material->material_name :"-",
                        'qty' => $api_detail->quantity,
                        'category' => $api_detail->material != null ? $api_detail->material->category : "-"
                    ];

                    $this->session->set_userdata('putaway', $session);


                    $return = [
    					'status' => 'success',
    					'message' => 'OK',
    					'data' => $api_detail,
    					'lokasi' => $lokasi,
                        // 'html' => $html,
    				];
                } else {
                    $this->session->set_userdata('putaway', $session);

    				$return = [
    					'status' => 'warning',
    					'message' => 'duplicate'
    				];
                }
            } else {
                $return = [
    				'status' => 'error',
    				'message' => 'error'
    			];
            }
        } else {
            $return = [
                'status' => 'error',
                'message' => 'error'
            ];
        }

		// set content as JSON
		$val = $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($return));

		// return statement
		return $val;
    }

	public function get()
	{
		if ($this->input->post()) {
            $id = $this->input->post('id');
            $data['get'] = $this->sql->select_table('tbl_replenishment', ['replenishment_id' => $id])->row_array();

            $this->load->view('replenishment/get', $data);
        } else {
            echo "error";
        }
	}

	public function delete()
	{
		if ($this->input->post()) {
            $id = $this->input->post('id');
            $where = ['replenishment_id' => $id];

            $this->sql->delete_table('tbl_replenishment_detail', $where);
            $this->sql->delete_table('tbl_replenishment', $where);
            echo "ok";
        } else {
            echo "error";
        }
	}

	public function update_status()
    {

        if($this->input->post()){
            $post = $this->input->post();

            $query = $this->db->query("SELECT * FROM `tbl_replenishment` WHERE replenishment_id = ".$post['replenishment_id']);
            $result = $query->result();

            $where = [
                'replenishment_id' => $post['replenishment_id'],
            ];

            $form_data = [
                'approval' => $post['approval'],
				'approved_by' => $this->session->user_id,
				'approved_at' => date('Y-m-d H:i:s')
            ];

            $this->sql->update_table('tbl_replenishment', $form_data, $where);

            var_dump($result);
            $data = [
				'replenishment_id' => $post['replenishment_id'],
				'qty'=> intval($result[0]->wo_pack),
				'date'=> date('Y-m-d H:i:s'),
				'status'=> 'No Started',
				'created_by' => $this->session->userdata("user_id"),
			];

			$check_data = $this->sql->select_table('tbl_productions',['replenishment_id' => $post['replenishment_id']])->result_array();
			$this->sql->insert_table('tbl_productions', $data);
			

			$ids = $this->db->insert_id();
			$datas = [
				'production_id' => $ids,
				'qty'=> 0,
				'status'=> 'No Started',
				'created_by' => $this->session->userdata("user_id"),
			];

			$this->sql->insert_table('tbl_production_logs', $datas);



            $message = '<div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            <div class="alert-message">
                                <strong>Success!</strong> Data has been updated!
                            </div>
                        </div>';
            $this->session->set_flashdata('msg', $message);
            redirect('admin/replenishment');

        } else {
            $message = '<div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            <div class="alert-message">
                                <strong>Error!</strong> Error!
                            </div>
                        </div>';
            $this->session->set_flashdata('msg', $message);
            redirect('admin/replenishment');
        }
    }

	// Dokument Release
	public function release_document($release_id)
	{
		if ($release_id) {
            $where['replenishment_id'] = $release_id;
            $data['get'] = $this->sql->select_table('tbl_replenishment', $where)->row_array();
            $data['fg_materials'] = $this->sql->select_table('tbl_replenishment_detail', $where)->result();

            $page = $this->load->view('replenishment/print_document', $data, true);
            $this->load->library('pdf');
            $this->pdf->setPaper('A4', 'potrait');
            $this->pdf->filename = "Material Release".$release_id.".pdf";
            $this->pdf->load_view('replenishment/print_document', $data);
			// log_material_release("print", "memprint Material Release");
        } else {
            redirect(base_url('admin/replenishment'));
        }
	}

	// middleware
	public function middleware()
	{
		if(!$this->session->name){
			redirect(base_url());
		}
	}
}
