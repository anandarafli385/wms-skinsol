<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Purchasing_order extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model("sql");
		$this->middleware();

    }

    public function index()
    {
        $data['api'] = $this->get_api();
        $data['site_title'] = "Purchasing Order";
        $data['subview'] = "purchasing_order/data";
        $this->load->view('index', $data);
		$this->middleware();
    }


    public function detail()
    {
        if ($this->input->post()) {
            $id = $this->input->post('id');
            $data['api_detail'] = $this->get_api_detail($id);
            $this->load->view('purchasing_order/detail', $data);
        } else {
            echo "error";
        }
    }

    public function get_api()
    {
        $url = "http://api.skinsolution.co.id/po";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );

        $resp = curl_exec($curl);
        curl_close($curl);

        $return = json_decode($resp);
        return $return;
    }

    public function get_api_detail($id)
    {
        $url = "http://api.skinsolution.co.id/po/".$id;

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );

        $resp = curl_exec($curl);
        curl_close($curl);

        $return = json_decode($resp);
        return $return;
    }

	// middleware
	public function middleware()
	{
		if(!$this->session->name){
			redirect(base_url());
		}
	}
}
