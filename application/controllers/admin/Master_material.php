<?php
defined('BASEPATH') or exit('No direct script access allowed');

require FCPATH.'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Master_material extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model("sql");
		$this->middleware();
		
    }

    public function index()
    {
        // $data['get'] = $this->sql->select_table('tbl_material', [
        //     'inventory_part_type !=' => 'Finished Goods'
        // ])->result_array();
        // $data['supp'] = $this->sql->select_table('tbl_supplier_old')->result_array();
        $data['api'] = $this->get_api();
        $data['site_title'] = "Master Material";
        $data['subview'] = "master_material/data";
        $this->load->view('index', $data);
		$this->middleware();
    }


    public function get_api()
    {
        $url = "http://api.skinsolution.co.id/stock";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );

        $resp = curl_exec($curl);
        curl_close($curl);

        $return = json_decode($resp);
        return $return;
    }
    

 //    public function add()
 //    {
 //        if ($this->input->post()) {
 //            $post = $this->input->post();

 //            $form_data = [
 //                'material_id' => $post['material_id'],
 //                'kode_material' => $post['kode_material'],
 //                'material_no' => $post['material_no'],
 //                'cas_num' => $post['cas_num'],
 //                'nama' => $post['nama'],
 //                'inci' => $post['inci'],
 //                'supplier_id' => $post['supplier_id'],
 //                'stok_minimum' => $post['stok_minimum'],
 //                'kategori' => $post['kategori'],
 //                'harga' => $post['harga'],
 //                'unit' => $post['unit'],
 //                'status' => $post['status'],
 //                // 'material_type' => $post['material_type'],
 //                // 'inventory_part_type' => $post['inventory_part_type'],
 //                'user_id' => $this->session->userdata('user_id')
 //            ];

 //            if(isset($post['deskripsi'])){
 //                $form_data['deskripsi'] = $post['deskripsi'];
 //            }

 //            $id = $this->sql->insert_table('tbl_material', $form_data);

 //            if($id){
 //                $message = '<div class="alert alert-success alert-dismissible" role="alert">
 //                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
 //                                <div class="alert-message">
 //                                    <strong>Success!</strong> New data has been created!
 //                                </div>
 //                            </div>';
 //                $this->session->set_flashdata('msg', $message);
 //                redirect('admin/master_material');
 //            }else{
 //                $message = '<div class="alert alert-danger alert-dismissible" role="alert">
 //                                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
 //                                <div class="alert-message">
 //                                    <strong>Error!</strong> Error!
 //                                </div>
 //                            </div>';
 //                $this->session->set_flashdata('msg', $message);
 //                redirect('admin/master_material');
 //            }
 //        } else {
 //            $message = '<div class="alert alert-danger alert-dismissible" role="alert">
	// 						<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
	// 						<div class="alert-message">
	// 							<strong>Error!</strong> Error!
	// 						</div>
	// 					</div>';
 //            $this->session->set_flashdata('msg', $message);
 //            redirect('admin/master_material');
 //        }
 //    }

 //    public function get()
 //    {
 //        if ($this->input->post()) {
 //            $id = $this->input->post('id');
 //            $data['get'] = $this->sql->select_table('tbl_material', ['material_id' => $id])->row_array();

 //            $this->load->view('master_material/get', $data);
 //        } else {
 //            echo "error";
 //        }
 //    }

	// public function upload_excel()
	// {
	// 	// Generate random string 16 digit
	// 	$unique = random_string('alnum', 16);

	// 	// Konfigurasi Upload Image
	// 	$config['upload_path']          = './assets/excel/';
	// 	$config['allowed_types']        = 'xls|xlsx|XLS|XLSX';
	// 	$config['file_name']            = $unique . '-' . date('dmY');
	// 	$config['overwrite']            = true;
	// 	$config['max_size']             = 1024 * 8; // 8MB

	// 	// Load library upload
	// 	$this->load->library('upload', $config);

	// 	// Jika berhasil upload gambar
	// 	if($this->upload->do_upload('upload_file')){
	// 		$uploaded_file = "./assets/excel/" . $this->upload->data('file_name');

	// 		$reader= new \PhpOffice\PhpSpreadsheet\Reader\Xls();
	// 		$spreadsheet= $reader->load($uploaded_file);
	// 		$sheetdata=$spreadsheet->getActiveSheet()->toArray();
	// 		$sheetcount=array_key_last($sheetdata);
			
	// 		echo $sheetcount;
	// 		for($n = 0; $n <= 10; $n++){
	// 			unset($sheetdata[$n]);
	// 		}
	
	// 		$i = 1;
	// 		$codes = [];
	// 		//Make sure code is unique
	// 		for ($n = 0; $n < $sheetcount; $n++) {
	// 			$kode = $this->RandomString();
	// 			while(in_array($kode, $codes)){
	// 				$kode = $this->RandomString();
	// 			}

	// 			array_push($codes, $kode);
	// 		}

	// 		// print_r($codes);


	// 		foreach ($sheetdata as $index => $t) {
	// 			if($index == $sheetcount){
	// 				break;
	// 			}

	// 				$kode = $this->RandomString();

	// 			// $item_no = $t[1];
	// 			// $desc = $t[5];
	// 			// $qty = $t[9];
	// 			// $price = (int)$t[13];
	// 			// $item_type = $t[17];
	// 			// $inventory_type = $t[21];
				
	// 			$item_id = $t[1];
	// 			$item_code = $t[2];
	// 			$cas_num = $t[3];
	// 			$name = $t[4];
	// 			$inchi = $t[5];
	// 			$supp = $t[6];
	// 			$stok_min = $t[7];
	// 			$category = $t[8];
	// 			$price = $t[9];
	// 			$halal = $t[10];
	// 			$created_at = $t[11];
	// 			// $updated_at = $t[11];


 //                $form_data = [
 //     //                'material_no' => $item_no,
 //     //                'nama' => $desc,
	// 				// 'deskripsi' => $desc,
	// 				// 'kode_material' => $codes[$index],
	                
	//                 'id' => $item_id,
	//                 'kode_material' => $item_code,
	//                 'material_no' => 99999,
	//                 'cas_num' => $cas_num,
	//                 'nama' => $name,
	//                 'inci' => $inchi,
	//                 'supplier_id' => $supp,
	//                 'stok_minimum' => $stok_min,
	//                 'kategori' => $category,
	// 				'harga' => $price,
	// 				'status' => $halal,
	// 				'created_at' => $created_at,
	// 				// 'updated_at' => $updated_at,
	// 				'unit' => 'g',
 //                    // 'material_type' => $item_type,
 //                    // 'inventory_part_type' => $inventory_type,
	// 				'user_id' => $this->session->userdata('user_id')
	// 			];

	// 			// echo "</br>$i. $desc ==== $price ";

	// 			$this->sql->insert_table('tbl_material', $form_data);
	// 			$i++;
	// 		}

			
	// 	}

	// 	$message = '<div class="alert alert-success alert-dismissible" role="alert">
	// 						<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
	// 						<div class="alert-message">
	// 							<strong>Success!</strong> Excel has been imported successfully
	// 						</div>
	// 					</div>';
 //            $this->session->set_flashdata('msg', $message);
 //            redirect('admin/master_material');

	// }

	// function RandomString()
 //    {
 //        // $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
 //        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	// 	$randstring = '';
 //        for ($i = 0; $i < 3; $i++) {
 //            $randstring .= $characters[rand(0, strlen($characters) - 1)];
 //        }
 //        return $randstring;
 //    }

 //    public function edit($id)
 //    {
 //        if ($this->input->post()) {
 //            $post = $this->input->post();

 //            $form_data = [
 //                'nama' => $post['nama'],
 //                'kode_material' => $post['kode_material'],
 //                'harga' => $post['harga'],
 //                'unit' => $post['unit'],
 //                'status' => $post['status'],
 //                'material_type' => $post['material_type'],
 //                'inventory_part_type' => $post['inventory_part_type'],
 //                // 'user_id' => $this->session->userdata('user_id')
 //            ];

 //            $where = ['material_id' => $id];

 //            $id = $this->sql->update_table('tbl_material', $form_data, $where);

 //            if($id){
 //                $message = '<div class="alert alert-success alert-dismissible" role="alert">
	// 						<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
	// 						<div class="alert-message">
	// 							<strong>Success!</strong> Data has been updated!
	// 						</div>
	// 					</div>';
 //                $this->session->set_flashdata('msg', $message);
 //                redirect('admin/master_material');
 //            }else{
 //                $message = '<div class="alert alert-danger alert-dismissible" role="alert">
	// 						<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
	// 						<div class="alert-message">
	// 							<strong>Error!</strong> Error!
	// 						</div>
	// 					</div>';
 //                $this->session->set_flashdata('msg', $message);
 //                redirect('admin/master_material');
 //            }
 //        } else {
 //            $message = '<div class="alert alert-danger alert-dismissible" role="alert">
	// 						<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
	// 						<div class="alert-message">
	// 							<strong>Error!</strong> Error!
	// 						</div>
	// 					</div>';
 //            $this->session->set_flashdata('msg', $message);
 //            redirect('admin/master_material');
 //        }
 //    }

 //    public function delete()
 //    {
 //        if ($this->input->post()) {
 //            $id = $this->input->post('id');
 //            $where = ['material_id' => $id];

 //            $this->sql->delete_table('tbl_material', $where);
 //            echo "ok";
 //        } else {
 //            echo "error";
 //        }
 //    }

	// middleware
	public function middleware()
	{
		if(!$this->session->name){
			redirect(base_url());
		}
	}
}
