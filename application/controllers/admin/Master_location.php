<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_location extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model("sql");
		$this->middleware();
    }

    public function index()
    {
        $data['get'] = $this->sql->select_table('tbl_location')->result_array();
        $data['site_title'] = "Master Location";
        $data['subview'] = "master_location/data";
        $this->load->view('index', $data);
    }

    public function add()
    {
        if ($this->input->post()) {
            $post = $this->input->post();

            $form_data = [
                'location_name' => $post['location_name'],
                'user_id' => $this->session->userdata('user_id')
            ];

            $id = $this->sql->insert_table('tbl_location', $form_data);

            $message = '<div class="alert alert-success alert-dismissible" role="alert">
							<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
							<div class="alert-message">
								<strong>Success!</strong> New data has been created!
							</div>
						</div>';
            $this->session->set_flashdata('msg', $message);
            redirect('admin/master_location');
        } else {
            $message = '<div class="alert alert-danger alert-dismissible" role="alert">
							<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
							<div class="alert-message">
								<strong>Error!</strong> Error!
							</div>
						</div>';
            $this->session->set_flashdata('msg', $message);
            redirect('admin/master_location');
        }
    }

    public function get()
    {
        if ($this->input->post()) {
            $id = $this->input->post('id');
            $data['get'] = $this->sql->select_table('tbl_location', ['location_id' => $id])->row_array();

            $this->load->view('master_location/get', $data);
        } else {
            echo "error";
        }
    }

    public function edit($id)
    {
        if ($this->input->post()) {
            $post = $this->input->post();

            $form_data = [
                'location_name' => $post['location_name'],
                'user_id' => $this->session->userdata('user_id')
            ];

            $where = ['location_id' => $id];

            $id = $this->sql->update_table('tbl_location', $form_data, $where);

            $message = '<div class="alert alert-success alert-dismissible" role="alert">
							<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
							<div class="alert-message">
								<strong>Success!</strong> Data has been updated!
							</div>
						</div>';
            $this->session->set_flashdata('msg', $message);
            redirect('admin/master_location');
        } else {
            $message = '<div class="alert alert-danger alert-dismissible" role="alert">
							<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
							<div class="alert-message">
								<strong>Error!</strong> Error!
							</div>
						</div>';
            $this->session->set_flashdata('msg', $message);
            redirect('admin/master_location');
        }
    }

    public function delete()
    {
        if($this->input->post()){
            $id = $this->input->post('id');
            $where = ['location_id' => $id];
            
            $this->sql->delete_table('tbl_location', $where);
            echo "ok";
        }else{
            echo "error";
        }
    }

	// middleware
	public function middleware()
	{
		if(!$this->session->name){
			redirect(base_url());
		}
	}
}
