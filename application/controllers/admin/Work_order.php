<?php

use phpDocumentor\Reflection\Types\This;

defined('BASEPATH') or exit('No direct script access allowed');

class Work_order extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model("sql");
		// $this->middleware();
		
    }

    public function index()
    {
    	$data['approval'] = $this->sql->select_table('tbl_work_order_approval')->result();
        $data['get'] = $this->get_api();
        $data['cust'] = $this->get_api_customer();
        $data['site_title'] = "Work Order";
        $data['subview'] = "work_order/data";
        $this->load->view('index', $data);
		$this->middleware();
    }

    public function get_api(){
        $url = "http://api.skinsolution.co.id/so";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);

        $return = json_decode($resp);
        return $return;
    }

    public function get_api_detail($id){
        $url = "http://api.skinsolution.co.id/so/".$id;

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);

        $return = json_decode($resp);
        return $return;
    }

    public function get_api_customer(){
        $url = "http://api.skinsolution.co.id/customer";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);

        $return = json_decode($resp);
        return $return;
    }

	public function detail()
	{
		if ($this->input->post()) {
            $id = $this->input->post('id');
            $data['api'] = $this->get_api_detail($id);
            $this->load->view('work_order/detail', $data);
        } else {
            echo "error";
        }
	}


    public function get()
    {
        if ($this->input->post()) {
            $id = $this->input->post('id');
            $data['api'] = $this->get_api_detail($id);
            $data['approval'] = $this->sql->select_table('tbl_work_order_approval')->result();
            $this->load->view('work_order/get', $data);
        } else {
            echo "error";
        }
    }	

    public function update()
    {
        if($this->input->post()){
            $post = $this->input->post();

            $form_data = [
                'work_order_id' => $post['work_order_id'],
                'work_order_num' => $post['work_order_num'],
                'customer_code' => $post['customer_code'],
                'customer_name' => $post['customer_name'],
                'approval' => $post['approval'],
            ];

            $this->sql->insert_table('tbl_work_order_approval', $form_data);
            $message = '<div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            <div class="alert-message">
                                <strong>Success!</strong> Data has been updated!
                            </div>
                        </div>';
            $this->session->set_flashdata('msg', $message);
            redirect('admin/work_order');

        } else {
            $message = '<div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            <div class="alert-message">
                                <strong>Error!</strong> Error!
                            </div>
                        </div>';
            $this->session->set_flashdata('msg', $message);
            redirect('admin/work_order');
        }
    }
	// public function status()
 //    {
 //        if ($this->input->post()) {
 //            $id = $this->input->post('id');
 //            $data['get'] = $this->sql->select_table('tbl_work_order', ['work_order_id' => $id])->row_array();
 //            // $data['fg'] = $this->sql->select_table('tbl_fg')->result_array();
	// 		$sql = "SELECT * from tbl_material where inventory_part_type = 'Work in Process' or inventory_part_type = 'Finished Goods' order by material_no";

	//         $data['fg'] = $this->sql->manual_query($sql)->result_array();
 //            // $data['customer'] = $this->sql->select_table('tbl_customer', ['customer_id' => $data['get']['customer_id']])->result_array();

	// 		// FG data 
	// 		$fg_in = [];
	// 		$data_qty = [];
	// 		$data['data_wo'] = $this->sql->select_table('tbl_work_order_detail', ['work_order_id' => $id], 'fg_id')->result_array();

	// 		foreach($data['data_wo'] as $item):
	// 			array_push($fg_in, $item['fg_id']);
	// 			array_push($data_qty, $item['qty']);
	// 		endforeach;

	// 		$data['qty'] = $data_qty;
 //            $data['detail'] = $this->sql->select_table_in('tbl_material', 'material_no', $fg_in)->result_array();

 //            $this->load->view('work_order/status', $data);
 //        } else {
 //            echo "error";
 //        }
 //    }

 //    public function delete()
 //    { 
 //        if ($this->input->post()) {
 //            $id = $this->input->post('id');
 //            $where = ['work_order_id' => $id];

 //            $this->sql->delete_table('tbl_work_order', $where);
 //            $this->sql->delete_table('tbl_work_order_detail', $where);
	// 		log_helper("delete", "menghapus work order");
 //            echo "ok";
 //        } else {
 //            echo "error";
 //        }
 //    }

	// middleware
	public function middleware()
	{
		if(!$this->session->name){
			redirect(base_url());
		}
	}

	// Print
	public function print($id)
	{
		if ($id) {
            $where['work_order_id'] = $id;
            $data['wo'] = $this->sql->select_table('tbl_work_order', $where)->row_array();
            $data['detail'] = $this->sql->select_table('tbl_work_order_detail', $where)->result();

            //load library
            // $this->load->library('zend');
            //load in folder Zend
            // $this->zend->load('Zend/Barcode');

            $this->load->library('pdf');
            $this->pdf->setPaper('A4', 'potrait');
            $this->pdf->filename = "Work Order.pdf";
            $this->pdf->load_view('work_order/print', $data);
			log_helper("print", "memprint work order");
        } else {
            echo "error";
        }
	}
}
