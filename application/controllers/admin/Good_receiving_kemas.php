<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Good_receiving_kemas extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model("sql");
		$this->middleware();

    }

    public function index()
    {
        // $data['get'] = $this->sql->good_receiving([
        //     'a.status_receiving' => 'done'
        // ], 'a.created_at', 'desc')->result_array();
        // $data['material'] = $this->sql->select_table('tbl_material', null, 'nama')->result_array();
        // $data['supplier'] = $this->sql->select_table('tbl_supplier')->result_array();
        $data['api'] = $this->get_api();
        $data['qc'] = $this->sql->select_table('tbl_grk_qc')->result();
        $data['site_title'] = "Good Receiving";
        $data['subview'] = "good_receiving_kemas/data";
        $this->load->view('index', $data);
		$this->middleware();
    }

    public function get_api()
    {
        $url = "http://api.skinsolution.co.id/packaging_receipt";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);

        $return = json_decode($resp);
        return $return;
    }

    public function get_api_packaging()
    {
        $url = "http://api.skinsolution.co.id/packaging";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);

        $return = json_decode($resp);
        return $return;
    }


    public function get_api_detail($id)
    {
        $url = "http://api.skinsolution.co.id/packaging_receipt/".$id;

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);

        $return = json_decode($resp);
        return $return;
    }


    public function detail()
    {
        if ($this->input->post()) {
            $id = $this->input->post('id');
            $data['get'] = $this->get_api_detail($id);
            $data['pkg'] = $this->get_api_packaging();
            $this->load->view('good_receiving_kemas/detail', $data);
        } else {
            echo "error";
        }
    }

    public function status()
    {
        if ($this->input->post()) {
            $id = $this->input->post('id');
            $data['get'] = $this->get_api_detail($id);
            $this->load->view('good_receiving_kemas/status', $data);
        } else {
            echo "error";
        }
    }

    public function update_status()
    {
        if($this->input->post()){
            $post = $this->input->post();

            $form_data = [
                'id' => $post['id'],
                'rc_id' => $post['rc_id'],
                'pkg_type' => $post['pkg_type'],
                'supplier_code' => $post['supplier_code'],
                'status' => $post['status'],
                'desc' => $post['status'] == 'ok' ? 'OK' : $post['desc'],
            ];

            $this->sql->insert_table('tbl_grk_qc', $form_data);
            $message = '<div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            <div class="alert-message">
                                <strong>Success!</strong> Data has been updated!
                            </div>
                        </div>';
            $this->session->set_flashdata('msg', $message);
            redirect('admin/good_receiving_kemas');

        } else {
            $message = '<div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            <div class="alert-message">
                                <strong>Error!</strong> Error!
                            </div>
                        </div>';
            $this->session->set_flashdata('msg', $message);
            redirect('admin/good_receiving_kemas');
        }
    }

    public function print($id)
    {
        if ($id) {
            $data['api'] = $this->get_api_detail($id);
            $data['pkg'] = $this->get_api_packaging();

            $this->load->library('ciqrcode'); //pemanggilan library QR CODE
     
            $config['cacheable']    = true; //boolean, the default is true
            $config['cachedir']     = './assets/'; //string, the default is application/cache/
            $config['errorlog']     = './assets/'; //string, the default is application/logs/
            $config['imagedir']     = './assets/barcode/kemas/'; //direktori penyimpanan qr code
            $config['quality']      = true; //boolean, the default is true
            $config['size']         = '1024'; //interger, the default is 1024
            $config['black']        = array(224,255,255); // array, default is array(255,255,255)
            $config['white']        = array(70,130,180); // array, default is array(0,0,0)
            $this->ciqrcode->initialize($config);
     
            $image_name=$id.'.png'; //buat name dari qr code sesuai dengan nim
     
            $params['data'] = $id; //data yang akan di jadikan QR CODE
            $params['level'] = 'H'; //H=High
            $params['size'] = 10;
            $params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
            $this->ciqrcode->generate($params); // fungsi untuk generate QR COD

            // // //load library
            // $this->load->library('zend');
            // //load in folder Zend
            // $this->zend->load('Zend/Barcode');

            // $this->load->library('pdf');
            // $this->pdf->setPaper('A6', 'landscape');
            // $this->pdf->filename = "Code GR.pdf";
            // $this->pdf->load_view('good_receiving_kemas/print_qr', $data);

            // Mpdf
            $mpdf = new \Mpdf\Mpdf([
                'mode' => 'utf-8',
                'format' => 'A6',
                'orientation' => 'L',
                'margin_top' => 0,
                'margin_left' => 5,
                'margin_right' => 5,
            ]);
            $mpdf->setAutoTopMargin = 'stretch';
            // foreach($data['api'] as $row){
                // $data['row'] = $row;
                $page = $this->load->view('good_receiving_kemas/print_qr', $data, true);

                $mpdf->addPage();
                $mpdf->WriteHTML($page);
            // }

		    $mpdf->Output();
        } else {
            echo "error";
        }
    }

	// middleware
	public function middleware()
	{
		if(!$this->session->name){
			redirect(base_url());
		}
	}
}
