<?php 
        
defined('BASEPATH') OR exit('No direct script access allowed');
        
class API extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Sql');
    }
    
    public function form_sales_order()
    {
        $this->load->view('good_receiving/form_input_so');
    }

    public function submit_so()
    {
        $form_data = [
            'sales_order_no' => $_POST['sales_order_no'],
            'sales_order_date' => $_POST['sales_order_date'],
            'ship_date' => $_POST['ship_date'],
        ];

        $id = $this->Sql->insert_table('tbl_sales_order', $form_data);

        $message = '<div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>Success</strong> New data has been created
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>';
        $this->session->set_flashdata('msg', $message);
        redirect('admin/API/form_sales_order');
    }

    public function get_work_order()
    {
        // Init data
        $data = [];

        // get all work order
        $work_order = $this->Sql->select_table('tbl_work_order', null, 'created_at')->result();
        
        // foreach work order
        foreach($work_order as $wo){
            $sql = "SELECT detail.*, fg.* from tbl_work_order_detail detail inner join tbl_material fg on fg.material_id = detail.fg_id where work_order_id = '$wo->work_order_id'";
            
            $detail_wo = $this->Sql->manual_query($sql)->result();

            $single_data = [
                'work_order' => [$wo],
                'work_order_detail' => $detail_wo
            ];

            array_push($data, $single_data);
        }

        $return = [
			'status' => 'success',
			'message' => 'data fetch successfully',
			'data' => $data,
		];

		$json = $this->output
			->set_content_type('application/json')
			->set_output(json_encode($return));

		return $json;
    }

    public function get_receiving()
    {
        // Init data
        $data = [];

        // get all work order
        $receiving = $this->Sql->select_table('tbl_gr', [
            'status_receiving' => 'done'
        ], 'created_at')->result();
        
        // foreach work order
        foreach($receiving as $gr){
            $sql = "SELECT detail.*, fg.* from tbl_gr_detail detail inner join tbl_material fg on fg.material_id = detail.material_id where gr_id = '$gr->gr_id'";
            
            $detail_gr = $this->Sql->manual_query($sql)->result();

            $single_data = [
                'work_order' => [$gr],
                'work_order_detail' => $detail_gr
            ];

            array_push($data, $single_data);
        }

        $return = [
			'status' => 'success',
			'message' => 'data fetch successfully',
			'data' => $data,
		];

		$json = $this->output
			->set_content_type('application/json')
			->set_output(json_encode($return));

		return $json;
    }
    
    public function get_purchase_order()
    {
        // Init data
        $data = [];

        // get all work order
        $receiving = $this->Sql->select_table('tbl_gr', [
            'status_receiving' => 'on progress'
        ], 'created_at')->result();
        
        // foreach work order
        foreach($receiving as $gr){
            $sql = "SELECT detail.*, fg.* from tbl_gr_detail detail inner join tbl_material fg on fg.material_id = detail.material_id where gr_id = '$gr->gr_id'";
            
            $detail_gr = $this->Sql->manual_query($sql)->result();

            $single_data = [
                'purchase_order' => [$gr],
                'purchase_order_detail' => $detail_gr
            ];

            array_push($data, $single_data);
        }

        $return = [
			'status' => 'success',
			'message' => 'data fetch successfully',
			'data' => $data,
		];

		$json = $this->output
			->set_content_type('application/json')
			->set_output(json_encode($return));

		return $json;
    }

    public function get_sales_order()
    {
        // Init data
        $data = [];

        // get all sales order
        $sales_order = $this->Sql->select_table('tbl_sales_order', [
            // 'status_receiving' => 'on progress'
        ], 'created_at')->result();
        
        // foreach sales order
        foreach($sales_order as $gr){
            $sql = "SELECT detail.*, fg.* from tbl_sales_order_detail detail inner join tbl_material fg on fg.material_id = detail.fg_id where so_id = '$gr->id'";
            
            $detail_gr = $this->Sql->manual_query($sql)->result();

            $single_data = [
                'sales_order' => [$gr],
                'sales_order_detail' => $detail_gr
            ];

            array_push($data, $single_data);
        }

        $return = [
			'status' => 'success',
			'message' => 'data fetch successfully',
			'data' => $data,
		];

		$json = $this->output
			->set_content_type('application/json')
			->set_output(json_encode($return));

		return $json;
    }

    public function detail_sales_order($so_id)
    {
        // $data['get'] = $this->Sql->select_table('tbl_gr', ['gr_id' => $so_id])->row_array();
        $data['detail'] = $this->Sql->gr_detail_list(['gr_id' => $so_id])->result_array();
        $data['get'] = $this->Sql->inner_join('a.*, b.nama' ,'tbl_gr a','tbl_supplier b', 'a.supplier_id','b.supplier_id', ['gr_id' => $so_id])->row_array();
        $data['box'] = $this->Sql->material_box_list(['c.gr_id' => $so_id])->result_array();

        $return = [
			'status' => 'success',
			'message' => 'data fetch successfully',
			'data' => $data,
		];

		$json = $this->output
			->set_content_type('application/json')
			->set_output(json_encode($return));

		return $json;
    }
}

/* End of file API.php and path /application/controllers/admin/API.php */