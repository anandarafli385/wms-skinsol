<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Production extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model(array("sql",'m_production'));
		$this->middleware();
    }

    public function index()
    {

		$this->load->database();
		$this->load->library('pagination');

		$jumlah_data = $this->m_production->jumlah_data();
		$config['total_rows'] = $jumlah_data;
		$config['per_page'] = 10;
		$from = $this->uri->segment(3);
		$this->pagination->initialize($config);		
		$data['production'] = $this->m_production->data();
		$data['replenishment'] =  $this->m_production->getReplenishment();
		// $data['log'] = $this->m_production->select_table();
		$data['subview'] = "production/data";
        $data['site_title'] = "Production";
        $this->load->view('index', $data);
    }

	public function add()
	{
		if ($this->input->post()) {
            $post = $this->input->post();


			$data = [
				'replenishment_id' => $_POST['replenishment_id'],
				'qty'=> $_POST['qty'],
				'date'=> $_POST['date'],
				'status'=> 'No Started',
				'created_by' => $this->session->userdata("user_id"),
			];

			$check_data = $this->sql->select_table('tbl_productions',['replenishment_id' => $_POST['replenishment_id']])->result_array();

			$this->sql->insert_table('tbl_productions', $data);
			

			$ids = $this->db->insert_id();
			$datas = [
				'production_id' => $ids,
				'qty'=> 0,
				'status'=> 'No Started',
				'created_by' => $this->session->userdata("user_id"),
			];

			$this->sql->insert_table('tbl_production_logs', $datas);

            $message = '<div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            <div class="alert-message">
                                <strong>Success!</strong> Data has been inserted!
                            </div>
                        </div>';
            $this->session->set_flashdata('msg', $message);
            redirect('admin/production');
        }else{
			$message = '<div class="alert alert-success alert-dismissible" role="alert">
				<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
				<div class="alert-message">
					<strong>Success!</strong> Error!
				</div>
			</div>';
			$this->session->set_flashdata('msg', $message);
			redirect('admin/production');
        }
	}

	public function detail_product($id){
		$data['repl'] =  $this->m_production->getReplenishment();
		$data['prods'] = $this->sql->select_table_join('tbl_productions', 'tbl_productions.*,tbl_replenishment.wo_formula AS replenishment,tbl_replenishment.wo_pack AS qty_replenishment,tbl_replenishment.work_order_batch AS wo_num', 'tbl_replenishment', 'tbl_replenishment.replenishment_id = tbl_productions.replenishment_id', 'left', $where = array('id' => $id), $order_by = null, $order = 'asc')->row_array();
		$data['log'] = $this->sql->select_table('tbl_production_logs', ['production_id' => $id])->result_array();
		$query = $this->db->query("SELECT sum(qty) AS `total` FROM `tbl_production_logs` WHERE `production_id` = ".$id." ORDER BY id DESC LIMIT 1");
		$persentase = $query->result();
		$json = [
			'replenishment' => $data['repl'],
			'production' => $data['prods'],
			'logs' => $data['log'] ,
			'persentase' => $persentase
		];
		
		$val = $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($json));
		return $val;
	}

	public function detail($id)
	{
		$data['replenishment'] =  $this->m_production->getReplenishment();
		$data['production'] = $this->sql->select_table_join('tbl_productions', 'tbl_productions.*,tbl_replenishment.wo_formula AS replenishment,tbl_replenishment.wo_pack AS qty_replenishment,tbl_replenishment.work_order_batch AS wo_num', 'tbl_replenishment', 'tbl_replenishment.replenishment_id = tbl_productions.replenishment_id', 'left', $where = array('id' => $id), $order_by = null, $order = 'asc')->row_array();
		$data['logs'] = $this->sql->select_table('tbl_production_logs', ['production_id' => $id])->result_array();
		$this->load->view('production/detail', $data);
	}

	public function get($id)
	{
		$data['replenishment'] =  $this->m_production->getReplenishment();
		$data['production'] = $this->sql->select_table('tbl_productions', ['id' => $id])->row_array();
		$this->load->view('production/get', $data);
	}

	public function edit()
    {
        if($this->input->post()){
            $post = $this->input->post();

			$data = [
				'replenishment_id' => $_POST['replenishment_id'],
				'qty'=> $_POST['qty'],
				'date'=> $_POST['date'],
				'status'=> $_POST['status'],
				'updated_by' => $this->session->userdata("user_id"),
			];

            $this->sql->update_table('tbl_productions', $data, array('id' => $post['id']));
            $message = '<div class="alert alert-success alert-dismissible" role="alert">
						<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
						<div class="alert-message">
							<strong>Success!</strong> Data has been inserted!
						</div>
					</div>';
			} else {
			$message = '<div class="alert alert-danger alert-dismissible" role="alert">
					<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
					<div class="alert-message">
						<strong>Error!</strong> Error!
					</div>
				</div>';
        }

		$this->session->set_flashdata('msg', $message);
		redirect('admin/production');
    }

	public function delete()
	{
		if ($this->input->post()) {
            $id = $this->input->post('id');
            $this->sql->delete_table('tbl_productions', array('id' => $id));
            $this->sql->delete_table('tbl_production_logs', array('production_id' => $id));
            echo "ok";
        } else {
            echo "error";
        }
	}

	public function add_log()
	{
		if ($this->input->post()) {
            $post = $this->input->post();

			$production = $this->sql->select_table('tbl_productions', ['id' => $_POST['production_id']])->row_array();
			$query = $this->db->query("SELECT sum(qty) AS `total` FROM `tbl_production_logs` WHERE `production_id` = ".$_POST['production_id']." ORDER BY id DESC LIMIT 1");
			$persentase = $query->result();
			$compare = intval($_POST['qty']) + intval($persentase[0]->total);

			if(intval($production['qty']) - $compare == 0){
				$data = [
					'production_id' => $_POST['production_id'],
					'qty'=> $_POST['qty'],
					'status'=> 'Done',
					'created_by' => $this->session->userdata("user_id"),
				];

				$update_status = [
					'status' => 'Done'
				];	
				$this->sql->update_table('tbl_productions',$update_status,array('id' => $_POST['production_id']));

			}else{
				$data = [
					'production_id' => $_POST['production_id'],
					'qty'=> $_POST['qty'],
					'status'=> $_POST['status'],
					'created_by' => $this->session->userdata("user_id"),
				];

				$update_status = [
					'status' => $_POST['status']
				];	
				$this->sql->update_table('tbl_productions',$update_status,array('id' => $_POST['production_id']));

			}
			$this->sql->insert_table('tbl_production_logs', $data);
			

            $message = '<div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                            <div class="alert-message">
                                <strong>Success!</strong> Data has been inserted!
                            </div>
                        </div>';
            $this->session->set_flashdata('msg', $message);
            redirect('admin/production');
        }else{
			$message = '<div class="alert alert-success alert-dismissible" role="alert">
				<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
				<div class="alert-message">
					<strong>Success!</strong> Error!
				</div>
			</div>';
			$this->session->set_flashdata('msg', $message);
			redirect('admin/production');
        }
	}

	public function middleware()
	{
		if(!$this->session->name){
			redirect(base_url());
		}
	}
}
