<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_user extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model("sql");
		$this->middleware();
    }

    public function index()
    {
        $data['get'] = $this->sql->select_table('tbl_user')->result_array();
        $data['site_title'] = "Master User";
        $data['subview'] = "master_user/data";
        $this->load->view('index', $data);
    }

    public function add()
    {
        if ($this->input->post()) {
            $post = $this->input->post();

            $check = $this->sql->select_table('tbl_user', ['username' => $post['username']])->num_rows();
            if ($check > 0) {
                $message = '<div class="alert alert-danger alert-dismissible" role="alert">
							<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
							<div class="alert-message">
								<strong>Error!</strong> Username sudah digunakan!
							</div>
						</div>';
                $this->session->set_flashdata('msg', $message);
                redirect('admin/master_user');
            }

            $form_data = [
                'name' => $post['name'],
                'username' => $post['username'],
                'password' => password_hash($post['password'], PASSWORD_DEFAULT),
                'level' => $post['level']
            ];

            $id = $this->sql->insert_table('tbl_user', $form_data);

            $message = '<div class="alert alert-success alert-dismissible" role="alert">
							<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
							<div class="alert-message">
								<strong>Success!</strong> New user has been registered!
							</div>
						</div>';
            $this->session->set_flashdata('msg', $message);
            redirect('admin/master_user');
        } else {
            $message = '<div class="alert alert-danger alert-dismissible" role="alert">
							<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
							<div class="alert-message">
								<strong>Error!</strong> Error!
							</div>
						</div>';
            $this->session->set_flashdata('msg', $message);
            redirect('admin/master_user');
        }
    }

    public function get()
    {
        if ($this->input->post()) {
            $id = $this->input->post('id');
            $data['get'] = $this->sql->select_table('tbl_user', ['user_id' => $id])->row_array();

            $this->load->view('master_user/get', $data);
        } else {
            echo "error";
        }
    }

    public function edit($id)
    {
        if ($this->input->post()) {
            $post = $this->input->post();

            $form_data = [
                'name' => $post['name'],
                'username' => $post['username'],
                'password' => password_hash($post['password'], PASSWORD_DEFAULT),
                'level' => $post['level']
            ];

            $where = ['user_id' => $id];

            $id = $this->sql->update_table('tbl_user', $form_data, $where);

            $message = '<div class="alert alert-success alert-dismissible" role="alert">
							<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
							<div class="alert-message">
								<strong>Success!</strong> Data has been updated!
							</div>
						</div>';
            $this->session->set_flashdata('msg', $message);
            redirect('admin/master_user');
        } else {
            $message = '<div class="alert alert-danger alert-dismissible" role="alert">
							<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
							<div class="alert-message">
								<strong>Error!</strong> Error!
							</div>
						</div>';
            $this->session->set_flashdata('msg', $message);
            redirect('admin/master_user');
        }
    }

    public function delete()
    {
        if($this->input->post()){
            $id = $this->input->post('id');
            $where = ['user_id' => $id];
            
            $this->sql->delete_table('tbl_user', $where);
            echo "ok";
        }else{
            echo "error";
        }
    }

	// middleware
	public function middleware()
	{
		if(!$this->session->name){
			redirect(base_url());
		}
	}
}
