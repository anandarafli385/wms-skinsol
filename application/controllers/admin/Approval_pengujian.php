	<?php

use phpDocumentor\Reflection\Types\This;

defined('BASEPATH') or exit('No direct script access allowed');

class Approval_pengujian extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model("sql");
		// $this->middleware();
		
    }

    public function index()
    {
		// $sql = "SELECT * from tbl_material where inventory_part_type = 'Work in Process' or inventory_part_type = 'Finished Goods' order by material_no";

        // $data['get'] = $this->sql->work_order()->result_array();
        $data['get'] = $this->sql->approval_pengujian()->result_array();
        // $data['fg'] = $this->sql->manual_query($sql)->result_array();
        // $data['customer'] = $this->sql->select_table('tbl_customer')->result_array();
        $data['site_title'] = "Approval Pengujian";
        $data['subview'] = "approval_pengujian/data";
        $this->load->view('index', $data);
		$this->middleware();
    }

    public function add()
    {
        if ($this->input->post()) {
            $post = $this->input->post();

            $form_data = [
				'no_dokumen' => $post['no_dokumen'],
				'departement' => $post['departement'],
				'merk' => $post['merk'],
				'kemasan' => $post['kemasan'],
				'nama_produk' => $post['nama_produk'],
				'netto' => $post['netto'],
				'tgl_kedatangan_sample' => $post['tgl_kedatangan_sample'],
				'no_analisa' => $post['no_analisa'],
				'no_batch' => $post['no_batch'],
				'tgl_pengujian' => $post['tgl_pengujian'],
				'jml_sample' => $post['jml_sample'],
				'tgl_selesai' => $post['tgl_selesai'],
				'pengujian' => $post['pengujian'],
				'spesifikasi_1' => $post['spesifikasi_1'],
				'spesifikasi_2' => $post['spesifikasi_2'],
				'spesifikasi_3' => $post['spesifikasi_3'],
				'spesifikasi_4' => $post['spesifikasi_4'],
				'spesifikasi_5' => $post['spesifikasi_5'],
				'syarat_1' => $post['syarat_1'],
				'syarat_2' => $post['syarat_2'],
				'syarat_3' => $post['syarat_3'],
				'syarat_4' => $post['syarat_4'],
				'syarat_5' => $post['syarat_5'],
				'hasil_1' => $post['hasil_1'],
				'hasil_2' => $post['hasil_2'],
				'hasil_3' => $post['hasil_3'],
				'hasil_4' => $post['hasil_4'],
				'hasil_5' => $post['hasil_5'],
				'uji_homogenitas' => $post['uji_homogenitas'],
				'homogen' => $post['homogen'],
				'hasil_homogenitas' => $post['hasil_homogenitas'],
				'syarat_kebocoran' => $post['syarat_kebocoran'],
				'hasil_kebocoran' => $post['hasil_kebocoran'],
				'diuji_oleh' => $post['diuji_oleh'],
				'disposisi' => $post['disposisi'],
				'diperiksa_oleh' => $post['diperiksa_oleh'],
                // 'user_id' => $this->session->userdata('user_id')
            ];

			// Validate no WO
			$data_dokumen = $this->sql->select_table('tbl_approval_pengujian', [
				'no_dokumen' => $post['no_dokumen'],
			])->row();

			if($data_dokumen){
				$message = '<div class="alert alert-danger alert-dismissible" role="alert">
								<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
								<div class="alert-message">
									<strong>Validation Error!</strong> Work Order number already exist, please specific a different number!
								</div>
							</div>';
				$this->session->set_flashdata('msg', $message);
				redirect('admin/approval_pengujian');
			}
			else {
				
				$id = $this->sql->insert_table('tbl_approval_pengujian', $form_data);
				foreach ($post['fg_id'] as $key => $value) {
					$form_data = [
						'approval_pengujian_id' => $id,
						// 'fg_id' => $post['fg_id'][$key],
						// 'qty' => $post['qty'][$key]
					];
	
					$id_detail = $this->sql->insert_table('tbl_approval_pengujian_detail', $form_data);
	
					// Get Material from FG
					// $materials = $this->sql->select_table('tbl_fg_material', [
					// 	'fg_id' =>  $post['fg_id'][$key]
					// ])->result_array();
	
					// foreach($materials as $material){
					// 	$stok_kurang = $material['qty'] * $post['qty'][$key];
	
						// Current Stok
						// $curr_stok = $this->sql->select_table('tbl_stock', [
						// 	'material_id' =>  $material['material_id']
						// ])->row_array();
	
						// Update stok
					// 	$update_stok = $this->sql->update_table('tbl_stock', [
					// 		'stok' => $curr_stok['stok'] - $stok_kurang
					// 	], [
					// 		'material_id' =>  $material['material_id']
					// 	]);
					// }
				}
	
				$message = '<div class="alert alert-success alert-dismissible" role="alert">
								<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
								<div class="alert-message">
									<strong>Success!</strong> New data has been created!
								</div>
							</div>';
				$this->session->set_flashdata('msg', $message);
				log_pengujian_helper("add", "menambahkan berkas pengujian");
				redirect('admin/approval_pengujian');
			}


        } else {
            $message = '<div class="alert alert-danger alert-dismissible" role="alert">
							<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
							<div class="alert-message">
								<strong>Error!</strong> Error!
							</div>
						</div>';
            $this->session->set_flashdata('msg', $message);
            redirect('admin/approval_pengujian');
        }
    }

    public function detail()
    {
        if ($this->input->post()) {
            $id = $this->input->post('id');
            $data['get'] = $this->sql->select_table('tbl_approval_pengujian', ['approval_pengujian_id' => $id])->row_array();
            // $data['fg'] = $this->sql->select_table('tbl_fg')->result_array();
            // $sql = "SELECT * from tbl_material where inventory_part_type = 'Work in Process' or inventory_part_type = 'Finished Goods' order by material_no";

	        // $data['fg'] = $this->sql->manual_query($sql)->result_array();
			// $data['customer'] = $this->sql->select_table('tbl_customer', ['customer_id' => $data['get']['customer_id']])->result_array();

			// FG data 
			// $fg_in = [];
			// $wo_detail_in = [];
			// $data_qty = [];
			// $data_wo = $this->sql->select_table('tbl_work_order_detail', ['work_order_id' => $id], 'fg_id')->result_array();

			// foreach($data_wo as $item):
			// 	array_push($fg_in, $item['fg_id']);
			// 	array_push($data_qty, $item['qty']);
			// 	array_push($wo_detail_in, $item['work_order_detail_id']);
			// endforeach;

			// $data['qty'] = $data_qty;
   //          $data['detail'] = $this->sql->select_table_in('tbl_material', 'material_no', $fg_in)->result_array();
			// $data['delivery'] = $this->sql->select_table_in('tbl_work_order_delivery', 'work_order_detail_id', $wo_detail_in, 'created_at')->result();

            $this->load->view('approval_pengujian/detail', $data);
        } else {
            echo "error";
        }
    }

	public function detail_approved()
    {
        if ($this->input->post()) {
            $id = $this->input->post('id');
            $data['get'] = $this->sql->select_table('tbl_approval_pengujian', ['approval_pengujian_id' => $id])->row_array();

            // $data['fg'] = $this->sql->select_table('tbl_fg')->result_array();
			// $sql = "SELECT * from tbl_material where inventory_part_type = 'Work in Process' or inventory_part_type = 'Finished Goods' order by material_no";

	  //       $data['fg'] = $this->sql->manual_query($sql)->result_array();
   //          // $data['customer'] = $this->sql->select_table('tbl_customer', ['customer_id' => $data['get']['customer_id']])->result_array();

			// // FG data 
			// $fg_in = [];
			// $data_qty = [];
			// $data_good = [];
			// $data_reject = [];
			// $wo_detail_in = [];

			// $data_wo = $this->sql->select_table('tbl_work_order_detail', ['work_order_id' => $id], 'fg_id')->result_array();

			// foreach($data_wo as $item):
			// 	array_push($fg_in, $item['fg_id']);
			// 	array_push($data_qty, $item['qty']);
			// 	array_push($data_good, $item['good']);
			// 	array_push($data_reject, $item['reject']);
			// 	array_push($wo_detail_in, $item['work_order_detail_id']);
			// endforeach;

			// $data['qty'] = $data_qty;
			// $data['good'] = $data_good;
			// $data['reject'] = $data_reject;

			// $data['delivery'] = $this->sql->select_table_in('tbl_work_order_delivery', 'work_order_detail_id', $wo_detail_in, 'created_at')->result();
   //          $data['detail'] = $this->sql->select_table_in('tbl_material', 'material_no', $fg_in)->result_array();

            $this->load->view('approval_pengujian/detail_approved', $data);
        } else {
            echo "error";
        }
    }

    public function get()
    {
        if ($this->input->post()) {
            $id = $this->input->post('id');
            $data['get'] = $this->sql->select_table('tbl_approval_pengujian', ['approval_pengujian_id' => $id])->row_array();
            // $data['fg'] = $this->sql->select_table('tbl_fg')->result_array();
			// $sql = "SELECT * from tbl_material where inventory_part_type = 'Work in Process' or inventory_part_type = 'Finished Goods' order by material_no";

	        // $data['fg'] = $this->sql->manual_query($sql)->result_array();
            // $data['customer'] = $this->sql->select_table('tbl_customer')->result_array();
            // $data['detail'] = $this->sql->select_table('tbl_approval_pengujian_detail', ['work_order_id' => $id], 'fg_id')->result_array();

            $this->load->view('approval_pengujian/get', $data);
        } else {
            echo "error";
        }
    }

	public function edit_approval($id)
    {
        if ($this->input->post()) {
            $post = $this->input->post();

			if($post['status_approval'] == "approved"){
				//Update status_approval Work Order Approval
				$this->sql->update_table('tbl_work_order', [
					'status_approval' => $post['status_approval'],
					'approved_by' => $this->session->name,
					'approved_at' => date('Y-m-d H:i:s')
				], [
					'work_order_id' => $id
				]);
				log_helper("edit", "mengubah status approval work order menjadi approved");
			}		

			// Update work order Approval_detail
			foreach($post['work_order_detail_id'] as $index => $work_order_detail_id){
				
				if($post['good'][$index] !== 0 && $post['reject'][$index] !== 0){
					$this->sql->insert_table('tbl_work_order_delivery', [
						'work_order_detail_id' => $work_order_detail_id,
						'reject' => $post['reject'][$index],
						'good' => $post['good'][$index]
					]);	
				}else{
					continue;
				}
			}

            print_r($post);

            $message = '<div class="alert alert-success alert-dismissible" role="alert">
							<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
							<div class="alert-message">
								<strong>Success!</strong> Status Approval has been updated!
							</div>
						</div>';
            $this->session->set_flashdata('msg', $message);
            redirect('admin/approval');
        } else  {
            $message = '<div class="alert alert-danger alert-dismissible" role="alert">
							<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
							<div class="alert-message">
								<strong>Error!</strong> Error!
							</div>
						</div>';
            $this->session->set_flashdata('msg', $message);
            redirect('admin/approval');
        }
    }

    public function edit($id)
    {
        if ($this->input->post()) {
            $post = $this->input->post();

            $form_data = [
                'no_dokumen' => $post['no_dokumen'],
                'tgl_pengujian' => $post['tgl_pengujian'],
                'disposisi' => $post['disposisi'],
                'diperiksa_oleh' => $post['diperiksa_oleh'],
                // 'user_id' => $this->session->userdata('user_id')
            ];

            $where = ['approval_pengujian_id' => $id];

            $this->sql->update_table('tbl_approval_pengujian', $form_data, $where);
			log_pengujian_helper("edit", "mengubah approval pengujian");

            $message = '<div class="alert alert-success alert-dismissible" role="alert">
							<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
							<div class="alert-message">
								<strong>Success!</strong> Data has been updated!
							</div>
						</div>';
            $this->session->set_flashdata('msg', $message);
            redirect('admin/approval_pengujian');
        } else {
            $message = '<div class="alert alert-danger alert-dismissible" role="alert">
							<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
							<div class="alert-message">
								<strong>Error!</strong> Error!
							</div>
						</div>';
            $this->session->set_flashdata('msg', $message);
            redirect('admin/approval_pengujian');
        }
    }


    public function delete()
    { 
        if ($this->input->post()) {
            $id = $this->input->post('id');
            $where = ['approval_pengujian_id' => $id];

            $this->sql->delete_table('tbl_approval_pengujian', $where);
			log_helper("delete", "menghapus approval pengujian");
            echo "ok";
        } else {
            echo "error";
        }
    }

	// middleware
	public function middleware()
	{
		if(!$this->session->name){
			redirect(base_url());
		}
	}

	// Print
	public function print($id)
	{
		if ($id) {
            $where['approval_pengujian_id'] = $id;
            $data['get'] = $this->sql->select_table('tbl_approval_pengujian', $where)->row_array();
            // $data['detail'] = $this->sql->select_table('tbl_work_order_detail', $where)->result();

            //load library
            // $this->load->library('zend');
            //load in folder Zend
            // $this->zend->load('Zend/Barcode');

			// $imgpath=base_url('var/www/html/skinsol-wms/assets/src/img/logo-skinsol.png');
			// $ext= pathinfo($imgpath, PATHINFO_EXTENSION);
			// $img = file_get_contents($imgpath);
			// $base64 = 'data:image/' . $ext. ';base64,' . base64_encode($img);
			// $data['image']='<img src="'.$base64.'"/>';

			// $path = base_url('assets/src/img/logo-skinsol.png');
			// $type = pathinfo($path, PATHINFO_EXTENSION);
			// $img = file_get_contents($path);
			// $data['image'] = 'data:image/' . $type . ';base64,' . base64_encode($img);

            $this->load->library('pdf');
            $this->pdf->setPaper('A4', 'potrait');
            $this->pdf->filename = "Approval Pengujian.pdf";
            $this->pdf->load_view('approval_pengujian/print', $data);
			// log_helper("print", "memprint approval pengujian");
        } else {
            echo "error";
        }
	}

	// Label Timbang
	public function scale_label($id)
	{
		// Data Work Order Approval
		$data['wo'] = $this->sql->select_table('tbl_work_order',[
			'work_order_id' => $id
		])->row();
		
		// Detail Work Order Approval (fetching contained FG_ID)
		$data['wo_detail'] = $this->sql->select_table('tbl_work_order_detail',[
			'work_order_id' => $id
		])->result();

		// Array to save material list in WO
		$materials_id = [];
		$get = [];
		$qty = [];
		// Foreach as much as FG contained
		foreach($data['wo_detail'] as $wo_detail){
			$qty_fg = $wo_detail->qty;
			array_push($qty, $wo_detail->qty);

			// Fetching material list from FG
			$materials = $this->sql->select_table('tbl_fg_material',[
				'fg_id' => $wo_detail->fg_id
			])->result();

			// Foreach as much as material in FG
			foreach($materials as $material){
				$qty_material = $material->qty;
				
				$detail_material = $this->sql->select_table('tbl_material',[
					'material_id' => $material->material_id
				])->row();

				if(!in_array($material->material_id, $materials_id)){
					// raw
					$raw = [
						'material_id' => $material->material_id,
						'nama' => $detail_material->nama,
						'deskripsi' => $detail_material->deskripsi,
						'kode_material' => $detail_material->kode_material,
						'harga' => $detail_material->harga,
						'unit' => $detail_material->unit,
						'qty' => $qty_material * $wo_detail->qty
					];
					
					// Push material
					array_push($materials_id, $material->material_id);
					array_push($get, $raw);
				}else{
					// Foreach to get material data from array get
					foreach($get as $index => $item){
						if($item['material_id'] == $material->material_id){
							$get[$index]['qty'] = $item['qty'] + ($qty_fg * $qty_material);
						}
					}
				}
			}
		}
		
		// print_r($get);
		$data['get'] = $get;

		//load library
		$this->load->library('zend');
		//load in folder Zend
		$this->zend->load('Zend/Barcode');

		$this->load->library('pdf');
		$this->pdf->setPaper('A8', 'landscape');
		$this->pdf->filename = "Scale Label.pdf";
		$this->pdf->load_view('approval_pengujian/scale_label', $data);
	}

	// API Cek Stock Material
	public function check_stock($fg_id)
	{
		$material = $this->db->select('material_id, qty');
		$material->where([
			'fg_id' => $fg_id
		]);
		$material = $material->get('tbl_fg_material')->result_array();

		// material id 
		$data_material = [];
		foreach($material as $data){
			$detail = $this->sql->select_table('tbl_material', [
				'material_no' => $data['material_id']
			])->row_array();
			
			$data['nama_material'] = $detail['nama'];
			$data['deskripsi'] = $detail['deskripsi'];
			$data['kode_material'] = $detail['kode_material'];
			$data['id'] = $detail['material_id'];
			


			// Jumlah Seluruh data digudang
			$query_gr = "SELECT sum(qty) as qty_masuk from tbl_material_box where material_id = '" . $data['material_id']. "'";

			$gr = $this->sql->manual_query($query_gr)->row_array();
			$data['jumlah_masuk'] = $gr['qty_masuk'];


			// Jumlah barang yang telah keluar
			$material_box_id = [];
			$boxex = $this->sql->select_table('tbl_material_box', [
				'material_id' => $data['material_id']
			])->result_array();

			foreach($boxex as $box){
				array_push($material_box_id, $box['material_box_id']);
			}

			// $out = $this->sql->sum_table_in('tbl_replenishment_detail', 'qty', 'material_box_id', $material_box_id)->row_array();

			$sisa = $this->sql->select_table('tbl_stock', [
				'material_id' => $data['id']
			])->row_array();

			// $data['jumlah_keluar'] = $out['qty'];
			// $data['jumlah_keluar'] = $sisa['stok'];

			// $data['sisa'] = $data['jumlah_masuk'] - $data['jumlah_keluar'];
			if($sisa != null){
				$data['sisa'] = $sisa['stok'];
			}else{
				$data['sisa'] = 0;
			}
			
			array_push($data_material, $data);
		}

		$return = [
			'status' => 'success',
			'message' => 'Data successfully fetch',
			'data' => $data_material
		];

		$val = $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($return));

		// return statement
		return $val;
		


	}

	// API get data WO
	public function get_work_order($wo_id)
	{
		$data_wo = $this->sql->select_table('tbl_work_order', [
			'work_order_id' => $wo_id
		])->row();

		$data_wo_detail = $this->sql->select_table('tbl_work_order_detail', [
			'work_order_id' => $wo_id
		])->result();

		$user_data = $this->sql->full_select('u.name','tbl_user u',[
			'user_id' => $data_wo->user_id
		])->row();

		$sql_material = "SELECT fg.nama, d.qty as qty_fg, m.material_id, m.nama as material, fgm.qty as material_each, sum(d.qty * fgm.qty) as material_needed, d.good, d.reject from tbl_material fg 
		inner join tbl_work_order_detail d on d.fg_id = fg.material_no 
		inner join tbl_fg_material fgm on fgm.fg_id = fg.material_no
		inner join tbl_material m on m.material_no = fgm.material_id where d.work_order_id = '$wo_id' group by material_id";

		$data_material = $this->sql->manual_query($sql_material)->result();
		
		$data_location = [];
		foreach($data_material as $material){
			$lokasi = $this->sql->full_select('box_code ,location, date_format(expired_date, "%d %b %Y") as expired_date', 'tbl_material_box', [
				'material_id' => $material->material_id
			], 'expired_date', 'asc')->result();

			array_push($data_location, $lokasi);
		}

		$data_return = [
			'data_wo_detail' => $data_wo_detail,
			'data_wo' => $data_wo,
			'data_user' => $user_data,
			'data_material' => $data_material,
			'data_location' => $data_location,
		];

		$return = [
			'status' => 'success',
			'message' => 'Data successfully fetch',
			'data' => $data_return
		];

		$val = $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($return));

		// return statement
		return $val;
	}
}
