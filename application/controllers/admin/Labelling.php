<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Labelling extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model("sql");
// 		$this->middleware();
    }

    public function index()
    {
        // $sql = "SELECT * from tbl_material where inventory_part_type = 'Work in Process' or inventory_part_type = 'Finished Goods' order by material_no";

        // $data['get'] = $this->sql->manual_query($sql)->result_array();
        $data['api'] = $this->get_api();
        $data['site_title'] = "Labelling Activity";
        $data['subview'] = "labelling/data";
        $this->load->view('index', $data);
    }

    public function get_api(){
        $url = "http://api.skinsolution.co.id/labeling";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);

        $return = json_decode($resp);
        return $return;
    }

    public function get_api_detail_labelling($id){
        $url = "http://api.skinsolution.co.id/labeling/".$id;

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);

        $return = json_decode($resp);
        return $return;
    }

    public function view()
    {
        if ($this->input->post()) {
            $id = $this->input->post('id');
            $data['api'] = $this->get_api_detail_labelling($id);
            $this->load->view('labelling/view', $data);
        } else {
            echo "error";
        }
    }

	// middleware
	public function middleware()
	{
		if(!$this->session->name){
			redirect(base_url());
		}
	}
}
