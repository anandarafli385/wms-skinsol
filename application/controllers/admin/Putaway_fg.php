<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Putaway_fg extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model("sql");
		$this->middleware();
    }

    public function index()
    {
		$sql_putaway_fg = "SELECT f.*,w.work_order_id as work_order, g.nama, u.name FROM tbl_putaway_fg f inner join tbl_material g on f.fg_id = g.material_no inner join tbl_user u on f.user_id = u.user_id inner join tbl_work_order w on f.work_order_id = w.work_order_id order by f.putaway_fg_date desc";

		$sql_wo = "SELECT w.* FROM tbl_work_order w where status = 'on progress' order by w.work_order_id desc";

        $data['site_title'] = "Results";
        $data['subview'] = "putaway_fg/data";
		$data['fg'] = $this->sql->select_table('tbl_fg')->result_array();
		$data['wo'] = $this->sql->select_table('tbl_work_order', [
		        'status' => 'on progress'
		    ], 'work_order_id', 'asc')->result();
		$data['putaway_fg'] = $this->sql->manual_query($sql_putaway_fg)->result_array();
		$data['location'] = $this->sql->select_table('tbl_location',[
            'category' => 'Finished Good'
        ], 'location_name')->result();

        $this->load->view('index', $data);
    }

	// Add
	public function add()
	{
		if($this->input->post()){
			$post = $this->input->post();

			// FG Data
			$fg = $this->sql->select_table('tbl_material', [
				'material_no' => $post['fg_id']
			])->row();

			// QR Code
			if($fg->inventory_part_type == 'Finished Goods'){
				$qr_code = strtoupper('FG-' . $fg->material_no . '-' . date('Ymd')) . str_pad(mt_rand(1, 9999), 8, '0', STR_PAD_LEFT);
			}else{
				$qr_code = strtoupper('WIP-' . $fg->material_no . '-' . date('Ymd')) . str_pad(mt_rand(1, 9999), 8, '0', STR_PAD_LEFT);
			}

			// Generate QR Code
			$this->sql->generate_qr($qr_code);

			$form_data = [
				'batch_no' => $post['batch_no'],
                'qc_no' => $post['qc_no'],
                'expired_date' => $post['expired_date'],
                'retest_date' => $post['retest_date'],
				'fg_id' => $post['fg_id'],
				'work_order_id' => $post['work_order_id'],
				'qty' => $post['qty'],
                'unit' => $fg->unit,
                'location' => $post['location'],
                'status' => 'warehouse',
				'qr_code' => $qr_code,
				'putaway_fg_date' => $post['fg_date'],
                'user_id' => $this->session->userdata('user_id')
            ];
            
            // Insert Putaway FG
            $putaway_id = $this->sql->insert_table('tbl_putaway_fg', $form_data);
            
            // Detail WO
            $detail_wo = $this->sql->select_table('tbl_work_order_detail', [
                    'fg_id' => $post['fg_id'],
				    'work_order_id' => $post['work_order_id'],
                ])->row();
                
            // Update qty in work order detail
            $this->sql->update_table('tbl_work_order_detail', [
                'good' => $detail_wo->good + $post['qty'],
            ],[
                'fg_id' => $post['fg_id'],
			    'work_order_id' => $post['work_order_id'],
            ]);

			// Get newest WO detail
			$detail_wo = $this->sql->select_table('tbl_work_order_detail', [
				'fg_id' => $post['fg_id'],
				'work_order_id' => $post['work_order_id'],
			])->row();

			// Jika deliver all fg
			if($detail_wo->qty <= $detail_wo->good){
				// Update status WO 
				$update_wo = $this->sql->update_table('tbl_work_order', [
					'status' => 'finished',
					'finished_by' => $this->session->userdata('user_id')
				],[
					'work_order_id' => $post['work_order_id'],
				]);
			}
                
            // Operation log
            $operation_log = $this->sql->insert_table('tbl_work_order_delivery', [
                'work_order_detail_id' => $detail_wo->work_order_detail_id,
                'good' => $post['qty'],
                'reject' => 0
            ]);
            
            // Cycle count
			$stok = $this->sql->select_table('tbl_stock', array('material_id' => $post['fg_id']));
			if($stok->num_rows() > 0){
				$stok = $stok->row_array();

				$data_stok = [
					'stok' => $stok['stok'] + $post['qty'],
				];

				print_r($data_stok);

				// Update stok
				$this->sql->update_table('tbl_stock', $data_stok, array('material_id' => $post['fg_id'])); 

				// Add History Stock
				$this->sql->insert_table('tbl_stock_history', [
					'material_id' => $post['fg_id'],
					'previous_qty' => $stok['stok'],
					'total' => $post['qty'],
					'current_qty' => $stok['stok'] + ($post['qty']),
					'created_by' => $this->session->userdata('user_id'),
				]);
			}else{
				$data_stok = [
					'stok' => ($post['qty']),
					'material_id' => $post['fg_id'],
					'type' => 'fg'
				];

				$this->sql->insert_table('tbl_stock', $data_stok);

				// Add History Stock
				$this->sql->insert_table('tbl_stock_history', [
					'material_id' => $post['fg_id'],
					'total' => $post['qty'],
					'current_qty' => ($post['qty']),
					'created_by' => $this->session->userdata('user_id'),
				]);
			}
            
			$message = '<div class="alert alert-success alert-dismissible" role="alert">
							<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
							<div class="alert-message">
								<strong>Success!</strong> New data has been created!
							</div>
						</div>';
            $this->session->set_flashdata('msg', $message);
            redirect('admin/putaway_fg');

		} else {
			$message = '<div class="alert alert-danger alert-dismissible" role="alert">
							<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
							<div class="alert-message">
								<strong>Error!</strong> Error!
							</div>
						</div>';
			$this->session->set_flashdata('msg', $message);
			redirect('admin/putaway_fg');
		}
	}

	// Get Modal Edit
	public function get()
	{
		if ($this->input->post()) {
            $id = $this->input->post('id');
			// $sql_wo = "SELECT w.*, c.nama FROM tbl_work_order w inner join tbl_customer c on w.customer_id = c.customer_id order by w.work_order_id desc";
			
			$sql_fg = "SELECT * from tbl_material where inventory_part_type = 'Work in Process' or inventory_part_type = 'Finished Goods' order by material_no";

            $data['get'] = $this->sql->select_table('tbl_putaway_fg', ['putaway_fg_id' => $id])->row_array();
			$data['wo'] = $this->sql->select_table('tbl_work_order',[
				'status' => 'on progress'
			])->result_array();
			$data['location'] = $this->sql->select_table('tbl_location', null, 'location_name')->result();


            $this->load->view('putaway_fg/get', $data);
        } else {
            echo "error";
        }
	}

	// Update
	public function update()
	{
		if($this->input->post()){
			$post = $this->input->post();

			$where = [
				'putaway_fg_id' => $post['putaway_fg_id'],
			];

			$form_data = [
				'batch_no' => $post['batch_no'],
                'qc_no' => $post['qc_no'],
                'expired_date' => $post['expired_date'],
                'retest_date' => $post['retest_date'],
				// 'qty' => $post['qty'],
                'location' => $post['location'],
				'putaway_fg_date' => $post['fg_date'],
            ];

            $this->sql->update_table('tbl_putaway_fg', $form_data, $where);
			$message = '<div class="alert alert-success alert-dismissible" role="alert">
							<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
							<div class="alert-message">
								<strong>Success!</strong> Data has been updated!
							</div>
						</div>';
            $this->session->set_flashdata('msg', $message);
            redirect('admin/putaway_fg');

		} else {
			$message = '<div class="alert alert-danger alert-dismissible" role="alert">
							<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
							<div class="alert-message">
								<strong>Error!</strong> Error!
							</div>
						</div>';
			$this->session->set_flashdata('msg', $message);
			redirect('admin/putaway_fg');
		}
	}

	// Delete
	public function delete(){
        if ($this->input->post()) {
            $id = $this->input->post('id');
            $where = ['putaway_fg_id' => $id];

            $this->sql->delete_table('tbl_putaway_fg', $where);
            echo "ok";
        } else {
            echo "error";
        }
    }

	public function filter_fg()
	{
		if ($this->input->post()) {
            $id = $this->input->post('id');
			
			// Get FG id from Work order detail based on work order id
			$fg = $this->sql->select_table('tbl_work_order_detail', [
				'work_order_id' => $id
			])->result_array();

			$fg_id = [];
			foreach($fg as $item){
				array_push($fg_id, $item['fg_id']);
			}

			// Select fg in
			$return = $this->sql->select_table_in('tbl_material', 'material_no', $fg_id)->result_array();
			// print_r($return);
			echo "<option value='0'> - Select One - </option>";

			foreach($return as $item){
				echo "<option value='" . $item['material_no'] . "'>" . $item['material_no'] . ' - ' . $item['nama'] . "</option>";
			}

        } else {
            echo "error";
        }
	}

	public function print($id)
    {
        if ($id) {
			$sql_putaway_fg = "SELECT f.*,w.work_order_id as work_order, w.no_work_order, g.nama, g.material_no, u.name, g.inventory_part_type FROM tbl_putaway_fg f inner join tbl_material g on f.fg_id = g.material_no inner join tbl_user u on f.user_id = u.user_id inner join tbl_work_order w on f.work_order_id = w.work_order_id where f.putaway_fg_id = '$id'";

            $where['gr_id'] = $id;
            $data['gr'] = $this->sql->select_table('tbl_gr', $where)->row_array();
            $data['get'] = $this->sql->manual_query($sql_putaway_fg)->row();

            // //load library
            // $this->load->library('zend');
            // //load in folder Zend
            // $this->zend->load('Zend/Barcode');

            // $this->load->library('pdf');
            // $this->pdf->setPaper('A6', 'landscape');
            // $this->pdf->filename = "Code GR.pdf";
            // $this->pdf->load_view('good_receiving/print_qr', $data);

            // Mpdf
            $mpdf = new \Mpdf\Mpdf([
                'mode' => 'utf-8',
                'format' => 'A6',
                'orientation' => 'L',
                'margin_top' => 0,
                'margin_left' => 5,
                'margin_right' => 5,
            ]);
            $mpdf->setAutoTopMargin = 'stretch';
            // for($n = 1; $n <= $data['get']->qty; $n++){
                // $data['row'] = $row;
                $page = $this->load->view('putaway_fg/print_qr', $data, true);

                $mpdf->addPage();
                $mpdf->WriteHTML($page);
            // }

		    $mpdf->Output();
			// print_r($data['get']);
        } else {
            echo "error";
        }
    }

	// API
	public function check_fg_left()
	{
		if ($this->input->post()) {
            $work_order_id = $this->input->post('work_order_id');
            $fg_id = $this->input->post('fg_id');

			
			// Get FG id from Work order detail based on work order id
			$detail = $this->sql->select_table('tbl_work_order_detail', [
				'work_order_id' => $work_order_id,
				'fg_id' => $fg_id,
			])->row();

			echo $detail->qty - $detail->good;

        } else {
            echo "error";
        }
	}

	// middleware
	public function middleware()
	{
		if(!$this->session->name){
			redirect(base_url());
		}
	}
}
