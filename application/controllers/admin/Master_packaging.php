<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_packaging extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model("sql");
		$this->middleware();
    }

    public function index()
    {
        // $data['get'] = $this->sql->select_table('tbl_Packaging')->result_array();
        $data['api'] = $this->get_api();
        $data['cust'] = $this->get_api_customer();
        $data['supp'] = $this->get_api_supplier();
        $data['site_title'] = "Master Packaging";
        $data['subview'] = "master_packaging/data";
        $this->load->view('index', $data);
	}

   public function get_api()
    {
        $url = "http://api.skinsolution.co.id/packaging";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );

        $resp = curl_exec($curl);
        curl_close($curl);

        $return = json_decode($resp);
        return $return;
    }
    
   public function get_api_customer()
    {
        $url = "http://api.skinsolution.co.id/customer";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );

        $resp = curl_exec($curl);
        curl_close($curl);

        $return = json_decode($resp);
        return $return;
    }
    
   public function get_api_supplier()
    {
        $url = "http://api.skinsolution.co.id/supplier";

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );

        $resp = curl_exec($curl);
        curl_close($curl);

        $return = json_decode($resp);
        return $return;
    }


    // public function add()
    // {
    //     if ($this->input->post()) {
    //         $post = $this->input->post();

    //         $form_data = [
    //             'salesman_no' => $post['salesman_no'],
    //             'name' => $post['name'],
    //             'address' => $post['address'],
    //             'phone' => $post['phone'],
    //             'email' => $post['email'],
    //         ];

    //         $id = $this->sql->insert_table('tbl_salesman', $form_data);

    //         $message = '<div class="alert alert-success alert-dismissible" role="alert">
				// 			<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
				// 			<div class="alert-message">
				// 				<strong>Success!</strong> New data has been created!
				// 			</div>
				// 		</div>';
    //         $this->session->set_flashdata('msg', $message);
    //         redirect('admin/master_salesman');
    //     } else {
    //         $message = '<div class="alert alert-danger alert-dismissible" role="alert">
				// 			<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
				// 			<div class="alert-message">
				// 				<strong>Error!</strong> Error!
				// 			</div>
				// 		</div>';
    //         $this->session->set_flashdata('msg', $message);
    //         redirect('admin/master_salesman');
    //     }
    // }

    // public function get()
    // {
    //     if ($this->input->post()) {
    //         $id = $this->input->post('id');
    //         $data['get'] = $this->sql->select_table('tbl_salesman', ['salesman_id' => $id])->row_array();

    //         $this->load->view('master_salesman/get', $data);
    //     } else {
    //         echo "error";
    //     }
    // }

    // public function edit($id)
    // {
    //     if ($this->input->post()) {
    //         $post = $this->input->post();

    //         $form_data = [
    //             'salesman_no' => $post['salesman_no'],
    //             'name' => $post['name'],
    //             'address' => $post['address'],
    //             'phone' => $post['phone'],
    //             'email' => $post['email'],
    //         ];

    //         $where = ['salesman_id' => $id];

    //         $id = $this->sql->update_table('tbl_salesman', $form_data, $where);

    //         $message = '<div class="alert alert-success alert-dismissible" role="alert">
				// 			<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
				// 			<div class="alert-message">
				// 				<strong>Success!</strong> Data has been updated!
				// 			</div>
				// 		</div>';
    //         $this->session->set_flashdata('msg', $message);
    //         redirect('admin/master_salesman');
    //     } else {
    //         $message = '<div class="alert alert-danger alert-dismissible" role="alert">
				// 			<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
				// 			<div class="alert-message">
				// 				<strong>Error!</strong> Error!
				// 			</div>
				// 		</div>';
    //         $this->session->set_flashdata('msg', $message);
    //         redirect('admin/master_salesman');
    //     }
    // }

    // public function delete()
    // {
    //     if($this->input->post()){
    //         $id = $this->input->post('id');
    //         $where = ['salesman_id' => $id];
            
    //         $this->sql->delete_table('tbl_salesman', $where);
    //         echo "ok";
    //     }else{
    //         echo "error";
    //     }
    // }

	// middleware
	public function middleware()
	{
		if(!$this->session->name){
			redirect(base_url());
		}
	}
}
