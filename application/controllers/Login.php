<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

		$this->load->model("sql");
	}

	public function index()
	{
		$this->middleware();
		$this->load->view('login');
	}

	public function login_process()
	{
		if ($this->input->post()) {
			$post = $this->input->post();
			$query = $this->sql->select_table('tbl_user', array('username' => $post['username']));
			if ($query->num_rows() > 0) {
				$get = $query->row_array();
				if (password_verify($post['password'], $get['password'])) {
					$data = [
						'user_id' => $get['user_id'],
						'name' => $get['name'],
						'level' => $get['level'],
						'image' => $get['image']
					];

					$this->session->set_userdata($data);
					if($get['level'] == 2){
						redirect("admin/purchasing_order");
					} else{
						redirect("admin/work_order");
					}
				} else {
					$message = '<div class="alert alert-danger alert-dismissible" role="alert">
									<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
									<div class="alert-message">
										<strong>Error!</strong> Username atau password yang dimasukkan salah!
									</div>
								</div>';
					$this->session->set_flashdata('msg', $message);
					redirect('login');
				}
			} else {
				$message = '<div class="alert alert-danger alert-dismissible" role="alert">
								<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
								<div class="alert-message">
									<strong>Error!</strong> Username atau password yang dimasukkan salah!
								</div>
							</div>';
				$this->session->set_flashdata('msg', $message);
				redirect('login');
			}
		} else {
			$message = '<div class="alert alert-danger alert-dismissible" role="alert">
							<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
							<div class="alert-message">
								<strong>Error!</strong> Error!
							</div>
						</div>';
			$this->session->set_flashdata('msg', $message);
			redirect('login');
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		print_r($this->session);
		redirect("login");
	}

	// middleware
	public function middleware()
	{
		if($this->session->name){
			redirect(base_url('admin/purchasing_order'));
		}
	}
}
