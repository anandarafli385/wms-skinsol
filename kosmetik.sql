-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 17, 2022 at 06:06 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kosmetik`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_expenditure`
--

CREATE TABLE `tbl_expenditure` (
  `expenditure_id` int(11) NOT NULL,
  `expenditure_num` varchar(255) NOT NULL,
  `expenditure_date` varchar(255) NOT NULL,
  `material_id` varchar(255) NOT NULL,
  `material_name` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `layer_loc` varchar(80) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_expenditure`
--

INSERT INTO `tbl_expenditure` (`expenditure_id`, `expenditure_num`, `expenditure_date`, `material_id`, `material_name`, `location`, `layer_loc`, `created_at`) VALUES
(7, 'BBK004', '01/05/2022', '706', 'Propylene Glycol', 'RM Warehouse B.860', 'Layer 1.Rak A', '2022-07-22 21:56:48');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gr_qc`
--

CREATE TABLE `tbl_gr_qc` (
  `id` int(11) NOT NULL,
  `gr_id` int(11) NOT NULL,
  `purchase_id` int(11) NOT NULL,
  `material_id` int(11) NOT NULL,
  `batch_num` varchar(255) NOT NULL,
  `analis_num` varchar(255) NOT NULL,
  `status` enum('ok','not ok') NOT NULL,
  `desc` text NOT NULL DEFAULT 'OK'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_gr_qc`
--

INSERT INTO `tbl_gr_qc` (`id`, `gr_id`, `purchase_id`, `material_id`, `batch_num`, `analis_num`, `status`, `desc`) VALUES
(9, 1014, 511, 564, '0000095425', 'ANBB0422016', 'ok', 'OK'),
(10, 551, 386, 558, '0000083346', 'BB/1121/   /001', 'ok', 'OK'),
(11, 567, 388, 626, '2020APC7107', 'BB/1220/S003/004', 'ok', 'OK'),
(12, 642, 392, 652, 'NM2012015', 'BB/0721/S039/002', 'ok', 'OK'),
(13, 746, 415, 677, 'LGB20200225', 'BB/1020/   /002', 'ok', 'OK'),
(14, 670, 401, 632, 'B045-P', 'BB/1221/S011/001', 'ok', 'OK'),
(15, 678, 401, 662, 'T109102', 'BB/1221/S053/002', 'ok', 'OK'),
(16, 860, 436, 706, 'ACG/QD-201015A', 'BB/0721/L030/003', 'ok', 'OK'),
(17, 955, 471, 645, '597537', 'BB/0322/P002/002', 'ok', 'OK'),
(18, 669, 401, 699, '32-325-T', 'BB/1221/L017/001', 'ok', 'OK'),
(19, 720, 410, 634, '20210113', 'BB/0121/S014/001', 'ok', 'OK'),
(20, 963, 476, 695, 'H135LB1011', 'BB/0322/C004/001', 'ok', 'OK'),
(21, 804, 432, 705, 'DEG4564323', 'BB/0621/L029/006', 'ok', 'OK'),
(22, 956, 471, 703, '2EF822IE', 'BB/0222/C012/001', 'ok', 'OK'),
(23, 924, 452, 710, '202104V3GN', 'BB/0122/C018/003', 'ok', 'OK'),
(24, 810, 433, 694, '12080071', 'BB/0521/L009/002', 'ok', 'OK'),
(25, 564, 388, 598, '201201907212', 'BB/1221/L046/001', 'ok', 'OK'),
(26, 681, 402, 732, '24111298', 'BB/0121/L007/001', 'ok', 'OK'),
(27, 808, 433, 625, '11111926', 'BB/0521/S002/001', 'ok', 'OK'),
(28, 738, 413, 743, 'F470121', 'BB/0721/L092/004', 'ok', 'OK'),
(29, 673, 401, 698, '21C130', 'BB/2421/L017/001', 'ok', 'OK'),
(30, 758, 417, 518, '2887588', 'BB/1021/F021/003', 'ok', 'OK'),
(31, 682, 402, 676, '5683301843', 'BB/0919/    /001', 'ok', 'OK'),
(32, 542, 380, 503, 'BBC-RO-SKI-2021', 'QC/BB/RO-SKI', 'ok', 'OK'),
(33, 964, 477, 652, 'NM2012015', 'BB/0322/P029/003', 'ok', 'OK'),
(34, 550, 386, 559, '0000090231', 'BB/1121/  001', 'ok', 'OK'),
(35, 876, 438, 626, '', '', 'ok', 'OK'),
(36, 599, 389, 778, 'E520C10310', 'BB/0721/L147/001', 'not ok', 'not ok'),
(37, 1038, 525, 585, 'YY00K7K054', 'ANBB0622004', 'not ok', 'not ok'),
(38, 588, 388, 666, '0001977', 'BB/0321/S057/001', 'not ok', 'barang tidak sesuai dengan perjanjian PO diawal,kurang 10gram\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_location`
--

CREATE TABLE `tbl_location` (
  `location_id` int(11) NOT NULL,
  `location_name` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_location`
--

INSERT INTO `tbl_location` (`location_id`, `location_name`, `user_id`, `created_at`) VALUES
(1, 'Rak A', 5, '2021-08-03 11:26:38'),
(2, 'Rak B', 5, '2021-08-03 11:26:41'),
(3, 'Rak C', 5, '2021-08-03 15:04:41'),
(5, 'Rak D', 5, '2021-08-18 13:51:43');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_productions`
--

CREATE TABLE `tbl_productions` (
  `id` bigint(20) NOT NULL,
  `replenishment_id` int(20) DEFAULT NULL,
  `qty` int(20) NOT NULL,
  `status` varchar(20) NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(20) NOT NULL,
  `updated_by` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_productions`
--

INSERT INTO `tbl_productions` (`id`, `replenishment_id`, `qty`, `status`, `date`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 47, 11, 'Production', '2022-08-19', '2022-08-17 08:42:17', '0000-00-00 00:00:00', 6, 6);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_production_logs`
--

CREATE TABLE `tbl_production_logs` (
  `id` int(20) NOT NULL,
  `production_id` int(20) NOT NULL,
  `qty` int(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(20) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(20) DEFAULT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_production_logs`
--

INSERT INTO `tbl_production_logs` (`id`, `production_id`, `qty`, `created_at`, `created_by`, `updated_at`, `updated_by`, `status`) VALUES
(2, 1, 0, '0000-00-00 00:00:00', 6, NULL, NULL, 'Production'),
(3, 1, 3, '0000-00-00 00:00:00', 6, NULL, NULL, 'Production'),
(4, 1, 10, '0000-00-00 00:00:00', 6, NULL, NULL, 'Production'),
(5, 1, 0, '0000-00-00 00:00:00', 6, NULL, NULL, 'Done');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_putaway_detail_new`
--

CREATE TABLE `tbl_putaway_detail_new` (
  `putaway_detail_id` int(11) NOT NULL,
  `putaway_id` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `material_id` int(11) NOT NULL,
  `layer` enum('Layer 1','Layer 2','Layer 3','') NOT NULL DEFAULT 'Layer 1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_putaway_detail_new`
--

INSERT INTO `tbl_putaway_detail_new` (`putaway_detail_id`, `putaway_id`, `batch_id`, `material_id`, `layer`) VALUES
(56, 36, 567, 626, 'Layer 1'),
(57, 37, 1014, 564, 'Layer 1'),
(58, 37, 551, 558, 'Layer 1'),
(59, 37, 567, 626, 'Layer 1'),
(60, 37, 746, 677, 'Layer 1'),
(61, 37, 670, 632, 'Layer 1'),
(62, 37, 678, 662, 'Layer 1'),
(63, 37, 860, 706, 'Layer 1'),
(64, 37, 955, 645, 'Layer 1'),
(65, 37, 669, 699, 'Layer 1'),
(66, 37, 720, 634, 'Layer 1'),
(67, 37, 963, 695, 'Layer 1'),
(68, 37, 804, 705, 'Layer 1'),
(69, 37, 956, 703, 'Layer 1'),
(70, 37, 924, 710, 'Layer 1'),
(71, 37, 810, 694, 'Layer 1'),
(72, 37, 564, 598, 'Layer 1'),
(73, 37, 681, 732, 'Layer 1'),
(74, 37, 808, 625, 'Layer 1'),
(75, 37, 738, 743, 'Layer 1'),
(76, 37, 673, 698, 'Layer 1'),
(77, 37, 758, 518, 'Layer 1'),
(78, 37, 682, 676, 'Layer 1'),
(79, 37, 542, 503, 'Layer 1'),
(80, 38, 964, 652, 'Layer 1'),
(81, 39, 550, 559, 'Layer 1'),
(82, 40, 876, 626, 'Layer 1'),
(84, 42, 804, 705, 'Layer 2'),
(85, 43, 804, 705, 'Layer 3');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_putaway_new`
--

CREATE TABLE `tbl_putaway_new` (
  `putaway_id` int(11) NOT NULL,
  `putaway_date` date NOT NULL,
  `location` varchar(100) NOT NULL,
  `category` enum('Layer 1','Layer 2','Layer 3','') NOT NULL DEFAULT 'Layer 1',
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_putaway_new`
--

INSERT INTO `tbl_putaway_new` (`putaway_id`, `putaway_date`, `location`, `category`, `user_id`, `created_at`) VALUES
(36, '2022-07-18', 'RM Warehouse B', 'Layer 1', 6, '2022-07-18 00:57:19'),
(37, '2022-07-18', 'RM Warehouse A', 'Layer 1', 6, '2022-07-18 00:44:04'),
(38, '2022-07-18', 'RM Warehouse A', 'Layer 1', 6, '2022-07-18 00:46:59'),
(39, '2022-07-18', 'RM Warehouse A', 'Layer 1', 6, '2022-07-18 00:47:27'),
(40, '2022-07-18', 'RM Warehouse A', 'Layer 1', 6, '2022-07-18 00:55:16'),
(42, '2022-07-23', 'Rak C', 'Layer 1', 6, '2022-07-22 21:21:15'),
(43, '2022-07-23', 'Rak B', 'Layer 1', 6, '2022-07-22 21:42:09');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_replenishment`
--

CREATE TABLE `tbl_replenishment` (
  `replenishment_id` int(11) NOT NULL,
  `replenishment_no` varchar(50) NOT NULL,
  `work_order_id` int(11) NOT NULL,
  `work_order_batch` varchar(255) NOT NULL,
  `wo_formula` text NOT NULL,
  `wo_po_num` varchar(255) NOT NULL,
  `wo_pack` varchar(255) NOT NULL,
  `wo_status` varchar(255) NOT NULL,
  `wo_product_code` varchar(255) NOT NULL,
  `wo_product_name` varchar(255) NOT NULL,
  `packaging_name` varchar(255) NOT NULL,
  `revision_num` varchar(255) NOT NULL,
  `tgl_berlaku` date DEFAULT NULL,
  `tgl_mulai` date DEFAULT NULL,
  `tgl_selesai` date DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `approval` enum('approved','not approved','waiting') NOT NULL DEFAULT 'waiting',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `approved_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `approved_by` int(11) DEFAULT 99
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_replenishment_detail`
--

CREATE TABLE `tbl_replenishment_detail` (
  `replenishment_detail_id` int(11) NOT NULL,
  `replenishment_id` int(11) NOT NULL,
  `wo_po_num` varchar(255) NOT NULL,
  `batch_id` varchar(100) NOT NULL,
  `material_box_id` varchar(100) NOT NULL,
  `material_name` varchar(255) NOT NULL,
  `qty` double NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `user_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` int(11) NOT NULL COMMENT 'superadmin, supervisor, warehouse, production,\r\napproval',
  `image` varchar(255) NOT NULL DEFAULT 'default.png',
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `name`, `username`, `password`, `level`, `image`, `created_at`) VALUES
(1, 'Putra Halim', 'halim', '$2y$10$uAh1TBC5uaBuX7Wl.1dFDuubc1GljBl3hKicq7Db4ErAEYpFmhbw.', 1, 'h8zuFoUkMlP3SsfQ-24082021.jpg', '2021-07-08 11:40:46'),
(4, 'Warehouse Skinsol', 'warehouse@skinsol.co.id', '$2y$10$0r4cIp3R9v4n04bVbnN4Xelv0O7xjKbCJ12slnwVpXApkLUjWw.KC', 2, 'GicqtZJfS9OwE10m-24082021.jpg', '2021-08-18 13:48:12'),
(5, 'Production Skinsol', 'production@skinsol.co.id', '$2y$10$Y0rROK/JBTfANUXxmrN87emuxVbMAQe.HvwZOBE7TOfrcULTns7Za', 3, 'default.png', '2021-09-28 17:11:17'),
(6, 'Superadmin Skinsol', 'superadmin@skinsol.co.id', '$2y$10$9/EKY55eu5woqhgBn9Et2OpdRO2EsUcqd2Gnw1PRSJEJsu7ip4.eC', 0, 'default.png', '2021-12-29 11:16:12'),
(7, 'test', 'test', '$2y$10$Em.KTQvo9ZYwB2qMurLE7e3R/L3IznQ2wjL4OFzhDky9Zv3ZDqX.q', 2, 'default.png', '2022-02-23 20:16:10'),
(9, 'Manager Skinsol', 'manager@skinsol.co.id', '$2y$10$DVQYU7P2dvpXRCahfbel5O.HWNs56IxCCcBGDiNjL8.yvwOORoqsi', 4, 'default.png', '2022-04-12 06:10:51');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_work_order`
--

CREATE TABLE `tbl_work_order` (
  `work_order_id` int(11) NOT NULL,
  `no_work_order` text NOT NULL,
  `date` date NOT NULL,
  `description` text DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `status` enum('on progress','finished') NOT NULL DEFAULT 'on progress',
  `finished_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `finished_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_work_order`
--

INSERT INTO `tbl_work_order` (`work_order_id`, `no_work_order`, `date`, `description`, `user_id`, `status`, `finished_by`, `created_at`, `finished_at`) VALUES
(1, 'WO-A010/2A-2021', '2021-11-19', 'adwa', 5, 'finished', 6, '2021-11-19 15:50:05', '2022-05-16 00:59:15'),
(2, 'WO-A09/1A-2021', '2021-11-30', 'Work Order Pasta Titanium Bulk', 1, 'finished', 1, '2021-11-30 16:48:45', NULL),
(3, 'swd', '2021-12-02', 'hahahahihi', 6, 'on progress', 0, '2021-12-02 09:29:04', NULL),
(4, 'WO-20220111_21599', '2022-12-30', '', 6, 'on progress', 0, '2022-05-16 00:59:50', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_work_order_approval`
--

CREATE TABLE `tbl_work_order_approval` (
  `work_order_id` int(11) NOT NULL,
  `work_order_num` varchar(255) DEFAULT NULL,
  `customer_code` varchar(255) DEFAULT NULL,
  `customer_name` varchar(255) DEFAULT NULL,
  `approval` enum('approved','not approved','waiting') NOT NULL DEFAULT 'waiting',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_work_order_approval`
--

INSERT INTO `tbl_work_order_approval` (`work_order_id`, `work_order_num`, `customer_code`, `customer_name`, `approval`, `created_at`) VALUES
(180, '001/PR/M11/07/21-FW', '-', '-', 'approved', '2022-06-28 20:40:41'),
(181, '002/PR/M11/07/21-DC', '-', '-', 'not approved', '2022-06-29 04:20:57'),
(332, '002/JO/D9/06/2022', 'CM340222', 'Wahyuning Tias', 'approved', '2022-06-29 04:19:26');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_work_order_detail`
--

CREATE TABLE `tbl_work_order_detail` (
  `work_order_detail_id` int(11) NOT NULL,
  `work_order_id` int(11) NOT NULL,
  `fg_id` int(11) NOT NULL,
  `qty` double NOT NULL,
  `good` int(32) DEFAULT 0,
  `reject` int(32) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_work_order_detail`
--

INSERT INTO `tbl_work_order_detail` (`work_order_detail_id`, `work_order_id`, `fg_id`, `qty`, `good`, `reject`) VALUES
(1, 1, 439, 10, 6, 0),
(2, 2, 391, 80, 80, 0),
(4, 3, 30001, 3, 0, 0),
(5, 4, 30029, 1, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_expenditure`
--
ALTER TABLE `tbl_expenditure`
  ADD PRIMARY KEY (`expenditure_id`);

--
-- Indexes for table `tbl_gr_qc`
--
ALTER TABLE `tbl_gr_qc`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_location`
--
ALTER TABLE `tbl_location`
  ADD PRIMARY KEY (`location_id`);

--
-- Indexes for table `tbl_productions`
--
ALTER TABLE `tbl_productions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_production_logs`
--
ALTER TABLE `tbl_production_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_putaway_detail_new`
--
ALTER TABLE `tbl_putaway_detail_new`
  ADD PRIMARY KEY (`putaway_detail_id`);

--
-- Indexes for table `tbl_putaway_new`
--
ALTER TABLE `tbl_putaway_new`
  ADD PRIMARY KEY (`putaway_id`);

--
-- Indexes for table `tbl_replenishment`
--
ALTER TABLE `tbl_replenishment`
  ADD PRIMARY KEY (`replenishment_id`);

--
-- Indexes for table `tbl_replenishment_detail`
--
ALTER TABLE `tbl_replenishment_detail`
  ADD PRIMARY KEY (`replenishment_detail_id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `tbl_work_order`
--
ALTER TABLE `tbl_work_order`
  ADD PRIMARY KEY (`work_order_id`);

--
-- Indexes for table `tbl_work_order_approval`
--
ALTER TABLE `tbl_work_order_approval`
  ADD PRIMARY KEY (`work_order_id`);

--
-- Indexes for table `tbl_work_order_detail`
--
ALTER TABLE `tbl_work_order_detail`
  ADD PRIMARY KEY (`work_order_detail_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_gr_qc`
--
ALTER TABLE `tbl_gr_qc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `tbl_location`
--
ALTER TABLE `tbl_location`
  MODIFY `location_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_productions`
--
ALTER TABLE `tbl_productions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_production_logs`
--
ALTER TABLE `tbl_production_logs`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_putaway_detail_new`
--
ALTER TABLE `tbl_putaway_detail_new`
  MODIFY `putaway_detail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;

--
-- AUTO_INCREMENT for table `tbl_putaway_new`
--
ALTER TABLE `tbl_putaway_new`
  MODIFY `putaway_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `tbl_replenishment`
--
ALTER TABLE `tbl_replenishment`
  MODIFY `replenishment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `tbl_replenishment_detail`
--
ALTER TABLE `tbl_replenishment_detail`
  MODIFY `replenishment_detail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=725;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_work_order`
--
ALTER TABLE `tbl_work_order`
  MODIFY `work_order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_work_order_detail`
--
ALTER TABLE `tbl_work_order_detail`
  MODIFY `work_order_detail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
